import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from '@/translations/en.json';
import sv from '@/translations/sv.json';
import { IAvailableLanguages } from '@/types';

const resources = {
  en,
  sv,
};

export const availableLanguages: IAvailableLanguages = {
  en: {
    nativeName: 'English',
  },
  sv: {
    nativeName: 'Svenska',
  },
};

i18n.use(initReactI18next).use(LanguageDetector).init({
  resources,
  defaultNS: 'common',
  fallbackLng: 'en',
});

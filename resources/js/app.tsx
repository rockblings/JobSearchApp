import React from 'react';
import { render } from 'react-dom';
import { InertiaApp } from '@inertiajs/inertia-react';
import { InertiaProgress } from '@inertiajs/progress';
import LoadingScreen from '@/Shared/Components/LoadingScreen';

const app = document.getElementById('app') as HTMLElement;

render(
  <InertiaApp
    initialPage={app ? JSON.parse(app.dataset.page as string) : '{}'}
    resolveComponent={name =>
      import(`@/Pages/${name}`).then(module => module.default)
    }
    initialComponent={LoadingScreen}
  />,
  app,
);
InertiaProgress.init({ color: '#FBBF24' });

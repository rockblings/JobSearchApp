export interface User {
  loggedInUserName: string;
  loggedInUserEmail: string;
  twoFactorEnabled: boolean;
}

export interface IExperience {
  xp: string | null;
  title: string | null;
  description: string;
  name: string | null;
  website?: string | null;
  email?: string | null;
  address?: string;
  telephone?: string | null;
  startDate: string | null;
  endDate: string | null;
  status: boolean;
}

export interface IProfile {
  name: string;
  email: string;
  description: string | number | readonly string[];
  language: number | null;
  phoneNumber: string | number | readonly string[];
  country: number | null;
  photo: File | null;
}

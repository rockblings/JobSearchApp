import React from 'react';
import ActivityReminder from '@/Shared/Components/HomePageComponent/ActivityReminder';
import RecentOpeningAnnouncement from '@/Shared/Components/HomePageComponent/RecentOpeningAnnouncement';
import BlogPanel from '@/Shared/Components/HomePageComponent/BlogPanel';
import { Page } from '@inertiajs/inertia';
import AppLayout from '@/Shared/Components/Layout/AppLayout';
import useTypedPage from '@/Hooks/useTypedPage';
import EmptyProfileImage from '@/Shared/Components/Image/EmptyProfileImage';
import { Link } from '@inertiajs/inertia-react';
import useRoute from '@/Hooks/useRoute';

const Index = () => {
  const {
    props: {
      user: { loggedInUserName, loggedInUserEmail },
      pageData: { imageUrl },
    },
  } = useTypedPage();

  const route = useRoute();

  const photoUrl = imageUrl || '';

  return (
    <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
      <h1 className="sr-only">Profile</h1>
      {/* Main 3 column grid */}
      <div className="grid grid-cols-1 items-start gap-4 lg:grid-cols-3 lg:gap-8">
        {/* Left column */}
        <div className="grid grid-cols-1 gap-4 lg:col-span-2">
          {/* Welcome panel */}
          <section aria-labelledby="profile-overview-title">
            <div className="overflow-hidden rounded-lg bg-white shadow">
              <h2 className="sr-only" id="profile-overview-title">
                Profile Overview
              </h2>
              <div className="bg-white p-6">
                <div className="sm:flex sm:items-center sm:justify-between">
                  <div className="sm:flex sm:space-x-5">
                    <div className="flex-shrink-0">
                      {photoUrl ? (
                        <img
                          className="mx-auto h-20 w-20 rounded-full"
                          src={photoUrl}
                          alt=""
                        />
                      ) : (
                        <EmptyProfileImage classes="inline-block h-20 w-20 overflow-hidden rounded-full bg-gray-100" />
                      )}
                    </div>
                    <div className="mt-4 text-center sm:mt-0 sm:pt-1 sm:text-left">
                      <p className="text-sm font-medium text-slate-600">
                        Welcome back,
                      </p>
                      <p className="text-xl font-bold text-slate-800 sm:text-2xl">
                        {loggedInUserName}
                      </p>
                      <p className="text-sm font-medium text-slate-600">
                        {loggedInUserEmail}
                      </p>
                    </div>
                  </div>
                  <div className="mt-5 flex justify-center sm:mt-0">
                    <Link
                      href={route('profile.show')}
                      className="flex items-center justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-2 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
                    >
                      Complete your profile
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </section>
          {/* Actions panel */}
          <BlogPanel />
        </div>
        {/* Right column */}
        <div className="grid grid-cols-1 gap-4">
          {/* Recent Job Openings */}
          <RecentOpeningAnnouncement />
          {/* Activities & Reminders */}
          <ActivityReminder />
        </div>
      </div>
    </div>
  );
};

Index.layout = (page: Page) => <AppLayout children={page} title="Home" />;

export default Index;

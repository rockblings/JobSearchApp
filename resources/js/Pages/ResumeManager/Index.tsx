import React from 'react';
import { Page } from '@inertiajs/inertia';
import AppLayout from '@/Shared/Components/Layout/AppLayout';
import {
  AcademicCapIcon,
  BriefcaseIcon,
  ChartSquareBarIcon,
  ClockIcon,
  CollectionIcon,
  ColorSwatchIcon,
} from '@heroicons/react/outline';
import useRoute from '@/Hooks/useRoute';
import { Link } from '@inertiajs/inertia-react';

const items = [
  {
    title: 'Work Experiences',
    description: 'This is where you add all your work experiences.',
    icon: BriefcaseIcon,
    background: 'bg-pink-500',
    url: 'work.experience.index',
  },
  {
    title: 'Education and Credentials',
    description:
      'This is where you add all your education qualification and credentials.',
    icon: AcademicCapIcon,
    background: 'bg-yellow-500',
    url: 'profile.show',
  },
  {
    title: 'Skills',
    description: 'This is where you add all your skills.',
    icon: ColorSwatchIcon,
    background: 'bg-green-500',
    url: 'profile.show',
  },
  {
    title: 'Career Summary',
    description:
      'This is where you summarize your work history in a presentation format.',
    icon: ChartSquareBarIcon,
    background: 'bg-blue-500',
    url: 'profile.show',
  },
  {
    title: 'Qualifications Summary',
    description:
      'This is where you summarize your education and training experience in a presentation format.',
    icon: CollectionIcon,
    background: 'bg-violet-500',
    url: 'profile.show',
  },
  {
    title: 'References',
    description: 'This is where you add all your work references if needed.',
    icon: ClockIcon,
    background: 'bg-orange-500',
    url: 'profile.show',
  },
];

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}

const Index = () => {
  const route = useRoute();
  return (
    <>
      <h2 className="text-lg font-medium text-slate-900">Resume Manager</h2>
      <p className="mt-1 text-sm text-gray-500">
        These are the different building blocks of a resume.
      </p>
      <ul
        role="list"
        className="mt-8 grid grid-cols-1 gap-6 border-t border-b border-gray-200 py-6 sm:grid-cols-2"
      >
        {items.map((item, itemIdx) => (
          <li key={itemIdx} className="flow-root">
            <div className="relative -m-2 flex items-center space-x-4 rounded-xl p-2 py-10 focus-within:ring-2 focus-within:ring-fuchsia-500 hover:bg-gray-50">
              <div
                className={classNames(
                  item.background,
                  'flex h-16 w-16 flex-shrink-0 items-center justify-center rounded-lg',
                )}
              >
                <item.icon className="h-6 w-6 text-white" aria-hidden="true" />
              </div>
              <div>
                <h3 className="text-sm font-medium text-gray-900">
                  <Link href={route(item.url)} className="focus:outline-none">
                    <span className="absolute inset-0" aria-hidden="true" />
                    {item.title}
                    <span aria-hidden="true"> &rarr;</span>
                  </Link>
                </h3>
                <p className="mt-1 text-sm text-gray-500">{item.description}</p>
              </div>
            </div>
          </li>
        ))}
      </ul>
      <div className="mt-10 flex justify-end">
        <a
          href="#"
          className="text-sm font-medium text-fuchsia-600 hover:text-fuchsia-400"
        >
          Generate a resume
          <span aria-hidden="true"> &rarr;</span>
        </a>
      </div>
    </>
  );
};

Index.layout = (page: Page) => (
  <AppLayout children={page} title="Resume Manager" />
);

export default Index;

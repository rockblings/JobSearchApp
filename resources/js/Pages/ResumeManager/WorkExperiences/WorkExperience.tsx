import React, { ChangeEvent, FormEvent, useState } from 'react';
import { Page } from '@inertiajs/inertia';
import AppLayout from '@/Shared/Components/Layout/AppLayout';
import {
  MailIcon,
  PlusSmIcon,
  TrashIcon,
  XIcon,
} from '@heroicons/react/outline';
import { Link, useForm } from '@inertiajs/inertia-react';
import { IExperience } from '@/interfaces';
import CustomDatePicker from '@/Shared/Components/DatePicker/CustomDatePicker';
import Checkbox from '@/Shared/Components/FormElement/Checkbox';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';
import Notification from '@/Shared/Components/Modal/Notification';

const WorkExperience = () => {
  const [showNotification, setShowNotification] = useState(false);
  const route = useRoute();
  const maxDate = new Date();
  const {
    props: {
      pageData: { workExperiences },
      flash: { success, error },
    },
  } = useTypedPage();

  let savedWorkExperiences = workExperiences.data;

  let emptyFormValue: IExperience = {
    xp: null,
    title: '',
    description: '',
    name: '',
    website: '',
    email: '',
    address: '',
    telephone: '',
    startDate: new Date().toLocaleDateString(),
    endDate: null,
    status: false,
  };

  let initialFormValue: IExperience[] =
    savedWorkExperiences.length > 0 ? savedWorkExperiences : [emptyFormValue];

  const { data, setData, post, processing, errors } = useForm(initialFormValue);

  const handleOnChange = <P extends keyof IExperience>(
    index: number,
    prop: P,
    value: IExperience[P],
  ) => {
    let newFormValues = [...data];
    newFormValues[index][prop] = value;
    setData(newFormValues);
  };

  const handleAddFormFields = () => {
    setData([...data, emptyFormValue]);
  };

  const handleOnRemoveFormField = (i: number) => {
    let newFormValues = [...data];
    newFormValues.splice(i, 1);
    setData(newFormValues);
  };

  const handleOnStartDateChange = (date: Date, index: number) => {
    let newFormValues = [...data];
    newFormValues[index]['startDate'] = new Date(date).toLocaleDateString();
    setData(newFormValues);
  };

  const handleOnEndDateChange = (date: Date, index: number) => {
    let newFormValues = [...data];
    newFormValues[index]['endDate'] = new Date(date).toLocaleDateString();
    setData(newFormValues);
  };

  const handleOnSubmit = (e: FormEvent) => {
    e.preventDefault();
    post(route('work.experience.store'), {
      errorBag: 'saveWorkExperience',
      preserveScroll: true,
      onSuccess: () => handleOnSaveSuccess(),
      onError: errors => console.log(errors),
    });
  };

  const handleOnSaveSuccess = () => {
    setShowNotification(true);
  };

  const closeNotification = () => {
    setShowNotification(false);
  };

  return (
    <>
      <form onSubmit={handleOnSubmit}>
        <div className="sm:overflow-hidden sm:rounded-md">
          <div className="space-y-6 bg-white py-6 px-4 sm:p-6">
            <div>
              <h3 className="text-lg font-medium leading-6 text-gray-900">
                Work Experiences
              </h3>
              <p className="mt-1 text-sm text-gray-500">
                Add all your work experiences.
              </p>
            </div>
            {data.map((el, index) => (
              <div key={index}>
                <div className="grid grid-cols-6 gap-6">
                  {index ? (
                    <div className="relative col-span-6">
                      <div
                        className="absolute inset-0 flex items-center"
                        aria-hidden="true"
                      >
                        <div className="w-full border-t border-gray-300" />
                      </div>
                      <div className="relative flex justify-center">
                        <button
                          type="button"
                          className="inline-flex items-center rounded-full border border-gray-300 bg-white px-4 py-1.5 text-sm font-medium leading-5 shadow-sm hover:bg-gray-100 hover:text-brand-50 focus:outline-none"
                          onClick={() => handleOnRemoveFormField(index)}
                        >
                          <TrashIcon
                            className="h-5 w-5 text-gray-500"
                            aria-hidden="true"
                          />
                        </button>
                      </div>
                    </div>
                  ) : null}
                  <div className="col-span-6 sm:col-span-4">
                    <label
                      htmlFor="job-title"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Job Title
                    </label>
                    <input
                      type="text"
                      name="title"
                      id="job-title"
                      autoComplete="job-title"
                      placeholder="Enter the job title"
                      className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm"
                      value={el.title || ''}
                      onChange={e =>
                        handleOnChange(index, 'title', e.target.value)
                      }
                      required={true}
                    />
                  </div>

                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="company-name"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Company Name
                    </label>
                    <input
                      type="text"
                      name="name"
                      id="company-name"
                      autoComplete="company-name"
                      placeholder="Enter the company name"
                      className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm"
                      value={el.name || ''}
                      onChange={(e: ChangeEvent<HTMLInputElement>) =>
                        handleOnChange(index, 'name', e.target.value)
                      }
                      required={true}
                    />
                  </div>

                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="email"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Company Email (Optional)
                    </label>
                    <div className="relative mt-1 rounded-md shadow-sm">
                      <div className="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
                        <MailIcon
                          className="h-5 w-5 text-gray-400"
                          aria-hidden="true"
                        />
                      </div>
                      <input
                        type="email"
                        name="email"
                        id="email"
                        className="block w-full rounded-md border-gray-300 pl-10 focus:border-purple-500 focus:ring-purple-500 sm:text-sm"
                        placeholder="info@example.com"
                        value={el.email || ''}
                        onChange={e =>
                          handleOnChange(index, 'email', e.target.value)
                        }
                      />
                    </div>
                  </div>

                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="telephone"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Telephone (Optional)
                    </label>
                    <input
                      type="text"
                      name="telephone"
                      id="telephone"
                      autoComplete="telephone"
                      placeholder="Enter the company telephone number."
                      className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm"
                      value={el.telephone || ''}
                      onChange={e =>
                        handleOnChange(index, 'telephone', e.target.value)
                      }
                    />
                  </div>

                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="company-website"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Company Website (Optional)
                    </label>
                    <div className="mt-1 flex rounded-md shadow-sm">
                      <span className="inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-50 px-3 text-gray-500 sm:text-sm">
                        https://
                      </span>
                      <input
                        type="text"
                        name="website"
                        id="company-website"
                        className="block w-full min-w-0 flex-1 rounded-none rounded-r-md border-gray-300 px-3 py-2 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
                        placeholder="www.example.com"
                        value={el.website || ''}
                        onChange={e =>
                          handleOnChange(index, 'website', e.target.value)
                        }
                      />
                    </div>
                  </div>
                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="startedDate"
                      className="mb-1 block text-sm font-medium text-slate-700"
                    >
                      Start Date
                    </label>
                    <CustomDatePicker
                      handleOnDateChange={handleOnStartDateChange}
                      index={index}
                      initialDate={el.startDate ? new Date(el.startDate) : null}
                      placeholder="Please select the start date"
                      maxDate={maxDate}
                    />
                  </div>
                  <div className="col-span-6 sm:col-span-3">
                    <label
                      htmlFor="endDate"
                      className="mb-1 block text-sm font-medium text-slate-700"
                    >
                      End Date
                    </label>
                    <CustomDatePicker
                      handleOnDateChange={handleOnEndDateChange}
                      index={index}
                      initialDate={el.endDate ? new Date(el.endDate) : null}
                      placeholder="Please select the end date if applicable"
                      maxDate={maxDate}
                    />
                  </div>
                  <div className="sm:col-span-6">
                    <label
                      htmlFor="address"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Company Address (Optional)
                    </label>
                    <div className="mt-1">
                      <textarea
                        id="address"
                        name="address"
                        rows={4}
                        className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                        value={el.address}
                        onChange={(e: ChangeEvent<HTMLTextAreaElement>) =>
                          handleOnChange(index, 'address', e.target.value)
                        }
                      />
                    </div>
                  </div>

                  <div className="sm:col-span-6">
                    <label
                      htmlFor="job-description"
                      className="block text-sm font-medium text-slate-700"
                    >
                      Job Description
                    </label>
                    <div className="mt-1">
                      <textarea
                        rows={5}
                        name="description"
                        id="job-description"
                        required={true}
                        className="block w-full rounded-md border-gray-300 shadow-sm focus:border-purple-500 focus:ring-purple-500 sm:text-sm"
                        defaultValue={el.description}
                        onChange={(e: ChangeEvent<HTMLTextAreaElement>) =>
                          handleOnChange(index, 'description', e.target.value)
                        }
                      />
                    </div>
                    <p className="mt-2 text-sm text-gray-500">
                      Briefly describe your job responsibilities
                    </p>
                  </div>

                  <div className="col-span-6">
                    <Checkbox
                      id="current_job"
                      name="status"
                      handleOnChange={handleOnChange}
                      checked={el.status}
                      label="Current Job (Tick if this is your current job)"
                      index={index}
                    />
                  </div>
                </div>
              </div>
            ))}
          </div>
          <div className="flex justify-end space-x-4 px-4 py-3 text-right sm:px-6">
            <button
              type="button"
              className="inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none"
              onClick={() => handleAddFormFields()}
            >
              <PlusSmIcon
                className="-ml-1.5 mr-1 h-5 w-5 text-gray-400"
                aria-hidden="true"
              />
              Add New Work Experience
            </button>
            <Link
              href={route('resume.manager.index')}
              type="button"
              className="inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none"
            >
              <XIcon
                className="-ml-1.5 mr-1 h-5 w-5 text-gray-400"
                aria-hidden="true"
              />
              Cancel
            </Link>
            <button
              type="submit"
              className="inline-flex justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
              disabled={processing}
            >
              Save
            </button>
          </div>
        </div>
      </form>
      {showNotification && (
        <ModalPortal>
          <Notification
            show={showNotification}
            title={success}
            closeNotification={closeNotification}
            messageType="success"
          />
        </ModalPortal>
      )}
    </>
  );
};

WorkExperience.layout = (page: Page) => (
  <AppLayout children={page} title="Work Experiences" />
);

export default WorkExperience;

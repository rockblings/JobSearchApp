import { Link, useForm } from '@inertiajs/inertia-react';
import React from 'react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import { Page } from '@inertiajs/inertia';
import GuestLayout from '@/Shared/Components/Layout/GuestLayout';
import { Trans, useTranslation } from 'react-i18next';

interface IData {
  name: string;
  email: string;
  password: string;
  password_confirmation: string;
  terms: boolean | undefined;
}

const initialValue: IData = {
  name: '',
  email: '',
  password: '',
  password_confirmation: '',
  terms: false,
};

const Register = () => {
  const { t } = useTranslation();
  const { props } = useTypedPage();
  const { jetstream } = props;

  const route = useRoute();

  const { data, setData, post, processing, errors, reset } =
    useForm(initialValue);

  const handleSubmit = (event: React.FormEvent) => {
    event.preventDefault();
    post(route('register'), {
      onFinish: () => reset('password', 'password_confirmation'),
    });
  };

  return (
    <>
      <div className="flex min-h-full flex-col justify-center bg-white py-12 sm:px-6 lg:px-8">
        <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
          <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
            <form className="space-y-6" onSubmit={handleSubmit}>
              <div>
                <label
                  htmlFor="full-name"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t('register.Full.Name.Label')}
                </label>
                <div className="mt-1">
                  <input
                    id="fullName"
                    name="name"
                    type="text"
                    autoComplete="name"
                    placeholder="John Doe"
                    aria-invalid="true"
                    aria-describedby="name-error"
                    value={data.name}
                    onChange={e => setData('name', e.target.value)}
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
                  />
                </div>
                {errors.name && (
                  <p className="mt-2 text-sm text-red-600" id="name-error">
                    {errors.name}
                  </p>
                )}
              </div>
              <div>
                <label
                  htmlFor="email-address"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t('login.Form.E-mail')}
                </label>
                <div className="mt-1">
                  <input
                    id="email-address"
                    name="email"
                    type="email"
                    autoComplete="email"
                    placeholder="john.doe@example.com"
                    aria-invalid="true"
                    aria-describedby="email-error"
                    value={data.email}
                    onChange={e => setData('email', e.target.value)}
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
                  />
                </div>
                {errors.email && (
                  <p className="mt-2 text-sm text-red-600" id="email-error">
                    {errors.email}
                  </p>
                )}
              </div>

              <div>
                <label
                  htmlFor="password"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t('login.Form.Password')}
                </label>
                <div className="mt-1">
                  <input
                    id="password"
                    name="password"
                    type="password"
                    autoComplete="current-password"
                    placeholder={t('login.Form.Password')}
                    value={data.password}
                    onChange={e => setData('password', e.target.value)}
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
                  />
                </div>
                {errors.password && (
                  <p className="mt-2 text-sm text-red-600" id="password-error">
                    {errors.password}
                  </p>
                )}
              </div>

              <div>
                <label
                  htmlFor="password-confirmation"
                  className="block text-sm font-medium text-gray-700"
                >
                  {t('register.Password.Confirmation.Label')}
                </label>
                <div className="mt-1">
                  <input
                    id="password-confirmation"
                    name="password_confirmation"
                    type="password"
                    autoComplete="current-password"
                    placeholder={t('register.Password.Confirmation.Label')}
                    value={data.password_confirmation}
                    onChange={e =>
                      setData('password_confirmation', e.target.value)
                    }
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
                  />
                </div>
                {errors.password_confirmation && (
                  <p className="mt-2 text-sm text-red-600" id="password-error">
                    {errors.password_confirmation}
                  </p>
                )}
              </div>

              <div className="flex items-center justify-between">
                <div className="flex items-center">
                  {jetstream.hasTermsAndPrivacyPolicyFeature && (
                    <>
                      <div className="flex items-center">
                        <input
                          id="terms"
                          name="terms"
                          type="checkbox"
                          className="h-4 w-4 rounded border-gray-300 text-fuchsia-700 focus:ring-fuchsia-700"
                          checked={data.terms}
                          onChange={e => setData('terms', e.target.checked)}
                          required
                        />
                      </div>
                      <div className="ml-2">
                        <label
                          htmlFor="terms"
                          className="text-sm font-medium text-gray-900"
                        >
                          <Trans i18nKey="register.Terms.And.Privacy.Policy">
                            I agree to the{' '}
                            <a
                              target="_blank"
                              href={route('terms.show')}
                              className="text-sm text-gray-500 underline hover:text-gray-900"
                            >
                              Terms of Service
                            </a>{' '}
                            and{' '}
                            <a
                              target="_blank"
                              href={route('policy.show')}
                              className="text-sm text-gray-500 underline hover:text-gray-900"
                            >
                              Privacy Policy
                            </a>
                          </Trans>
                        </label>
                      </div>
                      {errors.terms && (
                        <p
                          className="mt-2 text-sm text-red-600"
                          id="password-error"
                        >
                          {errors.terms}
                        </p>
                      )}
                    </>
                  )}
                </div>
              </div>

              <div className="flex items-center justify-between">
                <div className="text-sm">
                  <Link
                    href={route('login')}
                    className="font-medium text-fuchsia-900 hover:text-fuchsia-500"
                  >
                    {t('register.Already.Registered.Link')}
                  </Link>
                </div>
                <div className="flex items-center">
                  <button
                    type="submit"
                    className="flex w-full justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
                    disabled={processing}
                  >
                    {t('general.Create')}
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

Register.layout = (page: Page) => <GuestLayout children={page} />;

export default Register;

import { useForm } from '@inertiajs/inertia-react';
import React from 'react';
import { useState } from 'react';
import useRoute from '@/Hooks/useRoute';

const TwoFactorChallenge = () => {
  const route = useRoute();
  const { data, setData, processing, errors, post } = useForm({
    code: '',
    recovery_code: '',
  });
  const [isRecovery, setIsRecovery] = useState(false);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    post(route('two-factor.login'), {
      preserveState: false,
    });
  };

  const handleToggleRecovery = (e: React.MouseEvent) => {
    e.preventDefault();
    setIsRecovery(isRecovery => !isRecovery);
  };

  return (
    <div className="flex min-h-screen flex-col justify-center bg-gray-50 py-12 sm:px-6 lg:px-8">
      <div className="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div className="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          {isRecovery ? (
            <p className="block text-sm font-medium text-gray-700">
              Please confirm access to your account by entering one of your
              emergency recovery codes.
            </p>
          ) : (
            <p className="block text-sm font-medium text-gray-700">
              Please confirm access to your account by entering the
              authentication code provided by your authenticator application.
            </p>
          )}

          <form className="mt-3 space-y-6" onSubmit={handleSubmit}>
            {!isRecovery ? (
              <div>
                <div className="mt-1">
                  <input
                    id="code"
                    name="code"
                    type="text"
                    autoComplete="one-time-code"
                    placeholder="Authentication Code"
                    value={data.code}
                    onChange={e => setData('code', e.target.value)}
                    required
                    inputMode="numeric"
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-gray-500 sm:text-sm"
                  />
                </div>
                {errors.code && (
                  <p className="mt-2 text-sm text-red-600" id="code-error">
                    {errors.code}
                  </p>
                )}
              </div>
            ) : (
              <div>
                <div className="mt-1">
                  <input
                    id="recovery_code"
                    name="recovery_code"
                    type="text"
                    autoComplete="one-time-code"
                    placeholder="Recovery code"
                    value={data.recovery_code}
                    onChange={e => setData('recovery_code', e.target.value)}
                    required
                    className="block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-gray-500 sm:text-sm"
                  />
                </div>
                {errors.recovery_code && (
                  <p
                    className="mt-2 text-sm text-red-600"
                    id="recovery_code-error"
                  >
                    {errors.recovery_code}
                  </p>
                )}
              </div>
            )}
            <div className="flex items-center justify-between">
              <div className="text-sm">
                <button
                  type="button"
                  className="cursor-pointer text-sm text-gray-600 underline hover:text-gray-900"
                  onClick={handleToggleRecovery}
                >
                  {isRecovery
                    ? 'Use an authentication code'
                    : 'Use a recovery code'}
                </button>
              </div>
              <div className="flex items-center">
                <button
                  type="submit"
                  className="flex w-full justify-center rounded-md border border-transparent bg-violet-700 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-violet-400 focus:outline-none"
                  disabled={processing}
                >
                  Login
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default TwoFactorChallenge;

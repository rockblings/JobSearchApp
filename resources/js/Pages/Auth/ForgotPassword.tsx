import { Link, useForm } from '@inertiajs/inertia-react';
import React from 'react';
import useRoute from '@/Hooks/useRoute';

const ForgotPassword = () => {
  const { data, setData, processing, errors, post } = useForm({
    email: '',
  });

  const route = useRoute();

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    post(route('password.email'));
  };

  return (
    <div className="mx-auto max-w-7xl px-4 pt-20 sm:px-6 lg:px-8">
      <div className="mx-auto max-w-3xl">
        <div className="bg-white shadow sm:rounded-lg">
          <div className="px-4 py-5 sm:p-6">
            <h3 className="text-lg font-medium leading-6 text-gray-500">
              Forgot Password
            </h3>
            <div className="mt-2 max-w-xl text-sm text-gray-500">
              <p>
                Forgot your password? No problem. Just let us know your email
                address and we will email you a password reset link that will
                allow you to choose a new one.
              </p>
            </div>
            <form
              className="mt-5 sm:flex sm:items-center"
              onSubmit={handleSubmit}
            >
              <div className="w-full sm:max-w-xs">
                <label htmlFor="email" className="sr-only">
                  Email
                </label>
                <input
                  type="text"
                  name="email"
                  id="email"
                  className="block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-500 focus:ring-gray-500 sm:text-sm"
                  placeholder="you@example.com"
                  value={data.email}
                  onChange={e => setData('email', e.target.value)}
                />
                {errors.email && (
                  <p className="mt-2 text-sm text-red-600" id="email-error">
                    {errors.email}
                  </p>
                )}
              </div>

              <div className="flex items-center justify-between">
                <button
                  type="submit"
                  className="mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-violet-600 px-4 py-2 font-medium text-white shadow-sm hover:bg-violet-700 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  disabled={processing}
                >
                  Send
                </button>
                <div>
                  <Link
                    href={route('login')}
                    className="mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-white px-4 py-2 font-medium text-gray-600 shadow-sm hover:bg-gray-50 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  >
                    Cancel
                  </Link>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ForgotPassword;

import { Page } from '@inertiajs/inertia';
import AppLayout from '@/Shared/Components/Layout/AppLayout';
import React, {
  ChangeEvent,
  FormEvent,
  useEffect,
  useRef,
  useState,
} from 'react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import { useForm } from '@inertiajs/inertia-react';
import Compressor from 'compressorjs';
import { DropDownData } from '@/types';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';
import Notification from '@/Shared/Components/Modal/Notification';
import EmptyProfileImage from '@/Shared/Components/Image/EmptyProfileImage';
import SimpleDropdown from '@/Shared/Components/FormElement/SimpleDropdown';
import { IProfile } from '@/interfaces';

interface IErrors {
  description?: string;
  language?: string;
  phoneNumber?: string;
  country?: string;
  photo?: string;
}

const Show = () => {
  const route = useRoute();
  const { props } = useTypedPage();

  const {
    user: { loggedInUserName, loggedInUserEmail },
    errors,
    pageData: { profile, languages, countries },
  } = props;

  const selectedCountry =
    countries.find((country: DropDownData) => country.id === profile.country) ||
    countries[0];

  const selectedLanguage =
    languages.find(
      (language: DropDownData) => language.id === profile.language,
    ) || languages[0];

  let imageUrl = profile.photoUrl || '';

  const updateProfileErrors: IErrors = errors?.updateProfile || null;

  let initialProfileData: IProfile = {
    name: loggedInUserName,
    email: loggedInUserEmail,
    description: profile.description || '',
    language: profile.language,
    phoneNumber: profile.phoneNumber || '',
    country: profile.country,
    photo: null as File | null,
  };

  const { data, setData, processing, post } = useForm(initialProfileData);

  const [photoPreview, setPhotoPreview] = useState<string | null>(imageUrl);
  const photoRef = useRef<HTMLInputElement>(null);
  const [showNotification, setShowNotification] = useState(false);

  const updatePhotoPreview = () => {
    const photo = photoRef.current?.files?.[0];
    if (!photo) {
      return;
    }
    const reader = new FileReader();
    reader.onload = e => {
      new Compressor(photo, {
        quality: 1, // 0.6 can also be used, but its not recommended to go below.
        resize: 'cover',
        success: compressedResult => {
          // compressedResult has the compressed file.
          // convert compressed photo from blob to file
          const compressedPhoto = new File([compressedResult], photo.name, {
            type: compressedResult.type,
          });
          setData('photo', compressedPhoto);
          setPhotoPreview(e.target?.result as string);
        },
      });
    };
    reader.readAsDataURL(photo);
    photoRef.current.blur();
  };

  const handleOnProfileUpdate = (e: FormEvent) => {
    e.preventDefault();
    post(route('profile.store'), {
      errorBag: 'updateProfile',
      preserveScroll: true,
      onSuccess: () => handleOnUpdateSuccess(),
    });
  };

  useEffect(() => {
    setPhotoPreview(profile.photoUrl);
    return () => {};
  }, [props]);

  const handleOnUpdateSuccess = () => {
    setShowNotification(true);
  };

  const handleOnSelect = <P extends keyof IProfile>(
    prop: P,
    value: IProfile[P],
  ) => {
    setData(prop, value);
  };

  const closeNotification = () => {
    setShowNotification(false);
  };

  return (
    <>
      <form
        className="sm:overflow-hidden sm:rounded-md"
        onSubmit={handleOnProfileUpdate}
      >
        {/* Profile section */}
        <div className="space-y-6 bg-white py-6 px-4 sm:p-6 lg:pb-8">
          <div className="grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6">
            <div className="mt-6 flex-grow lg:mt-0 lg:ml-6 lg:flex-shrink-0 lg:flex-grow-0">
              <p
                className="text-sm font-medium text-slate-500"
                aria-hidden="true"
              >
                Photo
              </p>
              <div className="mt-1 lg:hidden">
                <div className="flex items-center">
                  <div
                    className="inline-block h-12 w-12 flex-shrink-0 overflow-hidden rounded-full"
                    aria-hidden="true"
                  >
                    {imageUrl ? (
                      <img
                        className="h-full w-full rounded-full"
                        src={imageUrl}
                        alt=""
                      />
                    ) : (
                      <EmptyProfileImage classes="inline-block h-12 w-12 overflow-hidden rounded-full bg-gray-100" />
                    )}
                  </div>
                  <div className="ml-5 rounded-md shadow-sm">
                    <div className="group relative flex items-center justify-center rounded-md border border-gray-300 py-2 px-3 focus-within:ring-2 focus-within:ring-fuchsia-500 focus-within:ring-offset-2 hover:bg-gray-50">
                      <label
                        htmlFor="mobile-user-photo"
                        className="pointer-events-none relative text-sm font-medium leading-4 text-gray-700"
                      >
                        <span>Change</span>
                        <span className="sr-only"> user photo</span>
                      </label>
                      <input
                        id="mobile-user-photo"
                        name="user-photo"
                        type="file"
                        className="absolute h-full w-full cursor-pointer rounded-md border-gray-300 opacity-0"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div className="relative hidden overflow-hidden rounded-full lg:block">
                {photoPreview ? (
                  <div className="mt-2">
                    <img
                      className="relative h-40 w-40 rounded-full"
                      src={photoPreview}
                      alt=""
                    />
                  </div>
                ) : imageUrl ? (
                  <img
                    className="relative h-40 w-40 rounded-full"
                    src={imageUrl}
                    alt=""
                  />
                ) : (
                  <EmptyProfileImage classes="inline-block h-36 w-36 overflow-hidden rounded-full bg-gray-100" />
                )}

                <label
                  htmlFor="desktop-user-photo"
                  className="absolute inset-0 flex h-full w-full items-center justify-center bg-black bg-opacity-75 text-sm font-medium text-white opacity-0 focus-within:opacity-100 hover:opacity-100"
                >
                  <span>Change</span>
                  <span className="sr-only"> user photo</span>
                  <input
                    accept="image/jpg, image/jpeg, image/png, image/svg, capture=camera"
                    type="file"
                    id="desktop-user-photo"
                    name="user-photo"
                    className="absolute inset-0 h-full w-full cursor-pointer rounded-md border-gray-300 opacity-0"
                    ref={photoRef}
                    onChange={() => updatePhotoPreview()}
                  />
                </label>
              </div>
              {updateProfileErrors && (
                <p className="mt-2 text-sm text-red-600" id="password-error">
                  {updateProfileErrors.photo}
                </p>
              )}
            </div>
            <div className="sm:col-span-6" />

            <div className="sm:col-span-3">
              <label
                htmlFor="first-name"
                className="block text-sm font-medium text-gray-700"
              >
                Full Name
              </label>
              <input
                type="text"
                name="first-name"
                id="first-name"
                autoComplete="given-name"
                className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
                placeholder="John Doe"
                value={data.name}
                onChange={(e: ChangeEvent<HTMLInputElement>) => {
                  setData('name', e.currentTarget.value);
                }}
              />
            </div>

            <div className="sm:col-span-3">
              <label
                htmlFor="email-address"
                className="block text-sm font-medium text-gray-700"
              >
                Email address
              </label>
              <input
                type="text"
                name="email-address"
                id="email-address"
                autoComplete="email"
                className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
                placeholder="john.doe@example.com"
                value={data.email}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setData('email', e.currentTarget.value)
                }
              />
            </div>

            <div className="sm:col-span-6">
              <label
                htmlFor="description"
                className="block text-sm font-medium text-gray-700"
              >
                Description
              </label>
              <div className="mt-1">
                <textarea
                  id="description"
                  name="description"
                  rows={4}
                  className="mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                  value={data.description}
                  onChange={(e: ChangeEvent<HTMLTextAreaElement>) =>
                    setData('description', e.currentTarget.value)
                  }
                />
              </div>
              <p className="mt-2 text-sm text-gray-500">
                Brief description of you.
              </p>
              {updateProfileErrors && (
                <p className="mt-2 text-sm text-red-600" id="password-error">
                  {updateProfileErrors.description}
                </p>
              )}
            </div>
          </div>

          <div className="sm:col-span-6">
            <label
              htmlFor="username"
              className="text-blue-gray-900 block text-sm font-medium"
            >
              Social Account
            </label>
            <div className="mt-1 flex rounded-md shadow-sm">
              <span className="inline-flex  items-center rounded-l-md border border-r-0 border-gray-300 px-3 sm:text-sm">
                https://www.linkedin.com/
              </span>
              <input
                type="text"
                name="linkedin"
                id="linkedin"
                autoComplete="linkedin"
                defaultValue={data.name}
                className="block w-full min-w-0 flex-1 rounded-none rounded-r-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500"
              />
            </div>
          </div>

          <div className="grid grid-cols-1 gap-y-6 pt-8 sm:grid-cols-6 sm:gap-x-6">
            <div className="sm:col-span-6" />
            <div className="relative sm:col-span-3">
              <SimpleDropdown
                dropDownData={languages}
                handleOnSelect={handleOnSelect}
                label="Main Spoken Language"
                initialSelection={selectedLanguage}
                name="language"
              />
              {updateProfileErrors && (
                <p className="mt-2 text-sm text-red-600" id="password-error">
                  {updateProfileErrors.language}
                </p>
              )}
            </div>

            <div className="sm:col-span-3">
              <label
                htmlFor="phone-number"
                className="block text-sm font-medium text-gray-700"
              >
                Phone number
              </label>
              <input
                type="text"
                name="phone-number"
                id="phone-number"
                autoComplete="tel"
                className="mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
                value={data.phoneNumber}
                onChange={(e: ChangeEvent<HTMLInputElement>) =>
                  setData('phoneNumber', e.currentTarget.value)
                }
              />
              {updateProfileErrors && (
                <p className="mt-2 text-sm text-red-600" id="password-error">
                  {updateProfileErrors.phoneNumber}
                </p>
              )}
            </div>
            <div className="sm:col-span-3">
              <SimpleDropdown
                dropDownData={countries}
                handleOnSelect={handleOnSelect}
                label="Country"
                initialSelection={selectedCountry}
                name="country"
              />
            </div>
          </div>

          <div className="flex justify-end pt-8">
            <button
              type="submit"
              className="ml-3 inline-flex justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
              disabled={processing}
            >
              Save
            </button>
          </div>
        </div>
      </form>
      {showNotification && (
        <ModalPortal>
          <Notification
            show={showNotification}
            title="Profile was successfully updated"
            closeNotification={closeNotification}
            messageType="success"
          />
        </ModalPortal>
      )}
    </>
  );
};

Show.layout = (page: Page) => <AppLayout children={page} title="Profile" />;

export default Show;

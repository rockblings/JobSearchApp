import ProfilePersonalInfo from './Components/ProfilePersonalInfo';
import ChangePasswordForm from './Components/ChangePasswordForm';
import TwoFactorAuthentication from './Components/TwoFactorAuthentication';
import LogoutOtherBrowserSessions from './Components/LogoutOtherBrowserSessions';
import DeleteUserForm from './Components/DeleteUserForm';
// @ts-ignore
import Dashboard from '@/Shared/Components/Dashboard';
import React from 'react';
import useTypedPage from '@/Hooks/useTypedPage';
import { Page } from '@inertiajs/inertia';
import AppLayout from '@/Shared/Components/Layout/AppLayout';

const Show = () => {
  const { props } = useTypedPage();

  return (
    <>
      <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
        {/* Form to update personal information*/}
        {props.jetstream.canUpdateProfileInformation && <ProfilePersonalInfo />}
        {/* Change password form*/}
        {props.jetstream.canUpdatePassword && <ChangePasswordForm />}
        {/* Two-factor authentication settings*/}
        {props.jetstream.canManageTwoFactorAuthentication && (
          <TwoFactorAuthentication />
        )}
        {/* Browser sessions*/}
        <LogoutOtherBrowserSessions />
        {/* Delete user account*/}
        {props.jetstream.hasAccountDeletionFeatures && <DeleteUserForm />}
      </div>
    </>
  );
};

Show.layout = (page: Page) => <AppLayout children={page} title="Settings" />;

export default Show;

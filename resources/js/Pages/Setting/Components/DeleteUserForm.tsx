import { useForm } from '@inertiajs/inertia-react';
import React, { useState } from 'react';
import useRoute from '@/Hooks/useRoute';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';
import PermissionModal from '@/Shared/Components/Modal/PermissionModal';

const DeleteUserForm = () => {
  const pageInfo = {
    title: 'Delete Account',
    description:
      'Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.',
    btnName: 'Delete Account',
  };

  const {
    data,
    setData,
    processing,
    delete: destroy,
  } = useForm({
    password: '',
  });

  const [isActive, setIsActive] = useState(false);
  const [error, setError] = useState(false);
  const route = useRoute();

  const handleActive = (active: boolean) => {
    return (e: React.MouseEvent<HTMLButtonElement>) => {
      e.preventDefault();
      setIsActive(active);
    };
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    destroy(route('current-user.destroy'), {
      preserveState: true,
      errorBag: 'deleteUser',
      onSuccess: page => {
        if (!page.props.errors.deleteUser) {
          setIsActive(false);
        }
      },
      onError: errors => {
        if (errors.password) {
          setError(true);
        }
      },
    });
  };

  return (
    <>
      <div className="bg-white shadow sm:rounded-lg">
        <div className="px-4 py-5 sm:p-6">
          <div>
            <h3 className="text-lg font-medium leading-6 text-fuchsia-900">
              Delete Account
            </h3>
            <p className="mt-1 text-sm text-gray-500">
              Permanently delete your account.
            </p>
            <p className="mt-1 text-sm text-gray-500">
              Once your account is deleted, all of its resources and data will
              be permanently deleted. Before deleting your account, please
              download any data or information that you wish to retain.
            </p>
          </div>

          <div className="mt-5">
            <button
              type="button"
              className="inline-flex items-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-red-50 shadow-sm hover:bg-red-300 focus:outline-none focus:ring-2 focus:ring-red-50 focus:ring-offset-2"
              onClick={handleActive(true)}
            >
              Delete Account
            </button>
          </div>
        </div>
      </div>
      <ModalPortal>
        <PermissionModal
          data={data}
          setData={setData}
          isActive={isActive}
          handleActive={handleActive}
          pageInfo={pageInfo}
          processing={processing}
          handleSubmit={handleSubmit}
          error={error}
        />
      </ModalPortal>
    </>
  );
};
export default DeleteUserForm;

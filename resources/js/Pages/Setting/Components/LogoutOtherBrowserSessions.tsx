import React, { useState } from 'react';
import { useForm } from '@inertiajs/inertia-react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';
import PermissionModal from '@/Shared/Components/Modal/PermissionModal';

const LogoutOtherBrowserSessions: React.FC = () => {
  const route = useRoute();
  const { props } = useTypedPage();

  const { sessions } = props;

  const pageInfo = {
    title: 'Log Out Other Browser Sessions',
    description:
      'Please enter your password to confirm you would like to log out of your other browser sessions across all of your devices.',
    btnName: 'Log Out Other Browser Sessions',
  };

  const {
    data,
    setData,
    processing,
    delete: destroy,
    reset,
  } = useForm({
    password: '',
  });

  const [isActive, setIsActive] = useState(false);
  const [error, setError] = useState(false);

  const handleActive = (active: boolean) => {
    return (e: React.FormEvent) => {
      e.preventDefault();
      setIsActive(active);
    };
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    destroy(route('other-browser-sessions.destroy'), {
      errorBag: 'logoutOtherBrowse',
      preserveScroll: true,
      onSuccess: () => {
        setIsActive(false);
      },
      onFinish: () => reset('password'),
      onError: errors => {
        if (errors.password) {
          setError(true);
        }
      },
    });
  };

  return (
    <>
      <div className="mb-4 bg-white shadow sm:rounded-lg">
        <div className="px-4 py-5 sm:p-6">
          <div>
            <h3 className="text-lg font-medium leading-6 text-fuchsia-900">
              Browser Sessions
            </h3>
            <p className="mt-1 text-sm text-gray-500">
              Manage and log out your active sessions on other browsers and
              devices.
            </p>
            <p className="mt-1 text-sm text-gray-500">
              If necessary, you may log out of all of your other browser
              sessions across all of your devices. Some of your recent sessions
              are listed below; however, this list may not be exhaustive. If you
              feel your account has been compromised, you should also update
              your password.
            </p>
          </div>
          {sessions.length > 0 ? (
            <div className="mt-5 space-y-6">
              {sessions.map((session, key) => (
                <div className="flex items-center" key={key}>
                  <div>
                    {session.agent.is_desktop ? (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                      >
                        <path
                          strokeLinecap="round"
                          strokeLinejoin="round"
                          strokeWidth="2"
                          d="M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
                        />
                      </svg>
                    ) : (
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 24 24"
                        strokeWidth="2"
                        stroke="currentColor"
                        fill="none"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        className="h-8 w-8 text-gray-500"
                      >
                        <path d="M0 0h24v24H0z" stroke="none" />
                        <rect x="7" y="4" width="10" height="16" rx="1" />
                        <path d="M11 5h2M12 17v.01" />
                      </svg>
                    )}
                  </div>

                  <div className="ml-3">
                    <div className="text-sm text-gray-600">
                      {session.agent.platform} - {session.agent.browser}
                    </div>

                    <div>
                      <div className="text-xs text-gray-500">
                        {session.ip_address},
                        {session.is_current_device ? (
                          <span className="font-semibold text-green-500">
                            {' '}
                            This device
                          </span>
                        ) : (
                          <span> Last active {session.last_active}</span>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : null}
          <div className="mt-5">
            <button
              type="button"
              className="inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none"
              onClick={handleActive(true)}
            >
              Log Out Other Browser Sessions
            </button>
          </div>
        </div>
      </div>
      <ModalPortal>
        <PermissionModal
          data={data}
          setData={setData}
          isActive={isActive}
          handleActive={handleActive}
          pageInfo={pageInfo}
          processing={processing}
          handleSubmit={handleSubmit}
          error={error}
        />
      </ModalPortal>
    </>
  );
};

export default LogoutOtherBrowserSessions;

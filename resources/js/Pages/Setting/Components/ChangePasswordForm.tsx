import { useForm } from '@inertiajs/inertia-react';
import React, { useState } from 'react';
// @ts-ignore
import Notification from '@/Shared/Components/Modal/Notification';
import useRoute from '@/Hooks/useRoute';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';

const ChangePasswordForm: React.FC = () => {
  const route = useRoute();
  const { data, setData, processing, errors, put, reset } = useForm({
    current_password: '',
    password: '',
    password_confirmation: '',
  });

  // toggle notification
  const [show, setShow] = useState(false);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    put(route('user-password.update'), {
      errorBag: 'updatePassword',
      preserveScroll: true,
      onSuccess: () => handleSuccessSubmit(),
      onError: () => reset(),
    });
  };

  const handleSuccessSubmit = () => {
    reset('current_password', 'password', 'password_confirmation');
    setShow(true);
  };

  const closeNotification = () => {
    setShow(false);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="mb-4 shadow sm:overflow-hidden sm:rounded-md">
          <div className="space-y-6 bg-white py-6 px-4 sm:p-6">
            <div>
              <h3 className="text-lg font-medium leading-6 text-fuchsia-900">
                Update Password
              </h3>
              <p className="mt-1 text-sm text-gray-500">
                Ensure your account is using a long, random password to stay
                secure.
              </p>
            </div>

            <div className="space-y-6 sm:space-y-5">
              <div className="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:border-t sm:border-gray-200 sm:pt-5">
                <label
                  htmlFor="current-password"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  Current Password
                </label>
                <div className="mt-1 sm:col-span-2 sm:mt-0">
                  <input
                    id="current-password"
                    name="current_password"
                    type="password"
                    autoComplete="current_password"
                    className="block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                    value={data.current_password}
                    onChange={e =>
                      setData('current_password', e.currentTarget.value)
                    }
                  />
                  {errors.current_password && (
                    <p
                      className="mt-2 text-sm text-red-600"
                      id="current_password-error"
                    >
                      {errors.current_password}
                    </p>
                  )}
                </div>
              </div>

              <div className="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5">
                <label
                  htmlFor="new-password"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  New Password
                </label>
                <div className="mt-1 sm:col-span-2 sm:mt-0">
                  <input
                    id="new-password"
                    name="password"
                    type="password"
                    autoComplete="new-password"
                    className="block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                    value={data.password}
                    onChange={e => setData('password', e.currentTarget.value)}
                  />
                  {errors.password && (
                    <p
                      className="mt-2 text-sm text-red-600"
                      id="password-error"
                    >
                      {errors.password}
                    </p>
                  )}
                </div>
              </div>
              <div className="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5">
                <label
                  htmlFor="password-confirmation"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  Confirm Password
                </label>
                <div className="mt-1 sm:col-span-2 sm:mt-0">
                  <input
                    id="password-confirmation"
                    name="password_confirmation"
                    type="password"
                    autoComplete="new-password"
                    className="block w-full max-w-lg appearance-none rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
                    value={data.password_confirmation}
                    onChange={e =>
                      setData('password_confirmation', e.currentTarget.value)
                    }
                  />
                  {errors.password_confirmation && (
                    <p
                      className="mt-2 text-sm text-red-600"
                      id="password_confirmation-error"
                    >
                      {errors.password_confirmation}
                    </p>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
            <button
              type="submit"
              className="inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none"
              disabled={processing}
            >
              Save
            </button>
          </div>
        </div>
      </form>
      {show && (
        <ModalPortal>
          <Notification
            show={show}
            title="Successfully Updated!"
            body="You password was updated successfully."
            closeNotification={closeNotification}
            messageType="success"
          />
        </ModalPortal>
      )}
    </>
  );
};

export default ChangePasswordForm;

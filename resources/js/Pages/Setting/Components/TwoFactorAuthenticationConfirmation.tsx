import React, { useState } from 'react';
import { useForm } from '@inertiajs/inertia-react';
import axios from 'axios';
import useRoute from '@/Hooks/useRoute';
import PermissionModal from '@/Shared/Components/Modal/PermissionModal';

type pageInfoType = {
  title: string;
  description: string;
  btnName: string;
};

interface ITwoFactorAuthenticationConfirmation {
  id: string;
  children?: React.ReactNode;
  onConfirmed: () => Promise<void>;
  pageInfo: pageInfoType;
}

const TwoFactorAuthenticationConfirmation = ({
  id,
  children,
  onConfirmed,
  pageInfo,
}: ITwoFactorAuthenticationConfirmation) => {
  const { data, setData, processing } = useForm({
    password: '',
  });

  const route = useRoute();

  const [isActive, setIsActive] = useState(false);
  const [error, setError] = useState(false);

  const handleActive = (active: boolean) => {
    return (e: React.FormEvent) => {
      e.preventDefault();
      if (active) {
        axios.get(route('password.confirmation')).then(response => {
          if (response.data.confirmed) {
            onConfirmed();
          } else {
            setIsActive(true);
          }
        });
      } else {
        setIsActive(false);
      }
    };
  };

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    axios
      .post(route('password.confirm'), {
        ...data,
      })
      .then(response => {
        onConfirmed();
        setIsActive(false);
      })
      .catch(error => {
        let {
          response: {
            data: { errors },
          },
        } = error;
        if (errors.password) {
          setError(true);
        }
      });
  };

  return (
    <>
      {React.isValidElement(children) &&
        React.cloneElement(children, {
          onClick: handleActive(true),
        })}
      <PermissionModal
        data={data}
        setData={setData}
        isActive={isActive}
        handleActive={handleActive}
        pageInfo={pageInfo}
        processing={processing}
        handleSubmit={handleSubmit}
        error={error}
      />
    </>
  );
};
export default TwoFactorAuthenticationConfirmation;

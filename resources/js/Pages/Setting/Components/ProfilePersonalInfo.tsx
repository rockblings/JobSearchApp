import { useForm } from '@inertiajs/inertia-react';
import React, { useState } from 'react';
import Notification from '@/Shared/Components/Modal/Notification';
import useTypedPage from '@/Hooks/useTypedPage';
import useRoute from '@/Hooks/useRoute';
import ModalPortal from '@/Shared/Components/Modal/ModalPortal';

const ProfilePersonalInfo: React.FC = () => {
  const route = useRoute();
  const { props } = useTypedPage();
  const { user } = props;

  const { data, setData, processing, errors, put } = useForm({
    name: user.loggedInUserName,
    email: user.loggedInUserEmail,
  });

  const [show, setShow] = useState(false);

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault();
    put(route('user-profile-information.update'), {
      errorBag: 'updateProfileInformation',
      preserveScroll: true,
      onSuccess: () => setShow(true),
    });
  };

  const closeNotification = () => {
    setShow(false);
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        <div className="mb-4 shadow sm:overflow-hidden sm:rounded-md">
          <div className="space-y-6 bg-white py-6 px-4 sm:p-6">
            <div>
              <h3 className="text-lg font-medium leading-6 text-fuchsia-900">
                Personal Information
              </h3>
              <p className="mt-1 text-sm text-gray-500">
                Update your account's profile information and email address.
              </p>
            </div>

            <div className="space-y-6 sm:space-y-5">
              <div className="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:border-t sm:border-gray-200 sm:pt-5">
                <label
                  htmlFor="fullName"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  Name
                </label>
                <div className="mt-1 sm:col-span-2 sm:mt-0">
                  <input
                    id="fullName"
                    name="name"
                    type="text"
                    autoComplete="name"
                    className="block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                    value={data.name}
                    onChange={e => setData('name', e.currentTarget.value)}
                    required
                  />
                  {errors.name && (
                    <p className="mt-2 text-sm text-red-600" id="name-error">
                      {errors.name}
                    </p>
                  )}
                </div>
              </div>

              <div className="sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5">
                <label
                  htmlFor="email"
                  className="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                >
                  Email
                </label>
                <div className="mt-1 sm:col-span-2 sm:mt-0">
                  <input
                    id="email"
                    name="email"
                    type="email"
                    autoComplete="email"
                    className="block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm"
                    value={data.email}
                    onChange={e => setData('email', e.currentTarget.value)}
                    required
                  />
                  {errors.email && (
                    <p className="mt-2 text-sm text-red-600" id="email-error">
                      {errors.email}
                    </p>
                  )}
                </div>
              </div>
            </div>
          </div>
          <div className="bg-gray-50 px-4 py-3 text-right sm:px-6">
            <button
              type="submit"
              className="inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none"
              disabled={processing}
            >
              Save
            </button>
          </div>
        </div>
      </form>
      {show && (
        <ModalPortal>
          <Notification
            show={show}
            title="Successfully Updated!"
            closeNotification={closeNotification}
            messageType="success"
          />
        </ModalPortal>
      )}
    </>
  );
};

export default ProfilePersonalInfo;

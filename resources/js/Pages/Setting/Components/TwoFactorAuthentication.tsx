import React, { useReducer } from 'react';
import { Inertia } from '@inertiajs/inertia';
import axios from 'axios';
import TwoFactorAuthenticationConfirmation from './TwoFactorAuthenticationConfirmation';
import useTypedPage from '@/Hooks/useTypedPage';

const TwoFactorAuthentication = () => {
  const { props } = useTypedPage();
  const twoFactorEnabled = props?.user?.twoFactorEnabled;

  const passwordVerificationData = {
    title: 'Confirm Password',
    description: 'For your security, please confirm your password to continue.',
    btnName: 'Confirm',
  };

  const initialState = {
    qrCode: undefined,
    recoveryCodes: [],
  };

  const [state, dispatch] = useReducer((state: any, { type, payload }: any) => {
    switch (type) {
      case 'SET_QRCODE':
        return {
          ...state,
          qrCode: payload,
        };
      case 'SET_RECOVERY_CODES':
        return {
          ...state,
          recoveryCodes: payload,
        };
    }
  }, initialState);

  const { qrCode, recoveryCodes } = state;

  const handleEnableTwoFactorAuthentication = async () => {
    await Inertia.post('/user/two-factor-authentication');
    let response = await axios.get('/user/two-factor-qr-code');
    dispatch({
      type: 'SET_QRCODE',
      payload: response.data,
    });

    response = await axios.get('/user/two-factor-recovery-codes');
    dispatch({
      type: 'SET_RECOVERY_CODES',
      payload: response.data,
    });
  };

  const handleShowRecoveryCodes = async () => {
    const response = await axios.get('/user/two-factor-recovery-codes');
    dispatch({
      type: 'SET_RECOVERY_CODES',
      payload: response.data,
    });
  };

  const handleRegenerateRecoveryCodes = async () => {
    await axios.post('/user/two-factor-recovery-codes');
    const response = await axios.get('/user/two-factor-recovery-codes');
    dispatch({
      type: 'SET_RECOVERY_CODES',
      payload: response.data,
    });
  };

  const handleDisableTwoFactorAuthentication = async () => {
    await Inertia.delete('/user/two-factor-authentication');
  };

  return (
    <div className="mb-4 bg-white shadow sm:rounded-lg">
      <div className="px-4 py-5 sm:p-6">
        <div>
          <h3 className="text-lg font-medium leading-6 text-fuchsia-900">
            Two Factor Authentication
          </h3>
          <p className="text-sm text-gray-500">
            Add additional security to your account using two factor
            authentication.
          </p>
          {twoFactorEnabled ? (
            <p className="mt-3 text-lg font-medium text-slate-600">
              You have enabled two factor authentication.
            </p>
          ) : (
            <p className="mt-3 text-lg font-medium text-brand-500">
              You have not enabled two factor authentication.
            </p>
          )}
          <p className="mt-2 text-sm text-gray-500">
            When two factor authentication is enabled, you will be prompted for
            a secure, random token during authentication. You may retrieve this
            token from your phone's Google Authenticator application.
          </p>
        </div>
        <div className="mt-5">
          {twoFactorEnabled ? (
            <>
              {qrCode && (
                <div className="mt-4 max-w-xl text-sm text-gray-600">
                  <p className="font-semibold">
                    Two factor authentication is now enabled. Scan the following
                    QR code using your phone's authenticator application.
                  </p>
                  <div
                    dangerouslySetInnerHTML={{ __html: qrCode.svg }}
                    className="mt-4"
                  />
                </div>
              )}
              {recoveryCodes.length > 0 && (
                <>
                  <div className="mt-4 max-w-xl text-sm text-gray-600">
                    <p className="font-semibold">
                      Store these recovery codes in a secure password manager.
                      They can be used to recover access to your account if your
                      two factor authentication device is lost.
                    </p>
                  </div>
                  <div className="mt-4 grid max-w-xl gap-1 rounded-lg bg-gray-100 px-4 py-4 font-mono text-sm">
                    {recoveryCodes.map((code: any) => (
                      <div key={code}>{code}</div>
                    ))}
                  </div>
                </>
              )}
              {recoveryCodes.length > 0 ? (
                <TwoFactorAuthenticationConfirmation
                  id="regenerate-recorvery-codes"
                  onConfirmed={handleRegenerateRecoveryCodes}
                  pageInfo={passwordVerificationData}
                >
                  <button
                    type="button"
                    className="mt-3 inline-flex items-center rounded-md border border-transparent bg-fuchsia-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-fuchsia-700 focus:outline-none"
                    onClick={handleRegenerateRecoveryCodes}
                  >
                    Regenerate Recovery Code
                  </button>
                </TwoFactorAuthenticationConfirmation>
              ) : (
                <TwoFactorAuthenticationConfirmation
                  id="show-recovery-codes"
                  onConfirmed={handleShowRecoveryCodes}
                  pageInfo={passwordVerificationData}
                >
                  <button
                    type="button"
                    className="inline-flex items-center rounded-md border border-transparent bg-fuchsia-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-fuchsia-700 focus:outline-none"
                    onClick={handleShowRecoveryCodes}
                  >
                    Show Recovery Code
                  </button>
                </TwoFactorAuthenticationConfirmation>
              )}
              <TwoFactorAuthenticationConfirmation
                id="disable-two-factor-authentication"
                onConfirmed={handleDisableTwoFactorAuthentication}
                pageInfo={passwordVerificationData}
              >
                <button
                  type="button"
                  className="mt-3 ml-3 inline-flex items-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-red-200 focus:outline-none"
                >
                  Disable
                </button>
              </TwoFactorAuthenticationConfirmation>
            </>
          ) : (
            <TwoFactorAuthenticationConfirmation
              id="enable-two-factor-authentication"
              onConfirmed={handleEnableTwoFactorAuthentication}
              pageInfo={passwordVerificationData}
            >
              <button
                type="button"
                className="inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none"
              >
                Enable
              </button>
            </TwoFactorAuthenticationConfirmation>
          )}
        </div>
      </div>
    </div>
  );
};

export default TwoFactorAuthentication;

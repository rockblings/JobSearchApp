import {
  AdjustmentsIcon,
  BellIcon,
  BriefcaseIcon,
  DocumentTextIcon,
  FilterIcon,
  SearchIcon,
} from '@heroicons/react/outline';
import React from 'react';
import { useTranslation } from 'react-i18next';
import GuestLayout from '@/Shared/Components/Layout/GuestLayout';
import { Page } from '@inertiajs/inertia';
import { ExternalLinkIcon } from '@heroicons/react/solid';
import TrendyIllustration from '@/Shared/Svg/TrendyIllustration';
import UpgradeIllustration from '@/Shared/Svg/UpgradeIllustration';
import { Link } from '@inertiajs/inertia-react';
import useRoute from '@/Hooks/useRoute';
import PersonalDevelopmentIllustration from '@/Shared/Svg/PersonalDevelopmentIllustration';

const Welcome = () => {
  const route = useRoute();
  const { t } = useTranslation();
  const features = [
    {
      name: t('welcome.First.Card.Title'),
      description: t('welcome.First.Card.Description'),
      icon: DocumentTextIcon,
    },
    {
      name: t('welcome.Second.Card.Title'),
      description: t('welcome.Second.Card.Description'),
      icon: SearchIcon,
    },
    {
      name: t('welcome.Third.Card.Title'),
      description: t('welcome.Third.Card.Description'),
      icon: AdjustmentsIcon,
    },
    {
      name: t('welcome.Fourth.Card.Title'),
      description: t('welcome.Fourth.Card.Description'),
      icon: BriefcaseIcon,
    },
    {
      name: t('welcome.Fifth.Card.Title'),
      description: t('welcome.Fifth.Card.Description'),
      icon: BellIcon,
    },
    {
      name: t('welcome.Sixth.Card.Title'),
      description: t('welcome.Sixth.Card.Description'),
      icon: FilterIcon,
    },
  ];
  const blogPosts = [
    {
      id: 1,
      title: 'Boost your conversion rate',
      href: '#',
      date: 'Mar 16, 2020',
      datetime: '2020-03-16',
      category: { name: 'Article', href: '#' },
      imageUrl:
        'https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
      preview:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto accusantium praesentium eius, ut atque fuga culpa, similique sequi cum eos quis dolorum.',
      author: {
        name: 'Roel Aufderehar',
        imageUrl:
          'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        href: '#',
      },
      readingLength: '6 min',
    },
    {
      id: 2,
      title: 'How to use search engine optimization to drive sales',
      href: '#',
      date: 'Mar 10, 2020',
      datetime: '2020-03-10',
      category: { name: 'Video', href: '#' },
      imageUrl:
        'https://images.unsplash.com/photo-1547586696-ea22b4d4235d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
      preview:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit facilis asperiores porro quaerat doloribus, eveniet dolore. Adipisci tempora aut inventore optio animi., tempore temporibus quo laudantium.',
      author: {
        name: 'Brenna Goyette',
        imageUrl:
          'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        href: '#',
      },
      readingLength: '4 min',
    },
    {
      id: 3,
      title: 'Improve your customer experience',
      href: '#',
      date: 'Feb 12, 2020',
      datetime: '2020-02-12',
      category: { name: 'Case Study', href: '#' },
      imageUrl:
        'https://images.unsplash.com/photo-1492724441997-5dc865305da7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
      preview:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.',
      author: {
        name: 'Daniela Metz',
        imageUrl:
          'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
        href: '#',
      },
      readingLength: '11 min',
    },
  ];
  return (
    <>
      <main>
        <div className="bg-white sm:pt-16 lg:overflow-hidden lg:pt-8 lg:pb-14">
          <div className="mx-auto max-w-7xl lg:px-8">
            <div className="lg:grid lg:grid-cols-2 lg:gap-8">
              <div className="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 sm:text-center lg:flex lg:items-center lg:px-0 lg:text-left">
                <div className="lg:py-24">
                  <h1 className="mt-4 text-4xl font-extrabold tracking-tight text-slate-500 sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl">
                    <span className="block">A better way to</span>
                    <span className="block bg-gradient-to-r from-orange-200 to-purple-400 bg-clip-text pb-3 text-transparent sm:pb-5">
                      kick start your career
                    </span>
                  </h1>
                  <p className="text-base text-slate-400 sm:text-xl lg:text-lg xl:text-xl">
                    Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure
                    qui Lorem cupidatat commodo. Elit sunt amet fugiat veniam
                    occaecat fugiat.
                  </p>
                  <div className="mt-10 sm:mt-12">
                    <form action="#" className="sm:mx-auto sm:max-w-xl lg:mx-0">
                      <div className="sm:flex">
                        <div className="mt-3 sm:mt-0">
                          <Link
                            href={route('register')}
                            className="block w-full rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-3 px-4 font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
                          >
                            {t('register.Create.Account.Button')}
                          </Link>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div className="mt-12 -mb-16 sm:-mb-48 lg:relative lg:m-0">
                <div className="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0">
                  {/* Illustration taken from Illustration: https://freesvgillustration.com/illustrations/ */}
                  <PersonalDevelopmentIllustration />
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Feature section with screenshot */}
        <div className="relative bg-gray-50 pt-16 sm:pt-24 lg:pt-32">
          <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
            <div>
              <h2 className="text-base font-semibold uppercase tracking-wider text-orange-600">
                {t('welcome.First.Headline')}
              </h2>
              <p className="mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl">
                No Resume? No problem.
              </p>
              <p className="mx-auto mt-5 max-w-prose text-xl text-gray-500">
                {t('welcome.Marketing.Pitch')}
              </p>
            </div>
            <div className="mt-12 -mb-10 flex justify-center sm:-mb-24 lg:-mb-80">
              <TrendyIllustration />
            </div>
          </div>
        </div>

        {/* Feature section with grid */}
        <div className="relative bg-white py-16 sm:py-24 lg:py-32">
          <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
            <h2 className="text-base font-semibold uppercase tracking-wider text-orange-600">
              {t('welcome.Second.Headline')}
            </h2>
            <p className="mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl">
              Everything you need to start your job search
            </p>
            <p className="mx-auto mt-5 max-w-prose text-xl text-gray-500">
              Below are some of the services offered to help you in your job
              search or career change.
            </p>
            <div className="mt-12">
              <div className="grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3">
                {features.map(feature => (
                  <div key={feature.name} className="pt-6">
                    <div className="flow-root rounded-lg bg-gray-50 px-6 pb-8">
                      <div className="-mt-6">
                        <div>
                          <span className="inline-flex items-center justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-orange-600 p-3 shadow-lg">
                            <feature.icon
                              className="h-6 w-6 text-white"
                              aria-hidden="true"
                            />
                          </span>
                        </div>
                        <h3 className="mt-8 text-lg font-medium tracking-tight text-gray-900">
                          {feature.name}
                        </h3>
                        <p className="mt-5 text-base text-gray-500">
                          {feature.description}
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>

        {/* Testimonial section */}
        <div className="bg-gradient-to-r from-fuchsia-800 to-purple-600 pb-16 lg:relative lg:z-10 lg:pb-0">
          <div className="lg:mx-auto lg:grid lg:max-w-7xl lg:grid-cols-3 lg:gap-8 lg:px-8">
            <div className="relative lg:-my-8">
              <div
                aria-hidden="true"
                className="absolute inset-x-0 top-0 h-1/2 bg-white lg:hidden"
              />
              <div className="mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:h-full lg:p-0">
                <div className="aspect-w-10 aspect-h-6 overflow-hidden rounded-xl shadow-xl sm:aspect-w-16 sm:aspect-h-7 lg:aspect-none lg:h-full">
                  <img
                    className="object-cover lg:h-full lg:w-full"
                    src="https://images.unsplash.com/reserve/LJIZlzHgQ7WPSh5KVTCB_Typewriter.jpg?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cmVzdW1lfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=900&q=60"
                    alt=""
                  />
                </div>
              </div>
            </div>
            <div className="mt-12 lg:col-span-2 lg:m-0 lg:pl-8">
              <div className="mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0 lg:py-20">
                <blockquote>
                  <div>
                    <svg
                      className="h-12 w-12 text-fuchsia-50 opacity-25"
                      fill="currentColor"
                      viewBox="0 0 32 32"
                      aria-hidden="true"
                    >
                      <path d="M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z" />
                    </svg>
                    <p className="mt-6 text-2xl font-medium text-fuchsia-50">
                      Start by doing what is necessary, then what is possible,
                      and suddenly you are doing the impossible.
                    </p>
                  </div>
                  <footer className="mt-6">
                    <p className="text-base font-medium text-orange-100">
                      Francis of Assisi
                    </p>
                  </footer>
                </blockquote>
              </div>
            </div>
          </div>
        </div>

        {/* Blog section */}
        <div className="relative bg-gray-50 py-16 sm:py-24 lg:py-32">
          <div className="relative">
            <div className="mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8">
              <h2 className="text-base font-semibold uppercase tracking-wider text-orange-600">
                Learn
              </h2>
              <p className="mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl">
                Helpful Resources
              </p>
              <p className="mx-auto mt-5 max-w-prose text-xl text-gray-500">
                Phasellus lorem quam molestie id quisque diam aenean nulla in.
                Accumsan in quis quis nunc, ullamcorper malesuada. Eleifend
                condimentum id viverra nulla.
              </p>
            </div>
            <div className="mx-auto mt-12 grid max-w-md gap-8 px-4 sm:max-w-lg sm:px-6 lg:max-w-7xl lg:grid-cols-3 lg:px-8">
              {blogPosts.map(post => (
                <div
                  key={post.id}
                  className="flex flex-col overflow-hidden rounded-lg shadow-lg"
                >
                  <div className="flex-shrink-0">
                    <img
                      className="h-48 w-full object-cover"
                      src={post.imageUrl}
                      alt=""
                    />
                  </div>
                  <div className="flex flex-1 flex-col justify-between bg-white p-6">
                    <div className="flex-1">
                      <p className="text-sm font-medium text-orange-600">
                        <a
                          href={post.category.href}
                          className="hover:underline"
                        >
                          {post.category.name}
                        </a>
                      </p>
                      <a href={post.href} className="mt-2 block">
                        <p className="text-xl font-semibold text-slate-600">
                          {post.title}
                        </p>
                        <p className="mt-3 text-base text-slate-500">
                          {post.preview}
                        </p>
                      </a>
                    </div>
                    <div className="mt-6 flex items-center">
                      <div className="flex-shrink-0">
                        <a href={post.author.href}>
                          <img
                            className="h-10 w-10 rounded-full"
                            src={post.author.imageUrl}
                            alt={post.author.name}
                          />
                        </a>
                      </div>
                      <div className="ml-3">
                        <p className="text-sm font-medium text-gray-900">
                          <a
                            href={post.author.href}
                            className="hover:underline"
                          >
                            {post.author.name}
                          </a>
                        </p>
                        <div className="flex space-x-1 text-sm text-gray-500">
                          <time dateTime={post.datetime}>{post.date}</time>
                          <span aria-hidden="true">&middot;</span>
                          <span>{post.readingLength} read</span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>

        {/* CTA Section */}
        <div className="relative bg-gradient-to-r from-fuchsia-800 to-purple-600">
          <div className="relative h-56 bg-white sm:h-72 md:absolute md:left-0 md:h-full md:w-1/2">
            <UpgradeIllustration />
            <div
              aria-hidden="true"
              className="absolute inset-0 bg-white mix-blend-multiply"
            />
          </div>
          <div className="relative mx-auto max-w-md px-4 py-12 sm:max-w-7xl sm:px-6 sm:py-20 md:py-28 lg:px-8 lg:py-32">
            <div className="md:ml-auto md:w-1/2 md:pl-10">
              <h2 className="text-base font-semibold uppercase tracking-wider text-fuchsia-50">
                Award winning support
              </h2>
              <p className="mt-2 text-3xl font-extrabold tracking-tight text-white sm:text-4xl">
                We’re here to help
              </p>
              <p className="mt-3 text-lg text-fuchsia-50">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et,
                egestas tempus tellus etiam sed. Quam a scelerisque amet
                ullamcorper eu enim et fermentum, augue. Aliquet amet volutpat
                quisque ut interdum tincidunt duis.
              </p>
              <div className="mt-8">
                <div className="inline-flex rounded-md shadow">
                  <a
                    href="#"
                    className="inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-gray-900 hover:bg-gray-50"
                  >
                    Visit the help center
                    <ExternalLinkIcon
                      className="-mr-1 ml-3 h-5 w-5 text-gray-400"
                      aria-hidden="true"
                    />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

Welcome.layout = (page: Page) => <GuestLayout children={page} />;

export default Welcome;

import { IExperience, User } from '@/interfaces';

type DateTime = string;

export type Nullable<T> = T | null;

export type InertiaSharedProps<T = {}> = T & {
  jetstream: {
    canManageTwoFactorAuthentication: boolean;
    canUpdatePassword: boolean;
    canUpdateProfileInformation: boolean;
    hasAccountDeletionFeatures: boolean;
    hasTermsAndPrivacyPolicyFeature: boolean;
  };
  user: User;
  flash: Flash;
  errorBags: any;
  errors: any;
  selectedPage: string;
  sessions: Session[];
  pageData: PageData;
};

export type Flash = {
  error: string;
  success: string;
};

export type PageData = {
  profile: Profile;
  countries: DropDownData[];
  languages: DropDownData[];
  imageUrl: string;
  workExperiences: {
    data: IExperience[];
  };
};

export type Profile = {
  country: number | null;
  description: string | number | readonly string[] | undefined;
  language: number | null;
  phoneNumber: string | number | readonly string[] | undefined;
  photoUrl: string | null;
};

export type DropDownData = {
  id: number;
  name: string;
};

export type Session = {
  id: number;
  ip_address: string;
  is_current_device: boolean;
  agent: {
    is_desktop: boolean;
    platform: string;
    browser: string;
  };
  last_active: DateTime;
};

export interface IAvailableLanguages {
  en: {
    nativeName: string;
  };
  sv: {
    nativeName: string;
  };
}

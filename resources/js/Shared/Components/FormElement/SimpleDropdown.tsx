import React from 'react';
import { DropDownData } from '@/types';

interface IDropDown {
  dropDownData: DropDownData[];
  handleOnSelect: Function;
  label: string;
  initialSelection: DropDownData;
  name: string;
}

const SimpleDropdown = ({
  dropDownData,
  handleOnSelect,
  label,
  initialSelection,
  name,
}: IDropDown) => {
  return (
    <div>
      <label
        htmlFor="dropdown"
        className="block text-sm font-medium text-gray-700"
      >
        {label}
      </label>
      <select
        id="dropdown"
        name={name}
        className="mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
        defaultValue={initialSelection.id}
        onChange={e => handleOnSelect(name, e.target.value)}
      >
        {dropDownData.map(item => (
          <option key={item.id} value={item.id}>
            {item.name}
          </option>
        ))}
      </select>
    </div>
  );
};

export default SimpleDropdown;

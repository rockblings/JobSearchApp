import React from 'react';

interface ICheckbox {
  id: string;
  name: string;
  handleOnChange: Function;
  checked: boolean;
  label: string;
  index: number;
}

const Checkbox = ({
  id,
  name,
  handleOnChange,
  checked,
  label,
  index,
}: ICheckbox) => {
  const handleOnCheck = (index: number, name: string, value: boolean) => {
    handleOnChange(index, name, value);
  };

  return (
    <fieldset className="space-y-5">
      <legend className="sr-only">Checkbox</legend>
      <div className="relative flex items-start">
        <div className="flex h-5 items-center">
          <input
            id={id}
            name={name}
            type="checkbox"
            className="h-4 w-4 rounded border-fuchsia-500 text-fuchsia-600"
            checked={checked}
            onChange={e => handleOnCheck(index, name, e.target.checked)}
          />
        </div>
        <div className="ml-2 text-sm">
          <label htmlFor={id} className="text-sm text-gray-500" id={id}>
            {label}
          </label>
        </div>
      </div>
    </fieldset>
  );
};

export default Checkbox;

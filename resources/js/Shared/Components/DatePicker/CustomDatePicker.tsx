import React, { useState } from 'react';
import DatePicker, { registerLocale, setDefaultLocale } from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { enGB } from 'date-fns/locale';

registerLocale('en', enGB);
setDefaultLocale('en');

interface IDatePicker {
  handleOnDateChange(date: Date, index: number): void;
  index: number;
  maxDate?: Date;
  minDate?: Date;
  initialDate: Date | null;
  placeholder?: string;
}

const CustomDatePicker = ({
  handleOnDateChange,
  index,
  minDate,
  maxDate,
  initialDate,
  placeholder,
}: IDatePicker) => {
  const [selectedDate, setSelectedDate] = useState<Date | null>(initialDate);

  const handleOnChange = (date: Date) => {
    setSelectedDate(date);
    handleOnDateChange(date, index);
  };

  return (
    <div className="col-span-6 sm:col-span-3">
      <DatePicker
        selected={selectedDate}
        onChange={handleOnChange}
        placeholderText={placeholder}
        locale="en"
        className="block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
        dateFormat="yyyy-MM-dd"
        maxDate={maxDate}
        minDate={minDate}
      />
    </div>
  );
};

export default CustomDatePicker;

import React from 'react';
import { Link } from '@inertiajs/inertia-react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';

function classNames(...classes: string[]) {
  return classes.filter(Boolean).join(' ');
}
const MainNavigation = () => {
  const {
    props: { selectedPage },
  } = useTypedPage();
  const navigation = [
    { name: 'Home', href: 'dashboard', current: false },
    { name: 'Profile', href: 'profile.show', current: false },
    { name: 'Resume Manager', href: 'resume.manager.index', current: false },
    { name: 'Job Search', href: 'dashboard', current: false },
    { name: 'Openings', href: 'dashboard', current: false },
    { name: 'Settings', href: 'settings.show', current: false },
  ];

  navigation.filter(nav => {
    nav.current = nav.name === selectedPage;
  });
  const route = useRoute();
  return (
    <nav className="flex space-x-4">
      {navigation.map(item => (
        <Link
          key={item.name}
          href={route(item.href)}
          className={classNames(
            item.current
              ? 'bg-fuchsia-50 text-fuchsia-900 hover:bg-purple-100 hover:text-fuchsia-500'
              : 'text-fuchsia-100 hover:bg-fuchsia-50 hover:text-fuchsia-900',
            'inline-flex items-center rounded-md py-2 px-3 text-sm font-medium',
          )}
          aria-current={item.current ? 'page' : undefined}
        >
          {item.name}
        </Link>
      ))}
    </nav>
  );
};

export default MainNavigation;

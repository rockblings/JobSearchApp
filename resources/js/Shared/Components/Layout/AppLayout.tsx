import React from 'react';
import Footer from '@/Shared/Components/Footer/Footer';
import DesktopHeader from '@/Shared/Components/Header/DesktopHeader';
import MobileHeader from '@/Shared/Components/Header/MobileHeader';
import { Popover } from '@headlessui/react';
import { Helmet } from 'react-helmet';

interface IAppLayout {
  children: React.ReactNode;
  title?: string | undefined;
}

const AppLayout = ({ children, title }: IAppLayout) => {
  return (
    <>
      <Helmet>
        <title>{title ? `JobAdssist - ${title}` : 'JobAdssist'}</title>
      </Helmet>
      <div className="min-h-full">
        <Popover
          as="header"
          className="bg-gradient-to-r from-fuchsia-800 to-purple-600 pb-24"
        >
          {({ open }) => (
            <>
              {/* Desktop Header*/}
              <DesktopHeader open={open} />
              {/* Mobile Header */}
              <MobileHeader />
            </>
          )}
        </Popover>
        <main className="-mt-20 pb-8">
          <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
            <div className="space-y-8 overflow-hidden rounded-lg bg-white py-8 px-4 sm:p-6 lg:pb-8">
              {children}
            </div>
          </div>
        </main>
        <Footer />
      </div>
    </>
  );
};

export default AppLayout;

import React from 'react';
import GuestHeader from '@/Shared/Components/Header/GuestHeader';
import GuestFooter from '@/Shared/Components/Footer/GuestFooter';

const GuestLayout: React.FC = ({ children }) => {
  return (
    <>
      <GuestHeader />
      <div>{children}</div>
      <GuestFooter />
    </>
  );
};

export default GuestLayout;

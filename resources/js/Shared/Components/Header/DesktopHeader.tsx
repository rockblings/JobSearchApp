import React from 'react';
import ApplicationLogo from '@/Shared/Components/Header/Logo/ApplicationLogo';
import { BellIcon, MenuIcon, XIcon } from '@heroicons/react/outline';
import { Popover } from '@headlessui/react';
import { Link } from '@inertiajs/inertia-react';
import useRoute from '@/Hooks/useRoute';
import useTypedPage from '@/Hooks/useTypedPage';
import useInitial from '@/Hooks/useInitial';
import MainNavigation from '@/Shared/Components/Navigation/MainNavigation';
import LanguageSelector from '@/Shared/Components/LanguageSelector/LanguageSelector';
import { useTranslation } from 'react-i18next';

interface IDesktopHeader {
  open: boolean;
}

const DesktopHeader = ({ open }: IDesktopHeader) => {
  const { t } = useTranslation();
  t('general.Logout');
  const route = useRoute();
  const { props } = useTypedPage();
  const loggedInUserName = props?.user?.loggedInUserName;
  // create an initial from the logged-in user name
  useInitial(loggedInUserName);
  return (
    <div className="mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8">
      <div className="relative flex flex-wrap items-center justify-center lg:justify-between">
        {/* Logo */}
        <div className="absolute left-0 hidden flex-shrink-0 py-5 md:block lg:static">
          <Link href={route('dashboard')}>
            <ApplicationLogo />
          </Link>
        </div>

        {/* Right section on desktop */}
        <div className="hidden lg:ml-4 lg:flex lg:items-center lg:py-5 lg:pr-0.5">
          <button
            type="button"
            className="flex-shrink-0 rounded-full p-1 text-purple-200 hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
          >
            <span className="sr-only">View notifications</span>
            <BellIcon className="h-6 w-6" aria-hidden="true" />
          </button>
          {/* Profile dropdown */}
          <div className="relative ml-4 flex-shrink-0">
            <Link
              method="post"
              href={route('logout')}
              className="inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-purple-100"
            >
              {t('general.Logout')}
            </Link>
          </div>
        </div>

        <div className="w-full py-5 lg:border-t lg:border-white lg:border-opacity-20">
          <div className="lg:grid lg:grid-cols-3 lg:items-center lg:gap-8">
            {/* Left nav */}
            <div className="hidden lg:col-span-2 lg:block">
              {/* Navigation */}
              <MainNavigation />
            </div>
            <div className="px-12 lg:px-0">
              {/* language selector */}
              <div className="mx-auto hidden justify-end sm:flex">
                <div className="w-1/2">
                  <LanguageSelector />
                </div>
              </div>
            </div>
          </div>
        </div>

        {/* Menu button */}
        <div className="absolute right-0 mt-4 flex flex-shrink-0 gap-3 lg:hidden">
          {/* Mobile menu button */}
          <LanguageSelector />
          <Popover.Button className="inline-flex items-center justify-center rounded-md bg-transparent p-2 text-purple-200 hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none focus:ring-2 focus:ring-white">
            <span className="sr-only">Open main menu</span>
            {open ? (
              <XIcon className="block h-6 w-6" aria-hidden="true" />
            ) : (
              <MenuIcon className="block h-6 w-6" aria-hidden="true" />
            )}
          </Popover.Button>
        </div>
      </div>
    </div>
  );
};

export default DesktopHeader;

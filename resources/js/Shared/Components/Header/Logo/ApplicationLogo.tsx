import React from 'react';

const ApplicationLogo = () => {
  return (
    <strong className="bg-gradient-to-r from-fuchsia-200 to-purple-200 bg-clip-text pb-3 text-4xl font-bold text-transparent">
      JobAdssist
    </strong>
  );
};

export default ApplicationLogo;

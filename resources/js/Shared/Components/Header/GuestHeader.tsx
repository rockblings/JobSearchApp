import { Popover, Transition } from '@headlessui/react';
import ApplicationLogo from './Logo/ApplicationLogo';
import { MenuIcon, XIcon } from '@heroicons/react/outline';
import { Link, usePage } from '@inertiajs/inertia-react';
import * as React from 'react';
import { Fragment } from 'react';
import route from 'ziggy-js';
import LanguageSelector from '@/Shared/Components/LanguageSelector/LanguageSelector';
import { Helmet } from 'react-helmet';
import { useTranslation } from 'react-i18next';

const GuestHeader: React.FC = () => {
  const { component } = usePage();

  const { t } = useTranslation();
  return (
    <>
      <Helmet>
        <title>JobAdssist</title>
      </Helmet>
      <header>
        <Popover className="relative bg-gradient-to-r from-fuchsia-800 to-purple-600">
          <div className="mx-auto flex items-center justify-between px-4 py-2 sm:px-6 md:justify-start md:space-x-10">
            <Link href={route('welcome')}>
              <ApplicationLogo />
            </Link>

            <>
              <div className="-my-2 -mr-2 md:hidden">
                <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500">
                  <span className="sr-only">Open menu</span>
                  <MenuIcon className="h-6 w-6" aria-hidden="true" />
                </Popover.Button>
              </div>

              <div className="hidden items-center justify-end md:flex md:flex-1 lg:w-0">
                <LanguageSelector />
                <div className="ml-3 space-x-4">
                  {!component.startsWith('Auth') && (
                    <Link
                      href={route('login')}
                      className="inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-fuchsia-50"
                    >
                      {t('login.Form.Log.In')}
                    </Link>
                  )}
                  {!component.startsWith('Auth/Register') &&
                    !component.startsWith('Welcome') && (
                      <Link
                        href={route('register')}
                        className="inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-fuchsia-50"
                      >
                        {t('register.Create.Account.Button')}
                      </Link>
                    )}
                </div>
              </div>
            </>
          </div>

          <Transition
            as={Fragment}
            enter="duration-200 ease-out"
            enterFrom="opacity-0 scale-95"
            enterTo="opacity-100 scale-100"
            leave="duration-100 ease-in"
            leaveFrom="opacity-100 scale-100"
            leaveTo="opacity-0 scale-95"
          >
            {!component.startsWith('Auth') && (
              <Popover.Panel
                focus
                className="absolute inset-x-0 top-0 z-30 origin-top-right transform p-2 transition md:hidden"
              >
                <div className="divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5">
                  <div className="px-5 pt-5 pb-6">
                    <div className="flex items-center justify-between">
                      <div />
                      <div className="-mr-2">
                        <Popover.Button className="inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500">
                          <span className="sr-only">Close menu</span>
                          <XIcon className="h-6 w-6" aria-hidden="true" />
                        </Popover.Button>
                      </div>
                    </div>
                  </div>
                  <div className="py-6 px-5">
                    <div className="mt-2">
                      <Link
                        href={route('login')}
                        className="inline-flex w-full items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-fuchsia-700 px-4 py-2 text-base font-medium text-white shadow-sm shadow-lg shadow-fuchsia-800/50 hover:bg-fuchsia-400 hover:shadow-fuchsia-500/50 sm:w-auto"
                      >
                        {t('login.Form.Log.In')}
                      </Link>
                      <Link
                        href="#"
                        className="inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-600 shadow-sm hover:bg-fuchsia-50"
                      >
                        Register
                      </Link>
                    </div>
                  </div>
                </div>
              </Popover.Panel>
            )}
          </Transition>
        </Popover>
      </header>
    </>
  );
};

export default GuestHeader;

import React from 'react';
import i18n from 'i18next';
import { availableLanguages } from '@/i18n/i18n';
import { IAvailableLanguages } from '@/types';

const LanguageSelector = () => {
  return (
    <div>
      <label
        htmlFor="languages"
        className="sr-only block text-sm font-medium text-gray-700"
      >
        Language Selector
      </label>
      <select
        id="languages"
        name="languages"
        className="mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm"
        defaultValue={i18n.language}
        onChange={e => i18n.changeLanguage(e.target.value)}
      >
        {Object.keys(availableLanguages).map(language => (
          <option key={language} value={language}>
            {
              availableLanguages[language as keyof IAvailableLanguages]
                .nativeName
            }
          </option>
        ))}
      </select>
    </div>
  );
};

export default LanguageSelector;

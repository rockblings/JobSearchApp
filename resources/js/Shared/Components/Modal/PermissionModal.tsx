import { Dialog, Transition } from '@headlessui/react';
import React, { ChangeEvent, FormEventHandler, Fragment } from 'react';
import { setDataByKeyValuePair } from '@inertiajs/inertia-react';

type pageInfoType = {
  title: string;
  description: string;
  btnName: string;
};

interface IModalData {
  data: {
    password: string;
  };
  setData: setDataByKeyValuePair<{ password: string }>;
  isActive: boolean;
  handleActive: Function;
  pageInfo: pageInfoType;
  processing: boolean | undefined;
  handleSubmit: FormEventHandler<HTMLFormElement> | undefined;
  error: boolean;
}

const PermissionModal = ({
  data,
  setData,
  isActive,
  handleActive,
  pageInfo,
  processing,
  handleSubmit,
  error,
}: IModalData) => {
  return (
    <Transition.Root show={isActive} as={Fragment}>
      <Dialog
        as="div"
        className="fixed inset-0 z-10 overflow-y-auto"
        onClose={handleActive(false)}
      >
        <div className="flex min-h-screen items-end justify-center px-4 pt-4 pb-20 text-center sm:block sm:p-0">
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
          </Transition.Child>

          {/* This element is to trick the browser into centering the modal contents. */}
          <span
            className="hidden sm:inline-block sm:h-screen sm:align-middle"
            aria-hidden="true"
          >
            &#8203;
          </span>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
            enterTo="opacity-100 translate-y-0 sm:scale-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100 translate-y-0 sm:scale-100"
            leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
          >
            <form
              onSubmit={handleSubmit}
              className="inline-block transform overflow-hidden rounded-lg bg-white text-left align-bottom shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg sm:align-middle"
            >
              <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                <div className="sm:flex sm:items-start">
                  <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                    <Dialog.Title
                      as="h3"
                      className="text-lg font-medium leading-6 text-violet-900"
                    >
                      {pageInfo.title}
                    </Dialog.Title>
                    <div className="mt-2">
                      <p className="text-sm text-gray-500">
                        {pageInfo.description}
                      </p>
                    </div>
                  </div>
                </div>
                <div className="text mt-3 pl-4 text-center">
                  <label htmlFor="password" className="sr-only">
                    Password
                  </label>
                  <input
                    type="password"
                    name="password"
                    id="password"
                    className="block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-500 focus:ring-gray-500 sm:text-sm"
                    placeholder="Password"
                    value={data.password}
                    onChange={(e: ChangeEvent<HTMLInputElement>) =>
                      setData('password', e.currentTarget.value)
                    }
                    required
                  />
                  {error && (
                    <p
                      className="mt-2 text-sm text-red-600"
                      id="password-error"
                    >
                      The provided password was incorrect.
                    </p>
                  )}
                </div>
              </div>
              <div className="bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6">
                <button
                  type="submit"
                  className={`inline-flex w-full justify-center rounded-md border border-transparent px-4 py-2 text-base font-medium text-white shadow-sm focus:outline-none sm:ml-3 sm:w-auto sm:text-sm ${
                    pageInfo.btnName.startsWith('Delete')
                      ? ' bg-red-600 hover:bg-red-300 focus:ring-red-500'
                      : ' bg-violet-600 hover:bg-violet-700  focus:ring-gray-50'
                  }`}
                  disabled={processing}
                >
                  {pageInfo.btnName}
                </button>
                <button
                  type="button"
                  className="mt-3 inline-flex w-full justify-center rounded-md border border-gray-300 bg-white bg-slate-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-slate-700 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                  onClick={handleActive(false)}
                >
                  Cancel
                </button>
              </div>
            </form>
          </Transition.Child>
        </div>
      </Dialog>
    </Transition.Root>
  );
};

export default PermissionModal;

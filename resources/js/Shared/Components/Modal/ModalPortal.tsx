import ReactDOM from 'react-dom';
import React from 'react';

interface IConfirmationModal {
  children: React.ReactNode;
}

const ModalPortal = ({ children }: IConfirmationModal) => {
  return ReactDOM.createPortal(children, document.body);
};
export default ModalPortal;

import React, { Fragment } from 'react';
import { Transition } from '@headlessui/react';
import {
  CheckCircleIcon,
  ExclamationCircleIcon,
} from '@heroicons/react/outline';
import useTimeout from '@/Hooks/useTimeout';

interface Props {
  show: boolean;
  title: string;
  body?: string;
  messageType?: string;
  closeNotification: () => void;
}

const Notification = ({
  show,
  title,
  body,
  closeNotification,
  messageType,
}: Props) => {
  const [setTimeout, clearTimeout] = useTimeout();
  // remove the notification after 3 seconds
  setTimeout(() => {
    closeNotification();
    clearTimeout();
  }, 2000);

  return (
    <>
      {/* Global notification live region, render this permanently at the end of the document */}
      <div
        aria-live="assertive"
        className="pointer-events-none fixed inset-0 flex items-end px-4 py-6 sm:items-start sm:p-6"
      >
        <div className="flex w-full flex-col items-end space-y-6">
          {/* Notification panel, dynamically insert this into the live region when it needs to be displayed */}
          <Transition
            show={show}
            as={Fragment}
            enter="transform ease-out duration-300 transition"
            enterFrom="translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2"
            enterTo="translate-y-0 opacity-100 sm:translate-x-0"
            leave="transition ease-in duration-100"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="pointer-events-auto w-full max-w-sm overflow-hidden rounded-lg bg-gradient-to-r from-purple-800 to-fuchsia-600 shadow-lg ring-1 ring-black ring-opacity-5">
              <div className="p-4">
                <div className="flex items-start">
                  <div className="flex-shrink-0">
                    {messageType === 'error' ? (
                      <ExclamationCircleIcon
                        className="h-6 w-6 text-rose-400"
                        aria-hidden="true"
                      />
                    ) : (
                      <CheckCircleIcon
                        className="h-6 w-6 text-lime-400"
                        aria-hidden="true"
                      />
                    )}
                  </div>
                  <div className="ml-3 w-0 flex-1 pt-0.5">
                    <p className="text-sm font-medium text-fuchsia-50">
                      {title}
                    </p>
                    {body && (
                      <p className="mt-1 text-sm text-fuchsia-50">{body}</p>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </Transition>
        </div>
      </div>
    </>
  );
};

export default Notification;

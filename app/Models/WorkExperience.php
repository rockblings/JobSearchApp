<?php

namespace App\Models;

use Database\Factories\WorkExperienceFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Shared\HasUuid;

/**
 * App\Models\WorkExperience
 *
 * @property-read User $user
 * @method static WorkExperienceFactory factory(...$parameters)
 * @method static Builder|WorkExperience newModelQuery()
 * @method static Builder|WorkExperience newQuery()
 * @method static Builder|WorkExperience query()
 * @mixin Eloquent
 * @property int $id
 * @property string $uuid
 * @property string $job_title
 * @property string $job_description
 * @property string $company
 * @property string|null $company_email
 * @property string|null $company_telephone
 * @property string|null $company_address
 * @property string|null $company_website
 * @property Carbon $started_at
 * @property Carbon|null $finished_at
 * @property bool $current
 * @property int $user_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|WorkExperience whereCompany($value)
 * @method static Builder|WorkExperience whereCompanyAddress($value)
 * @method static Builder|WorkExperience whereCompanyEmail($value)
 * @method static Builder|WorkExperience whereCompanyTelephone($value)
 * @method static Builder|WorkExperience whereCompanyWebsite($value)
 * @method static Builder|WorkExperience whereCreatedAt($value)
 * @method static Builder|WorkExperience whereCurrent($value)
 * @method static Builder|WorkExperience whereFinishedAt($value)
 * @method static Builder|WorkExperience whereId($value)
 * @method static Builder|WorkExperience whereJobDescription($value)
 * @method static Builder|WorkExperience whereJobTitle($value)
 * @method static Builder|WorkExperience whereStartedAt($value)
 * @method static Builder|WorkExperience whereUpdatedAt($value)
 * @method static Builder|WorkExperience whereUserId($value)
 * @method static Builder|WorkExperience whereUuid($value)
 */
class WorkExperience extends CustomBaseModel
{
  use HasUuid;
  use HasFactory;

  protected $guarded = ['id'];

  protected $casts = [
    'current' => 'boolean',
  ];

  /**
   * @return BelongsTo<User, WorkExperience>
   */
  public function user(): BelongsTo
  {
    return $this->belongsTo(User::class, 'user_id');
  }
}

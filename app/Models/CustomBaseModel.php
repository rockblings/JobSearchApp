<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\CustomBaseModel
 *
 * @property string $uuid
 * @method static \Illuminate\Database\Eloquent\Builder|CustomBaseModel newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomBaseModel newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CustomBaseModel query()
 * @mixin \Eloquent
 */
class CustomBaseModel extends Model
{
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ResumeManager
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\ResumeManagerFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager query()
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ResumeManager whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class ResumeManager extends Model
{
    use HasFactory;
}

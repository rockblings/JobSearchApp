<?php

namespace App\Models;

use Domains\Profile\QueryBuilders\ProfileQueryBuilder;
use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Shared\HasUuid;

/**
 * App\Models\Profile
 *
 * @property int $id
 * @property string $uuid
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $user_id
 * @property string $description
 * @property string $language
 * @property string $telephone
 * @property string $country
 * @property string|null $profile_photo_path
 * @property-read User $user
 * @method static \Database\Factories\ProfileFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereProfilePhotoPath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereTelephone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereUserId($value)
 * @mixin Eloquent
 * @mixin Profile
 * @method static ProfileQueryBuilder|Profile whereUuid($value)
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WorkExperience[] $experiences
 * @property-read int|null $experiences_count
 */
class Profile extends CustomBaseModel
{
  use HasUuid;
  use HasFactory;

  protected $fillable = [
    'uuid',
    'user_id',
    'description',
    'language',
    'telephone',
    'country',
    'profile_photo_path',
  ];

  protected $casts = [
    'country' => 'integer',
    'language' => 'integer',
  ];

  /**
   * @param Builder $query
   *
   * @return ProfileQueryBuilder<Profile>
   */
  public function newEloquentBuilder($query)
  {
    return new ProfileQueryBuilder($query);
  }

  /**
   * @return BelongsTo<User, Profile>
   */
  public function user(): BelongsTo
  {
    return $this->belongsTo(User::class, 'user_id');
  }
}

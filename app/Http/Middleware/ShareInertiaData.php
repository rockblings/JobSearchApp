<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Inertia\Inertia;
use Laravel\Fortify\Features;
use Laravel\Jetstream\Jetstream;

class ShareInertiaData
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param callable $next
     * @return Response
     */
    public function handle($request, $next)
    {
        Inertia::share(
            array_filter([
        'jetstream' => function () use ($request) {
            if (!$request->user()) {
                return [
              'hasTermsAndPrivacyPolicyFeature' => Jetstream::hasTermsAndPrivacyPolicyFeature()
            ];
            }
            return [
            'hasTermsAndPrivacyPolicyFeature' => Jetstream::hasTermsAndPrivacyPolicyFeature() &&
              //Jetstream::hasTeamFeatures() &&
              Gate::forUser($request->user())->check('create', Jetstream::newTeamModel()),
            'canManageTwoFactorAuthentication' => Features::canManageTwoFactorAuthentication(),
            'canUpdatePassword' => Features::enabled(Features::updatePasswords()),
            'canUpdateProfileInformation' => Features::canUpdateProfileInformation(),
            'hasAccountDeletionFeatures' => Jetstream::hasAccountDeletionFeatures()
          ];
        },
        'user' => function () use ($request) {
            if (!$request->user()) {
                return;
            }

            return [
            'loggedInUserName' => $request->user()->name,
            'loggedInUserEmail' => $request->user()->email,
            'twoFactorEnabled' => !is_null($request->user()->two_factor_secret)
          ];
        },
        'errorBags' => function () {
            return collect(optional(Session::get('errors'))->getBags() ?: [])->mapWithKeys(fn ($bag, $key) => [$key => $bag->messages()])->all(
          );
        },
      ])
        );
        return $next($request);
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'description' => 'nullable|string|max:1000|min:10',
            'language' => 'nullable|integer',
            'phoneNumber' => 'nullable|string',
            'country' => 'nullable|integer',
            'photo' => 'nullable|file|image|mimes:jpeg,png,jpg|max:2048',
        ];
    }
}

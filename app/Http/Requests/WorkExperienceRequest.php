<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkExperienceRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules(): array
  {
    return [
      'title' => 'string|min:2',
      'description' => 'string|min:2',
      'name' => 'string|min:2',
      'email' => 'nullable|email',
      'telephone' => 'nullable|string|min:4',
      'address' => 'nullable|string|min:4',
      'website' => 'nullable|string|url|min:4',
      'startDate' => 'date',
      'endDate' => 'nullable|date|after:startDate',
    ];
  }
}

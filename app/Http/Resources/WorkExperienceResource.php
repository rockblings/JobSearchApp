<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Carbon;

/**
 * App\Http\Resources\WorkExperienceResource
 *
 * @property string $uuid
 * @property string|null $job_title
 * @property string|null $job_description
 * @property string|null $company
 * @property string|null $company_email
 * @property string|null $company_address
 * @property string|null $company_telephone
 * @property string|null $company_website
 * @property Carbon|null $started_at
 * @property Carbon|null $finished_at
 * @property boolean $current
 */
class WorkExperienceResource extends JsonResource
{
  /**
   * Transform the resource into an array.
   *
   * @param Request $request
   * @return array
   */
  public function toArray($request): array
  {
    return [
      'xp' => $this->uuid,
      'title' => $this->job_title,
      'description' => $this->job_description,
      'name' => $this->company,
      'email' => $this->company_email,
      'telephone' => $this->company_telephone,
      'address' => $this->company_address ?? '',
      'website' => $this->company_website,
      'startDate' => $this->started_at,
      'endDate' => $this->finished_at,
      'status' => $this->current,
    ];
  }
}

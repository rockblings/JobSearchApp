<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

/**
 * App\Http\Resources\ProfileResource
 *
 * @property string|null $description
 * @property string|null $language
 * @property string|null $telephone
 * @property int|null $country
 * @property string|null $profile_photo_path
 */
class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'description' => $this->description,
            'language' => $this->language,
            'phoneNumber' => $this->telephone,
            'country' => $this->country,
            'photoUrl' => $this->profile_photo_path,
        ];
    }
}

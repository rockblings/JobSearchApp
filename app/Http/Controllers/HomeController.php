<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Contracts\Profile\GetsProfile;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class HomeController extends Controller
{
  /**
   *
   * @param GetsProfile $getsProfile
   * @return Response
   */
  final public function index(GetsProfile $getsProfile): Response
  {
    /** @var User $user */
    $user = Auth::user();
    return Inertia::render('Home/Index', [
      'selectedPage' => 'Home',
      'pageData' => [
        'imageUrl' => $getsProfile($user)['profile_photo_path'] ?? '',
      ],
    ]);
  }
}

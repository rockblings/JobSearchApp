<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\User;
use Domains\Settings\DTO\SessionData;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Inertia\Inertia;
use Inertia\Response;
use Jenssegers\Agent\Agent;
use stdClass;

class UserSettingsController extends Controller
{
  /**
   * Show the general profile settings screen.
   *
   * @param Request $request
   *
   * @return Response
   */
  final public function show(Request $request): Response
  {
    return Inertia::render('Setting/Show', [
      'sessions' => $this->sessions($request)->all(),
      'selectedPage' => 'Settings',
    ]);
  }

  /**
   * Get the current sessions.
   *
   * @param Request $request
   *
   * @return Collection<int, stdClass>
   */
  final public function sessions(Request $request): Collection
  {
    if (config('session.driver') !== 'database' || !$request->user()) {
      return new Collection();
    }
    /** @var string|Builder $sessionTable */
    $sessionTable = config('session.table', 'sessions');
    /** @var string|null $connection */
    $connection = config('session.connection');
    /** @var User $user */
    $user = $request->user();
    return DB::connection($connection)
      ->table($sessionTable)
      ->where('user_id', $user->getAuthIdentifier())
      ->orderBy('last_activity', 'desc')
      ->get()->map(function ($session) use ($request) {
        /** @var SessionData|stdClass $session */
        $agent = $this->createAgent($session);
        return (object)[
          'agent' => [
            'is_desktop' => $agent->isDesktop(),
            'platform' => $agent->platform(),
            'browser' => $agent->browser(),
          ],
          'ip_address' => $session->ip_address,
          'is_current_device' => $session->id === $request->session()->getId(),
          'last_active' => Carbon::createFromTimestamp($session->last_activity)->diffForHumans(),
        ];
      });
  }

  /**
   * Create a new agent instance from the given session.
   *
   * @param SessionData|stdClass $session
   *
   * @return Agent
   */
  protected function createAgent(SessionData|stdClass $session): Agent
  {
    return tap(new Agent(), static function ($agent) use ($session) {
      $agent->setUserAgent($session->user_agent);
    });
  }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\WorkExperienceRequest;
use App\Models\User;
use Contracts\ResumeManager\WorkExperience\GetsWorkExperience;
use Contracts\ResumeManager\WorkExperience\SavesWorkExperience;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class WorkExperienceController extends Controller
{
  /**
   * @param GetsWorkExperience $getsWorkExperience
   * @return Response
   */
  final public function index(GetsWorkExperience $getsWorkExperience): Response
  {
    /** @var User $user */
    $user = Auth::user();
    return Inertia::render('ResumeManager/WorkExperiences/WorkExperience', [
      'selectedPage' => 'Resume Manager',
      'pageData' => [
        'workExperiences' => $getsWorkExperience($user),
      ],
    ]);
  }

  /**
   * @param WorkExperienceRequest $request
   * @param SavesWorkExperience $savesWorkExperience
   * @return RedirectResponse
   */
  final public function store(WorkExperienceRequest $request, SavesWorkExperience $savesWorkExperience): RedirectResponse
  {
    $savesWorkExperience($request);
    return to_route('work.experience.index')->with('success', 'Work experiences was successfully updated.');
  }
}

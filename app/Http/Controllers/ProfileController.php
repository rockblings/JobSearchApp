<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Contracts\Countries\GetsCountryList;
use Contracts\Languages\GetsLanguageList;
use Contracts\Profile\GetsProfile;
use Contracts\Profile\SavesProfile;
use Contracts\Profile\SavesProfilePhoto;
use Contracts\Profile\UpdatesProfile;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;
use Inertia\Response;

class ProfileController extends Controller
{
  /**
   * Show authenticated user profile.
   *
   * @param GetsProfile $getsProfile
   * @param GetsCountryList $getsCountryList
   * @param GetsLanguageList $getsLanguageList
   * @return Response
   */
  final public function show(GetsProfile $getsProfile, GetsCountryList $getsCountryList, GetsLanguageList $getsLanguageList): Response
  {
    /** @var User $user */
    $user = Auth::user();
    return Inertia::render('Profile/Show', [
      'selectedPage' => 'Profile',
      'pageData' => [
        'profile' => $getsProfile($user),
        'countries' => $getsCountryList(),
        'languages' => $getsLanguageList(),
      ]
    ]);
  }

  /**
   * Store a newly created profile in the database.
   *
   * @param ProfileRequest $request
   * @param SavesProfile $savesProfile
   * @param SavesProfilePhoto $savesProfilePhoto
   * @param UpdatesProfile $updatesProfile
   * @return RedirectResponse
   *
   */
  final public function store(
    ProfileRequest $request,
    SavesProfile $savesProfile,
    SavesProfilePhoto $savesProfilePhoto,
    UpdatesProfile $updatesProfile
  ): RedirectResponse {
    /** @var User $user */
    $user = $request->user();
    // check if profile exist
    if ($user->profile) {
      $updatesProfile($request, $savesProfilePhoto);
      return to_route('profile.show')->with('success', 'Profile was successfully updated.');
    }
    $savesProfile($request, $savesProfilePhoto);
    return to_route('profile.show')->with('success', 'Profile was successfully created.');
  }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Inertia\Inertia;
use Inertia\Response;

class ResumeManagerController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  final public function index(): Response
  {
    return Inertia::render('ResumeManager/Index', [
      'selectedPage' => 'Resume Manager',
    ]);
  }
}

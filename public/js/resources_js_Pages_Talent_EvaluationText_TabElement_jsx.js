"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Talent_EvaluationText_TabElement_jsx"],
    {
        /***/ "./resources/js/Pages/Talent/EvaluationText/TabElement.jsx":
            /*!*****************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx ***!
  \*****************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                var TabElement = [
                    {
                        name: "Ball Text",
                        href: "balltext.index",
                        current: true,
                    },
                    {
                        name: "Point Text",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Work Style",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Reference Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Control Point",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Questions",
                        href: "balltext.index",
                        current: false,
                    },
                ];
                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TabElement;

                /***/
            },
    },
]);

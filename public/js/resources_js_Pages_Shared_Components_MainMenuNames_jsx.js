"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Components_MainMenuNames_jsx"],{

/***/ "./resources/js/Pages/Shared/Components/MainMenuNames.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/MainMenuNames.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TALENT_MENU": () => (/* binding */ TALENT_MENU),
/* harmony export */   "CAREER_MENU": () => (/* binding */ CAREER_MENU),
/* harmony export */   "LOGIQ_MENU": () => (/* binding */ LOGIQ_MENU),
/* harmony export */   "SCREEN_MENU": () => (/* binding */ SCREEN_MENU),
/* harmony export */   "FIVE_MENU": () => (/* binding */ FIVE_MENU),
/* harmony export */   "SETTINGS_MENU": () => (/* binding */ SETTINGS_MENU)
/* harmony export */ });
var TALENT_MENU = 'Talent';
var CAREER_MENU = 'Career';
var LOGIQ_MENU = 'Logiq';
var SCREEN_MENU = 'Screen';
var FIVE_MENU = 'Five';
var SETTINGS_MENU = 'Settings';

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_TabNav_ResumeTabs_jsx"],{

/***/ "./resources/js/Pages/Shared/TabNav/ResumeTabs.jsx":
/*!*********************************************************!*\
  !*** ./resources/js/Pages/Shared/TabNav/ResumeTabs.jsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Resume/Tabs/MobileResumeTabs'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Resume/Tabs/DesktopResumeTabs'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");







var GENERAL_URL = 'dashboard';
var WORK_EXPERIENCE_URL = 'work-experience';
var EDUCATION_QUALIFICATION_URL = 'education-qualification';
var SKILLS_URL = 'skills';
var REFERENCES_URL = 'references';
var CERTIFICATES_URL = 'certificates';
var tabs = [{
  name: 'General',
  href: GENERAL_URL,
  current: true
}, {
  name: 'Work Experience',
  href: WORK_EXPERIENCE_URL,
  current: false
}, {
  name: 'Education Qualification',
  href: EDUCATION_QUALIFICATION_URL,
  current: false
}, {
  name: 'Skills',
  href: SKILLS_URL,
  current: false
}, {
  name: 'References',
  href: REFERENCES_URL,
  current: false
}, {
  name: 'Certificates & Awards',
  href: CERTIFICATES_URL,
  current: false
}];

var ResumeTabs = function ResumeTabs() {
  tabs.find(function (tab) {
    switch (tab.href) {
      case WORK_EXPERIENCE_URL:
      case EDUCATION_QUALIFICATION_URL:
      case SKILLS_URL:
      case REFERENCES_URL:
      case CERTIFICATES_URL:
        tab.current = !!route().current(tab.href + '*');
        break;

      case GENERAL_URL:
        tab.current = !!route().current(tab.href);
        break;

      default:
        tab.current = false;
    }
  });
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
      className: "lg:hidden",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
        htmlFor: "selected-tab",
        className: "sr-only",
        children: "Select a tab"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Resume/Tabs/MobileResumeTabs'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
        tabs: tabs
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
      className: "hidden lg:block",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Resume/Tabs/DesktopResumeTabs'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
        tabs: tabs
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ResumeTabs);

/***/ })

}]);
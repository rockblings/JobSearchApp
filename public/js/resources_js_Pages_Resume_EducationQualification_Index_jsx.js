"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Resume_EducationQualification_Index_jsx"],
    {
        /***/ "./resources/js/Pages/Resume/EducationQualification/Index.jsx":
            /*!********************************************************************!*\
  !*** ./resources/js/Pages/Resume/EducationQualification/Index.jsx ***!
  \********************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_Layout__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components */ "./resources/js/Pages/Shared/Layout.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Dashboard__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Dashboard */ "./resources/js/Pages/Shared/Dashboard.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var Index = function Index() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                        "form",
                        {
                            className: "space-y-8 divide-y divide-gray-200",
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "space-y-8 shadow overflow-hidden sm:rounded-lg mt-6",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                "div",
                                                {
                                                    className:
                                                        "px-4 py-5 sm:px-6 bg-gray-50",
                                                    children: [
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                            "h3",
                                                            {
                                                                className:
                                                                    "text-lg leading-6 font-medium text-fuchsia-700",
                                                                children:
                                                                    "Education Qualification Information",
                                                            }
                                                        ),
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                            "p",
                                                            {
                                                                className:
                                                                    "mt-1 max-w-2xl text-sm text-gray-500",
                                                                children:
                                                                    "Previous work experiences.",
                                                            }
                                                        ),
                                                    ],
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "border-t border-gray-200 px-4 py-5 sm:px-6",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                        "dl",
                                                        {
                                                            className:
                                                                "grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "School Name",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "J\xF6nk\xF6ping Internation Business School, J\xF6nk\xF6ping University",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Field of Study",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Business Management & Logistics",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-2",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Degree",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Master Degrees in Business Management",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "From Date",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "2009-08",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "To Date",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "2011-07",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "City",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "J\xF6nk\xF6ping",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Country",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Sweden",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                            ],
                                                        }
                                                    ),
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "border-t border-gray-200 px-4 py-5 sm:px-6",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                        "dl",
                                                        {
                                                            className:
                                                                "grid grid-cols-1 gap-x-4 gap-y-8 sm:grid-cols-2",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "School Name",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "University of Wollongong",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Field of Study",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Computer Science",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-2",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Degree",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Bachelor in Computer Science",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "From Date",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "2004-01",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "To Date",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "2007-08",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "City",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Wollongong",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "sm:col-span-1",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dt",
                                                                                    {
                                                                                        className:
                                                                                            "text-sm font-medium text-gray-500",
                                                                                        children:
                                                                                            "Country",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                    "dd",
                                                                                    {
                                                                                        className:
                                                                                            "mt-1 text-sm text-gray-900",
                                                                                        children:
                                                                                            "Australia",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                            ],
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                    "div",
                                    {
                                        className: "pt-5",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                            "div",
                                            {
                                                className: "flex justify-end",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                    _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link,
                                                    {
                                                        as: "button",
                                                        href: route(
                                                            "education-qualification-create"
                                                        ),
                                                        type: "button",
                                                        className:
                                                            "ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-fuchsia-700 hover:bg-fuchsia-400 focus:outline-none",
                                                        children: "Add New",
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                Index.layout = function (page) {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                        _Pages_Shared_Layout__WEBPACK_IMPORTED_MODULE_0__[
                            "default"
                        ],
                        {
                            title: "Resume",
                            selectedSideBarMenu: "Resume",
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                _Pages_Shared_Dashboard__WEBPACK_IMPORTED_MODULE_1__[
                                    "default"
                                ],
                                {
                                    children: page,
                                    pageTitle: "Education Qualifications",
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Index;

                /***/
            },

        /***/ "./resources/js/Pages/Resume/Tabs/DesktopResumeTabs.jsx":
            /*!**************************************************************!*\
  !*** ./resources/js/Pages/Resume/Tabs/DesktopViewTabs.jsx ***!
  \**************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var DesktopResumeTabs = function DesktopResumeTabs(_ref) {
                    var tabs = _ref.tabs;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
                        className: "border-b border-gray-200",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                            "nav",
                            {
                                className: "-mb-px flex space-x-8",
                                children: tabs.map(function (tab) {
                                    return /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.Link,
                                        {
                                            href: route(tab.href),
                                            className: classNames(
                                                tab.current
                                                    ? "border-fuchsia-500 text-fuchsia-600"
                                                    : "border-transparent text-gray-500 hover:border-gray-300 hover:text-gray-700",
                                                "whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm"
                                            ),
                                            children: tab.name,
                                        },
                                        tab.name
                                    );
                                }),
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    DesktopResumeTabs;

                /***/
            },

        /***/ "./resources/js/Pages/Resume/Tabs/MobileResumeTabs.jsx":
            /*!*************************************************************!*\
  !*** ./resources/js/Pages/Resume/Tabs/MobileViewTabs.jsx ***!
  \*************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/listbox/listbox.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js"
                    );
                /* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );
                function _slicedToArray(arr, i) {
                    return (
                        _arrayWithHoles(arr) ||
                        _iterableToArrayLimit(arr, i) ||
                        _unsupportedIterableToArray(arr, i) ||
                        _nonIterableRest()
                    );
                }

                function _nonIterableRest() {
                    throw new TypeError(
                        "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                    );
                }

                function _unsupportedIterableToArray(o, minLen) {
                    if (!o) return;
                    if (typeof o === "string")
                        return _arrayLikeToArray(o, minLen);
                    var n = Object.prototype.toString.call(o).slice(8, -1);
                    if (n === "Object" && o.constructor) n = o.constructor.name;
                    if (n === "Map" || n === "Set") return Array.from(o);
                    if (
                        n === "Arguments" ||
                        /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                    )
                        return _arrayLikeToArray(o, minLen);
                }

                function _arrayLikeToArray(arr, len) {
                    if (len == null || len > arr.length) len = arr.length;
                    for (var i = 0, arr2 = new Array(len); i < len; i++) {
                        arr2[i] = arr[i];
                    }
                    return arr2;
                }

                function _iterableToArrayLimit(arr, i) {
                    var _i =
                        arr == null
                            ? null
                            : (typeof Symbol !== "undefined" &&
                                  arr[Symbol.iterator]) ||
                              arr["@@iterator"];
                    if (_i == null) return;
                    var _arr = [];
                    var _n = true;
                    var _d = false;
                    var _s, _e;
                    try {
                        for (
                            _i = _i.call(arr);
                            !(_n = (_s = _i.next()).done);
                            _n = true
                        ) {
                            _arr.push(_s.value);
                            if (i && _arr.length === i) break;
                        }
                    } catch (err) {
                        _d = true;
                        _e = err;
                    } finally {
                        try {
                            if (!_n && _i["return"] != null) _i["return"]();
                        } finally {
                            if (_d) throw _e;
                        }
                    }
                    return _arr;
                }

                function _arrayWithHoles(arr) {
                    if (Array.isArray(arr)) return arr;
                }

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var MobileResumeTabs = function MobileResumeTabs(_ref) {
                    var tabs = _ref.tabs;
                    var defaultSelectedTab = tabs.find(function (tab) {
                        return tab.current;
                    });

                    var _useState = (0,
                        react__WEBPACK_IMPORTED_MODULE_0__.useState)(
                            defaultSelectedTab
                        ),
                        _useState2 = _slicedToArray(_useState, 2),
                        selected = _useState2[0],
                        setSelected = _useState2[1];

                    var handleTabDropDown = function handleTabDropDown(
                        selected
                    ) {
                        setSelected(selected);
                        _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_2__.Inertia.get(
                            route(selected.href)
                        );
                    };

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                        _headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Listbox,
                        {
                            value: selected,
                            onChange: function onChange(selected) {
                                return handleTabDropDown(selected);
                            },
                            children: function children(_ref2) {
                                var open = _ref2.open;
                                return /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                                    {
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                            "div",
                                            {
                                                className: "mt-1 relative",
                                                children: [
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                        _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                            .Listbox.Button,
                                                        {
                                                            className:
                                                                "bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-fuchsia-500 focus:border-fuchsia-500 sm:text-sm",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "block truncate",
                                                                        children:
                                                                            selected.name,
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none",
                                                                        children:
                                                                            /*#__PURE__*/ (0,
                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.SelectorIcon,
                                                                                {
                                                                                    className:
                                                                                        "h-5 w-5 text-gray-400",
                                                                                    "aria-hidden":
                                                                                        "true",
                                                                                }
                                                                            ),
                                                                    }
                                                                ),
                                                            ],
                                                        }
                                                    ),
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        _headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition,
                                                        {
                                                            show: open,
                                                            as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                            leave: "transition ease-in duration-100",
                                                            leaveFrom:
                                                                "opacity-100",
                                                            leaveTo:
                                                                "opacity-0",
                                                            children:
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                        .Listbox
                                                                        .Options,
                                                                    {
                                                                        className:
                                                                            "absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm",
                                                                        children:
                                                                            tabs.map(
                                                                                function (
                                                                                    tab
                                                                                ) {
                                                                                    return /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                        _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                                            .Listbox
                                                                                            .Option,
                                                                                        {
                                                                                            className:
                                                                                                function className(
                                                                                                    _ref3
                                                                                                ) {
                                                                                                    var active =
                                                                                                        _ref3.active;
                                                                                                    return classNames(
                                                                                                        active
                                                                                                            ? "text-white bg-fuchsia-600"
                                                                                                            : "text-gray-900",
                                                                                                        "cursor-default select-none relative py-2 pl-3 pr-9"
                                                                                                    );
                                                                                                },
                                                                                            value: tab,
                                                                                            children:
                                                                                                function children(
                                                                                                    _ref4
                                                                                                ) {
                                                                                                    var selected =
                                                                                                            _ref4.selected,
                                                                                                        active =
                                                                                                            _ref4.active;
                                                                                                    return /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                                                                                                        {
                                                                                                            children:
                                                                                                                [
                                                                                                                    /*#__PURE__*/ (0,
                                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                        "span",
                                                                                                                        {
                                                                                                                            className:
                                                                                                                                classNames(
                                                                                                                                    selected
                                                                                                                                        ? "font-semibold"
                                                                                                                                        : "font-normal",
                                                                                                                                    "block truncate"
                                                                                                                                ),
                                                                                                                            children:
                                                                                                                                tab.name,
                                                                                                                        }
                                                                                                                    ),
                                                                                                                    selected
                                                                                                                        ? /*#__PURE__*/ (0,
                                                                                                                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                              "span",
                                                                                                                              {
                                                                                                                                  className:
                                                                                                                                      classNames(
                                                                                                                                          active
                                                                                                                                              ? "text-white"
                                                                                                                                              : "text-fuchsia-600",
                                                                                                                                          "absolute inset-y-0 right-0 flex items-center pr-4"
                                                                                                                                      ),
                                                                                                                                  children:
                                                                                                                                      /*#__PURE__*/ (0,
                                                                                                                                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                                          _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.CheckIcon,
                                                                                                                                          {
                                                                                                                                              className:
                                                                                                                                                  "h-5 w-5",
                                                                                                                                              "aria-hidden":
                                                                                                                                                  "true",
                                                                                                                                          }
                                                                                                                                      ),
                                                                                                                              }
                                                                                                                          )
                                                                                                                        : null,
                                                                                                                ],
                                                                                                        }
                                                                                                    );
                                                                                                },
                                                                                        },
                                                                                        tab.name
                                                                                    );
                                                                                }
                                                                            ),
                                                                    }
                                                                ),
                                                        }
                                                    ),
                                                ],
                                            }
                                        ),
                                    }
                                );
                            },
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    MobileResumeTabs;

                /***/
            },

        /***/ "./resources/js/Pages/Resume/Tabs/ResumeTabs.jsx":
            /*!*******************************************************!*\
  !*** ./resources/js/Pages/Resume/Tabs/TabContainer.jsx ***!
  \*******************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js"
                    );
                /* harmony import */ var _Pages_Resume_Tabs_MobileResumeTabs__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @/Pages/Resume/Tabs/MobileViewTabs */ "./resources/js/Pages/Resume/Tabs/MobileResumeTabs.jsx"
                    );
                /* harmony import */ var _Pages_Resume_Tabs_DesktopResumeTabs__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @/Pages/Resume/Tabs/DesktopViewTabs */ "./resources/js/Pages/Resume/Tabs/DesktopResumeTabs.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var GENERAL_URL = "dashboard";
                var WORK_EXPERIENCE_URL = "work-experience";
                var EDUCATION_QUALIFICATION_URL = "education-qualification";
                var SKILLS_URL = "skills";
                var REFERENCES_URL = "references";
                var CERTIFICATES_URL = "certificates";
                var tabs = [
                    {
                        name: "General",
                        href: GENERAL_URL,
                        current: true,
                    },
                    {
                        name: "Work Experience",
                        href: WORK_EXPERIENCE_URL,
                        current: false,
                    },
                    {
                        name: "Education Qualification",
                        href: EDUCATION_QUALIFICATION_URL,
                        current: false,
                    },
                    {
                        name: "Skills",
                        href: SKILLS_URL,
                        current: false,
                    },
                    {
                        name: "References",
                        href: REFERENCES_URL,
                        current: false,
                    },
                    {
                        name: "Certificates & Awards",
                        href: CERTIFICATES_URL,
                        current: false,
                    },
                ];

                var ResumeTabs = function ResumeTabs() {
                    tabs.find(function (tab) {
                        switch (tab.href) {
                            case WORK_EXPERIENCE_URL:
                            case EDUCATION_QUALIFICATION_URL:
                            case SKILLS_URL:
                            case REFERENCES_URL:
                            case CERTIFICATES_URL:
                                tab.current = !!route().current(tab.href + "*");
                                break;

                            case GENERAL_URL:
                                tab.current = !!route().current(tab.href);
                                break;

                            default:
                                tab.current = false;
                        }
                    });
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                    "div",
                                    {
                                        className: "lg:hidden",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                "label",
                                                {
                                                    htmlFor: "selected-tab",
                                                    className: "sr-only",
                                                    children: "Select a tab",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                _Pages_Resume_Tabs_MobileResumeTabs__WEBPACK_IMPORTED_MODULE_2__[
                                                    "default"
                                                ],
                                                {
                                                    tabs: tabs,
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                    "div",
                                    {
                                        className: "hidden lg:block",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                            _Pages_Resume_Tabs_DesktopResumeTabs__WEBPACK_IMPORTED_MODULE_3__[
                                                "default"
                                            ],
                                            {
                                                tabs: tabs,
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    ResumeTabs;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Dashboard.jsx":
            /*!*************************************************!*\
  !*** ./resources/js/Pages/Shared/Dashboard.jsx ***!
  \*************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/TemplateContainer */ "./resources/js/Pages/Shared/TemplateContainer.jsx"
                    );
                /* harmony import */ var _Pages_Resume_Tabs_ResumeTabs__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Resume/Tabs/Tabs */ "./resources/js/Pages/Resume/Tabs/ResumeTabs.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var Dashboard = function Dashboard(_ref) {
                    var children = _ref.children,
                        pageTitle = _ref.pageTitle;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
                        component = _usePage.component;

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                        {
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                _Pages_Shared_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__[
                                    "default"
                                ],
                                {
                                    title: pageTitle,
                                    children: /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                        "div",
                                        {
                                            className: "py-6",
                                            children: [
                                                /* Tabs */
                                                component.startsWith(
                                                    "Resume"
                                                ) &&
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                        _Pages_Resume_Tabs_ResumeTabs__WEBPACK_IMPORTED_MODULE_1__[
                                                            "default"
                                                        ],
                                                        {}
                                                    ),
                                                children,
                                            ],
                                        }
                                    ),
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Dashboard;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/General/ApplicationLogo.jsx":
            /*!***************************************************************!*\
  !*** ./resources/js/Pages/Shared/General/ApplicationLogo.jsx ***!
  \***************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var ApplicationLogo = function ApplicationLogo() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("svg", {
                        fill: "#ED8B00",
                        width: "149",
                        height: "24",
                        id: "jm-logo",
                        viewBox: "0 0 148.9 24",
                        xmlns: "http://www.w3.org/2000/svg",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                            "g",
                            {
                                className: "logo-jobmatch",
                                children: [
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                        "g",
                                        {
                                            className: "word-jobmatch",
                                            children: [
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M10.411 16.656c0 4.502-2.638 6.612-7.069 6.612-.991 0-1.912-.113-2.746-.286-.351-.072-.596-.389-.596-.746v-2.608c0-.446.384-.797.828-.763.582.044 1.08.077 1.494.077 2.251 0 3.341-.703 3.341-2.778v-14.359c0-.422.343-.765.766-.765h3.216c.423 0 .766.343.766.765v14.851z",
                                                        className: "jobmatch-j",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M16.338 14.139c0 2.744 1.864 4.713 4.502 4.713 2.567 0 4.502-2.004 4.502-4.713 0-2.743-1.935-4.783-4.502-4.783-2.638 0-4.502 2.04-4.502 4.783m13.855.153c-.104 5.246-4.564 9.175-9.901 8.909-4.88-.245-8.853-4.371-8.772-9.256.087-5.224 4.442-9.168 9.832-8.903 4.909.243 8.94 4.336 8.841 9.25",
                                                        className: "jobmatch-o",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M40.389 9.341c2.462 0 4.502 1.829 4.502 4.783 0 2.954-2.005 4.819-4.502 4.819-2.181 0-4.396-1.443-4.396-4.784 0-3.306 2.18-4.818 4.396-4.818zm-5.479 13.506c.423 0 .766-.343.766-.766v-1.415c1.196 1.654 3.201 2.603 5.592 2.603 4.924 0 8.441-3.869 8.441-9.11 0-5.24-3.517-9.109-8.441-9.109-2.18 0-4.009.738-5.205 2.075v-5.319c0-.423-.343-.766-.766-.766h-3.217c-.422 0-.765.343-.765.766v20.275c0 .423.343.766.765.766h2.83z",
                                                        className: "jobmatch-b",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                                    "g",
                                                    {
                                                        className: "jobmatch-m",
                                                        children: [
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "path",
                                                                {
                                                                    d: "M64.165 7.546c-1.055-1.583-2.814-2.497-5.171-2.497-2.075 0-3.763.738-4.923 1.935v-.783c0-.423-.343-.765-.766-.765h-2.83c-.297 0-.57.224-.697.47 1.827 2.121 2.797 4.945 2.797 8.254 0 3.303-.954 6.152-2.773 8.272.131.235.385.414.673.414h3.217c.422 0 .765-.343.765-.766v-9.364c0-2.075 1.302-3.376 3.201-3.376 1.864 0 3.095 1.301 3.095 3.376v9.364c0 .423.343.766.766.766h3.217c.422 0 .765-.343.765-.766v-9.469c0-1.935 1.301-3.271 3.201-3.271 1.864 0 3.095 1.301 3.095 3.376v9.364c0 .423.342.766.765.766h3.217c.423 0 .766-.343.766-.766v-10.032c0-4.186-2.427-6.999-6.577-6.999-2.427 0-4.502.984-5.803 2.497z",
                                                                    className:
                                                                        "jobmatch-m-letter",
                                                                }
                                                            ),
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "path",
                                                                {
                                                                    d: "M49.802 5.856c-.055.104-.093.22-.093.346v2.07c.976 1.653 1.532 3.656 1.532 5.887 0 2.229-.556 4.233-1.532 5.886v2.036c0 .134.045.255.106.366 1.819-2.12 2.895-4.986 2.895-8.288 0-3.31-1.081-6.182-2.908-8.303",
                                                                    className:
                                                                        "jobmatch-m-edge",
                                                                }
                                                            ),
                                                        ],
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M88.349 16.34v-.611h-3.729c-1.547 0-2.426.798-2.426 1.959 0 1.196 1.019 2.098 2.497 2.098 1.269 0 3.658-1.109 3.658-3.446m4.748-3.834v9.575c0 .423-.343.766-.766.766h-2.83c-.423 0-.765-.343-.765-.766v-.923c-1.232 1.302-2.463 2.111-5.312 2.111-3.798 0-6.013-2.451-6.013-5.406 0-3.165 2.567-5.324 6.577-5.324h4.361v-.314c0-1.9-1.091-3.06-3.236-3.06-1.618 0-2.811.662-4.04 1.824-.316.299-.822.266-1.102-.067l-1.498-1.776c-.246-.291-.248-.729.014-1.007 1.955-2.088 4.311-3.089 7.153-3.089 4.432 0 7.457 2.497 7.457 7.456",
                                                        className: "jobmatch-a",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M106.476 19.7v2.537c0 .357-.245.673-.594.746-.815.172-1.69.285-2.677.285-4.291 0-6.964-2.075-6.964-6.542v-7.28h-1.978c-.423 0-.766-.343-.766-.766v-2.478c0-.423.343-.766.766-.766h1.978v-3.631c0-.422.342-.765.765-.765h3.217c.423 0 .766.343.766.765v3.631h4.159c.423 0 .766.343.766.766v2.478c0 .423-.343.766-.766.766h-4.159v6.858c0 2.005 1.055 2.709 3.236 2.709.383 0 .864-.033 1.421-.076.445-.034.83.317.83.763",
                                                        className: "jobmatch-t",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M122.258 8.855c.237-.328.235-.788-.036-1.089-1.521-1.686-3.717-2.717-6.432-2.717-5.205 0-9.32 3.94-9.32 9.075 0 5.169 4.115 9.144 9.32 9.144 2.668 0 4.865-1.038 6.393-2.712.273-.299.278-.76.042-1.089l-1.507-2.102c-.322-.449-.982-.507-1.355-.1-.9.986-2.004 1.642-3.501 1.642-3.366 0-5.428-3.149-4.23-6.885.333-1.037 1.501-2.142 2.555-2.415 2.427-.63 4.008.087 5.185 1.418.374.422 1.054.358 1.384-.098l1.502-2.072z",
                                                        className: "jobmatch-c",
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "path",
                                                    {
                                                        d: "M139.385 12.049v10.032c0 .423-.343.766-.766.766h-3.181c-.423 0-.766-.343-.766-.766v-9.364c0-2.04-1.302-3.376-3.236-3.376-1.934 0-3.306 1.371-3.306 3.376v9.364c0 .423-.343.766-.765.766h-3.217c-.423 0-.766-.343-.766-.766v-20.275c0-.423.343-.766.766-.766h3.217c.422 0 .765.343.765.766v5.108c1.16-1.161 2.814-1.864 4.783-1.864 3.975 0 6.472 2.813 6.472 6.999",
                                                        className: "jobmatch-h",
                                                    }
                                                ),
                                            ],
                                        }
                                    ),
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                        "circle",
                                        {
                                            cx: "145.5",
                                            cy: "20.1",
                                            r: "3.4",
                                            className: "dot",
                                        }
                                    ),
                                ],
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    ApplicationLogo;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout.jsx":
            /*!**********************************************!*\
  !*** ./resources/js/Pages/Shared/Components.jsx ***!
  \**********************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var _MobileSideBar__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! ./MobileSideBar */ "./resources/js/Pages/Shared/MobileSideBar.jsx"
                    );
                /* harmony import */ var _StaticSideBar__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! ./StaticSideBar */ "./resources/js/Pages/Shared/StaticSideBar.jsx"
                    );
                /* harmony import */ var _MainHeader__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! ./MainHeader */ "./resources/js/Pages/Shared/MainHeader.jsx"
                    );
                /* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );
                function _slicedToArray(arr, i) {
                    return (
                        _arrayWithHoles(arr) ||
                        _iterableToArrayLimit(arr, i) ||
                        _unsupportedIterableToArray(arr, i) ||
                        _nonIterableRest()
                    );
                }

                function _nonIterableRest() {
                    throw new TypeError(
                        "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                    );
                }

                function _unsupportedIterableToArray(o, minLen) {
                    if (!o) return;
                    if (typeof o === "string")
                        return _arrayLikeToArray(o, minLen);
                    var n = Object.prototype.toString.call(o).slice(8, -1);
                    if (n === "Object" && o.constructor) n = o.constructor.name;
                    if (n === "Map" || n === "Set") return Array.from(o);
                    if (
                        n === "Arguments" ||
                        /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                    )
                        return _arrayLikeToArray(o, minLen);
                }

                function _arrayLikeToArray(arr, len) {
                    if (len == null || len > arr.length) len = arr.length;
                    for (var i = 0, arr2 = new Array(len); i < len; i++) {
                        arr2[i] = arr[i];
                    }
                    return arr2;
                }

                function _iterableToArrayLimit(arr, i) {
                    var _i =
                        arr == null
                            ? null
                            : (typeof Symbol !== "undefined" &&
                                  arr[Symbol.iterator]) ||
                              arr["@@iterator"];
                    if (_i == null) return;
                    var _arr = [];
                    var _n = true;
                    var _d = false;
                    var _s, _e;
                    try {
                        for (
                            _i = _i.call(arr);
                            !(_n = (_s = _i.next()).done);
                            _n = true
                        ) {
                            _arr.push(_s.value);
                            if (i && _arr.length === i) break;
                        }
                    } catch (err) {
                        _d = true;
                        _e = err;
                    } finally {
                        try {
                            if (!_n && _i["return"] != null) _i["return"]();
                        } finally {
                            if (_d) throw _e;
                        }
                    }
                    return _arr;
                }

                function _arrayWithHoles(arr) {
                    if (Array.isArray(arr)) return arr;
                }

                var navigation = [
                    {
                        name: "Talent",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.FolderIcon,
                        current: true,
                        children: [
                            {
                                name: "Evaluation Text",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Evaluation Rules",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Score Config",
                                href: "#",
                                active: true,
                            },
                            {
                                name: "Profiles",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Job Tags",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Special Reports",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Test Questions",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Elimination Rules",
                                href: "#",
                                active: false,
                            },
                        ],
                    },
                    {
                        name: "Career",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.FolderIcon,
                        current: false,
                        children: [
                            {
                                name: "Evaluation Text",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Evaluation Rules",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Score Config",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Profiles",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Job Tags",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Elimination Rules",
                                href: "#",
                                active: false,
                            },
                        ],
                    },
                    {
                        name: "Logiq",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.FolderIcon,
                        current: false,
                        children: [
                            {
                                name: "Norm Groups",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Test List",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Score Config",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Test Questions",
                                href: "#",
                                active: false,
                            },
                        ],
                    },
                    {
                        name: "Screen",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.FolderIcon,
                        current: false,
                        children: [
                            {
                                name: "Evaluation Text",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Evaluation Rules",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Score Config",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Profiles",
                                href: "#",
                                active: false,
                            },
                        ],
                    },
                    {
                        name: "Five Factor",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.FolderIcon,
                        current: false,
                        children: [
                            {
                                name: "Evaluation Text",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Evaluation Rules",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Score Config",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Traits",
                                href: "#",
                                active: false,
                            },
                            {
                                name: "Elimination Rules",
                                href: "#",
                                active: false,
                            },
                        ],
                    },
                    {
                        name: "Settings",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.CogIcon,
                        current: false,
                        href: "#",
                    },
                ];

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var Layout = function Layout(_ref) {
                    var children = _ref.children,
                        selectedSideBarMenu = _ref.selectedSideBarMenu,
                        title = _ref.title;

                    var _useState = (0,
                        react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
                        _useState2 = _slicedToArray(_useState, 2),
                        sidebarOpen = _useState2[0],
                        setSidebarOpen = _useState2[1];

                    navigation.filter(function (nav) {
                        nav.current = nav.name === selectedSideBarMenu;
                    });
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.Fragment,
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                    react_helmet__WEBPACK_IMPORTED_MODULE_5__.Helmet,
                                    {
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                            "title",
                                            {
                                                children: title
                                                    ? "JTC - ".concat(title)
                                                    : "JTC",
                                            }
                                        ),
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "relative h-screen flex overflow-hidden bg-white",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                                _MobileSideBar__WEBPACK_IMPORTED_MODULE_2__[
                                                    "default"
                                                ],
                                                {
                                                    sidebarOpen: sidebarOpen,
                                                    setSidebarOpen:
                                                        setSidebarOpen,
                                                    navigation: navigation,
                                                    classNames: classNames,
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                                _StaticSideBar__WEBPACK_IMPORTED_MODULE_3__[
                                                    "default"
                                                ],
                                                {
                                                    navigation: navigation,
                                                    classNames: classNames,
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)(
                                                "div",
                                                {
                                                    className:
                                                        "flex-1 overflow-auto focus:outline-none",
                                                    children: [
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                                            _MainHeader__WEBPACK_IMPORTED_MODULE_4__[
                                                                "default"
                                                            ],
                                                            {
                                                                setSidebarOpen:
                                                                    setSidebarOpen,
                                                                classNames:
                                                                    classNames,
                                                            }
                                                        ),
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(
                                                            "main",
                                                            {
                                                                children:
                                                                    children,
                                                            }
                                                        ),
                                                    ],
                                                }
                                            ),
                                        ],
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Layout;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/MainHeader.jsx":
            /*!**************************************************!*\
  !*** ./resources/js/Pages/Shared/MainHeader.jsx ***!
  \**************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var _ProfileDropdown__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! ./ProfileDropdown */ "./resources/js/Pages/Shared/ProfileDropdown.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var MainHeader = function MainHeader(_ref) {
                    var setSidebarOpen = _ref.setSidebarOpen,
                        classNames = _ref.classNames;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                        "div",
                        {
                            className:
                                "relative z-10 flex-shrink-0 flex h-16 bg-white border-b border-gray-200 lg:border-none",
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                    "button",
                                    {
                                        type: "button",
                                        className:
                                            "px-4 border-r border-gray-200 text-gray-400 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500 lg:hidden",
                                        onClick: function onClick() {
                                            return setSidebarOpen(true);
                                        },
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                "span",
                                                {
                                                    className: "sr-only",
                                                    children: "Open sidebar",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.MenuAlt1Icon,
                                                {
                                                    className: "h-6 w-6",
                                                    "aria-hidden": "true",
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "flex-1 px-4 flex justify-between sm:px-6 lg:max-w-8xl lg:mx-auto lg:px-8 mt-2",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                "div",
                                                {
                                                    className: "flex-1 flex",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "ml-4 flex items-center md:ml-6",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                        _ProfileDropdown__WEBPACK_IMPORTED_MODULE_1__[
                                                            "default"
                                                        ],
                                                        {
                                                            classNames:
                                                                classNames,
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    MainHeader;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/MobileSideBar.jsx":
            /*!*****************************************************!*\
  !*** ./resources/js/Pages/Shared/MobileSideBar.jsx ***!
  \*****************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js"
                    );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var _General_ApplicationLogo__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! ./General/ApplicationLogo */ "./resources/js/Pages/Shared/General/ApplicationLogo.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var MobileSideBar = function MobileSideBar(_ref) {
                    var sidebarOpen = _ref.sidebarOpen,
                        setSidebarOpen = _ref.setSidebarOpen,
                        navigation = _ref.navigation,
                        classNames = _ref.classNames;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                        _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                            .Transition.Root,
                        {
                            show: sidebarOpen,
                            as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                _headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Dialog,
                                {
                                    as: "div",
                                    className:
                                        "fixed inset-0 flex z-40 lg:hidden",
                                    onClose: setSidebarOpen,
                                    children: [
                                        /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                .Transition.Child,
                                            {
                                                as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                enter: "transition-opacity ease-linear duration-300",
                                                enterFrom: "opacity-0",
                                                enterTo: "opacity-100",
                                                leave: "transition-opacity ease-linear duration-300",
                                                leaveFrom: "opacity-100",
                                                leaveTo: "opacity-0",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_6__
                                                        .Dialog.Overlay,
                                                    {
                                                        className:
                                                            "fixed inset-0 bg-gray-600 bg-opacity-75",
                                                    }
                                                ),
                                            }
                                        ),
                                        /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                .Transition.Child,
                                            {
                                                as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                enter: "transition ease-in-out duration-300 transform",
                                                enterFrom: "-translate-x-full",
                                                enterTo: "translate-x-0",
                                                leave: "transition ease-in-out duration-300 transform",
                                                leaveFrom: "translate-x-0",
                                                leaveTo: "-translate-x-full",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                    "div",
                                                    {
                                                        className:
                                                            "relative flex-1 flex flex-col max-w-xs w-full pt-5 pb-4 bg-slate-100",
                                                        children: [
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                    .Transition
                                                                    .Child,
                                                                {
                                                                    as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                                    enter: "ease-in-out duration-300",
                                                                    enterFrom:
                                                                        "opacity-0",
                                                                    enterTo:
                                                                        "opacity-100",
                                                                    leave: "ease-in-out duration-300",
                                                                    leaveFrom:
                                                                        "opacity-100",
                                                                    leaveTo:
                                                                        "opacity-0",
                                                                    children:
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                            "div",
                                                                            {
                                                                                className:
                                                                                    "absolute top-0 right-0 -mr-12 pt-2",
                                                                                children:
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                                        "button",
                                                                                        {
                                                                                            type: "button",
                                                                                            className:
                                                                                                "ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white",
                                                                                            onClick:
                                                                                                function onClick() {
                                                                                                    return setSidebarOpen(
                                                                                                        false
                                                                                                    );
                                                                                                },
                                                                                            children:
                                                                                                [
                                                                                                    /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                        "span",
                                                                                                        {
                                                                                                            className:
                                                                                                                "sr-only",
                                                                                                            children:
                                                                                                                "Close sidebar",
                                                                                                        }
                                                                                                    ),
                                                                                                    /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                        _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.XIcon,
                                                                                                        {
                                                                                                            className:
                                                                                                                "h-6 w-6 text-white",
                                                                                                            "aria-hidden":
                                                                                                                "true",
                                                                                                        }
                                                                                                    ),
                                                                                                ],
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                }
                                                            ),
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                "div",
                                                                {
                                                                    className:
                                                                        "flex-shrink-0 flex items-center px-4",
                                                                    children:
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                            _General_ApplicationLogo__WEBPACK_IMPORTED_MODULE_2__[
                                                                                "default"
                                                                            ],
                                                                            {}
                                                                        ),
                                                                }
                                                            ),
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                "nav",
                                                                {
                                                                    className:
                                                                        "mt-5 flex-shrink-0 h-full divide-y divide-gray-800 overflow-y-auto",
                                                                    "aria-label":
                                                                        "Sidebar",
                                                                    children:
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                            "div",
                                                                            {
                                                                                className:
                                                                                    "px-2 space-y-1",
                                                                                children:
                                                                                    navigation.map(
                                                                                        function (
                                                                                            item
                                                                                        ) {
                                                                                            return /*#__PURE__*/ (0,
                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                                                _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__.Link,
                                                                                                {
                                                                                                    href: route(
                                                                                                        "dashboard"
                                                                                                    ),
                                                                                                    className:
                                                                                                        classNames(
                                                                                                            item.current
                                                                                                                ? "bg-slate-300 hover:bg-gray-800 text-fuchsia-700"
                                                                                                                : "bg-slate-100 text-fuchsia-700 hover:text-fuchsia-400 hover:bg-slate-400",
                                                                                                            "group flex items-center px-2 py-2 text-base font-medium rounded-md"
                                                                                                        ),
                                                                                                    "aria-current":
                                                                                                        item.current
                                                                                                            ? "page"
                                                                                                            : undefined,
                                                                                                    children:
                                                                                                        [
                                                                                                            /*#__PURE__*/ (0,
                                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                item.icon,
                                                                                                                {
                                                                                                                    className:
                                                                                                                        "mr-4 flex-shrink-0 h-6 w-6 text-fuchsia-700",
                                                                                                                    "aria-hidden":
                                                                                                                        "true",
                                                                                                                }
                                                                                                            ),
                                                                                                            item.name,
                                                                                                        ],
                                                                                                },
                                                                                                item.name
                                                                                            );
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                }
                                                            ),
                                                        ],
                                                    }
                                                ),
                                            }
                                        ),
                                        /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                            "div",
                                            {
                                                className: "flex-shrink-0 w-14",
                                                "aria-hidden": "true",
                                            }
                                        ),
                                    ],
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    MobileSideBar;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/ProfileDropdown.jsx":
            /*!*******************************************************!*\
  !*** ./resources/js/Pages/Shared/ProfileDropdown.jsx ***!
  \*******************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/menu/menu.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var ProfileDropdown = function ProfileDropdown(_ref) {
                    var classNames = _ref.classNames;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
                        props = _usePage.props;

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                        _headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Menu,
                        {
                            as: "div",
                            className: "ml-3 relative",
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                    "div",
                                    {
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_4__
                                                .Menu.Button,
                                            {
                                                className:
                                                    "max-w-xs bg-white rounded-full flex items-center text-sm focus:outline-none lg:p-2 lg:rounded-md lg:hover:bg-gray-50",
                                                children: [
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                        "img",
                                                        {
                                                            className:
                                                                "h-8 w-8 rounded-full",
                                                            src: props.user
                                                                .profile_photo_url,
                                                            alt: props.user
                                                                .name,
                                                        }
                                                    ),
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                        "span",
                                                        {
                                                            className:
                                                                "hidden ml-3 text-fuchsia-700 text-sm font-medium lg:block",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "sr-only",
                                                                        children:
                                                                            "Open user menu for ",
                                                                    }
                                                                ),
                                                                props.user.name,
                                                            ],
                                                        }
                                                    ),
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                        _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.ChevronDownIcon,
                                                        {
                                                            className:
                                                                "hidden flex-shrink-0 ml-1 h-5 w-5 text-gray-400 lg:block",
                                                            "aria-hidden":
                                                                "true",
                                                        }
                                                    ),
                                                ],
                                            }
                                        ),
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Transition,
                                    {
                                        as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                        enter: "transition ease-out duration-100",
                                        enterFrom:
                                            "transform opacity-0 scale-95",
                                        enterTo:
                                            "transform opacity-100 scale-100",
                                        leave: "transition ease-in duration-75",
                                        leaveFrom:
                                            "transform opacity-100 scale-100",
                                        leaveTo: "transform opacity-0 scale-95",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_4__
                                                .Menu.Items,
                                            {
                                                className:
                                                    "origin-top-right absolute right-0 mt-1 w-20 rounded-md shadow-lg py-1 bg-white focus:outline-none",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_4__
                                                        .Menu.Item,
                                                    {
                                                        children:
                                                            function children(
                                                                _ref2
                                                            ) {
                                                                var active =
                                                                    _ref2.active;
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                    _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link,
                                                                    {
                                                                        href: route(
                                                                            "logout"
                                                                        ),
                                                                        className:
                                                                            classNames(
                                                                                active
                                                                                    ? "bg-gray-100"
                                                                                    : "",
                                                                                "block px-4 py-2 text-sm text-fuchsia-700"
                                                                            ),
                                                                        method: "post",
                                                                        as: "button",
                                                                        children:
                                                                            "Logout",
                                                                    }
                                                                );
                                                            },
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    ProfileDropdown;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/SideNavChildren.jsx":
            /*!*******************************************************!*\
  !*** ./resources/js/Pages/Shared/SideNavChildren.jsx ***!
  \*******************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/disclosure/disclosure.esm.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var SideNavChildren = function SideNavChildren(_ref) {
                    var children = _ref.children;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                        _headlessui_react__WEBPACK_IMPORTED_MODULE_1__
                            .Disclosure.Panel,
                        {
                            className: "space-y-1",
                            static: true,
                            children: children.map(function (subItem) {
                                return /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_1__
                                        .Disclosure.Button,
                                    {
                                        as: "a",
                                        href: subItem.href,
                                        className: classNames(
                                            subItem.active
                                                ? "bg-violet-50 text-violet-900 hover:bg-violet-700 hover:text-white"
                                                : "text-gray-600 hover:bg-violet-700 hover:text-white",
                                            "group w-full flex items-center pl-2 pr-1 py-2 text-left text-sm font-medium rounded-md focus:outline-none"
                                        ),
                                        children: subItem.name,
                                    },
                                    subItem.name
                                );
                            }),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    SideNavChildren;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/StaticSideBar.jsx":
            /*!*****************************************************!*\
  !*** ./resources/js/Pages/Shared/StaticSideBar.jsx ***!
  \*****************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/disclosure/disclosure.esm.js"
                    );
                /* harmony import */ var _Pages_Shared_General_ApplicationLogo__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/General/ApplicationLogo */ "./resources/js/Pages/Shared/General/ApplicationLogo.jsx"
                    );
                /* harmony import */ var _Pages_Shared_SideNavChildren__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/SideNavChildren */ "./resources/js/Pages/Shared/SideNavChildren.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var StaticSideBar = function StaticSideBar(_ref) {
                    var navigation = _ref.navigation;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
                        className: "hidden lg:flex lg:flex-shrink-0",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                            "div",
                            {
                                className: "flex flex-col w-64",
                                children: /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "flex flex-col flex-grow border-r border-gray-200 pt-5 pb-4 bg-white overflow-y-auto",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "flex items-center flex-shrink-0 px-4",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                        _Pages_Shared_General_ApplicationLogo__WEBPACK_IMPORTED_MODULE_0__[
                                                            "default"
                                                        ],
                                                        {}
                                                    ),
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "mt-8 flex-grow flex flex-col",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                        "nav",
                                                        {
                                                            className:
                                                                "flex-1 px-2 space-y-1 bg-white",
                                                            "aria-label":
                                                                "Sidebar",
                                                            children:
                                                                navigation.map(
                                                                    function (
                                                                        item
                                                                    ) {
                                                                        return !item.children
                                                                            ? /*#__PURE__*/ (0,
                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                  "div",
                                                                                  {
                                                                                      children:
                                                                                          /*#__PURE__*/ (0,
                                                                                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                                              "a",
                                                                                              {
                                                                                                  href: "#",
                                                                                                  className:
                                                                                                      classNames(
                                                                                                          item.current
                                                                                                              ? "bg-violet-700 text-white hover:bg-violet-400"
                                                                                                              : "text-gray-600 hover:bg-violet-700 hover:text-white",
                                                                                                          "group w-full flex items-center pl-2 pr-1 py-2 text-left text-sm font-medium rounded-md focus:outline-none"
                                                                                                      ),
                                                                                                  children:
                                                                                                      [
                                                                                                          /*#__PURE__*/ (0,
                                                                                                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                              item.icon,
                                                                                                              {
                                                                                                                  className:
                                                                                                                      classNames(
                                                                                                                          item.current
                                                                                                                              ? "text-white"
                                                                                                                              : "text-gray-600",
                                                                                                                          "mr-3 flex-shrink-0 h-6 w-6 group-hover:text-white"
                                                                                                                      ),
                                                                                                                  "aria-hidden":
                                                                                                                      "true",
                                                                                                              }
                                                                                                          ),
                                                                                                          item.name,
                                                                                                      ],
                                                                                              }
                                                                                          ),
                                                                                  },
                                                                                  item.name
                                                                              )
                                                                            : /*#__PURE__*/ (0,
                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                  _headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Disclosure,
                                                                                  {
                                                                                      as: "div",
                                                                                      className:
                                                                                          "space-y-1",
                                                                                      children:
                                                                                          function children() {
                                                                                              return /*#__PURE__*/ (0,
                                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                                                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.Fragment,
                                                                                                  {
                                                                                                      children:
                                                                                                          [
                                                                                                              /*#__PURE__*/ (0,
                                                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                                                                  _headlessui_react__WEBPACK_IMPORTED_MODULE_3__
                                                                                                                      .Disclosure
                                                                                                                      .Button,
                                                                                                                  {
                                                                                                                      className:
                                                                                                                          classNames(
                                                                                                                              item.current
                                                                                                                                  ? "bg-violet-700 text-white hover:bg-violet-400"
                                                                                                                                  : "text-gray-600 hover:bg-violet-700 hover:text-white",
                                                                                                                              "group w-full flex items-center pl-2 pr-1 py-2 text-left text-sm font-medium rounded-md focus:outline-none"
                                                                                                                          ),
                                                                                                                      children:
                                                                                                                          [
                                                                                                                              /*#__PURE__*/ (0,
                                                                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                                  item.icon,
                                                                                                                                  {
                                                                                                                                      className:
                                                                                                                                          classNames(
                                                                                                                                              item.current
                                                                                                                                                  ? "text-white"
                                                                                                                                                  : "text-gray-600",
                                                                                                                                              "mr-3 flex-shrink-0   h-6 w-6 group-hover:text-white"
                                                                                                                                          ),
                                                                                                                                      "aria-hidden":
                                                                                                                                          "true",
                                                                                                                                  }
                                                                                                                              ),
                                                                                                                              /*#__PURE__*/ (0,
                                                                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                                  "span",
                                                                                                                                  {
                                                                                                                                      className:
                                                                                                                                          "flex-1",
                                                                                                                                      children:
                                                                                                                                          item.name,
                                                                                                                                  }
                                                                                                                              ),
                                                                                                                          ],
                                                                                                                  }
                                                                                                              ),
                                                                                                              /*#__PURE__*/ (0,
                                                                                                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                  _Pages_Shared_SideNavChildren__WEBPACK_IMPORTED_MODULE_1__[
                                                                                                                      "default"
                                                                                                                  ],
                                                                                                                  {
                                                                                                                      children:
                                                                                                                          item.children,
                                                                                                                  }
                                                                                                              ),
                                                                                                          ],
                                                                                                  }
                                                                                              );
                                                                                          },
                                                                                  },
                                                                                  item.name
                                                                              );
                                                                    }
                                                                ),
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    StaticSideBar;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/TemplateContainer.jsx":
            /*!*********************************************************!*\
  !*** ./resources/js/Pages/Shared/TemplateContainer.jsx ***!
  \*********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var TemplateContainer = function TemplateContainer(_ref) {
                    var children = _ref.children,
                        title = _ref.title;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
                        className: "mt-1",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            "main",
                            {
                                className: "flex-1",
                                children: /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    "div",
                                    {
                                        className:
                                            "relative max-w-4xl mx-auto md:px-8 xl:px-0",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                            "div",
                                            {
                                                className: "pt-10 pb-16",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                                    "div",
                                                    {
                                                        className:
                                                            "px-4 sm:px-6 md:px-0",
                                                        children: [
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "h1",
                                                                {
                                                                    className:
                                                                        "text-3xl font-extrabold text-gray-600",
                                                                    children:
                                                                        title,
                                                                }
                                                            ),
                                                            children,
                                                        ],
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TemplateContainer;

                /***/
            },
    },
]);

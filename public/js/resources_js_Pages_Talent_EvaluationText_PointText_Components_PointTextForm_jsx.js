"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Talent_EvaluationText_PointText_Components_PointTextForm_jsx"],{

/***/ "./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextForm.jsx":
/*!*****************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextForm.jsx ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var PointTextForm = function PointTextForm(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("fieldset", {
    className: "mt-4",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("legend", {
      className: "sr-only",
      children: "Point Text Type"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
      className: "space-y-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-10",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex items-center",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
          id: "positive",
          name: "pointTextType",
          type: "radio",
          defaultChecked: data["positive"],
          value: data["positive"],
          onChange: function onChange(e) {
            return setData("pointTextType", e.target.checked);
          },
          className: "focus:ring-violet-500 h-4 w-4 text-violet-600 border-gray-300"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
          htmlFor: "positive",
          className: "ml-3 block text-sm font-medium text-gray-700",
          children: "Positive"
        })]
      }, "positive"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex items-center",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
          id: "neutral",
          name: "pointTextType",
          type: "radio",
          defaultChecked: data["neutral"],
          value: data["neutral"],
          onChange: function onChange(e) {
            return setData("pointTextType", e.target.checked);
          },
          className: "focus:ring-violet-500 h-4 w-4 text-violet-600 border-gray-300"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
          htmlFor: "neutral",
          className: "ml-3 block text-sm font-medium text-gray-700",
          children: "Neutral"
        })]
      }, "neutral"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
        className: "flex items-center",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
          id: "negative",
          name: "pointTextType",
          type: "radio",
          defaultChecked: data["negative"],
          value: data["negative"],
          onChange: function onChange(e) {
            return setData("pointTextType", e.target.checked);
          },
          className: "focus:ring-violet-500 h-4 w-4 text-violet-600 border-gray-300"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
          htmlFor: "negative",
          className: "ml-3 block text-sm font-medium text-gray-700",
          children: "Negative"
        })]
      }, "negative")]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PointTextForm);

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Shared_Layout_Dashboard_jsx"],
    {
        /***/ "./resources/js/Pages/Shared/Layout/Dashboard.jsx":
            /*!********************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/Dashboard.jsx ***!
  \********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_Layout_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Layout/TemplateContainer */ "./resources/js/Pages/Shared/Layout/TemplateContainer.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_Tabs__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Layout/Tabs */ "./resources/js/Pages/Shared/Layout/Tabs.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Layout/MainMenuNames */ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var Dashboard = function Dashboard(_ref) {
                    var children = _ref.children,
                        formFields = _ref.formFields;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
                        selectedTab = _usePage.props.metaData.selectedTab;

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                        {
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                _Pages_Shared_Layout_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__[
                                    "default"
                                ],
                                {
                                    title: selectedTab,
                                    formFields: formFields,
                                    children: /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                        "div",
                                        {
                                            className: "py-6",
                                            children: [
                                                selectedTab !==
                                                    _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_3__.SETTINGS_MENU &&
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        _Pages_Shared_Layout_Tabs__WEBPACK_IMPORTED_MODULE_1__[
                                                            "default"
                                                        ],
                                                        {}
                                                    ),
                                                children,
                                            ],
                                        }
                                    ),
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Dashboard;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx":
            /*!************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/MainMenuNames.jsx ***!
  \************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ TALENT_MENU: () =>
                            /* binding */ TALENT_MENU,
                        /* harmony export */ CAREER_MENU: () =>
                            /* binding */ CAREER_MENU,
                        /* harmony export */ LOGIQ_MENU: () =>
                            /* binding */ LOGIQ_MENU,
                        /* harmony export */ SCREEN_MENU: () =>
                            /* binding */ SCREEN_MENU,
                        /* harmony export */ FIVE_MENU: () =>
                            /* binding */ FIVE_MENU,
                        /* harmony export */ SETTINGS_MENU: () =>
                            /* binding */ SETTINGS_MENU,
                        /* harmony export */
                    }
                );
                var TALENT_MENU = "Talent";
                var CAREER_MENU = "Career";
                var LOGIQ_MENU = "Logiq";
                var SCREEN_MENU = "Screen";
                var FIVE_MENU = "Five";
                var SETTINGS_MENU = "Settings";

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/SlideOverLayout.jsx":
            /*!**************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/SlideOverLayout.jsx ***!
  \**************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js"
                    );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                Object(
                    (function webpackMissingModule() {
                        var e = new Error(
                            "Cannot find module '@/Pages/Shared/General/InputField'"
                        );
                        e.code = "MODULE_NOT_FOUND";
                        throw e;
                    })()
                );
                Object(
                    (function webpackMissingModule() {
                        var e = new Error(
                            "Cannot find module '@/Pages/Shared/General/TextAreaField'"
                        );
                        e.code = "MODULE_NOT_FOUND";
                        throw e;
                    })()
                );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                Object(
                    (function webpackMissingModule() {
                        var e = new Error(
                            "Cannot find module '@/Pages/Shared/General/Notification'"
                        );
                        e.code = "MODULE_NOT_FOUND";
                        throw e;
                    })()
                );
                Object(
                    (function webpackMissingModule() {
                        var e = new Error(
                            "Cannot find module '@/Pages/Shared/General/SlideOverForm'"
                        );
                        e.code = "MODULE_NOT_FOUND";
                        throw e;
                    })()
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );
                function _slicedToArray(arr, i) {
                    return (
                        _arrayWithHoles(arr) ||
                        _iterableToArrayLimit(arr, i) ||
                        _unsupportedIterableToArray(arr, i) ||
                        _nonIterableRest()
                    );
                }

                function _nonIterableRest() {
                    throw new TypeError(
                        "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                    );
                }

                function _unsupportedIterableToArray(o, minLen) {
                    if (!o) return;
                    if (typeof o === "string")
                        return _arrayLikeToArray(o, minLen);
                    var n = Object.prototype.toString.call(o).slice(8, -1);
                    if (n === "Object" && o.constructor) n = o.constructor.name;
                    if (n === "Map" || n === "Set") return Array.from(o);
                    if (
                        n === "Arguments" ||
                        /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                    )
                        return _arrayLikeToArray(o, minLen);
                }

                function _arrayLikeToArray(arr, len) {
                    if (len == null || len > arr.length) len = arr.length;
                    for (var i = 0, arr2 = new Array(len); i < len; i++) {
                        arr2[i] = arr[i];
                    }
                    return arr2;
                }

                function _iterableToArrayLimit(arr, i) {
                    var _i =
                        arr == null
                            ? null
                            : (typeof Symbol !== "undefined" &&
                                  arr[Symbol.iterator]) ||
                              arr["@@iterator"];
                    if (_i == null) return;
                    var _arr = [];
                    var _n = true;
                    var _d = false;
                    var _s, _e;
                    try {
                        for (
                            _i = _i.call(arr);
                            !(_n = (_s = _i.next()).done);
                            _n = true
                        ) {
                            _arr.push(_s.value);
                            if (i && _arr.length === i) break;
                        }
                    } catch (err) {
                        _d = true;
                        _e = err;
                    } finally {
                        try {
                            if (!_n && _i["return"] != null) _i["return"]();
                        } finally {
                            if (_d) throw _e;
                        }
                    }
                    return _arr;
                }

                function _arrayWithHoles(arr) {
                    if (Array.isArray(arr)) return arr;
                }

                var SlideOverLayout = function SlideOverLayout(_ref) {
                    var open = _ref.open,
                        closeSlideOver = _ref.closeSlideOver,
                        title = _ref.title,
                        formFields = _ref.formFields,
                        description = _ref.description;

                    // prevent the modal from closing when outside the form is clicked
                    var dummyClose = function dummyClose() {};

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__.usePage)(),
                        _usePage$props$flash = _usePage.props.flash,
                        success = _usePage$props$flash.success,
                        error = _usePage$props$flash.error;

                    var message = "";
                    var messageType = "success";

                    if (success) {
                        message = success;
                    }

                    if (error) {
                        message = error;
                        messageType = "error";
                    }

                    var _useState = (0,
                        react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
                        _useState2 = _slicedToArray(_useState, 2),
                        show = _useState2[0],
                        setShow = _useState2[1];

                    var closeNotification = function closeNotification() {
                        setShow(false);
                    };

                    var openNotification = function openNotification() {
                        setShow(true);
                    };

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                        .Transition.Root,
                                    {
                                        show: open,
                                        as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Dialog,
                                            {
                                                as: "div",
                                                className:
                                                    "fixed inset-0 overflow-hidden",
                                                onClose: function onClose() {
                                                    return dummyClose();
                                                },
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                    "div",
                                                    {
                                                        className:
                                                            "absolute inset-0 overflow-hidden",
                                                        children: [
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                _headlessui_react__WEBPACK_IMPORTED_MODULE_6__
                                                                    .Dialog
                                                                    .Overlay,
                                                                {
                                                                    className:
                                                                        "absolute inset-0",
                                                                }
                                                            ),
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                "div",
                                                                {
                                                                    className:
                                                                        "fixed inset-y-0 pl-16 max-w-full right-0 flex",
                                                                    children:
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                                .Transition
                                                                                .Child,
                                                                            {
                                                                                as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                                                enter: "transform transition ease-in-out duration-500 sm:duration-700",
                                                                                enterFrom:
                                                                                    "translate-x-full",
                                                                                enterTo:
                                                                                    "translate-x-0",
                                                                                leave: "transform transition ease-in-out duration-500 sm:duration-700",
                                                                                leaveFrom:
                                                                                    "translate-x-0",
                                                                                leaveTo:
                                                                                    "translate-x-full",
                                                                                children:
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                        "div",
                                                                                        {
                                                                                            className:
                                                                                                "w-screen max-w-md mt-12",
                                                                                            children:
                                                                                                /*#__PURE__*/ (0,
                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                    Object(
                                                                                                        (function webpackMissingModule() {
                                                                                                            var e =
                                                                                                                new Error(
                                                                                                                    "Cannot find module '@/Pages/Shared/General/SlideOverForm'"
                                                                                                                );
                                                                                                            e.code =
                                                                                                                "MODULE_NOT_FOUND";
                                                                                                            throw e;
                                                                                                        })()
                                                                                                    ),
                                                                                                    {
                                                                                                        formFields:
                                                                                                            formFields,
                                                                                                        closeSlideOver:
                                                                                                            closeSlideOver,
                                                                                                        title: title,
                                                                                                        description:
                                                                                                            description,
                                                                                                        openNotification:
                                                                                                            openNotification,
                                                                                                    }
                                                                                                ),
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                }
                                                            ),
                                                        ],
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                                show &&
                                    message &&
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                        Object(
                                            (function webpackMissingModule() {
                                                var e = new Error(
                                                    "Cannot find module '@/Pages/Shared/General/Notification'"
                                                );
                                                e.code = "MODULE_NOT_FOUND";
                                                throw e;
                                            })()
                                        ),
                                        {
                                            show: show,
                                            title: message,
                                            closeNotification:
                                                closeNotification,
                                            messageType: messageType,
                                        }
                                    ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    SlideOverLayout;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/Tabs.jsx":
            /*!***************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/TabContainer.jsx ***!
  \***************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var tabs = [
                    {
                        name: "Ball Text",
                        href: "balltext.index",
                        current: true,
                    },
                    {
                        name: "Point Text",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Work Style",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Reference Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Control Point",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Questions",
                        href: "balltext.index",
                        current: false,
                    },
                ];

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var Tabs = function Tabs() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        "div",
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                    "div",
                                    {
                                        className: "sm:hidden",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "label",
                                                {
                                                    htmlFor: "tabs",
                                                    className: "sr-only",
                                                    children: "Select a tab",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "select",
                                                {
                                                    id: "tabs",
                                                    name: "tabs",
                                                    className:
                                                        "block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md",
                                                    defaultValue: tabs.find(
                                                        function (tab) {
                                                            return tab.current;
                                                        }
                                                    ).name,
                                                    children: tabs.map(
                                                        function (tab) {
                                                            return /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "option",
                                                                {
                                                                    children:
                                                                        tab.name,
                                                                },
                                                                tab.name
                                                            );
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    "div",
                                    {
                                        className: "hidden sm:block",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                            "div",
                                            {
                                                className:
                                                    "border-b border-gray-200",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "nav",
                                                    {
                                                        className:
                                                            "flex space-x-4",
                                                        "aria-label": "Tabs",
                                                        children: tabs.map(
                                                            function (tab) {
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: tab.href,
                                                                        className:
                                                                            classNames(
                                                                                tab.current
                                                                                    ? "bg-violet-100 text-violet-700 mb-4 shadow-sm"
                                                                                    : "text-gray-500 hover:text-violet-700 hover:bg-violet-100 hover:mb-4",
                                                                                "px-3 py-2 font-medium text-sm rounded-md"
                                                                            ),
                                                                        "aria-current":
                                                                            tab.current
                                                                                ? "page"
                                                                                : undefined,
                                                                        children:
                                                                            tab.name,
                                                                    },
                                                                    tab.name
                                                                );
                                                            }
                                                        ),
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Tabs;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/TemplateContainer.jsx":
            /*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/TemplateContainer.jsx ***!
  \****************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                Object(
                    (function webpackMissingModule() {
                        var e = new Error(
                            "Cannot find module '@/Pages/Shared/General/SearchWithSortForm'"
                        );
                        e.code = "MODULE_NOT_FOUND";
                        throw e;
                    })()
                );
                /* harmony import */ var _Pages_Shared_Layout_SlideOverLayout__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Layout/SlideOverLayout */ "./resources/js/Pages/Shared/Layout/SlideOverLayout.jsx"
                    );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );
                function _slicedToArray(arr, i) {
                    return (
                        _arrayWithHoles(arr) ||
                        _iterableToArrayLimit(arr, i) ||
                        _unsupportedIterableToArray(arr, i) ||
                        _nonIterableRest()
                    );
                }

                function _nonIterableRest() {
                    throw new TypeError(
                        "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                    );
                }

                function _unsupportedIterableToArray(o, minLen) {
                    if (!o) return;
                    if (typeof o === "string")
                        return _arrayLikeToArray(o, minLen);
                    var n = Object.prototype.toString.call(o).slice(8, -1);
                    if (n === "Object" && o.constructor) n = o.constructor.name;
                    if (n === "Map" || n === "Set") return Array.from(o);
                    if (
                        n === "Arguments" ||
                        /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                    )
                        return _arrayLikeToArray(o, minLen);
                }

                function _arrayLikeToArray(arr, len) {
                    if (len == null || len > arr.length) len = arr.length;
                    for (var i = 0, arr2 = new Array(len); i < len; i++) {
                        arr2[i] = arr[i];
                    }
                    return arr2;
                }

                function _iterableToArrayLimit(arr, i) {
                    var _i =
                        arr == null
                            ? null
                            : (typeof Symbol !== "undefined" &&
                                  arr[Symbol.iterator]) ||
                              arr["@@iterator"];
                    if (_i == null) return;
                    var _arr = [];
                    var _n = true;
                    var _d = false;
                    var _s, _e;
                    try {
                        for (
                            _i = _i.call(arr);
                            !(_n = (_s = _i.next()).done);
                            _n = true
                        ) {
                            _arr.push(_s.value);
                            if (i && _arr.length === i) break;
                        }
                    } catch (err) {
                        _d = true;
                        _e = err;
                    } finally {
                        try {
                            if (!_n && _i["return"] != null) _i["return"]();
                        } finally {
                            if (_d) throw _e;
                        }
                    }
                    return _arr;
                }

                function _arrayWithHoles(arr) {
                    if (Array.isArray(arr)) return arr;
                }

                var sortOptions = [
                    {
                        name: "ID",
                        href: "#",
                        current: true,
                    },
                    {
                        name: "Text",
                        href: "#",
                        current: false,
                    },
                    {
                        name: "Priority",
                        href: "#",
                        current: false,
                    },
                ];

                var TemplateContainer = function TemplateContainer(_ref) {
                    var children = _ref.children,
                        title = _ref.title,
                        formFields = _ref.formFields;

                    var _useState = (0,
                        react__WEBPACK_IMPORTED_MODULE_2__.useState)(false),
                        _useState2 = _slicedToArray(_useState, 2),
                        openSlideOver = _useState2[0],
                        setOpenSlideOver = _useState2[1];

                    var closeSlideOver = function closeSlideOver() {
                        setOpenSlideOver(false);
                    };

                    var createTitle = "New ".concat(title);
                    var createDescription =
                        "Get started by filling in the information below to create a new ".concat(
                            title,
                            "."
                        );
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "px-3 mt-3 pb-5 border-b border-gray-200 sm:flex sm:items-center sm:justify-between",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                "h3",
                                                {
                                                    className:
                                                        "text-lg leading-6 font-medium text-gray-600",
                                                    children: title,
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                                "div",
                                                {
                                                    className:
                                                        "mt-3 flex sm:mt-0 sm:ml-4",
                                                    children: [
                                                        title !== "Settings" &&
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                Object(
                                                                    (function webpackMissingModule() {
                                                                        var e =
                                                                            new Error(
                                                                                "Cannot find module '@/Pages/Shared/General/SearchWithSortForm'"
                                                                            );
                                                                        e.code =
                                                                            "MODULE_NOT_FOUND";
                                                                        throw e;
                                                                    })()
                                                                ),
                                                                {
                                                                    title: title,
                                                                    sortOptions:
                                                                        sortOptions,
                                                                }
                                                            ),
                                                        title !== "Settings" &&
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                "button",
                                                                {
                                                                    type: "button",
                                                                    className:
                                                                        "ml-3 inline-flex items-center px-4 py-2 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 bg-white hover:bg-gray-50 focus:outline-none",
                                                                    children:
                                                                        "Simulate",
                                                                }
                                                            ),
                                                        title !== "Settings" &&
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                "button",
                                                                {
                                                                    type: "button",
                                                                    className:
                                                                        "ml-3 inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-violet-600 hover:bg-violet-700 focus:outline-none",
                                                                    onClick:
                                                                        function onClick() {
                                                                            return setOpenSlideOver(
                                                                                true
                                                                            );
                                                                        },
                                                                    children:
                                                                        "Create",
                                                                }
                                                            ),
                                                    ],
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(
                                    "div",
                                    {
                                        className: "mt-1",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                "main",
                                                {
                                                    className: "flex-1",
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                        "div",
                                                        {
                                                            className:
                                                                "relative max-w-4xl mx-auto md:px-8 xl:px-0",
                                                            children:
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                    "div",
                                                                    {
                                                                        className:
                                                                            "pt-10 pb-16",
                                                                        children:
                                                                            /*#__PURE__*/ (0,
                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                                                "div",
                                                                                {
                                                                                    className:
                                                                                        "px-4 sm:px-6 md:px-0",
                                                                                    children:
                                                                                        children,
                                                                                }
                                                                            ),
                                                                    }
                                                                ),
                                                        }
                                                    ),
                                                }
                                            ),
                                            title !== "Settings" &&
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                    _Pages_Shared_Layout_SlideOverLayout__WEBPACK_IMPORTED_MODULE_1__[
                                                        "default"
                                                    ],
                                                    {
                                                        open: openSlideOver,
                                                        closeSlideOver:
                                                            closeSlideOver,
                                                        title: createTitle,
                                                        formFields: formFields,
                                                        description:
                                                            createDescription,
                                                    }
                                                ),
                                        ],
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TemplateContainer;

                /***/
            },
    },
]);

(self["webpackChunk"] = self["webpackChunk"] || []).push([["/js/app"],{

/***/ "./resources/js/Hooks/useInitial.ts":
/*!******************************************!*\
  !*** ./resources/js/Hooks/useInitial.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var useInitial = function useInitial(name) {
  if (!name) {
    return;
  }

  return name.split(' ').map(function (word) {
    return word[0];
  }).join('');
};

exports["default"] = useInitial;

/***/ }),

/***/ "./resources/js/Hooks/useRoute.ts":
/*!****************************************!*\
  !*** ./resources/js/Hooks/useRoute.ts ***!
  \****************************************/
/***/ ((__unused_webpack_module, exports) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

function useRoute() {
  function routeWrapper(name, params, absolute) {
    return window.route(name, params, absolute);
  }

  return routeWrapper;
}

exports["default"] = useRoute;

/***/ }),

/***/ "./resources/js/Hooks/useTimeout.ts":
/*!******************************************!*\
  !*** ./resources/js/Hooks/useTimeout.ts ***!
  \******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __spreadArray = this && this.__spreadArray || function (to, from, pack) {
  if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
    if (ar || !(i in from)) {
      if (!ar) ar = Array.prototype.slice.call(from, 0, i);
      ar[i] = from[i];
    }
  }
  return to.concat(ar || Array.prototype.slice.call(from));
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var useTimeout = function useTimeout() {
  var timeout = (0, react_1.useRef)();

  var set = function set(handler, timeoutInMS) {
    var args = [];

    for (var _i = 2; _i < arguments.length; _i++) {
      args[_i - 2] = arguments[_i];
    }

    timeout.current = window.setTimeout.apply(window, __spreadArray([handler, timeoutInMS], args, false));
  };

  var clear = function clear() {
    if (timeout.current !== undefined) {
      clearTimeout(timeout.current);
    }
  };

  return [set, clear];
};

exports["default"] = useTimeout;

/***/ }),

/***/ "./resources/js/Hooks/useTypedPage.ts":
/*!********************************************!*\
  !*** ./resources/js/Hooks/useTypedPage.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {

"use strict";


Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

function useTypedPage() {
  return (0, inertia_react_1.usePage)();
}

exports["default"] = useTypedPage;

/***/ }),

/***/ "./resources/js/Pages/Auth/ForgotPassword.tsx":
/*!****************************************************!*\
  !*** ./resources/js/Pages/Auth/ForgotPassword.tsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var ForgotPassword = function ForgotPassword() {
  var _a = (0, inertia_react_1.useForm)({
    email: ''
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      errors = _a.errors,
      post = _a.post;

  var route = (0, useRoute_1["default"])();

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    post(route('password.email'));
  };

  return react_1["default"].createElement("div", {
    className: "mx-auto max-w-7xl px-4 pt-20 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl"
  }, react_1["default"].createElement("div", {
    className: "bg-white shadow sm:rounded-lg"
  }, react_1["default"].createElement("div", {
    className: "px-4 py-5 sm:p-6"
  }, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-gray-500"
  }, "Forgot Password"), react_1["default"].createElement("div", {
    className: "mt-2 max-w-xl text-sm text-gray-500"
  }, react_1["default"].createElement("p", null, "Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.")), react_1["default"].createElement("form", {
    className: "mt-5 sm:flex sm:items-center",
    onSubmit: handleSubmit
  }, react_1["default"].createElement("div", {
    className: "w-full sm:max-w-xs"
  }, react_1["default"].createElement("label", {
    htmlFor: "email",
    className: "sr-only"
  }, "Email"), react_1["default"].createElement("input", {
    type: "text",
    name: "email",
    id: "email",
    className: "block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-500 focus:ring-gray-500 sm:text-sm",
    placeholder: "you@example.com",
    value: data.email,
    onChange: function onChange(e) {
      return setData('email', e.target.value);
    }
  }), errors.email && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "email-error"
  }, errors.email)), react_1["default"].createElement("div", {
    className: "flex items-center justify-between"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-violet-600 px-4 py-2 font-medium text-white shadow-sm hover:bg-violet-700 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm",
    disabled: processing
  }, "Send"), react_1["default"].createElement("div", null, react_1["default"].createElement(inertia_react_1.Link, {
    href: route('login'),
    className: "mt-3 inline-flex w-full items-center justify-center rounded-md border border-transparent bg-white px-4 py-2 font-medium text-gray-600 shadow-sm hover:bg-gray-50 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
  }, "Cancel"))))))));
};

exports["default"] = ForgotPassword;

/***/ }),

/***/ "./resources/js/Pages/Auth/Login.tsx":
/*!*******************************************!*\
  !*** ./resources/js/Pages/Auth/Login.tsx ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var solid_1 = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var GuestLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/GuestLayout */ "./resources/js/Shared/Components/Layout/GuestLayout.tsx"));

var Login = function Login() {
  var _a = (0, inertia_react_1.useForm)({
    email: '',
    password: '',
    remember: ''
  }),
      data = _a.data,
      setData = _a.setData,
      post = _a.post,
      processing = _a.processing,
      errors = _a.errors;

  var route = (0, useRoute_1["default"])();

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    post(route('login'));
  };

  var t = (0, react_i18next_1.useTranslation)().t;
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "flex min-h-full flex-col justify-center bg-white py-12 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "sm:mx-auto sm:w-full sm:max-w-md"
  }, react_1["default"].createElement("h2", {
    className: "mt-6 text-center text-3xl font-extrabold text-gray-600"
  }, t('login.Headline.Sign.Into.Account.Label'))), react_1["default"].createElement("div", {
    className: "mt-8 sm:mx-auto sm:w-full sm:max-w-md"
  }, react_1["default"].createElement("div", {
    className: "bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10"
  }, react_1["default"].createElement("form", {
    className: "space-y-6",
    onSubmit: handleSubmit
  }, errors.email && errors.email.startsWith('These') && react_1["default"].createElement("div", {
    className: "rounded-md bg-red-50 p-4"
  }, react_1["default"].createElement("div", {
    className: "flex"
  }, react_1["default"].createElement("div", {
    className: "flex-shrink-0"
  }, react_1["default"].createElement(solid_1.XCircleIcon, {
    className: "h-5 w-5 text-red-400",
    "aria-hidden": "true"
  })), react_1["default"].createElement("div", {
    className: "ml-3"
  }, react_1["default"].createElement("h3", {
    className: "text-sm font-medium text-red-800"
  }, t('errors.Whoops!!')), react_1["default"].createElement("div", {
    className: "mt-2 text-sm text-red-700"
  }, react_1["default"].createElement("ul", {
    role: "list",
    className: "list-disc space-y-1 pl-5"
  }, react_1["default"].createElement(react_i18next_1.Trans, {
    i18nKey: "errors.Non.matching.Credentials"
  }, react_1["default"].createElement("li", null, errors.email))))))), react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "email",
    className: "block text-sm font-medium text-gray-700"
  }, t('login.Form.E-mail')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "email-address",
    name: "email",
    type: "email",
    autoComplete: "email",
    placeholder: "john.doe@example.com",
    "aria-invalid": "true",
    "aria-describedby": "email-error",
    required: true,
    value: data.email,
    onChange: function onChange(e) {
      return setData('email', e.currentTarget.value);
    },
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-fuchsia-700 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.email && !errors.email.startsWith('These') && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "email-error"
  }, errors.email)), react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "password",
    className: "block text-sm font-medium text-gray-700"
  }, t('login.Form.Password')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "password",
    name: "password",
    type: "password",
    autoComplete: "current-password",
    placeholder: t('login.Form.Password'),
    required: true,
    value: data.password,
    onChange: function onChange(e) {
      return setData('password', e.currentTarget.value);
    },
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-fuchsia-700 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.password && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "email-error"
  }, errors.password)), react_1["default"].createElement("div", {
    className: "flex items-center justify-between"
  }, react_1["default"].createElement("div", {
    className: "flex items-center"
  }, react_1["default"].createElement("input", {
    id: "remember-me",
    name: "remember-me",
    type: "checkbox",
    value: data.remember,
    onChange: function onChange(e) {
      return setData('remember', e.currentTarget.checked ? 'on' : '');
    },
    className: "h-4 w-4 rounded text-fuchsia-600 focus:border-gray-300 focus:ring-fuchsia-500"
  }), react_1["default"].createElement("label", {
    htmlFor: "remember-me",
    className: "ml-2 block text-sm text-gray-600"
  }, t('login.Form.Remember.Me'))), react_1["default"].createElement("div", {
    className: "text-sm"
  }, react_1["default"].createElement("a", {
    href: route('password.request'),
    className: "font-medium text-fuchsia-900 hover:text-fuchsia-600"
  }, t('login.Form.Forgot.Password')))), react_1["default"].createElement("div", null, react_1["default"].createElement("button", {
    type: "submit",
    className: "flex w-full justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-3 px-2 font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none",
    disabled: processing
  }, t('login.Form.Log.In')))), react_1["default"].createElement("div", {
    className: "mt-6"
  }, react_1["default"].createElement("div", {
    className: "relative"
  }, react_1["default"].createElement("div", {
    className: "absolute inset-0 flex items-center"
  }, react_1["default"].createElement("div", {
    className: "w-full border-t border-gray-300"
  })), react_1["default"].createElement("div", {
    className: "relative flex justify-center text-sm"
  }, react_1["default"].createElement("span", {
    className: "bg-white px-2 text-gray-500"
  }, "Or continue with"))), react_1["default"].createElement("div", {
    className: "mt-6 grid grid-cols-3 gap-3"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("a", {
    href: "#",
    className: "inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
  }, react_1["default"].createElement("span", {
    className: "sr-only"
  }, "Sign in with Facebook"), react_1["default"].createElement("svg", {
    className: "h-5 w-5",
    "aria-hidden": "true",
    fill: "currentColor",
    viewBox: "0 0 20 20"
  }, react_1["default"].createElement("path", {
    fillRule: "evenodd",
    d: "M20 10c0-5.523-4.477-10-10-10S0 4.477 0 10c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V10h2.54V7.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V10h2.773l-.443 2.89h-2.33v6.988C16.343 19.128 20 14.991 20 10z",
    clipRule: "evenodd"
  })))), react_1["default"].createElement("div", null, react_1["default"].createElement("a", {
    href: "#",
    className: "inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
  }, react_1["default"].createElement("span", {
    className: "sr-only"
  }, "Sign in with Twitter"), react_1["default"].createElement("svg", {
    className: "h-5 w-5",
    "aria-hidden": "true",
    fill: "currentColor",
    viewBox: "0 0 20 20"
  }, react_1["default"].createElement("path", {
    d: "M6.29 18.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0020 3.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.073 4.073 0 01.8 7.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 010 16.407a11.616 11.616 0 006.29 1.84"
  })))), react_1["default"].createElement("div", null, react_1["default"].createElement("a", {
    href: "#",
    className: "inline-flex w-full justify-center rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-500 shadow-sm hover:bg-gray-50"
  }, react_1["default"].createElement("span", {
    className: "sr-only"
  }, "Sign in with GitHub"), react_1["default"].createElement("svg", {
    className: "h-5 w-5",
    "aria-hidden": "true",
    fill: "currentColor",
    viewBox: "0 0 20 20"
  }, react_1["default"].createElement("path", {
    fillRule: "evenodd",
    d: "M10 0C4.477 0 0 4.484 0 10.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0110 4.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.203 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.942.359.31.678.921.678 1.856 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0020 10.017C20 4.484 15.522 0 10 0z",
    clipRule: "evenodd"
  }))))))))));
};

Login.layout = function (page) {
  return react_1["default"].createElement(GuestLayout_1["default"], {
    children: page
  });
};

exports["default"] = Login;

/***/ }),

/***/ "./resources/js/Pages/Auth/Register.tsx":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Auth/Register.tsx ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var GuestLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/GuestLayout */ "./resources/js/Shared/Components/Layout/GuestLayout.tsx"));

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var initialValue = {
  name: '',
  email: '',
  password: '',
  password_confirmation: '',
  terms: false
};

var Register = function Register() {
  var t = (0, react_i18next_1.useTranslation)().t;
  var props = (0, useTypedPage_1["default"])().props;
  var jetstream = props.jetstream;
  var route = (0, useRoute_1["default"])();

  var _a = (0, inertia_react_1.useForm)(initialValue),
      data = _a.data,
      setData = _a.setData,
      post = _a.post,
      processing = _a.processing,
      errors = _a.errors,
      reset = _a.reset;

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    post(route('register'), {
      onFinish: function onFinish() {
        return reset('password', 'password_confirmation');
      }
    });
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "flex min-h-full flex-col justify-center bg-white py-12 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "mt-8 sm:mx-auto sm:w-full sm:max-w-md"
  }, react_1["default"].createElement("div", {
    className: "bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10"
  }, react_1["default"].createElement("form", {
    className: "space-y-6",
    onSubmit: handleSubmit
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "full-name",
    className: "block text-sm font-medium text-gray-700"
  }, t('register.Full.Name.Label')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "fullName",
    name: "name",
    type: "text",
    autoComplete: "name",
    placeholder: "John Doe",
    "aria-invalid": "true",
    "aria-describedby": "name-error",
    value: data.name,
    onChange: function onChange(e) {
      return setData('name', e.target.value);
    },
    required: true,
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.name && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "name-error"
  }, errors.name)), react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "email-address",
    className: "block text-sm font-medium text-gray-700"
  }, t('login.Form.E-mail')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "email-address",
    name: "email",
    type: "email",
    autoComplete: "email",
    placeholder: "john.doe@example.com",
    "aria-invalid": "true",
    "aria-describedby": "email-error",
    value: data.email,
    onChange: function onChange(e) {
      return setData('email', e.target.value);
    },
    required: true,
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.email && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "email-error"
  }, errors.email)), react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "password",
    className: "block text-sm font-medium text-gray-700"
  }, t('login.Form.Password')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "password",
    name: "password",
    type: "password",
    autoComplete: "current-password",
    placeholder: t('login.Form.Password'),
    value: data.password,
    onChange: function onChange(e) {
      return setData('password', e.target.value);
    },
    required: true,
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.password && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, errors.password)), react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "password-confirmation",
    className: "block text-sm font-medium text-gray-700"
  }, t('register.Password.Confirmation.Label')), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "password-confirmation",
    name: "password_confirmation",
    type: "password",
    autoComplete: "current-password",
    placeholder: t('register.Password.Confirmation.Label'),
    value: data.password_confirmation,
    onChange: function onChange(e) {
      return setData('password_confirmation', e.target.value);
    },
    required: true,
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-fuchsia-700 sm:text-sm"
  })), errors.password_confirmation && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, errors.password_confirmation)), react_1["default"].createElement("div", {
    className: "flex items-center justify-between"
  }, react_1["default"].createElement("div", {
    className: "flex items-center"
  }, jetstream.hasTermsAndPrivacyPolicyFeature && react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "flex items-center"
  }, react_1["default"].createElement("input", {
    id: "terms",
    name: "terms",
    type: "checkbox",
    className: "h-4 w-4 rounded border-gray-300 text-fuchsia-700 focus:ring-fuchsia-700",
    checked: data.terms,
    onChange: function onChange(e) {
      return setData('terms', e.target.checked);
    },
    required: true
  })), react_1["default"].createElement("div", {
    className: "ml-2"
  }, react_1["default"].createElement("label", {
    htmlFor: "terms",
    className: "text-sm font-medium text-gray-900"
  }, react_1["default"].createElement(react_i18next_1.Trans, {
    i18nKey: "register.Terms.And.Privacy.Policy"
  }, "I agree to the", ' ', react_1["default"].createElement("a", {
    target: "_blank",
    href: route('terms.show'),
    className: "text-sm text-gray-500 underline hover:text-gray-900"
  }, "Terms of Service"), ' ', "and", ' ', react_1["default"].createElement("a", {
    target: "_blank",
    href: route('policy.show'),
    className: "text-sm text-gray-500 underline hover:text-gray-900"
  }, "Privacy Policy")))), errors.terms && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, errors.terms)))), react_1["default"].createElement("div", {
    className: "flex items-center justify-between"
  }, react_1["default"].createElement("div", {
    className: "text-sm"
  }, react_1["default"].createElement(inertia_react_1.Link, {
    href: route('login'),
    className: "font-medium text-fuchsia-900 hover:text-fuchsia-500"
  }, t('register.Already.Registered.Link'))), react_1["default"].createElement("div", {
    className: "flex items-center"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "flex w-full justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none",
    disabled: processing
  }, t('general.Create')))))))));
};

Register.layout = function (page) {
  return react_1["default"].createElement(GuestLayout_1["default"], {
    children: page
  });
};

exports["default"] = Register;

/***/ }),

/***/ "./resources/js/Pages/Auth/TwoFactorChallenge.tsx":
/*!********************************************************!*\
  !*** ./resources/js/Pages/Auth/TwoFactorChallenge.tsx ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_2 = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var TwoFactorChallenge = function TwoFactorChallenge() {
  var route = (0, useRoute_1["default"])();

  var _a = (0, inertia_react_1.useForm)({
    code: '',
    recovery_code: ''
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      errors = _a.errors,
      post = _a.post;

  var _b = (0, react_2.useState)(false),
      isRecovery = _b[0],
      setIsRecovery = _b[1];

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    post(route('two-factor.login'), {
      preserveState: false
    });
  };

  var handleToggleRecovery = function handleToggleRecovery(e) {
    e.preventDefault();
    setIsRecovery(function (isRecovery) {
      return !isRecovery;
    });
  };

  return react_1["default"].createElement("div", {
    className: "flex min-h-screen flex-col justify-center bg-gray-50 py-12 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "mt-8 sm:mx-auto sm:w-full sm:max-w-md"
  }, react_1["default"].createElement("div", {
    className: "bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10"
  }, isRecovery ? react_1["default"].createElement("p", {
    className: "block text-sm font-medium text-gray-700"
  }, "Please confirm access to your account by entering one of your emergency recovery codes.") : react_1["default"].createElement("p", {
    className: "block text-sm font-medium text-gray-700"
  }, "Please confirm access to your account by entering the authentication code provided by your authenticator application."), react_1["default"].createElement("form", {
    className: "mt-3 space-y-6",
    onSubmit: handleSubmit
  }, !isRecovery ? react_1["default"].createElement("div", null, react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "code",
    name: "code",
    type: "text",
    autoComplete: "one-time-code",
    placeholder: "Authentication Code",
    value: data.code,
    onChange: function onChange(e) {
      return setData('code', e.target.value);
    },
    required: true,
    inputMode: "numeric",
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-gray-500 sm:text-sm"
  })), errors.code && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "code-error"
  }, errors.code)) : react_1["default"].createElement("div", null, react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("input", {
    id: "recovery_code",
    name: "recovery_code",
    type: "text",
    autoComplete: "one-time-code",
    placeholder: "Recovery code",
    value: data.recovery_code,
    onChange: function onChange(e) {
      return setData('recovery_code', e.target.value);
    },
    required: true,
    className: "block w-full appearance-none rounded-md border border-gray-300 px-3 py-2 placeholder-gray-400 shadow-sm focus:border-gray-500 focus:outline-none focus:ring-gray-500 sm:text-sm"
  })), errors.recovery_code && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "recovery_code-error"
  }, errors.recovery_code)), react_1["default"].createElement("div", {
    className: "flex items-center justify-between"
  }, react_1["default"].createElement("div", {
    className: "text-sm"
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "cursor-pointer text-sm text-gray-600 underline hover:text-gray-900",
    onClick: handleToggleRecovery
  }, isRecovery ? 'Use an authentication code' : 'Use a recovery code')), react_1["default"].createElement("div", {
    className: "flex items-center"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "flex w-full justify-center rounded-md border border-transparent bg-violet-700 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-violet-400 focus:outline-none",
    disabled: processing
  }, "Login")))))));
};

exports["default"] = TwoFactorChallenge;

/***/ }),

/***/ "./resources/js/Pages/Home/Index.tsx":
/*!*******************************************!*\
  !*** ./resources/js/Pages/Home/Index.tsx ***!
  \*******************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ActivityReminder_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/HomePageComponent/ActivityReminder */ "./resources/js/Shared/Components/HomePageComponent/ActivityReminder.tsx"));

var RecentOpeningAnnouncement_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/HomePageComponent/RecentOpeningAnnouncement */ "./resources/js/Shared/Components/HomePageComponent/RecentOpeningAnnouncement.tsx"));

var BlogPanel_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/HomePageComponent/BlogPanel */ "./resources/js/Shared/Components/HomePageComponent/BlogPanel.tsx"));

var AppLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/AppLayout */ "./resources/js/Shared/Components/Layout/AppLayout.tsx"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var EmptyProfileImage_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Image/EmptyProfileImage */ "./resources/js/Shared/Components/Image/EmptyProfileImage.tsx"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var Index = function Index() {
  var _a = (0, useTypedPage_1["default"])().props,
      _b = _a.user,
      loggedInUserName = _b.loggedInUserName,
      loggedInUserEmail = _b.loggedInUserEmail,
      imageUrl = _a.pageData.imageUrl;
  var route = (0, useRoute_1["default"])();
  var photoUrl = imageUrl || '';
  return react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("h1", {
    className: "sr-only"
  }, "Profile"), react_1["default"].createElement("div", {
    className: "grid grid-cols-1 items-start gap-4 lg:grid-cols-3 lg:gap-8"
  }, react_1["default"].createElement("div", {
    className: "grid grid-cols-1 gap-4 lg:col-span-2"
  }, react_1["default"].createElement("section", {
    "aria-labelledby": "profile-overview-title"
  }, react_1["default"].createElement("div", {
    className: "overflow-hidden rounded-lg bg-white shadow"
  }, react_1["default"].createElement("h2", {
    className: "sr-only",
    id: "profile-overview-title"
  }, "Profile Overview"), react_1["default"].createElement("div", {
    className: "bg-white p-6"
  }, react_1["default"].createElement("div", {
    className: "sm:flex sm:items-center sm:justify-between"
  }, react_1["default"].createElement("div", {
    className: "sm:flex sm:space-x-5"
  }, react_1["default"].createElement("div", {
    className: "flex-shrink-0"
  }, photoUrl ? react_1["default"].createElement("img", {
    className: "mx-auto h-20 w-20 rounded-full",
    src: photoUrl,
    alt: ""
  }) : react_1["default"].createElement(EmptyProfileImage_1["default"], {
    classes: "inline-block h-20 w-20 overflow-hidden rounded-full bg-gray-100"
  })), react_1["default"].createElement("div", {
    className: "mt-4 text-center sm:mt-0 sm:pt-1 sm:text-left"
  }, react_1["default"].createElement("p", {
    className: "text-sm font-medium text-slate-600"
  }, "Welcome back,"), react_1["default"].createElement("p", {
    className: "text-xl font-bold text-slate-800 sm:text-2xl"
  }, loggedInUserName), react_1["default"].createElement("p", {
    className: "text-sm font-medium text-slate-600"
  }, loggedInUserEmail))), react_1["default"].createElement("div", {
    className: "mt-5 flex justify-center sm:mt-0"
  }, react_1["default"].createElement(inertia_react_1.Link, {
    href: route('profile.show'),
    className: "flex items-center justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-2 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
  }, "Complete your profile")))))), react_1["default"].createElement(BlogPanel_1["default"], null)), react_1["default"].createElement("div", {
    className: "grid grid-cols-1 gap-4"
  }, react_1["default"].createElement(RecentOpeningAnnouncement_1["default"], null), react_1["default"].createElement(ActivityReminder_1["default"], null))));
};

Index.layout = function (page) {
  return react_1["default"].createElement(AppLayout_1["default"], {
    children: page,
    title: "Home"
  });
};

exports["default"] = Index;

/***/ }),

/***/ "./resources/js/Pages/PrivacyPolicy.tsx":
/*!**********************************************!*\
  !*** ./resources/js/Pages/PrivacyPolicy.tsx ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PrivacyPolicy = function PrivacyPolicy() {
  return react_1["default"].createElement("div", {
    className: "overflow-hidden bg-gray-50 py-16"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-7xl space-y-8 px-4 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-prose text-base lg:max-w-none"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-semibold uppercase tracking-wide text-gray-600"
  }, "Privacy Policy"), react_1["default"].createElement("p", {
    className: "mt-2 text-3xl font-extrabold leading-8 tracking-tight text-gray-900 sm:text-4xl"
  }, "What makes us different")), react_1["default"].createElement("div", {
    className: "relative z-10 mx-auto max-w-prose text-base lg:mx-0 lg:max-w-5xl lg:pr-72"
  }, react_1["default"].createElement("p", {
    className: "text-lg text-gray-500"
  }, "Sagittis scelerisque nulla cursus in enim consectetur quam. Dictum urna sed consectetur neque tristique pellentesque. Blandit amet, sed aenean erat arcu morbi. Cursus faucibus nunc nisl netus morbi vel porttitor vitae ut. Amet vitae fames senectus vitae.")), react_1["default"].createElement("div", {
    className: "lg:grid lg:grid-cols-2 lg:items-start lg:gap-8"
  }, react_1["default"].createElement("div", {
    className: "relative z-10"
  }, react_1["default"].createElement("div", {
    className: "prose prose-indigo mx-auto text-gray-500 lg:max-w-none"
  }, react_1["default"].createElement("p", null, "Sollicitudin tristique eros erat odio sed vitae, consequat turpis elementum. Lorem nibh vel, eget pretium arcu vitae. Eros eu viverra donec ut volutpat donec laoreet quam urna."), react_1["default"].createElement("ul", {
    role: "list"
  }, react_1["default"].createElement("li", null, "Quis elit egestas venenatis mattis dignissim."), react_1["default"].createElement("li", null, "Cras cras lobortis vitae vivamus ultricies facilisis tempus."), react_1["default"].createElement("li", null, "Orci in sit morbi dignissim metus diam arcu pretium.")), react_1["default"].createElement("p", null, "Rhoncus nisl, libero egestas diam fermentum dui. At quis tincidunt vel ultricies. Vulputate aliquet velit faucibus semper. Pellentesque in venenatis vestibulum consectetur nibh id. In id ut tempus egestas. Enim sit aliquam nec, a. Morbi enim fermentum lacus in. Viverra."), react_1["default"].createElement("h3", null, "We\u2019re here to help"), react_1["default"].createElement("p", null, "Tincidunt integer commodo, cursus etiam aliquam neque, et. Consectetur pretium in volutpat, diam. Montes, magna cursus nulla feugiat dignissim id lobortis amet. Laoreet sem est phasellus eu proin massa, lectus. Diam rutrum posuere donec ultricies non morbi. Mi a platea auctor mi.")), react_1["default"].createElement("div", {
    className: "mx-auto mt-10 flex max-w-prose text-base lg:max-w-none"
  }, react_1["default"].createElement("div", {
    className: "rounded-md shadow"
  }, react_1["default"].createElement("a", {
    href: "#",
    className: "flex w-full items-center justify-center rounded-md border border-transparent bg-gray-600 px-5 py-3 text-base font-medium text-white hover:bg-gray-700"
  }, "Contact Us")))), react_1["default"].createElement("div", {
    className: "relative mx-auto mt-12 max-w-prose text-base lg:mt-0 lg:max-w-none"
  }, react_1["default"].createElement("svg", {
    className: "absolute top-0 right-0 -mt-20 -mr-20 lg:top-auto lg:right-auto lg:bottom-1/2 lg:left-1/2 lg:mt-0 lg:mr-0 xl:top-0 xl:right-0 xl:-mt-20 xl:-mr-20",
    width: 404,
    height: 384,
    fill: "none",
    viewBox: "0 0 404 384",
    "aria-hidden": "true"
  }, react_1["default"].createElement("defs", null, react_1["default"].createElement("pattern", {
    id: "bedc54bc-7371-44a2-a2bc-dc68d819ae60",
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    patternUnits: "userSpaceOnUse"
  }, react_1["default"].createElement("rect", {
    x: 0,
    y: 0,
    width: 4,
    height: 4,
    className: "text-gray-200",
    fill: "currentColor"
  }))), react_1["default"].createElement("rect", {
    width: 404,
    height: 384,
    fill: "url(#bedc54bc-7371-44a2-a2bc-dc68d819ae60)"
  })), react_1["default"].createElement("blockquote", {
    className: "relative rounded-lg bg-white shadow-lg"
  }, react_1["default"].createElement("div", {
    className: "rounded-t-lg px-6 py-8 sm:px-10 sm:pt-10 sm:pb-8"
  }, react_1["default"].createElement("img", {
    src: "https://tailwindui.com/img/logos/workcation-logo-gray-600-mark-gray-800-and-gray-600-text.svg",
    alt: "Workcation",
    className: "h-8"
  }), react_1["default"].createElement("div", {
    className: "relative mt-8 text-lg font-medium text-gray-700"
  }, react_1["default"].createElement("svg", {
    className: "absolute top-0 left-0 h-8 w-8 -translate-x-3 -translate-y-2 transform text-gray-200",
    fill: "currentColor",
    viewBox: "0 0 32 32",
    "aria-hidden": "true"
  }, react_1["default"].createElement("path", {
    d: "M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z"
  })), react_1["default"].createElement("p", {
    className: "relative"
  }, "Tincidunt integer commodo, cursus etiam aliquam neque, et. Consectetur pretium in volutpat, diam. Montes, magna cursus nulla feugiat dignissim id lobortis amet. Laoreet sem est phasellus eu proin massa, lectus."))), react_1["default"].createElement("cite", {
    className: "relative flex items-center rounded-b-lg bg-gray-600 py-5 px-6 not-italic sm:mt-10 sm:items-start sm:py-5 sm:pl-12 sm:pr-10"
  }, react_1["default"].createElement("div", {
    className: "relative rounded-full border-2 border-white sm:absolute sm:top-0 sm:-translate-y-1/2 sm:transform"
  }, react_1["default"].createElement("img", {
    className: "h-12 w-12 rounded-full bg-gray-300 sm:h-20 sm:w-20",
    src: "https://images.unsplash.com/photo-1500917293891-ef795e70e1f6?ixlib=rb-1.2.1&auto=format&fit=facearea&facepad=2.5&w=160&h=160&q=80",
    alt: ""
  })), react_1["default"].createElement("span", {
    className: "relative ml-4 font-semibold leading-6 text-gray-300 sm:ml-24 sm:pl-1"
  }, react_1["default"].createElement("p", {
    className: "font-semibold text-white sm:inline"
  }, "Judith Black"), ' ', react_1["default"].createElement("p", {
    className: "sm:inline"
  }, "CEO at Workcation"))))))));
};

exports["default"] = PrivacyPolicy;

/***/ }),

/***/ "./resources/js/Pages/Profile/Show.tsx":
/*!*********************************************!*\
  !*** ./resources/js/Pages/Profile/Show.tsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var AppLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/AppLayout */ "./resources/js/Shared/Components/Layout/AppLayout.tsx"));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var compressorjs_1 = __importDefault(__webpack_require__(/*! compressorjs */ "./node_modules/compressorjs/dist/compressor.js"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var Notification_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/Notification */ "./resources/js/Shared/Components/Modal/Notification.tsx"));

var EmptyProfileImage_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Image/EmptyProfileImage */ "./resources/js/Shared/Components/Image/EmptyProfileImage.tsx"));

var SimpleDropdown_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/FormElement/SimpleDropdown */ "./resources/js/Shared/Components/FormElement/SimpleDropdown.tsx"));

var Show = function Show() {
  var route = (0, useRoute_1["default"])();
  var props = (0, useTypedPage_1["default"])().props;
  var _a = props.user,
      loggedInUserName = _a.loggedInUserName,
      loggedInUserEmail = _a.loggedInUserEmail,
      errors = props.errors,
      _b = props.pageData,
      profile = _b.profile,
      languages = _b.languages,
      countries = _b.countries;
  var selectedCountry = countries.find(function (country) {
    return country.id === profile.country;
  }) || countries[0];
  var selectedLanguage = languages.find(function (language) {
    return language.id === profile.language;
  }) || languages[0];
  var imageUrl = profile.photoUrl || '';
  var updateProfileErrors = (errors === null || errors === void 0 ? void 0 : errors.updateProfile) || null;
  var initialProfileData = {
    name: loggedInUserName,
    email: loggedInUserEmail,
    description: profile.description || '',
    language: profile.language,
    phoneNumber: profile.phoneNumber || '',
    country: profile.country,
    photo: null
  };

  var _c = (0, inertia_react_1.useForm)(initialProfileData),
      data = _c.data,
      setData = _c.setData,
      processing = _c.processing,
      post = _c.post;

  var _d = (0, react_1.useState)(imageUrl),
      photoPreview = _d[0],
      setPhotoPreview = _d[1];

  var photoRef = (0, react_1.useRef)(null);

  var _e = (0, react_1.useState)(false),
      showNotification = _e[0],
      setShowNotification = _e[1];

  var updatePhotoPreview = function updatePhotoPreview() {
    var _a, _b;

    var photo = (_b = (_a = photoRef.current) === null || _a === void 0 ? void 0 : _a.files) === null || _b === void 0 ? void 0 : _b[0];

    if (!photo) {
      return;
    }

    var reader = new FileReader();

    reader.onload = function (e) {
      new compressorjs_1["default"](photo, {
        quality: 1,
        resize: 'cover',
        success: function success(compressedResult) {
          var _a; // compressedResult has the compressed file.
          // convert compressed photo from blob to file


          var compressedPhoto = new File([compressedResult], photo.name, {
            type: compressedResult.type
          });
          setData('photo', compressedPhoto);
          setPhotoPreview((_a = e.target) === null || _a === void 0 ? void 0 : _a.result);
        }
      });
    };

    reader.readAsDataURL(photo);
    photoRef.current.blur();
  };

  var handleOnProfileUpdate = function handleOnProfileUpdate(e) {
    e.preventDefault();
    post(route('profile.store'), {
      errorBag: 'updateProfile',
      preserveScroll: true,
      onSuccess: function onSuccess() {
        return handleOnUpdateSuccess();
      }
    });
  };

  (0, react_1.useEffect)(function () {
    setPhotoPreview(profile.photoUrl);
    return function () {};
  }, [props]);

  var handleOnUpdateSuccess = function handleOnUpdateSuccess() {
    setShowNotification(true);
  };

  var handleOnSelect = function handleOnSelect(prop, value) {
    setData(prop, value);
  };

  var closeNotification = function closeNotification() {
    setShowNotification(false);
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("form", {
    className: "sm:overflow-hidden sm:rounded-md",
    onSubmit: handleOnProfileUpdate
  }, react_1["default"].createElement("div", {
    className: "space-y-6 bg-white py-6 px-4 sm:p-6 lg:pb-8"
  }, react_1["default"].createElement("div", {
    className: "grid grid-cols-1 gap-y-6 sm:grid-cols-6 sm:gap-x-6"
  }, react_1["default"].createElement("div", {
    className: "mt-6 flex-grow lg:mt-0 lg:ml-6 lg:flex-shrink-0 lg:flex-grow-0"
  }, react_1["default"].createElement("p", {
    className: "text-sm font-medium text-slate-500",
    "aria-hidden": "true"
  }, "Photo"), react_1["default"].createElement("div", {
    className: "mt-1 lg:hidden"
  }, react_1["default"].createElement("div", {
    className: "flex items-center"
  }, react_1["default"].createElement("div", {
    className: "inline-block h-12 w-12 flex-shrink-0 overflow-hidden rounded-full",
    "aria-hidden": "true"
  }, imageUrl ? react_1["default"].createElement("img", {
    className: "h-full w-full rounded-full",
    src: imageUrl,
    alt: ""
  }) : react_1["default"].createElement(EmptyProfileImage_1["default"], {
    classes: "inline-block h-12 w-12 overflow-hidden rounded-full bg-gray-100"
  })), react_1["default"].createElement("div", {
    className: "ml-5 rounded-md shadow-sm"
  }, react_1["default"].createElement("div", {
    className: "group relative flex items-center justify-center rounded-md border border-gray-300 py-2 px-3 focus-within:ring-2 focus-within:ring-fuchsia-500 focus-within:ring-offset-2 hover:bg-gray-50"
  }, react_1["default"].createElement("label", {
    htmlFor: "mobile-user-photo",
    className: "pointer-events-none relative text-sm font-medium leading-4 text-gray-700"
  }, react_1["default"].createElement("span", null, "Change"), react_1["default"].createElement("span", {
    className: "sr-only"
  }, " user photo")), react_1["default"].createElement("input", {
    id: "mobile-user-photo",
    name: "user-photo",
    type: "file",
    className: "absolute h-full w-full cursor-pointer rounded-md border-gray-300 opacity-0"
  }))))), react_1["default"].createElement("div", {
    className: "relative hidden overflow-hidden rounded-full lg:block"
  }, photoPreview ? react_1["default"].createElement("div", {
    className: "mt-2"
  }, react_1["default"].createElement("img", {
    className: "relative h-40 w-40 rounded-full",
    src: photoPreview,
    alt: ""
  })) : imageUrl ? react_1["default"].createElement("img", {
    className: "relative h-40 w-40 rounded-full",
    src: imageUrl,
    alt: ""
  }) : react_1["default"].createElement(EmptyProfileImage_1["default"], {
    classes: "inline-block h-36 w-36 overflow-hidden rounded-full bg-gray-100"
  }), react_1["default"].createElement("label", {
    htmlFor: "desktop-user-photo",
    className: "absolute inset-0 flex h-full w-full items-center justify-center bg-black bg-opacity-75 text-sm font-medium text-white opacity-0 focus-within:opacity-100 hover:opacity-100"
  }, react_1["default"].createElement("span", null, "Change"), react_1["default"].createElement("span", {
    className: "sr-only"
  }, " user photo"), react_1["default"].createElement("input", {
    accept: "image/jpg, image/jpeg, image/png, image/svg, capture=camera",
    type: "file",
    id: "desktop-user-photo",
    name: "user-photo",
    className: "absolute inset-0 h-full w-full cursor-pointer rounded-md border-gray-300 opacity-0",
    ref: photoRef,
    onChange: function onChange() {
      return updatePhotoPreview();
    }
  }))), updateProfileErrors && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, updateProfileErrors.photo)), react_1["default"].createElement("div", {
    className: "sm:col-span-6"
  }), react_1["default"].createElement("div", {
    className: "sm:col-span-3"
  }, react_1["default"].createElement("label", {
    htmlFor: "first-name",
    className: "block text-sm font-medium text-gray-700"
  }, "Full Name"), react_1["default"].createElement("input", {
    type: "text",
    name: "first-name",
    id: "first-name",
    autoComplete: "given-name",
    className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    placeholder: "John Doe",
    value: data.name,
    onChange: function onChange(e) {
      setData('name', e.currentTarget.value);
    }
  })), react_1["default"].createElement("div", {
    className: "sm:col-span-3"
  }, react_1["default"].createElement("label", {
    htmlFor: "email-address",
    className: "block text-sm font-medium text-gray-700"
  }, "Email address"), react_1["default"].createElement("input", {
    type: "text",
    name: "email-address",
    id: "email-address",
    autoComplete: "email",
    className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    placeholder: "john.doe@example.com",
    value: data.email,
    onChange: function onChange(e) {
      return setData('email', e.currentTarget.value);
    }
  })), react_1["default"].createElement("div", {
    className: "sm:col-span-6"
  }, react_1["default"].createElement("label", {
    htmlFor: "description",
    className: "block text-sm font-medium text-gray-700"
  }, "Description"), react_1["default"].createElement("div", {
    className: "mt-1"
  }, react_1["default"].createElement("textarea", {
    id: "description",
    name: "description",
    rows: 4,
    className: "mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
    value: data.description,
    onChange: function onChange(e) {
      return setData('description', e.currentTarget.value);
    }
  })), react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-gray-500"
  }, "Brief description of you."), updateProfileErrors && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, updateProfileErrors.description))), react_1["default"].createElement("div", {
    className: "sm:col-span-6"
  }, react_1["default"].createElement("label", {
    htmlFor: "username",
    className: "text-blue-gray-900 block text-sm font-medium"
  }, "Social Account"), react_1["default"].createElement("div", {
    className: "mt-1 flex rounded-md shadow-sm"
  }, react_1["default"].createElement("span", {
    className: "inline-flex  items-center rounded-l-md border border-r-0 border-gray-300 px-3 sm:text-sm"
  }, "https://www.linkedin.com/"), react_1["default"].createElement("input", {
    type: "text",
    name: "linkedin",
    id: "linkedin",
    autoComplete: "linkedin",
    defaultValue: data.name,
    className: "block w-full min-w-0 flex-1 rounded-none rounded-r-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500"
  }))), react_1["default"].createElement("div", {
    className: "grid grid-cols-1 gap-y-6 pt-8 sm:grid-cols-6 sm:gap-x-6"
  }, react_1["default"].createElement("div", {
    className: "sm:col-span-6"
  }), react_1["default"].createElement("div", {
    className: "relative sm:col-span-3"
  }, react_1["default"].createElement(SimpleDropdown_1["default"], {
    dropDownData: languages,
    handleOnSelect: handleOnSelect,
    label: "Main Spoken Language",
    initialSelection: selectedLanguage,
    name: "language"
  }), updateProfileErrors && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, updateProfileErrors.language)), react_1["default"].createElement("div", {
    className: "sm:col-span-3"
  }, react_1["default"].createElement("label", {
    htmlFor: "phone-number",
    className: "block text-sm font-medium text-gray-700"
  }, "Phone number"), react_1["default"].createElement("input", {
    type: "text",
    name: "phone-number",
    id: "phone-number",
    autoComplete: "tel",
    className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    value: data.phoneNumber,
    onChange: function onChange(e) {
      return setData('phoneNumber', e.currentTarget.value);
    }
  }), updateProfileErrors && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, updateProfileErrors.phoneNumber)), react_1["default"].createElement("div", {
    className: "sm:col-span-3"
  }, react_1["default"].createElement(SimpleDropdown_1["default"], {
    dropDownData: countries,
    handleOnSelect: handleOnSelect,
    label: "Country",
    initialSelection: selectedCountry,
    name: "country"
  }))), react_1["default"].createElement("div", {
    className: "flex justify-end pt-8"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "ml-3 inline-flex justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none",
    disabled: processing
  }, "Save")))), showNotification && react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(Notification_1["default"], {
    show: showNotification,
    title: "Profile was successfully updated",
    closeNotification: closeNotification,
    messageType: "success"
  })));
};

Show.layout = function (page) {
  return react_1["default"].createElement(AppLayout_1["default"], {
    children: page,
    title: "Profile"
  });
};

exports["default"] = Show;

/***/ }),

/***/ "./resources/js/Pages/ResumeManager/Index.tsx":
/*!****************************************************!*\
  !*** ./resources/js/Pages/ResumeManager/Index.tsx ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var AppLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/AppLayout */ "./resources/js/Shared/Components/Layout/AppLayout.tsx"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var items = [{
  title: 'Work Experiences',
  description: 'This is where you add all your work experiences.',
  icon: outline_1.BriefcaseIcon,
  background: 'bg-pink-500',
  url: 'work.experience.index'
}, {
  title: 'Education and Credentials',
  description: 'This is where you add all your education qualification and credentials.',
  icon: outline_1.AcademicCapIcon,
  background: 'bg-yellow-500',
  url: 'profile.show'
}, {
  title: 'Skills',
  description: 'This is where you add all your skills.',
  icon: outline_1.ColorSwatchIcon,
  background: 'bg-green-500',
  url: 'profile.show'
}, {
  title: 'Career Summary',
  description: 'This is where you summarize your work history in a presentation format.',
  icon: outline_1.ChartSquareBarIcon,
  background: 'bg-blue-500',
  url: 'profile.show'
}, {
  title: 'Qualifications Summary',
  description: 'This is where you summarize your education and training experience in a presentation format.',
  icon: outline_1.CollectionIcon,
  background: 'bg-violet-500',
  url: 'profile.show'
}, {
  title: 'References',
  description: 'This is where you add all your work references if needed.',
  icon: outline_1.ClockIcon,
  background: 'bg-orange-500',
  url: 'profile.show'
}];

function classNames() {
  var classes = [];

  for (var _i = 0; _i < arguments.length; _i++) {
    classes[_i] = arguments[_i];
  }

  return classes.filter(Boolean).join(' ');
}

var Index = function Index() {
  var route = (0, useRoute_1["default"])();
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("h2", {
    className: "text-lg font-medium text-slate-900"
  }, "Resume Manager"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "These are the different building blocks of a resume."), react_1["default"].createElement("ul", {
    role: "list",
    className: "mt-8 grid grid-cols-1 gap-6 border-t border-b border-gray-200 py-6 sm:grid-cols-2"
  }, items.map(function (item, itemIdx) {
    return react_1["default"].createElement("li", {
      key: itemIdx,
      className: "flow-root"
    }, react_1["default"].createElement("div", {
      className: "relative -m-2 flex items-center space-x-4 rounded-xl p-2 py-10 focus-within:ring-2 focus-within:ring-fuchsia-500 hover:bg-gray-50"
    }, react_1["default"].createElement("div", {
      className: classNames(item.background, 'flex h-16 w-16 flex-shrink-0 items-center justify-center rounded-lg')
    }, react_1["default"].createElement(item.icon, {
      className: "h-6 w-6 text-white",
      "aria-hidden": "true"
    })), react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
      className: "text-sm font-medium text-gray-900"
    }, react_1["default"].createElement(inertia_react_1.Link, {
      href: route(item.url),
      className: "focus:outline-none"
    }, react_1["default"].createElement("span", {
      className: "absolute inset-0",
      "aria-hidden": "true"
    }), item.title, react_1["default"].createElement("span", {
      "aria-hidden": "true"
    }, " \u2192"))), react_1["default"].createElement("p", {
      className: "mt-1 text-sm text-gray-500"
    }, item.description))));
  })), react_1["default"].createElement("div", {
    className: "mt-10 flex justify-end"
  }, react_1["default"].createElement("a", {
    href: "#",
    className: "text-sm font-medium text-fuchsia-600 hover:text-fuchsia-400"
  }, "Generate a resume", react_1["default"].createElement("span", {
    "aria-hidden": "true"
  }, " \u2192"))));
};

Index.layout = function (page) {
  return react_1["default"].createElement(AppLayout_1["default"], {
    children: page,
    title: "Resume Manager"
  });
};

exports["default"] = Index;

/***/ }),

/***/ "./resources/js/Pages/ResumeManager/WorkExperiences/WorkExperience.tsx":
/*!*****************************************************************************!*\
  !*** ./resources/js/Pages/ResumeManager/WorkExperiences/WorkExperience.tsx ***!
  \*****************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __spreadArray = this && this.__spreadArray || function (to, from, pack) {
  if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
    if (ar || !(i in from)) {
      if (!ar) ar = Array.prototype.slice.call(from, 0, i);
      ar[i] = from[i];
    }
  }
  return to.concat(ar || Array.prototype.slice.call(from));
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var AppLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/AppLayout */ "./resources/js/Shared/Components/Layout/AppLayout.tsx"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var CustomDatePicker_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/DatePicker/CustomDatePicker */ "./resources/js/Shared/Components/DatePicker/CustomDatePicker.tsx"));

var Checkbox_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/FormElement/Checkbox */ "./resources/js/Shared/Components/FormElement/Checkbox.tsx"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var Notification_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/Notification */ "./resources/js/Shared/Components/Modal/Notification.tsx"));

var WorkExperience = function WorkExperience() {
  var _a = (0, react_1.useState)(false),
      showNotification = _a[0],
      setShowNotification = _a[1];

  var route = (0, useRoute_1["default"])();
  var maxDate = new Date();
  var _b = (0, useTypedPage_1["default"])().props,
      workExperiences = _b.pageData.workExperiences,
      _c = _b.flash,
      success = _c.success,
      error = _c.error;
  var savedWorkExperiences = workExperiences.data;
  var emptyFormValue = {
    xp: null,
    title: '',
    description: '',
    name: '',
    website: '',
    email: '',
    address: '',
    telephone: '',
    startDate: new Date().toLocaleDateString(),
    endDate: null,
    status: false
  };
  var initialFormValue = savedWorkExperiences.length > 0 ? savedWorkExperiences : [emptyFormValue];

  var _d = (0, inertia_react_1.useForm)(initialFormValue),
      data = _d.data,
      setData = _d.setData,
      post = _d.post,
      processing = _d.processing,
      errors = _d.errors;

  var handleOnChange = function handleOnChange(index, prop, value) {
    var newFormValues = __spreadArray([], data, true);

    newFormValues[index][prop] = value;
    setData(newFormValues);
  };

  var handleAddFormFields = function handleAddFormFields() {
    setData(__spreadArray(__spreadArray([], data, true), [emptyFormValue], false));
  };

  var handleOnRemoveFormField = function handleOnRemoveFormField(i) {
    var newFormValues = __spreadArray([], data, true);

    newFormValues.splice(i, 1);
    setData(newFormValues);
  };

  var handleOnStartDateChange = function handleOnStartDateChange(date, index) {
    var newFormValues = __spreadArray([], data, true);

    newFormValues[index]['startDate'] = new Date(date).toLocaleDateString();
    setData(newFormValues);
  };

  var handleOnEndDateChange = function handleOnEndDateChange(date, index) {
    var newFormValues = __spreadArray([], data, true);

    newFormValues[index]['endDate'] = new Date(date).toLocaleDateString();
    setData(newFormValues);
  };

  var handleOnSubmit = function handleOnSubmit(e) {
    e.preventDefault();
    post(route('work.experience.store'), {
      errorBag: 'saveWorkExperience',
      preserveScroll: true,
      onSuccess: function onSuccess() {
        return handleOnSaveSuccess();
      },
      onError: function onError(errors) {
        return console.log(errors);
      }
    });
  };

  var handleOnSaveSuccess = function handleOnSaveSuccess() {
    setShowNotification(true);
  };

  var closeNotification = function closeNotification() {
    setShowNotification(false);
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("form", {
    onSubmit: handleOnSubmit
  }, react_1["default"].createElement("div", {
    className: "sm:overflow-hidden sm:rounded-md"
  }, react_1["default"].createElement("div", {
    className: "space-y-6 bg-white py-6 px-4 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-gray-900"
  }, "Work Experiences"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Add all your work experiences.")), data.map(function (el, index) {
    return react_1["default"].createElement("div", {
      key: index
    }, react_1["default"].createElement("div", {
      className: "grid grid-cols-6 gap-6"
    }, index ? react_1["default"].createElement("div", {
      className: "relative col-span-6"
    }, react_1["default"].createElement("div", {
      className: "absolute inset-0 flex items-center",
      "aria-hidden": "true"
    }, react_1["default"].createElement("div", {
      className: "w-full border-t border-gray-300"
    })), react_1["default"].createElement("div", {
      className: "relative flex justify-center"
    }, react_1["default"].createElement("button", {
      type: "button",
      className: "inline-flex items-center rounded-full border border-gray-300 bg-white px-4 py-1.5 text-sm font-medium leading-5 shadow-sm hover:bg-gray-100 hover:text-brand-50 focus:outline-none",
      onClick: function onClick() {
        return handleOnRemoveFormField(index);
      }
    }, react_1["default"].createElement(outline_1.TrashIcon, {
      className: "h-5 w-5 text-gray-500",
      "aria-hidden": "true"
    })))) : null, react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-4"
    }, react_1["default"].createElement("label", {
      htmlFor: "job-title",
      className: "block text-sm font-medium text-slate-700"
    }, "Job Title"), react_1["default"].createElement("input", {
      type: "text",
      name: "title",
      id: "job-title",
      autoComplete: "job-title",
      placeholder: "Enter the job title",
      className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm",
      value: el.title || '',
      onChange: function onChange(e) {
        return handleOnChange(index, 'title', e.target.value);
      },
      required: true
    })), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "company-name",
      className: "block text-sm font-medium text-slate-700"
    }, "Company Name"), react_1["default"].createElement("input", {
      type: "text",
      name: "name",
      id: "company-name",
      autoComplete: "company-name",
      placeholder: "Enter the company name",
      className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm",
      value: el.name || '',
      onChange: function onChange(e) {
        return handleOnChange(index, 'name', e.target.value);
      },
      required: true
    })), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "email",
      className: "block text-sm font-medium text-slate-700"
    }, "Company Email (Optional)"), react_1["default"].createElement("div", {
      className: "relative mt-1 rounded-md shadow-sm"
    }, react_1["default"].createElement("div", {
      className: "pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3"
    }, react_1["default"].createElement(outline_1.MailIcon, {
      className: "h-5 w-5 text-gray-400",
      "aria-hidden": "true"
    })), react_1["default"].createElement("input", {
      type: "email",
      name: "email",
      id: "email",
      className: "block w-full rounded-md border-gray-300 pl-10 focus:border-purple-500 focus:ring-purple-500 sm:text-sm",
      placeholder: "info@example.com",
      value: el.email || '',
      onChange: function onChange(e) {
        return handleOnChange(index, 'email', e.target.value);
      }
    }))), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "telephone",
      className: "block text-sm font-medium text-slate-700"
    }, "Telephone (Optional)"), react_1["default"].createElement("input", {
      type: "text",
      name: "telephone",
      id: "telephone",
      autoComplete: "telephone",
      placeholder: "Enter the company telephone number.",
      className: "mt-1 block w-full rounded-md border border-gray-300 py-2 px-3 shadow-sm focus:border-purple-500 focus:outline-none focus:ring-purple-500 sm:text-sm",
      value: el.telephone || '',
      onChange: function onChange(e) {
        return handleOnChange(index, 'telephone', e.target.value);
      }
    })), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "company-website",
      className: "block text-sm font-medium text-slate-700"
    }, "Company Website (Optional)"), react_1["default"].createElement("div", {
      className: "mt-1 flex rounded-md shadow-sm"
    }, react_1["default"].createElement("span", {
      className: "inline-flex items-center rounded-l-md border border-r-0 border-gray-300 bg-gray-50 px-3 text-gray-500 sm:text-sm"
    }, "https://"), react_1["default"].createElement("input", {
      type: "text",
      name: "website",
      id: "company-website",
      className: "block w-full min-w-0 flex-1 rounded-none rounded-r-md border-gray-300 px-3 py-2 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm",
      placeholder: "www.example.com",
      value: el.website || '',
      onChange: function onChange(e) {
        return handleOnChange(index, 'website', e.target.value);
      }
    }))), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "startedDate",
      className: "mb-1 block text-sm font-medium text-slate-700"
    }, "Start Date"), react_1["default"].createElement(CustomDatePicker_1["default"], {
      handleOnDateChange: handleOnStartDateChange,
      index: index,
      initialDate: el.startDate ? new Date(el.startDate) : null,
      placeholder: "Please select the start date",
      maxDate: maxDate
    })), react_1["default"].createElement("div", {
      className: "col-span-6 sm:col-span-3"
    }, react_1["default"].createElement("label", {
      htmlFor: "endDate",
      className: "mb-1 block text-sm font-medium text-slate-700"
    }, "End Date"), react_1["default"].createElement(CustomDatePicker_1["default"], {
      handleOnDateChange: handleOnEndDateChange,
      index: index,
      initialDate: el.endDate ? new Date(el.endDate) : null,
      placeholder: "Please select the end date if applicable",
      maxDate: maxDate
    })), react_1["default"].createElement("div", {
      className: "sm:col-span-6"
    }, react_1["default"].createElement("label", {
      htmlFor: "address",
      className: "block text-sm font-medium text-slate-700"
    }, "Company Address (Optional)"), react_1["default"].createElement("div", {
      className: "mt-1"
    }, react_1["default"].createElement("textarea", {
      id: "address",
      name: "address",
      rows: 4,
      className: "mt-1 block w-full rounded-md border border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
      value: el.address,
      onChange: function onChange(e) {
        return handleOnChange(index, 'address', e.target.value);
      }
    }))), react_1["default"].createElement("div", {
      className: "sm:col-span-6"
    }, react_1["default"].createElement("label", {
      htmlFor: "job-description",
      className: "block text-sm font-medium text-slate-700"
    }, "Job Description"), react_1["default"].createElement("div", {
      className: "mt-1"
    }, react_1["default"].createElement("textarea", {
      rows: 5,
      name: "description",
      id: "job-description",
      required: true,
      className: "block w-full rounded-md border-gray-300 shadow-sm focus:border-purple-500 focus:ring-purple-500 sm:text-sm",
      defaultValue: el.description,
      onChange: function onChange(e) {
        return handleOnChange(index, 'description', e.target.value);
      }
    })), react_1["default"].createElement("p", {
      className: "mt-2 text-sm text-gray-500"
    }, "Briefly describe your job responsibilities")), react_1["default"].createElement("div", {
      className: "col-span-6"
    }, react_1["default"].createElement(Checkbox_1["default"], {
      id: "current_job",
      name: "status",
      handleOnChange: handleOnChange,
      checked: el.status,
      label: "Current Job (Tick if this is your current job)",
      index: index
    }))));
  })), react_1["default"].createElement("div", {
    className: "flex justify-end space-x-4 px-4 py-3 text-right sm:px-6"
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none",
    onClick: function onClick() {
      return handleAddFormFields();
    }
  }, react_1["default"].createElement(outline_1.PlusSmIcon, {
    className: "-ml-1.5 mr-1 h-5 w-5 text-gray-400",
    "aria-hidden": "true"
  }), "Add New Work Experience"), react_1["default"].createElement(inertia_react_1.Link, {
    href: route('resume.manager.index'),
    type: "button",
    className: "inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none"
  }, react_1["default"].createElement(outline_1.XIcon, {
    className: "-ml-1.5 mr-1 h-5 w-5 text-gray-400",
    "aria-hidden": "true"
  }), "Cancel"), react_1["default"].createElement("button", {
    type: "submit",
    className: "inline-flex justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-2 px-4 text-sm font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none",
    disabled: processing
  }, "Save")))), showNotification && react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(Notification_1["default"], {
    show: showNotification,
    title: success,
    closeNotification: closeNotification,
    messageType: "success"
  })));
};

WorkExperience.layout = function (page) {
  return react_1["default"].createElement(AppLayout_1["default"], {
    children: page,
    title: "Work Experiences"
  });
};

exports["default"] = WorkExperience;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/ChangePasswordForm.tsx":
/*!**********************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/ChangePasswordForm.tsx ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js")); // @ts-ignore


var Notification_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/Notification */ "./resources/js/Shared/Components/Modal/Notification.tsx"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var ChangePasswordForm = function ChangePasswordForm() {
  var route = (0, useRoute_1["default"])();

  var _a = (0, inertia_react_1.useForm)({
    current_password: '',
    password: '',
    password_confirmation: ''
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      errors = _a.errors,
      put = _a.put,
      reset = _a.reset; // toggle notification


  var _b = (0, react_1.useState)(false),
      show = _b[0],
      setShow = _b[1];

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    put(route('user-password.update'), {
      errorBag: 'updatePassword',
      preserveScroll: true,
      onSuccess: function onSuccess() {
        return handleSuccessSubmit();
      },
      onError: function onError() {
        return reset();
      }
    });
  };

  var handleSuccessSubmit = function handleSuccessSubmit() {
    reset('current_password', 'password', 'password_confirmation');
    setShow(true);
  };

  var closeNotification = function closeNotification() {
    setShow(false);
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("form", {
    onSubmit: handleSubmit
  }, react_1["default"].createElement("div", {
    className: "mb-4 shadow sm:overflow-hidden sm:rounded-md"
  }, react_1["default"].createElement("div", {
    className: "space-y-6 bg-white py-6 px-4 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-fuchsia-900"
  }, "Update Password"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Ensure your account is using a long, random password to stay secure.")), react_1["default"].createElement("div", {
    className: "space-y-6 sm:space-y-5"
  }, react_1["default"].createElement("div", {
    className: "sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:border-t sm:border-gray-200 sm:pt-5"
  }, react_1["default"].createElement("label", {
    htmlFor: "current-password",
    className: "block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
  }, "Current Password"), react_1["default"].createElement("div", {
    className: "mt-1 sm:col-span-2 sm:mt-0"
  }, react_1["default"].createElement("input", {
    id: "current-password",
    name: "current_password",
    type: "password",
    autoComplete: "current_password",
    className: "block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
    value: data.current_password,
    onChange: function onChange(e) {
      return setData('current_password', e.currentTarget.value);
    }
  }), errors.current_password && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "current_password-error"
  }, errors.current_password))), react_1["default"].createElement("div", {
    className: "sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5"
  }, react_1["default"].createElement("label", {
    htmlFor: "new-password",
    className: "block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
  }, "New Password"), react_1["default"].createElement("div", {
    className: "mt-1 sm:col-span-2 sm:mt-0"
  }, react_1["default"].createElement("input", {
    id: "new-password",
    name: "password",
    type: "password",
    autoComplete: "new-password",
    className: "block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
    value: data.password,
    onChange: function onChange(e) {
      return setData('password', e.currentTarget.value);
    }
  }), errors.password && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, errors.password))), react_1["default"].createElement("div", {
    className: "sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5"
  }, react_1["default"].createElement("label", {
    htmlFor: "password-confirmation",
    className: "block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
  }, "Confirm Password"), react_1["default"].createElement("div", {
    className: "mt-1 sm:col-span-2 sm:mt-0"
  }, react_1["default"].createElement("input", {
    id: "password-confirmation",
    name: "password_confirmation",
    type: "password",
    autoComplete: "new-password",
    className: "block w-full max-w-lg appearance-none rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    value: data.password_confirmation,
    onChange: function onChange(e) {
      return setData('password_confirmation', e.currentTarget.value);
    }
  }), errors.password_confirmation && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password_confirmation-error"
  }, errors.password_confirmation))))), react_1["default"].createElement("div", {
    className: "bg-gray-50 px-4 py-3 text-right sm:px-6"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none",
    disabled: processing
  }, "Save")))), show && react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(Notification_1["default"], {
    show: show,
    title: "Successfully Updated!",
    body: "You password was updated successfully.",
    closeNotification: closeNotification,
    messageType: "success"
  })));
};

exports["default"] = ChangePasswordForm;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/DeleteUserForm.tsx":
/*!******************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/DeleteUserForm.tsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var PermissionModal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/PermissionModal */ "./resources/js/Shared/Components/Modal/PermissionModal.tsx"));

var DeleteUserForm = function DeleteUserForm() {
  var pageInfo = {
    title: 'Delete Account',
    description: 'Are you sure you want to delete your account? Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.',
    btnName: 'Delete Account'
  };

  var _a = (0, inertia_react_1.useForm)({
    password: ''
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      destroy = _a["delete"];

  var _b = (0, react_1.useState)(false),
      isActive = _b[0],
      setIsActive = _b[1];

  var _c = (0, react_1.useState)(false),
      error = _c[0],
      setError = _c[1];

  var route = (0, useRoute_1["default"])();

  var handleActive = function handleActive(active) {
    return function (e) {
      e.preventDefault();
      setIsActive(active);
    };
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    destroy(route('current-user.destroy'), {
      preserveState: true,
      errorBag: 'deleteUser',
      onSuccess: function onSuccess(page) {
        if (!page.props.errors.deleteUser) {
          setIsActive(false);
        }
      },
      onError: function onError(errors) {
        if (errors.password) {
          setError(true);
        }
      }
    });
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "bg-white shadow sm:rounded-lg"
  }, react_1["default"].createElement("div", {
    className: "px-4 py-5 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-fuchsia-900"
  }, "Delete Account"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Permanently delete your account."), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.")), react_1["default"].createElement("div", {
    className: "mt-5"
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "inline-flex items-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-red-50 shadow-sm hover:bg-red-300 focus:outline-none focus:ring-2 focus:ring-red-50 focus:ring-offset-2",
    onClick: handleActive(true)
  }, "Delete Account")))), react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(PermissionModal_1["default"], {
    data: data,
    setData: setData,
    isActive: isActive,
    handleActive: handleActive,
    pageInfo: pageInfo,
    processing: processing,
    handleSubmit: handleSubmit,
    error: error
  })));
};

exports["default"] = DeleteUserForm;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/LogoutOtherBrowserSessions.tsx":
/*!******************************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/LogoutOtherBrowserSessions.tsx ***!
  \******************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var PermissionModal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/PermissionModal */ "./resources/js/Shared/Components/Modal/PermissionModal.tsx"));

var LogoutOtherBrowserSessions = function LogoutOtherBrowserSessions() {
  var route = (0, useRoute_1["default"])();
  var props = (0, useTypedPage_1["default"])().props;
  var sessions = props.sessions;
  var pageInfo = {
    title: 'Log Out Other Browser Sessions',
    description: 'Please enter your password to confirm you would like to log out of your other browser sessions across all of your devices.',
    btnName: 'Log Out Other Browser Sessions'
  };

  var _a = (0, inertia_react_1.useForm)({
    password: ''
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      destroy = _a["delete"],
      reset = _a.reset;

  var _b = (0, react_1.useState)(false),
      isActive = _b[0],
      setIsActive = _b[1];

  var _c = (0, react_1.useState)(false),
      error = _c[0],
      setError = _c[1];

  var handleActive = function handleActive(active) {
    return function (e) {
      e.preventDefault();
      setIsActive(active);
    };
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    destroy(route('other-browser-sessions.destroy'), {
      errorBag: 'logoutOtherBrowse',
      preserveScroll: true,
      onSuccess: function onSuccess() {
        setIsActive(false);
      },
      onFinish: function onFinish() {
        return reset('password');
      },
      onError: function onError(errors) {
        if (errors.password) {
          setError(true);
        }
      }
    });
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "mb-4 bg-white shadow sm:rounded-lg"
  }, react_1["default"].createElement("div", {
    className: "px-4 py-5 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-fuchsia-900"
  }, "Browser Sessions"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Manage and log out your active sessions on other browsers and devices."), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "If necessary, you may log out of all of your other browser sessions across all of your devices. Some of your recent sessions are listed below; however, this list may not be exhaustive. If you feel your account has been compromised, you should also update your password.")), sessions.length > 0 ? react_1["default"].createElement("div", {
    className: "mt-5 space-y-6"
  }, sessions.map(function (session, key) {
    return react_1["default"].createElement("div", {
      className: "flex items-center",
      key: key
    }, react_1["default"].createElement("div", null, session.agent.is_desktop ? react_1["default"].createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      className: "h-6 w-6",
      fill: "none",
      viewBox: "0 0 24 24",
      stroke: "currentColor"
    }, react_1["default"].createElement("path", {
      strokeLinecap: "round",
      strokeLinejoin: "round",
      strokeWidth: "2",
      d: "M9.75 17L9 20l-1 1h8l-1-1-.75-3M3 13h18M5 17h14a2 2 0 002-2V5a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"
    })) : react_1["default"].createElement("svg", {
      xmlns: "http://www.w3.org/2000/svg",
      viewBox: "0 0 24 24",
      strokeWidth: "2",
      stroke: "currentColor",
      fill: "none",
      strokeLinecap: "round",
      strokeLinejoin: "round",
      className: "h-8 w-8 text-gray-500"
    }, react_1["default"].createElement("path", {
      d: "M0 0h24v24H0z",
      stroke: "none"
    }), react_1["default"].createElement("rect", {
      x: "7",
      y: "4",
      width: "10",
      height: "16",
      rx: "1"
    }), react_1["default"].createElement("path", {
      d: "M11 5h2M12 17v.01"
    }))), react_1["default"].createElement("div", {
      className: "ml-3"
    }, react_1["default"].createElement("div", {
      className: "text-sm text-gray-600"
    }, session.agent.platform, " - ", session.agent.browser), react_1["default"].createElement("div", null, react_1["default"].createElement("div", {
      className: "text-xs text-gray-500"
    }, session.ip_address, ",", session.is_current_device ? react_1["default"].createElement("span", {
      className: "font-semibold text-green-500"
    }, ' ', "This device") : react_1["default"].createElement("span", null, " Last active ", session.last_active)))));
  })) : null, react_1["default"].createElement("div", {
    className: "mt-5"
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none",
    onClick: handleActive(true)
  }, "Log Out Other Browser Sessions")))), react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(PermissionModal_1["default"], {
    data: data,
    setData: setData,
    isActive: isActive,
    handleActive: handleActive,
    pageInfo: pageInfo,
    processing: processing,
    handleSubmit: handleSubmit,
    error: error
  })));
};

exports["default"] = LogoutOtherBrowserSessions;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/ProfilePersonalInfo.tsx":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/ProfilePersonalInfo.tsx ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Notification_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/Notification */ "./resources/js/Shared/Components/Modal/Notification.tsx"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var ModalPortal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/ModalPortal */ "./resources/js/Shared/Components/Modal/ModalPortal.tsx"));

var ProfilePersonalInfo = function ProfilePersonalInfo() {
  var route = (0, useRoute_1["default"])();
  var props = (0, useTypedPage_1["default"])().props;
  var user = props.user;

  var _a = (0, inertia_react_1.useForm)({
    name: user.loggedInUserName,
    email: user.loggedInUserEmail
  }),
      data = _a.data,
      setData = _a.setData,
      processing = _a.processing,
      errors = _a.errors,
      put = _a.put;

  var _b = (0, react_1.useState)(false),
      show = _b[0],
      setShow = _b[1];

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    put(route('user-profile-information.update'), {
      errorBag: 'updateProfileInformation',
      preserveScroll: true,
      onSuccess: function onSuccess() {
        return setShow(true);
      }
    });
  };

  var closeNotification = function closeNotification() {
    setShow(false);
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("form", {
    onSubmit: handleSubmit
  }, react_1["default"].createElement("div", {
    className: "mb-4 shadow sm:overflow-hidden sm:rounded-md"
  }, react_1["default"].createElement("div", {
    className: "space-y-6 bg-white py-6 px-4 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-fuchsia-900"
  }, "Personal Information"), react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-gray-500"
  }, "Update your account's profile information and email address.")), react_1["default"].createElement("div", {
    className: "space-y-6 sm:space-y-5"
  }, react_1["default"].createElement("div", {
    className: "sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:border-t sm:border-gray-200 sm:pt-5"
  }, react_1["default"].createElement("label", {
    htmlFor: "fullName",
    className: "block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
  }, "Name"), react_1["default"].createElement("div", {
    className: "mt-1 sm:col-span-2 sm:mt-0"
  }, react_1["default"].createElement("input", {
    id: "fullName",
    name: "name",
    type: "text",
    autoComplete: "name",
    className: "block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
    value: data.name,
    onChange: function onChange(e) {
      return setData('name', e.currentTarget.value);
    },
    required: true
  }), errors.name && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "name-error"
  }, errors.name))), react_1["default"].createElement("div", {
    className: "sm:grid sm:grid-cols-3 sm:items-start sm:gap-4 sm:pt-5"
  }, react_1["default"].createElement("label", {
    htmlFor: "email",
    className: "block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
  }, "Email"), react_1["default"].createElement("div", {
    className: "mt-1 sm:col-span-2 sm:mt-0"
  }, react_1["default"].createElement("input", {
    id: "email",
    name: "email",
    type: "email",
    autoComplete: "email",
    className: "block w-full max-w-lg rounded-md border-gray-300 shadow-sm focus:border-fuchsia-500 focus:ring-fuchsia-500 sm:text-sm",
    value: data.email,
    onChange: function onChange(e) {
      return setData('email', e.currentTarget.value);
    },
    required: true
  }), errors.email && react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "email-error"
  }, errors.email))))), react_1["default"].createElement("div", {
    className: "bg-gray-50 px-4 py-3 text-right sm:px-6"
  }, react_1["default"].createElement("button", {
    type: "submit",
    className: "inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none",
    disabled: processing
  }, "Save")))), show && react_1["default"].createElement(ModalPortal_1["default"], null, react_1["default"].createElement(Notification_1["default"], {
    show: show,
    title: "Successfully Updated!",
    closeNotification: closeNotification,
    messageType: "success"
  })));
};

exports["default"] = ProfilePersonalInfo;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/TwoFactorAuthentication.tsx":
/*!***************************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/TwoFactorAuthentication.tsx ***!
  \***************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var inertia_1 = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");

var axios_1 = __importDefault(__webpack_require__(/*! axios */ "./node_modules/axios/index.js"));

var TwoFactorAuthenticationConfirmation_1 = __importDefault(__webpack_require__(/*! ./TwoFactorAuthenticationConfirmation */ "./resources/js/Pages/Setting/Components/TwoFactorAuthenticationConfirmation.tsx"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var TwoFactorAuthentication = function TwoFactorAuthentication() {
  var _a;

  var props = (0, useTypedPage_1["default"])().props;
  var twoFactorEnabled = (_a = props === null || props === void 0 ? void 0 : props.user) === null || _a === void 0 ? void 0 : _a.twoFactorEnabled;
  var passwordVerificationData = {
    title: 'Confirm Password',
    description: 'For your security, please confirm your password to continue.',
    btnName: 'Confirm'
  };
  var initialState = {
    qrCode: undefined,
    recoveryCodes: []
  };

  var _b = (0, react_1.useReducer)(function (state, _a) {
    var type = _a.type,
        payload = _a.payload;

    switch (type) {
      case 'SET_QRCODE':
        return __assign(__assign({}, state), {
          qrCode: payload
        });

      case 'SET_RECOVERY_CODES':
        return __assign(__assign({}, state), {
          recoveryCodes: payload
        });
    }
  }, initialState),
      state = _b[0],
      dispatch = _b[1];

  var qrCode = state.qrCode,
      recoveryCodes = state.recoveryCodes;

  var handleEnableTwoFactorAuthentication = function handleEnableTwoFactorAuthentication() {
    return __awaiter(void 0, void 0, void 0, function () {
      var response;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , inertia_1.Inertia.post('/user/two-factor-authentication')];

          case 1:
            _a.sent();

            return [4
            /*yield*/
            , axios_1["default"].get('/user/two-factor-qr-code')];

          case 2:
            response = _a.sent();
            dispatch({
              type: 'SET_QRCODE',
              payload: response.data
            });
            return [4
            /*yield*/
            , axios_1["default"].get('/user/two-factor-recovery-codes')];

          case 3:
            response = _a.sent();
            dispatch({
              type: 'SET_RECOVERY_CODES',
              payload: response.data
            });
            return [2
            /*return*/
            ];
        }
      });
    });
  };

  var handleShowRecoveryCodes = function handleShowRecoveryCodes() {
    return __awaiter(void 0, void 0, void 0, function () {
      var response;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/user/two-factor-recovery-codes')];

          case 1:
            response = _a.sent();
            dispatch({
              type: 'SET_RECOVERY_CODES',
              payload: response.data
            });
            return [2
            /*return*/
            ];
        }
      });
    });
  };

  var handleRegenerateRecoveryCodes = function handleRegenerateRecoveryCodes() {
    return __awaiter(void 0, void 0, void 0, function () {
      var response;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].post('/user/two-factor-recovery-codes')];

          case 1:
            _a.sent();

            return [4
            /*yield*/
            , axios_1["default"].get('/user/two-factor-recovery-codes')];

          case 2:
            response = _a.sent();
            dispatch({
              type: 'SET_RECOVERY_CODES',
              payload: response.data
            });
            return [2
            /*return*/
            ];
        }
      });
    });
  };

  var handleDisableTwoFactorAuthentication = function handleDisableTwoFactorAuthentication() {
    return __awaiter(void 0, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , inertia_1.Inertia["delete"]('/user/two-factor-authentication')];

          case 1:
            _a.sent();

            return [2
            /*return*/
            ];
        }
      });
    });
  };

  return react_1["default"].createElement("div", {
    className: "mb-4 bg-white shadow sm:rounded-lg"
  }, react_1["default"].createElement("div", {
    className: "px-4 py-5 sm:p-6"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-lg font-medium leading-6 text-fuchsia-900"
  }, "Two Factor Authentication"), react_1["default"].createElement("p", {
    className: "text-sm text-gray-500"
  }, "Add additional security to your account using two factor authentication."), twoFactorEnabled ? react_1["default"].createElement("p", {
    className: "mt-3 text-lg font-medium text-slate-600"
  }, "You have enabled two factor authentication.") : react_1["default"].createElement("p", {
    className: "mt-3 text-lg font-medium text-brand-500"
  }, "You have not enabled two factor authentication."), react_1["default"].createElement("p", {
    className: "mt-2 text-sm text-gray-500"
  }, "When two factor authentication is enabled, you will be prompted for a secure, random token during authentication. You may retrieve this token from your phone's Google Authenticator application.")), react_1["default"].createElement("div", {
    className: "mt-5"
  }, twoFactorEnabled ? react_1["default"].createElement(react_1["default"].Fragment, null, qrCode && react_1["default"].createElement("div", {
    className: "mt-4 max-w-xl text-sm text-gray-600"
  }, react_1["default"].createElement("p", {
    className: "font-semibold"
  }, "Two factor authentication is now enabled. Scan the following QR code using your phone's authenticator application."), react_1["default"].createElement("div", {
    dangerouslySetInnerHTML: {
      __html: qrCode.svg
    },
    className: "mt-4"
  })), recoveryCodes.length > 0 && react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "mt-4 max-w-xl text-sm text-gray-600"
  }, react_1["default"].createElement("p", {
    className: "font-semibold"
  }, "Store these recovery codes in a secure password manager. They can be used to recover access to your account if your two factor authentication device is lost.")), react_1["default"].createElement("div", {
    className: "mt-4 grid max-w-xl gap-1 rounded-lg bg-gray-100 px-4 py-4 font-mono text-sm"
  }, recoveryCodes.map(function (code) {
    return react_1["default"].createElement("div", {
      key: code
    }, code);
  }))), recoveryCodes.length > 0 ? react_1["default"].createElement(TwoFactorAuthenticationConfirmation_1["default"], {
    id: "regenerate-recorvery-codes",
    onConfirmed: handleRegenerateRecoveryCodes,
    pageInfo: passwordVerificationData
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "mt-3 inline-flex items-center rounded-md border border-transparent bg-fuchsia-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-fuchsia-700 focus:outline-none",
    onClick: handleRegenerateRecoveryCodes
  }, "Regenerate Recovery Code")) : react_1["default"].createElement(TwoFactorAuthenticationConfirmation_1["default"], {
    id: "show-recovery-codes",
    onConfirmed: handleShowRecoveryCodes,
    pageInfo: passwordVerificationData
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "inline-flex items-center rounded-md border border-transparent bg-fuchsia-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-fuchsia-700 focus:outline-none",
    onClick: handleShowRecoveryCodes
  }, "Show Recovery Code")), react_1["default"].createElement(TwoFactorAuthenticationConfirmation_1["default"], {
    id: "disable-two-factor-authentication",
    onConfirmed: handleDisableTwoFactorAuthentication,
    pageInfo: passwordVerificationData
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "mt-3 ml-3 inline-flex items-center rounded-md border border-transparent bg-red-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-red-200 focus:outline-none"
  }, "Disable"))) : react_1["default"].createElement(TwoFactorAuthenticationConfirmation_1["default"], {
    id: "enable-two-factor-authentication",
    onConfirmed: handleEnableTwoFactorAuthentication,
    pageInfo: passwordVerificationData
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "inline-flex items-center rounded-md border border-transparent bg-purple-800 px-4 py-2 text-sm font-medium text-purple-50 shadow-sm hover:bg-fuchsia-700 focus:outline-none"
  }, "Enable")))));
};

exports["default"] = TwoFactorAuthentication;

/***/ }),

/***/ "./resources/js/Pages/Setting/Components/TwoFactorAuthenticationConfirmation.tsx":
/*!***************************************************************************************!*\
  !*** ./resources/js/Pages/Setting/Components/TwoFactorAuthenticationConfirmation.tsx ***!
  \***************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var axios_1 = __importDefault(__webpack_require__(/*! axios */ "./node_modules/axios/index.js"));

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var PermissionModal_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Modal/PermissionModal */ "./resources/js/Shared/Components/Modal/PermissionModal.tsx"));

var TwoFactorAuthenticationConfirmation = function TwoFactorAuthenticationConfirmation(_a) {
  var id = _a.id,
      children = _a.children,
      onConfirmed = _a.onConfirmed,
      pageInfo = _a.pageInfo;

  var _b = (0, inertia_react_1.useForm)({
    password: ''
  }),
      data = _b.data,
      setData = _b.setData,
      processing = _b.processing;

  var route = (0, useRoute_1["default"])();

  var _c = (0, react_1.useState)(false),
      isActive = _c[0],
      setIsActive = _c[1];

  var _d = (0, react_1.useState)(false),
      error = _d[0],
      setError = _d[1];

  var handleActive = function handleActive(active) {
    return function (e) {
      e.preventDefault();

      if (active) {
        axios_1["default"].get(route('password.confirmation')).then(function (response) {
          if (response.data.confirmed) {
            onConfirmed();
          } else {
            setIsActive(true);
          }
        });
      } else {
        setIsActive(false);
      }
    };
  };

  var handleSubmit = function handleSubmit(e) {
    e.preventDefault();
    axios_1["default"].post(route('password.confirm'), __assign({}, data)).then(function (response) {
      onConfirmed();
      setIsActive(false);
    })["catch"](function (error) {
      var errors = error.response.data.errors;

      if (errors.password) {
        setError(true);
      }
    });
  };

  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].isValidElement(children) && react_1["default"].cloneElement(children, {
    onClick: handleActive(true)
  }), react_1["default"].createElement(PermissionModal_1["default"], {
    data: data,
    setData: setData,
    isActive: isActive,
    handleActive: handleActive,
    pageInfo: pageInfo,
    processing: processing,
    handleSubmit: handleSubmit,
    error: error
  }));
};

exports["default"] = TwoFactorAuthenticationConfirmation;

/***/ }),

/***/ "./resources/js/Pages/Setting/Show.tsx":
/*!*********************************************!*\
  !*** ./resources/js/Pages/Setting/Show.tsx ***!
  \*********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var ProfilePersonalInfo_1 = __importDefault(__webpack_require__(/*! ./Components/ProfilePersonalInfo */ "./resources/js/Pages/Setting/Components/ProfilePersonalInfo.tsx"));

var ChangePasswordForm_1 = __importDefault(__webpack_require__(/*! ./Components/ChangePasswordForm */ "./resources/js/Pages/Setting/Components/ChangePasswordForm.tsx"));

var TwoFactorAuthentication_1 = __importDefault(__webpack_require__(/*! ./Components/TwoFactorAuthentication */ "./resources/js/Pages/Setting/Components/TwoFactorAuthentication.tsx"));

var LogoutOtherBrowserSessions_1 = __importDefault(__webpack_require__(/*! ./Components/LogoutOtherBrowserSessions */ "./resources/js/Pages/Setting/Components/LogoutOtherBrowserSessions.tsx"));

var DeleteUserForm_1 = __importDefault(__webpack_require__(/*! ./Components/DeleteUserForm */ "./resources/js/Pages/Setting/Components/DeleteUserForm.tsx"));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var AppLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/AppLayout */ "./resources/js/Shared/Components/Layout/AppLayout.tsx"));

var Show = function Show() {
  var props = (0, useTypedPage_1["default"])().props;
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8"
  }, props.jetstream.canUpdateProfileInformation && react_1["default"].createElement(ProfilePersonalInfo_1["default"], null), props.jetstream.canUpdatePassword && react_1["default"].createElement(ChangePasswordForm_1["default"], null), props.jetstream.canManageTwoFactorAuthentication && react_1["default"].createElement(TwoFactorAuthentication_1["default"], null), react_1["default"].createElement(LogoutOtherBrowserSessions_1["default"], null), props.jetstream.hasAccountDeletionFeatures && react_1["default"].createElement(DeleteUserForm_1["default"], null)));
};

Show.layout = function (page) {
  return react_1["default"].createElement(AppLayout_1["default"], {
    children: page,
    title: "Settings"
  });
};

exports["default"] = Show;

/***/ }),

/***/ "./resources/js/Pages/TermsOfService.tsx":
/*!***********************************************!*\
  !*** ./resources/js/Pages/TermsOfService.tsx ***!
  \***********************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var TermsOfService = function TermsOfService() {
  return react_1["default"].createElement("div", {
    className: "relative overflow-hidden bg-white py-16"
  }, react_1["default"].createElement("div", {
    className: "hidden lg:absolute lg:inset-y-0 lg:block lg:h-full lg:w-full"
  }, react_1["default"].createElement("div", {
    className: "relative mx-auto h-full max-w-prose text-lg",
    "aria-hidden": "true"
  }, react_1["default"].createElement("svg", {
    className: "absolute top-12 left-full translate-x-32 transform",
    width: 404,
    height: 384,
    fill: "none",
    viewBox: "0 0 404 384"
  }, react_1["default"].createElement("defs", null, react_1["default"].createElement("pattern", {
    id: "74b3fd99-0a6f-4271-bef2-e80eeafdf357",
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    patternUnits: "userSpaceOnUse"
  }, react_1["default"].createElement("rect", {
    x: 0,
    y: 0,
    width: 4,
    height: 4,
    className: "text-gray-200",
    fill: "currentColor"
  }))), react_1["default"].createElement("rect", {
    width: 404,
    height: 384,
    fill: "url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)"
  })), react_1["default"].createElement("svg", {
    className: "absolute top-1/2 right-full -translate-y-1/2 -translate-x-32 transform",
    width: 404,
    height: 384,
    fill: "none",
    viewBox: "0 0 404 384"
  }, react_1["default"].createElement("defs", null, react_1["default"].createElement("pattern", {
    id: "f210dbf6-a58d-4871-961e-36d5016a0f49",
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    patternUnits: "userSpaceOnUse"
  }, react_1["default"].createElement("rect", {
    x: 0,
    y: 0,
    width: 4,
    height: 4,
    className: "text-gray-200",
    fill: "currentColor"
  }))), react_1["default"].createElement("rect", {
    width: 404,
    height: 384,
    fill: "url(#f210dbf6-a58d-4871-961e-36d5016a0f49)"
  })), react_1["default"].createElement("svg", {
    className: "absolute bottom-12 left-full translate-x-32 transform",
    width: 404,
    height: 384,
    fill: "none",
    viewBox: "0 0 404 384"
  }, react_1["default"].createElement("defs", null, react_1["default"].createElement("pattern", {
    id: "d3eb07ae-5182-43e6-857d-35c643af9034",
    x: 0,
    y: 0,
    width: 20,
    height: 20,
    patternUnits: "userSpaceOnUse"
  }, react_1["default"].createElement("rect", {
    x: 0,
    y: 0,
    width: 4,
    height: 4,
    className: "text-gray-200",
    fill: "currentColor"
  }))), react_1["default"].createElement("rect", {
    width: 404,
    height: 384,
    fill: "url(#d3eb07ae-5182-43e6-857d-35c643af9034)"
  })))), react_1["default"].createElement("div", {
    className: "relative px-4 sm:px-6 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-prose text-lg"
  }, react_1["default"].createElement("h1", null, react_1["default"].createElement("span", {
    className: "mt-2 block text-center text-3xl font-extrabold leading-8 tracking-tight text-gray-900 sm:text-4xl"
  }, "Terms of Service")), react_1["default"].createElement("p", {
    className: "mt-8 text-xl leading-8 text-gray-500"
  }, "Aliquet nec orci mattis amet quisque ullamcorper neque, nibh sem. At arcu, sit dui mi, nibh dui, diam eget aliquam. Quisque id at vitae feugiat egestas ac. Diam nulla orci at in viverra scelerisque eget. Eleifend egestas fringilla sapien.")), react_1["default"].createElement("div", {
    className: "prose prose-lg prose-indigo mx-auto mt-6 text-gray-500"
  }, react_1["default"].createElement("p", null, "Faucibus commodo massa rhoncus, volutpat. ", react_1["default"].createElement("strong", null, "Dignissim"), ' ', "sed ", react_1["default"].createElement("strong", null, "eget risus enim"), ". Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit. Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim.", ' ', react_1["default"].createElement("a", {
    href: "#"
  }, "Mattis mauris semper"), " sed amet vitae sed turpis id."), react_1["default"].createElement("ul", {
    role: "list"
  }, react_1["default"].createElement("li", null, "Quis elit egestas venenatis mattis dignissim."), react_1["default"].createElement("li", null, "Cras cras lobortis vitae vivamus ultricies facilisis tempus."), react_1["default"].createElement("li", null, "Orci in sit morbi dignissim metus diam arcu pretium.")), react_1["default"].createElement("p", null, "Quis semper vulputate aliquam venenatis egestas sagittis quisque orci. Donec commodo sit viverra aliquam porttitor ultrices gravida eu. Tincidunt leo, elementum mattis elementum ut nisl, justo, amet, mattis. Nunc purus, diam commodo tincidunt turpis. Amet, duis sed elit interdum dignissim."), react_1["default"].createElement("h2", null, "From beginner to expert in 30 days"), react_1["default"].createElement("p", null, "Id orci tellus laoreet id ac. Dolor, aenean leo, ac etiam consequat in. Convallis arcu ipsum urna nibh. Pharetra, euismod vitae interdum mauris enim, consequat vulputate nibh. Maecenas pellentesque id sed tellus mauris, ultrices mauris. Tincidunt enim cursus ridiculus mi. Pellentesque nam sed nullam sed diam turpis ipsum eu a sed convallis diam."), react_1["default"].createElement("blockquote", null, react_1["default"].createElement("p", null, "Sagittis scelerisque nulla cursus in enim consectetur quam. Dictum urna sed consectetur neque tristique pellentesque. Blandit amet, sed aenean erat arcu morbi.")), react_1["default"].createElement("p", null, "Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim. Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit."), react_1["default"].createElement("figure", null, react_1["default"].createElement("img", {
    className: "w-full rounded-lg",
    src: "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&auto=format&fit=facearea&w=1310&h=873&q=80&facepad=3",
    alt: "",
    width: 1310,
    height: 873
  }), react_1["default"].createElement("figcaption", null, "Sagittis scelerisque nulla cursus in enim consectetur quam.")), react_1["default"].createElement("h2", null, "Everything you need to get up and running"), react_1["default"].createElement("p", null, "Purus morbi dignissim senectus mattis ", react_1["default"].createElement("a", {
    href: "#"
  }, "adipiscing"), ". Amet, massa quam varius orci dapibus volutpat cras. In amet eu ridiculus leo sodales cursus tristique. Tincidunt sed tempus ut viverra ridiculus non molestie. Gravida quis fringilla amet eget dui tempor dignissim. Facilisis auctor venenatis varius nunc, congue erat ac. Cras fermentum convallis quam."), react_1["default"].createElement("p", null, "Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim. Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit."))));
};

exports["default"] = TermsOfService;

/***/ }),

/***/ "./resources/js/Pages/Welcome.tsx":
/*!****************************************!*\
  !*** ./resources/js/Pages/Welcome.tsx ***!
  \****************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var GuestLayout_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Layout/GuestLayout */ "./resources/js/Shared/Components/Layout/GuestLayout.tsx"));

var solid_1 = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");

var TrendyIllustration_1 = __importDefault(__webpack_require__(/*! @/Shared/Svg/TrendyIllustration */ "./resources/js/Shared/Svg/TrendyIllustration.tsx"));

var UpgradeIllustration_1 = __importDefault(__webpack_require__(/*! @/Shared/Svg/UpgradeIllustration */ "./resources/js/Shared/Svg/UpgradeIllustration.tsx"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var PersonalDevelopmentIllustration_1 = __importDefault(__webpack_require__(/*! @/Shared/Svg/PersonalDevelopmentIllustration */ "./resources/js/Shared/Svg/PersonalDevelopmentIllustration.tsx"));

var Welcome = function Welcome() {
  var route = (0, useRoute_1["default"])();
  var t = (0, react_i18next_1.useTranslation)().t;
  var features = [{
    name: t('welcome.First.Card.Title'),
    description: t('welcome.First.Card.Description'),
    icon: outline_1.DocumentTextIcon
  }, {
    name: t('welcome.Second.Card.Title'),
    description: t('welcome.Second.Card.Description'),
    icon: outline_1.SearchIcon
  }, {
    name: t('welcome.Third.Card.Title'),
    description: t('welcome.Third.Card.Description'),
    icon: outline_1.AdjustmentsIcon
  }, {
    name: t('welcome.Fourth.Card.Title'),
    description: t('welcome.Fourth.Card.Description'),
    icon: outline_1.BriefcaseIcon
  }, {
    name: t('welcome.Fifth.Card.Title'),
    description: t('welcome.Fifth.Card.Description'),
    icon: outline_1.BellIcon
  }, {
    name: t('welcome.Sixth.Card.Title'),
    description: t('welcome.Sixth.Card.Description'),
    icon: outline_1.FilterIcon
  }];
  var blogPosts = [{
    id: 1,
    title: 'Boost your conversion rate',
    href: '#',
    date: 'Mar 16, 2020',
    datetime: '2020-03-16',
    category: {
      name: 'Article',
      href: '#'
    },
    imageUrl: 'https://images.unsplash.com/photo-1496128858413-b36217c2ce36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
    preview: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Architecto accusantium praesentium eius, ut atque fuga culpa, similique sequi cum eos quis dolorum.',
    author: {
      name: 'Roel Aufderehar',
      imageUrl: 'https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      href: '#'
    },
    readingLength: '6 min'
  }, {
    id: 2,
    title: 'How to use search engine optimization to drive sales',
    href: '#',
    date: 'Mar 10, 2020',
    datetime: '2020-03-10',
    category: {
      name: 'Video',
      href: '#'
    },
    imageUrl: 'https://images.unsplash.com/photo-1547586696-ea22b4d4235d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
    preview: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit facilis asperiores porro quaerat doloribus, eveniet dolore. Adipisci tempora aut inventore optio animi., tempore temporibus quo laudantium.',
    author: {
      name: 'Brenna Goyette',
      imageUrl: 'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      href: '#'
    },
    readingLength: '4 min'
  }, {
    id: 3,
    title: 'Improve your customer experience',
    href: '#',
    date: 'Feb 12, 2020',
    datetime: '2020-02-12',
    category: {
      name: 'Case Study',
      href: '#'
    },
    imageUrl: 'https://images.unsplash.com/photo-1492724441997-5dc865305da7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1679&q=80',
    preview: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint harum rerum voluptatem quo recusandae magni placeat saepe molestiae, sed excepturi cumque corporis perferendis hic.',
    author: {
      name: 'Daniela Metz',
      imageUrl: 'https://images.unsplash.com/photo-1487412720507-e7ab37603c6f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
      href: '#'
    },
    readingLength: '11 min'
  }];
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("main", null, react_1["default"].createElement("div", {
    className: "bg-white sm:pt-16 lg:overflow-hidden lg:pt-8 lg:pb-14"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-7xl lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "lg:grid lg:grid-cols-2 lg:gap-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 sm:text-center lg:flex lg:items-center lg:px-0 lg:text-left"
  }, react_1["default"].createElement("div", {
    className: "lg:py-24"
  }, react_1["default"].createElement("h1", {
    className: "mt-4 text-4xl font-extrabold tracking-tight text-slate-500 sm:mt-5 sm:text-6xl lg:mt-6 xl:text-6xl"
  }, react_1["default"].createElement("span", {
    className: "block"
  }, "A better way to"), react_1["default"].createElement("span", {
    className: "block bg-gradient-to-r from-orange-200 to-purple-400 bg-clip-text pb-3 text-transparent sm:pb-5"
  }, "kick start your career")), react_1["default"].createElement("p", {
    className: "text-base text-slate-400 sm:text-xl lg:text-lg xl:text-xl"
  }, "Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui Lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat."), react_1["default"].createElement("div", {
    className: "mt-10 sm:mt-12"
  }, react_1["default"].createElement("form", {
    action: "#",
    className: "sm:mx-auto sm:max-w-xl lg:mx-0"
  }, react_1["default"].createElement("div", {
    className: "sm:flex"
  }, react_1["default"].createElement("div", {
    className: "mt-3 sm:mt-0"
  }, react_1["default"].createElement(inertia_react_1.Link, {
    href: route('register'),
    className: "block w-full rounded-md bg-gradient-to-r from-fuchsia-500 to-purple-600 py-3 px-4 font-medium text-fuchsia-50 shadow hover:from-fuchsia-600 hover:to-orange-700 focus:outline-none"
  }, t('register.Create.Account.Button')))))))), react_1["default"].createElement("div", {
    className: "mt-12 -mb-16 sm:-mb-48 lg:relative lg:m-0"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0"
  }, react_1["default"].createElement(PersonalDevelopmentIllustration_1["default"], null)))))), react_1["default"].createElement("div", {
    className: "relative bg-gray-50 pt-16 sm:pt-24 lg:pt-32"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h2", {
    className: "text-base font-semibold uppercase tracking-wider text-orange-600"
  }, t('welcome.First.Headline')), react_1["default"].createElement("p", {
    className: "mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl"
  }, "No Resume? No problem."), react_1["default"].createElement("p", {
    className: "mx-auto mt-5 max-w-prose text-xl text-gray-500"
  }, t('welcome.Marketing.Pitch'))), react_1["default"].createElement("div", {
    className: "mt-12 -mb-10 flex justify-center sm:-mb-24 lg:-mb-80"
  }, react_1["default"].createElement(TrendyIllustration_1["default"], null)))), react_1["default"].createElement("div", {
    className: "relative bg-white py-16 sm:py-24 lg:py-32"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-semibold uppercase tracking-wider text-orange-600"
  }, t('welcome.Second.Headline')), react_1["default"].createElement("p", {
    className: "mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl"
  }, "Everything you need to start your job search"), react_1["default"].createElement("p", {
    className: "mx-auto mt-5 max-w-prose text-xl text-gray-500"
  }, "Below are some of the services offered to help you in your job search or career change."), react_1["default"].createElement("div", {
    className: "mt-12"
  }, react_1["default"].createElement("div", {
    className: "grid grid-cols-1 gap-8 sm:grid-cols-2 lg:grid-cols-3"
  }, features.map(function (feature) {
    return react_1["default"].createElement("div", {
      key: feature.name,
      className: "pt-6"
    }, react_1["default"].createElement("div", {
      className: "flow-root rounded-lg bg-gray-50 px-6 pb-8"
    }, react_1["default"].createElement("div", {
      className: "-mt-6"
    }, react_1["default"].createElement("div", null, react_1["default"].createElement("span", {
      className: "inline-flex items-center justify-center rounded-md bg-gradient-to-r from-fuchsia-500 to-orange-600 p-3 shadow-lg"
    }, react_1["default"].createElement(feature.icon, {
      className: "h-6 w-6 text-white",
      "aria-hidden": "true"
    }))), react_1["default"].createElement("h3", {
      className: "mt-8 text-lg font-medium tracking-tight text-gray-900"
    }, feature.name), react_1["default"].createElement("p", {
      className: "mt-5 text-base text-gray-500"
    }, feature.description))));
  }))))), react_1["default"].createElement("div", {
    className: "bg-gradient-to-r from-fuchsia-800 to-purple-600 pb-16 lg:relative lg:z-10 lg:pb-0"
  }, react_1["default"].createElement("div", {
    className: "lg:mx-auto lg:grid lg:max-w-7xl lg:grid-cols-3 lg:gap-8 lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "relative lg:-my-8"
  }, react_1["default"].createElement("div", {
    "aria-hidden": "true",
    className: "absolute inset-x-0 top-0 h-1/2 bg-white lg:hidden"
  }), react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 sm:max-w-3xl sm:px-6 lg:h-full lg:p-0"
  }, react_1["default"].createElement("div", {
    className: "aspect-w-10 aspect-h-6 overflow-hidden rounded-xl shadow-xl sm:aspect-w-16 sm:aspect-h-7 lg:aspect-none lg:h-full"
  }, react_1["default"].createElement("img", {
    className: "object-cover lg:h-full lg:w-full",
    src: "https://images.unsplash.com/reserve/LJIZlzHgQ7WPSh5KVTCB_Typewriter.jpg?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8NHx8cmVzdW1lfGVufDB8MXwwfHw%3D&auto=format&fit=crop&w=900&q=60",
    alt: ""
  })))), react_1["default"].createElement("div", {
    className: "mt-12 lg:col-span-2 lg:m-0 lg:pl-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 sm:max-w-2xl sm:px-6 lg:max-w-none lg:px-0 lg:py-20"
  }, react_1["default"].createElement("blockquote", null, react_1["default"].createElement("div", null, react_1["default"].createElement("svg", {
    className: "h-12 w-12 text-fuchsia-50 opacity-25",
    fill: "currentColor",
    viewBox: "0 0 32 32",
    "aria-hidden": "true"
  }, react_1["default"].createElement("path", {
    d: "M9.352 4C4.456 7.456 1 13.12 1 19.36c0 5.088 3.072 8.064 6.624 8.064 3.36 0 5.856-2.688 5.856-5.856 0-3.168-2.208-5.472-5.088-5.472-.576 0-1.344.096-1.536.192.48-3.264 3.552-7.104 6.624-9.024L9.352 4zm16.512 0c-4.8 3.456-8.256 9.12-8.256 15.36 0 5.088 3.072 8.064 6.624 8.064 3.264 0 5.856-2.688 5.856-5.856 0-3.168-2.304-5.472-5.184-5.472-.576 0-1.248.096-1.44.192.48-3.264 3.456-7.104 6.528-9.024L25.864 4z"
  })), react_1["default"].createElement("p", {
    className: "mt-6 text-2xl font-medium text-fuchsia-50"
  }, "Start by doing what is necessary, then what is possible, and suddenly you are doing the impossible.")), react_1["default"].createElement("footer", {
    className: "mt-6"
  }, react_1["default"].createElement("p", {
    className: "text-base font-medium text-orange-100"
  }, "Francis of Assisi"))))))), react_1["default"].createElement("div", {
    className: "relative bg-gray-50 py-16 sm:py-24 lg:py-32"
  }, react_1["default"].createElement("div", {
    className: "relative"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 text-center sm:max-w-3xl sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-semibold uppercase tracking-wider text-orange-600"
  }, "Learn"), react_1["default"].createElement("p", {
    className: "mt-2 text-3xl font-extrabold tracking-tight text-slate-600 sm:text-4xl"
  }, "Helpful Resources"), react_1["default"].createElement("p", {
    className: "mx-auto mt-5 max-w-prose text-xl text-gray-500"
  }, "Phasellus lorem quam molestie id quisque diam aenean nulla in. Accumsan in quis quis nunc, ullamcorper malesuada. Eleifend condimentum id viverra nulla.")), react_1["default"].createElement("div", {
    className: "mx-auto mt-12 grid max-w-md gap-8 px-4 sm:max-w-lg sm:px-6 lg:max-w-7xl lg:grid-cols-3 lg:px-8"
  }, blogPosts.map(function (post) {
    return react_1["default"].createElement("div", {
      key: post.id,
      className: "flex flex-col overflow-hidden rounded-lg shadow-lg"
    }, react_1["default"].createElement("div", {
      className: "flex-shrink-0"
    }, react_1["default"].createElement("img", {
      className: "h-48 w-full object-cover",
      src: post.imageUrl,
      alt: ""
    })), react_1["default"].createElement("div", {
      className: "flex flex-1 flex-col justify-between bg-white p-6"
    }, react_1["default"].createElement("div", {
      className: "flex-1"
    }, react_1["default"].createElement("p", {
      className: "text-sm font-medium text-orange-600"
    }, react_1["default"].createElement("a", {
      href: post.category.href,
      className: "hover:underline"
    }, post.category.name)), react_1["default"].createElement("a", {
      href: post.href,
      className: "mt-2 block"
    }, react_1["default"].createElement("p", {
      className: "text-xl font-semibold text-slate-600"
    }, post.title), react_1["default"].createElement("p", {
      className: "mt-3 text-base text-slate-500"
    }, post.preview))), react_1["default"].createElement("div", {
      className: "mt-6 flex items-center"
    }, react_1["default"].createElement("div", {
      className: "flex-shrink-0"
    }, react_1["default"].createElement("a", {
      href: post.author.href
    }, react_1["default"].createElement("img", {
      className: "h-10 w-10 rounded-full",
      src: post.author.imageUrl,
      alt: post.author.name
    }))), react_1["default"].createElement("div", {
      className: "ml-3"
    }, react_1["default"].createElement("p", {
      className: "text-sm font-medium text-gray-900"
    }, react_1["default"].createElement("a", {
      href: post.author.href,
      className: "hover:underline"
    }, post.author.name)), react_1["default"].createElement("div", {
      className: "flex space-x-1 text-sm text-gray-500"
    }, react_1["default"].createElement("time", {
      dateTime: post.datetime
    }, post.date), react_1["default"].createElement("span", {
      "aria-hidden": "true"
    }, "\xB7"), react_1["default"].createElement("span", null, post.readingLength, " read"))))));
  })))), react_1["default"].createElement("div", {
    className: "relative bg-gradient-to-r from-fuchsia-800 to-purple-600"
  }, react_1["default"].createElement("div", {
    className: "relative h-56 bg-white sm:h-72 md:absolute md:left-0 md:h-full md:w-1/2"
  }, react_1["default"].createElement(UpgradeIllustration_1["default"], null), react_1["default"].createElement("div", {
    "aria-hidden": "true",
    className: "absolute inset-0 bg-white mix-blend-multiply"
  })), react_1["default"].createElement("div", {
    className: "relative mx-auto max-w-md px-4 py-12 sm:max-w-7xl sm:px-6 sm:py-20 md:py-28 lg:px-8 lg:py-32"
  }, react_1["default"].createElement("div", {
    className: "md:ml-auto md:w-1/2 md:pl-10"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-semibold uppercase tracking-wider text-fuchsia-50"
  }, "Award winning support"), react_1["default"].createElement("p", {
    className: "mt-2 text-3xl font-extrabold tracking-tight text-white sm:text-4xl"
  }, "We\u2019re here to help"), react_1["default"].createElement("p", {
    className: "mt-3 text-lg text-fuchsia-50"
  }, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Et, egestas tempus tellus etiam sed. Quam a scelerisque amet ullamcorper eu enim et fermentum, augue. Aliquet amet volutpat quisque ut interdum tincidunt duis."), react_1["default"].createElement("div", {
    className: "mt-8"
  }, react_1["default"].createElement("div", {
    className: "inline-flex rounded-md shadow"
  }, react_1["default"].createElement("a", {
    href: "#",
    className: "inline-flex items-center justify-center rounded-md border border-transparent bg-white px-5 py-3 text-base font-medium text-gray-900 hover:bg-gray-50"
  }, "Visit the help center", react_1["default"].createElement(solid_1.ExternalLinkIcon, {
    className: "-mr-1 ml-3 h-5 w-5 text-gray-400",
    "aria-hidden": "true"
  })))))))));
};

Welcome.layout = function (page) {
  return react_1["default"].createElement(GuestLayout_1["default"], {
    children: page
  });
};

exports["default"] = Welcome;

/***/ }),

/***/ "./resources/js/Shared/Components/DatePicker/CustomDatePicker.tsx":
/*!************************************************************************!*\
  !*** ./resources/js/Shared/Components/DatePicker/CustomDatePicker.tsx ***!
  \************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_datepicker_1 = __importStar(__webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js"));

__webpack_require__(/*! react-datepicker/dist/react-datepicker.css */ "./node_modules/react-datepicker/dist/react-datepicker.css");

var locale_1 = __webpack_require__(/*! date-fns/locale */ "./node_modules/date-fns/esm/locale/index.js");

(0, react_datepicker_1.registerLocale)('en', locale_1.enGB);
(0, react_datepicker_1.setDefaultLocale)('en');

var CustomDatePicker = function CustomDatePicker(_a) {
  var handleOnDateChange = _a.handleOnDateChange,
      index = _a.index,
      minDate = _a.minDate,
      maxDate = _a.maxDate,
      initialDate = _a.initialDate,
      placeholder = _a.placeholder;

  var _b = (0, react_1.useState)(initialDate),
      selectedDate = _b[0],
      setSelectedDate = _b[1];

  var handleOnChange = function handleOnChange(date) {
    setSelectedDate(date);
    handleOnDateChange(date, index);
  };

  return react_1["default"].createElement("div", {
    className: "col-span-6 sm:col-span-3"
  }, react_1["default"].createElement(react_datepicker_1["default"], {
    selected: selectedDate,
    onChange: handleOnChange,
    placeholderText: placeholder,
    locale: "en",
    className: "block w-full rounded-md border-gray-300 shadow-sm focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm",
    dateFormat: "yyyy-MM-dd",
    maxDate: maxDate,
    minDate: minDate
  }));
};

exports["default"] = CustomDatePicker;

/***/ }),

/***/ "./resources/js/Shared/Components/Footer/Footer.tsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/Footer/Footer.tsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Footer = function Footer() {
  return react_1["default"].createElement("footer", null, react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "border-t border-gray-200 py-4 text-center text-sm text-gray-500 sm:text-left"
  }, react_1["default"].createElement("span", {
    className: "block sm:inline"
  }, "\xA9 2022 JobAssist Ab."), ' ', react_1["default"].createElement("span", {
    className: "block sm:inline"
  }, "All rights reserved."))));
};

exports["default"] = Footer;

/***/ }),

/***/ "./resources/js/Shared/Components/Footer/GuestFooter.tsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/Footer/GuestFooter.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var footerNavigation = {
  company: [{
    name: 'About',
    href: '#'
  }],
  legal: [{
    name: 'Privacy',
    href: '#'
  }, {
    name: 'Terms',
    href: '#'
  }],
  social: [{
    name: 'Facebook',
    href: '#',
    icon: function icon(props) {
      return react_1["default"].createElement("svg", __assign({
        fill: "currentColor",
        viewBox: "0 0 24 24"
      }, props), react_1["default"].createElement("path", {
        fillRule: "evenodd",
        d: "M22 12c0-5.523-4.477-10-10-10S2 6.477 2 12c0 4.991 3.657 9.128 8.438 9.878v-6.987h-2.54V12h2.54V9.797c0-2.506 1.492-3.89 3.777-3.89 1.094 0 2.238.195 2.238.195v2.46h-1.26c-1.243 0-1.63.771-1.63 1.562V12h2.773l-.443 2.89h-2.33v6.988C18.343 21.128 22 16.991 22 12z",
        clipRule: "evenodd"
      }));
    }
  }, {
    name: 'Instagram',
    href: '#',
    icon: function icon(props) {
      return react_1["default"].createElement("svg", __assign({
        fill: "currentColor",
        viewBox: "0 0 24 24"
      }, props), react_1["default"].createElement("path", {
        fillRule: "evenodd",
        d: "M12.315 2c2.43 0 2.784.013 3.808.06 1.064.049 1.791.218 2.427.465a4.902 4.902 0 011.772 1.153 4.902 4.902 0 011.153 1.772c.247.636.416 1.363.465 2.427.048 1.067.06 1.407.06 4.123v.08c0 2.643-.012 2.987-.06 4.043-.049 1.064-.218 1.791-.465 2.427a4.902 4.902 0 01-1.153 1.772 4.902 4.902 0 01-1.772 1.153c-.636.247-1.363.416-2.427.465-1.067.048-1.407.06-4.123.06h-.08c-2.643 0-2.987-.012-4.043-.06-1.064-.049-1.791-.218-2.427-.465a4.902 4.902 0 01-1.772-1.153 4.902 4.902 0 01-1.153-1.772c-.247-.636-.416-1.363-.465-2.427-.047-1.024-.06-1.379-.06-3.808v-.63c0-2.43.013-2.784.06-3.808.049-1.064.218-1.791.465-2.427a4.902 4.902 0 011.153-1.772A4.902 4.902 0 015.45 2.525c.636-.247 1.363-.416 2.427-.465C8.901 2.013 9.256 2 11.685 2h.63zm-.081 1.802h-.468c-2.456 0-2.784.011-3.807.058-.975.045-1.504.207-1.857.344-.467.182-.8.398-1.15.748-.35.35-.566.683-.748 1.15-.137.353-.3.882-.344 1.857-.047 1.023-.058 1.351-.058 3.807v.468c0 2.456.011 2.784.058 3.807.045.975.207 1.504.344 1.857.182.466.399.8.748 1.15.35.35.683.566 1.15.748.353.137.882.3 1.857.344 1.054.048 1.37.058 4.041.058h.08c2.597 0 2.917-.01 3.96-.058.976-.045 1.505-.207 1.858-.344.466-.182.8-.398 1.15-.748.35-.35.566-.683.748-1.15.137-.353.3-.882.344-1.857.048-1.055.058-1.37.058-4.041v-.08c0-2.597-.01-2.917-.058-3.96-.045-.976-.207-1.505-.344-1.858a3.097 3.097 0 00-.748-1.15 3.098 3.098 0 00-1.15-.748c-.353-.137-.882-.3-1.857-.344-1.023-.047-1.351-.058-3.807-.058zM12 6.865a5.135 5.135 0 110 10.27 5.135 5.135 0 010-10.27zm0 1.802a3.333 3.333 0 100 6.666 3.333 3.333 0 000-6.666zm5.338-3.205a1.2 1.2 0 110 2.4 1.2 1.2 0 010-2.4z",
        clipRule: "evenodd"
      }));
    }
  }, {
    name: 'Twitter',
    href: '#',
    icon: function icon(props) {
      return react_1["default"].createElement("svg", __assign({
        fill: "currentColor",
        viewBox: "0 0 24 24"
      }, props), react_1["default"].createElement("path", {
        d: "M8.29 20.251c7.547 0 11.675-6.253 11.675-11.675 0-.178 0-.355-.012-.53A8.348 8.348 0 0022 5.92a8.19 8.19 0 01-2.357.646 4.118 4.118 0 001.804-2.27 8.224 8.224 0 01-2.605.996 4.107 4.107 0 00-6.993 3.743 11.65 11.65 0 01-8.457-4.287 4.106 4.106 0 001.27 5.477A4.072 4.072 0 012.8 9.713v.052a4.105 4.105 0 003.292 4.022 4.095 4.095 0 01-1.853.07 4.108 4.108 0 003.834 2.85A8.233 8.233 0 012 18.407a11.616 11.616 0 006.29 1.84"
      }));
    }
  }, {
    name: 'GitHub',
    href: '#',
    icon: function icon(props) {
      return react_1["default"].createElement("svg", __assign({
        fill: "currentColor",
        viewBox: "0 0 24 24"
      }, props), react_1["default"].createElement("path", {
        fillRule: "evenodd",
        d: "M12 2C6.477 2 2 6.484 2 12.017c0 4.425 2.865 8.18 6.839 9.504.5.092.682-.217.682-.483 0-.237-.008-.868-.013-1.703-2.782.605-3.369-1.343-3.369-1.343-.454-1.158-1.11-1.466-1.11-1.466-.908-.62.069-.608.069-.608 1.003.07 1.531 1.032 1.531 1.032.892 1.53 2.341 1.088 2.91.832.092-.647.35-1.088.636-1.338-2.22-.253-4.555-1.113-4.555-4.951 0-1.093.39-1.988 1.029-2.688-.103-.253-.446-1.272.098-2.65 0 0 .84-.27 2.75 1.026A9.564 9.564 0 0112 6.844c.85.004 1.705.115 2.504.337 1.909-1.296 2.747-1.027 2.747-1.027.546 1.379.202 2.398.1 2.651.64.7 1.028 1.595 1.028 2.688 0 3.848-2.339 4.695-4.566 4.943.359.309.678.92.678 1.855 0 1.338-.012 2.419-.012 2.747 0 .268.18.58.688.482A10.019 10.019 0 0022 12.017C22 6.484 17.522 2 12 2z",
        clipRule: "evenodd"
      }));
    }
  }, {
    name: 'Dribbble',
    href: '#',
    icon: function icon(props) {
      return react_1["default"].createElement("svg", __assign({
        fill: "currentColor",
        viewBox: "0 0 24 24"
      }, props), react_1["default"].createElement("path", {
        fillRule: "evenodd",
        d: "M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10c5.51 0 10-4.48 10-10S17.51 2 12 2zm6.605 4.61a8.502 8.502 0 011.93 5.314c-.281-.054-3.101-.629-5.943-.271-.065-.141-.12-.293-.184-.445a25.416 25.416 0 00-.564-1.236c3.145-1.28 4.577-3.124 4.761-3.362zM12 3.475c2.17 0 4.154.813 5.662 2.148-.152.216-1.443 1.941-4.48 3.08-1.399-2.57-2.95-4.675-3.189-5A8.687 8.687 0 0112 3.475zm-3.633.803a53.896 53.896 0 013.167 4.935c-3.992 1.063-7.517 1.04-7.896 1.04a8.581 8.581 0 014.729-5.975zM3.453 12.01v-.26c.37.01 4.512.065 8.775-1.215.25.477.477.965.694 1.453-.109.033-.228.065-.336.098-4.404 1.42-6.747 5.303-6.942 5.629a8.522 8.522 0 01-2.19-5.705zM12 20.547a8.482 8.482 0 01-5.239-1.8c.152-.315 1.888-3.656 6.703-5.337.022-.01.033-.01.054-.022a35.318 35.318 0 011.823 6.475 8.4 8.4 0 01-3.341.684zm4.761-1.465c-.086-.52-.542-3.015-1.659-6.084 2.679-.423 5.022.271 5.314.369a8.468 8.468 0 01-3.655 5.715z",
        clipRule: "evenodd"
      }));
    }
  }]
};

var GuestFooter = function GuestFooter() {
  return react_1["default"].createElement("footer", {
    className: "bg-gray-50",
    "aria-labelledby": "footer-heading"
  }, react_1["default"].createElement("h2", {
    id: "footer-heading",
    className: "sr-only"
  }, "Footer"), react_1["default"].createElement("div", {
    className: "mx-auto max-w-md px-4 pt-12 sm:max-w-7xl sm:px-6 lg:px-8 lg:pt-16"
  }, react_1["default"].createElement("div", {
    className: "xl:grid xl:grid-cols-3 xl:gap-8"
  }, react_1["default"].createElement("div", {
    className: "space-y-8 xl:col-span-1"
  }, react_1["default"].createElement("img", {
    className: "h-10",
    src: "https://tailwindui.com/img/logos/workflow-mark-gray-300.svg",
    alt: "JobAdssist"
  }), react_1["default"].createElement("p", {
    className: "text-base text-gray-500"
  }, "Making job search and career change an easy process."), react_1["default"].createElement("div", {
    className: "flex space-x-6"
  }, footerNavigation.social.map(function (item) {
    return react_1["default"].createElement("a", {
      key: item.name,
      href: item.href,
      className: "text-gray-400 hover:text-gray-500"
    }, react_1["default"].createElement("span", {
      className: "sr-only"
    }, item.name), react_1["default"].createElement(item.icon, {
      className: "h-6 w-6",
      "aria-hidden": "true"
    }));
  }))), react_1["default"].createElement("div", {
    className: "mt-12 grid grid-cols-2 gap-8 xl:col-span-2 xl:mt-0"
  }, react_1["default"].createElement("div", {
    className: "md:grid md:grid-cols-2 md:gap-8"
  }), react_1["default"].createElement("div", {
    className: "md:grid md:grid-cols-2 md:gap-8"
  }, react_1["default"].createElement("div", null, react_1["default"].createElement("h3", {
    className: "text-sm font-semibold uppercase tracking-wider text-gray-400"
  }, "Company"), react_1["default"].createElement("ul", {
    role: "list",
    className: "mt-4 space-y-4"
  }, footerNavigation.company.map(function (item) {
    return react_1["default"].createElement("li", {
      key: item.name
    }, react_1["default"].createElement("a", {
      href: item.href,
      className: "text-base text-gray-500 hover:text-gray-900"
    }, item.name));
  }))), react_1["default"].createElement("div", {
    className: "mt-12 md:mt-0"
  }, react_1["default"].createElement("h3", {
    className: "text-sm font-semibold uppercase tracking-wider text-gray-400"
  }, "Legal"), react_1["default"].createElement("ul", {
    role: "list",
    className: "mt-4 space-y-4"
  }, footerNavigation.legal.map(function (item) {
    return react_1["default"].createElement("li", {
      key: item.name
    }, react_1["default"].createElement("a", {
      href: item.href,
      className: "text-base text-gray-500 hover:text-gray-900"
    }, item.name));
  })))))), react_1["default"].createElement("div", {
    className: "mt-12 border-t border-gray-200 py-8"
  }, react_1["default"].createElement("p", {
    className: "text-base text-gray-400 xl:text-center"
  }, "\xA9 2022 JobAdssist Ab. All rights reserved."))));
};

exports["default"] = GuestFooter;

/***/ }),

/***/ "./resources/js/Shared/Components/FormElement/Checkbox.tsx":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Components/FormElement/Checkbox.tsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Checkbox = function Checkbox(_a) {
  var id = _a.id,
      name = _a.name,
      handleOnChange = _a.handleOnChange,
      checked = _a.checked,
      label = _a.label,
      index = _a.index;

  var handleOnCheck = function handleOnCheck(index, name, value) {
    handleOnChange(index, name, value);
  };

  return react_1["default"].createElement("fieldset", {
    className: "space-y-5"
  }, react_1["default"].createElement("legend", {
    className: "sr-only"
  }, "Checkbox"), react_1["default"].createElement("div", {
    className: "relative flex items-start"
  }, react_1["default"].createElement("div", {
    className: "flex h-5 items-center"
  }, react_1["default"].createElement("input", {
    id: id,
    name: name,
    type: "checkbox",
    className: "h-4 w-4 rounded border-fuchsia-500 text-fuchsia-600",
    checked: checked,
    onChange: function onChange(e) {
      return handleOnCheck(index, name, e.target.checked);
    }
  })), react_1["default"].createElement("div", {
    className: "ml-2 text-sm"
  }, react_1["default"].createElement("label", {
    htmlFor: id,
    className: "text-sm text-gray-500",
    id: id
  }, label))));
};

exports["default"] = Checkbox;

/***/ }),

/***/ "./resources/js/Shared/Components/FormElement/SimpleDropdown.tsx":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Components/FormElement/SimpleDropdown.tsx ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var SimpleDropdown = function SimpleDropdown(_a) {
  var dropDownData = _a.dropDownData,
      handleOnSelect = _a.handleOnSelect,
      label = _a.label,
      initialSelection = _a.initialSelection,
      name = _a.name;
  return react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "dropdown",
    className: "block text-sm font-medium text-gray-700"
  }, label), react_1["default"].createElement("select", {
    id: "dropdown",
    name: name,
    className: "mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    defaultValue: initialSelection.id,
    onChange: function onChange(e) {
      return handleOnSelect(name, e.target.value);
    }
  }, dropDownData.map(function (item) {
    return react_1["default"].createElement("option", {
      key: item.id,
      value: item.id
    }, item.name);
  })));
};

exports["default"] = SimpleDropdown;

/***/ }),

/***/ "./resources/js/Shared/Components/Header/DesktopHeader.tsx":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Components/Header/DesktopHeader.tsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ApplicationLogo_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Header/Logo/ApplicationLogo */ "./resources/js/Shared/Components/Header/Logo/ApplicationLogo.tsx"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var react_2 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

var useInitial_1 = __importDefault(__webpack_require__(/*! @/Hooks/useInitial */ "./resources/js/Hooks/useInitial.ts"));

var MainNavigation_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Navigation/MainNavigation */ "./resources/js/Shared/Components/Navigation/MainNavigation.tsx"));

var LanguageSelector_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/LanguageSelector/LanguageSelector */ "./resources/js/Shared/Components/LanguageSelector/LanguageSelector.tsx"));

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var DesktopHeader = function DesktopHeader(_a) {
  var _b;

  var open = _a.open;
  var t = (0, react_i18next_1.useTranslation)().t;
  t('general.Logout');
  var route = (0, useRoute_1["default"])();
  var props = (0, useTypedPage_1["default"])().props;
  var loggedInUserName = (_b = props === null || props === void 0 ? void 0 : props.user) === null || _b === void 0 ? void 0 : _b.loggedInUserName; // create an initial from the logged-in user name

  (0, useInitial_1["default"])(loggedInUserName);
  return react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "relative flex flex-wrap items-center justify-center lg:justify-between"
  }, react_1["default"].createElement("div", {
    className: "absolute left-0 hidden flex-shrink-0 py-5 md:block lg:static"
  }, react_1["default"].createElement(inertia_react_1.Link, {
    href: route('dashboard')
  }, react_1["default"].createElement(ApplicationLogo_1["default"], null))), react_1["default"].createElement("div", {
    className: "hidden lg:ml-4 lg:flex lg:items-center lg:py-5 lg:pr-0.5"
  }, react_1["default"].createElement("button", {
    type: "button",
    className: "flex-shrink-0 rounded-full p-1 text-purple-200 hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
  }, react_1["default"].createElement("span", {
    className: "sr-only"
  }, "View notifications"), react_1["default"].createElement(outline_1.BellIcon, {
    className: "h-6 w-6",
    "aria-hidden": "true"
  })), react_1["default"].createElement("div", {
    className: "relative ml-4 flex-shrink-0"
  }, react_1["default"].createElement(inertia_react_1.Link, {
    method: "post",
    href: route('logout'),
    className: "inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-purple-100"
  }, t('general.Logout')))), react_1["default"].createElement("div", {
    className: "w-full py-5 lg:border-t lg:border-white lg:border-opacity-20"
  }, react_1["default"].createElement("div", {
    className: "lg:grid lg:grid-cols-3 lg:items-center lg:gap-8"
  }, react_1["default"].createElement("div", {
    className: "hidden lg:col-span-2 lg:block"
  }, react_1["default"].createElement(MainNavigation_1["default"], null)), react_1["default"].createElement("div", {
    className: "px-12 lg:px-0"
  }, react_1["default"].createElement("div", {
    className: "mx-auto hidden justify-end sm:flex"
  }, react_1["default"].createElement("div", {
    className: "w-1/2"
  }, react_1["default"].createElement(LanguageSelector_1["default"], null)))))), react_1["default"].createElement("div", {
    className: "absolute right-0 mt-4 flex flex-shrink-0 gap-3 lg:hidden"
  }, react_1["default"].createElement(LanguageSelector_1["default"], null), react_1["default"].createElement(react_2.Popover.Button, {
    className: "inline-flex items-center justify-center rounded-md bg-transparent p-2 text-purple-200 hover:bg-white hover:bg-opacity-10 hover:text-white focus:outline-none focus:ring-2 focus:ring-white"
  }, react_1["default"].createElement("span", {
    className: "sr-only"
  }, "Open main menu"), open ? react_1["default"].createElement(outline_1.XIcon, {
    className: "block h-6 w-6",
    "aria-hidden": "true"
  }) : react_1["default"].createElement(outline_1.MenuIcon, {
    className: "block h-6 w-6",
    "aria-hidden": "true"
  })))));
};

exports["default"] = DesktopHeader;

/***/ }),

/***/ "./resources/js/Shared/Components/Header/GuestHeader.tsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/Header/GuestHeader.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var ApplicationLogo_1 = __importDefault(__webpack_require__(/*! ./Logo/ApplicationLogo */ "./resources/js/Shared/Components/Header/Logo/ApplicationLogo.tsx"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var React = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_2 = __webpack_require__(/*! react */ "./node_modules/react/index.js");

var ziggy_js_1 = __importDefault(__webpack_require__(/*! ziggy-js */ "./node_modules/ziggy-js/dist/index.js"));

var LanguageSelector_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/LanguageSelector/LanguageSelector */ "./resources/js/Shared/Components/LanguageSelector/LanguageSelector.tsx"));

var react_helmet_1 = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var GuestHeader = function GuestHeader() {
  var component = (0, inertia_react_1.usePage)().component;
  var t = (0, react_i18next_1.useTranslation)().t;
  return React.createElement(React.Fragment, null, React.createElement(react_helmet_1.Helmet, null, React.createElement("title", null, "JobAdssist")), React.createElement("header", null, React.createElement(react_1.Popover, {
    className: "relative bg-gradient-to-r from-fuchsia-800 to-purple-600"
  }, React.createElement("div", {
    className: "mx-auto flex items-center justify-between px-4 py-2 sm:px-6 md:justify-start md:space-x-10"
  }, React.createElement(inertia_react_1.Link, {
    href: (0, ziggy_js_1["default"])('welcome')
  }, React.createElement(ApplicationLogo_1["default"], null)), React.createElement(React.Fragment, null, React.createElement("div", {
    className: "-my-2 -mr-2 md:hidden"
  }, React.createElement(react_1.Popover.Button, {
    className: "inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500"
  }, React.createElement("span", {
    className: "sr-only"
  }, "Open menu"), React.createElement(outline_1.MenuIcon, {
    className: "h-6 w-6",
    "aria-hidden": "true"
  }))), React.createElement("div", {
    className: "hidden items-center justify-end md:flex md:flex-1 lg:w-0"
  }, React.createElement(LanguageSelector_1["default"], null), React.createElement("div", {
    className: "ml-3 space-x-4"
  }, !component.startsWith('Auth') && React.createElement(inertia_react_1.Link, {
    href: (0, ziggy_js_1["default"])('login'),
    className: "inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-fuchsia-50"
  }, t('login.Form.Log.In')), !component.startsWith('Auth/Register') && !component.startsWith('Welcome') && React.createElement(inertia_react_1.Link, {
    href: (0, ziggy_js_1["default"])('register'),
    className: "inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-800 shadow-sm hover:bg-fuchsia-50"
  }, t('register.Create.Account.Button')))))), React.createElement(react_1.Transition, {
    as: react_2.Fragment,
    enter: "duration-200 ease-out",
    enterFrom: "opacity-0 scale-95",
    enterTo: "opacity-100 scale-100",
    leave: "duration-100 ease-in",
    leaveFrom: "opacity-100 scale-100",
    leaveTo: "opacity-0 scale-95"
  }, !component.startsWith('Auth') && React.createElement(react_1.Popover.Panel, {
    focus: true,
    className: "absolute inset-x-0 top-0 z-30 origin-top-right transform p-2 transition md:hidden"
  }, React.createElement("div", {
    className: "divide-y-2 divide-gray-50 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5"
  }, React.createElement("div", {
    className: "px-5 pt-5 pb-6"
  }, React.createElement("div", {
    className: "flex items-center justify-between"
  }, React.createElement("div", null), React.createElement("div", {
    className: "-mr-2"
  }, React.createElement(react_1.Popover.Button, {
    className: "inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500"
  }, React.createElement("span", {
    className: "sr-only"
  }, "Close menu"), React.createElement(outline_1.XIcon, {
    className: "h-6 w-6",
    "aria-hidden": "true"
  }))))), React.createElement("div", {
    className: "py-6 px-5"
  }, React.createElement("div", {
    className: "mt-2"
  }, React.createElement(inertia_react_1.Link, {
    href: (0, ziggy_js_1["default"])('login'),
    className: "inline-flex w-full items-center justify-center whitespace-nowrap rounded-md border border-transparent bg-fuchsia-700 px-4 py-2 text-base font-medium text-white shadow-sm shadow-lg shadow-fuchsia-800/50 hover:bg-fuchsia-400 hover:shadow-fuchsia-500/50 sm:w-auto"
  }, t('login.Form.Log.In')), React.createElement(inertia_react_1.Link, {
    href: "#",
    className: "inline-block rounded-md border border-transparent bg-white py-2 px-4 text-base font-medium text-fuchsia-600 shadow-sm hover:bg-fuchsia-50"
  }, "Register")))))))));
};

exports["default"] = GuestHeader;

/***/ }),

/***/ "./resources/js/Shared/Components/Header/Logo/ApplicationLogo.tsx":
/*!************************************************************************!*\
  !*** ./resources/js/Shared/Components/Header/Logo/ApplicationLogo.tsx ***!
  \************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ApplicationLogo = function ApplicationLogo() {
  return react_1["default"].createElement("strong", {
    className: "bg-gradient-to-r from-fuchsia-200 to-purple-200 bg-clip-text pb-3 text-4xl font-bold text-transparent"
  }, "JobAdssist");
};

exports["default"] = ApplicationLogo;

/***/ }),

/***/ "./resources/js/Shared/Components/Header/MobileHeader.tsx":
/*!****************************************************************!*\
  !*** ./resources/js/Shared/Components/Header/MobileHeader.tsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var react_2 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var ApplicationLogo_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Header/Logo/ApplicationLogo */ "./resources/js/Shared/Components/Header/Logo/ApplicationLogo.tsx"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var MobileHeader = function MobileHeader() {
  var navigation = [{
    name: 'Home',
    href: '#',
    current: true
  }, {
    name: 'Profile',
    href: '#',
    current: false
  }, {
    name: 'Resume Manager',
    href: '#',
    current: false
  }, {
    name: 'Job Search',
    href: '#',
    current: false
  }, {
    name: 'Openings',
    href: '#',
    current: false
  }];
  var userNavigation = [{
    name: 'Your Profile',
    href: 'settings.show'
  }, {
    name: 'Settings',
    href: 'settings.show'
  }, {
    name: 'Logout',
    href: 'logout'
  }];
  var user = {
    name: 'Chelsea Hagon',
    email: 'chelseahagon@example.com',
    role: 'Human Resources Manager',
    imageUrl: 'https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80'
  };
  return react_2["default"].createElement(react_2["default"].Fragment, null, react_2["default"].createElement(react_1.Transition.Root, {
    as: react_2.Fragment
  }, react_2["default"].createElement("div", {
    className: "lg:hidden"
  }, react_2["default"].createElement(react_1.Transition.Child, {
    as: react_2.Fragment,
    enter: "duration-150 ease-out",
    enterFrom: "opacity-0",
    enterTo: "opacity-100",
    leave: "duration-150 ease-in",
    leaveFrom: "opacity-100",
    leaveTo: "opacity-0"
  }, react_2["default"].createElement(react_1.Popover.Overlay, {
    className: "fixed inset-0 z-20 bg-black bg-opacity-25"
  })), react_2["default"].createElement(react_1.Transition.Child, {
    as: react_2.Fragment,
    enter: "duration-150 ease-out",
    enterFrom: "opacity-0 scale-95",
    enterTo: "opacity-100 scale-100",
    leave: "duration-150 ease-in",
    leaveFrom: "opacity-100 scale-100",
    leaveTo: "opacity-0 scale-95"
  }, react_2["default"].createElement(react_1.Popover.Panel, {
    focus: true,
    className: "absolute inset-x-0 top-0 z-30 mx-auto w-full max-w-3xl origin-top transform p-2 transition"
  }, react_2["default"].createElement("div", {
    className: "divide-y divide-gray-200 rounded-lg bg-white shadow-lg ring-1 ring-black ring-opacity-5"
  }, react_2["default"].createElement("div", {
    className: "pt-3 pb-2"
  }, react_2["default"].createElement("div", {
    className: "flex items-center justify-between px-4"
  }, react_2["default"].createElement(ApplicationLogo_1["default"], null), react_2["default"].createElement("div", {
    className: "-mr-2"
  }, react_2["default"].createElement(react_1.Popover.Button, {
    className: "inline-flex items-center justify-center rounded-md bg-white p-2 text-gray-400 hover:bg-gray-100 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-cyan-500"
  }, react_2["default"].createElement("span", {
    className: "sr-only"
  }, "Close menu"), react_2["default"].createElement(outline_1.XIcon, {
    className: "h-6 w-6",
    "aria-hidden": "true"
  })))), react_2["default"].createElement("div", {
    className: "mt-3 space-y-1 px-2"
  }, navigation.map(function (item) {
    return react_2["default"].createElement("a", {
      key: item.name,
      href: item.href,
      className: "block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-100 hover:text-gray-800"
    }, item.name);
  }))), react_2["default"].createElement("div", {
    className: "pt-4 pb-2"
  }, react_2["default"].createElement("div", {
    className: "flex items-center px-5"
  }, react_2["default"].createElement("div", {
    className: "flex-shrink-0"
  }, react_2["default"].createElement("img", {
    className: "h-10 w-10 rounded-full",
    src: user.imageUrl,
    alt: ""
  })), react_2["default"].createElement("div", {
    className: "ml-3 min-w-0 flex-1"
  }, react_2["default"].createElement("div", {
    className: "truncate text-base font-medium text-gray-800"
  }, user.name), react_2["default"].createElement("div", {
    className: "truncate text-sm font-medium text-gray-500"
  }, user.email)), react_2["default"].createElement("button", {
    type: "button",
    className: "ml-auto flex-shrink-0 rounded-full bg-white p-1 text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-cyan-500 focus:ring-offset-2"
  }, react_2["default"].createElement("span", {
    className: "sr-only"
  }, "View notifications"), react_2["default"].createElement(outline_1.BellIcon, {
    className: "h-6 w-6",
    "aria-hidden": "true"
  }))), react_2["default"].createElement("div", {
    className: "mt-3 space-y-1 px-2"
  }, userNavigation.map(function (item) {
    return react_2["default"].createElement("a", {
      key: item.name,
      href: item.href,
      className: "block rounded-md px-3 py-2 text-base font-medium text-gray-900 hover:bg-gray-100 hover:text-gray-800"
    }, item.name);
  })))))))));
};

exports["default"] = MobileHeader;

/***/ }),

/***/ "./resources/js/Shared/Components/HomePageComponent/ActivityReminder.tsx":
/*!*******************************************************************************!*\
  !*** ./resources/js/Shared/Components/HomePageComponent/ActivityReminder.tsx ***!
  \*******************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var recentHires = [{
  name: 'Leonard Krasner',
  handle: 'leonardkrasner',
  imageUrl: 'https://images.unsplash.com/photo-1519345182560-3f2917c472ef?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  href: '#'
}, {
  name: 'Floyd Miles',
  handle: 'floydmiles',
  imageUrl: 'https://images.unsplash.com/photo-1463453091185-61582044d556?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  href: '#'
}, {
  name: 'Emily Selman',
  handle: 'emilyselman',
  imageUrl: 'https://images.unsplash.com/photo-1502685104226-ee32379fefbe?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  href: '#'
}, {
  name: 'Kristin Watson',
  handle: 'kristinwatson',
  imageUrl: 'https://images.unsplash.com/photo-1500917293891-ef795e70e1f6?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80',
  href: '#'
}];

var ActivityReminder = function ActivityReminder() {
  return react_1["default"].createElement("section", {
    "aria-labelledby": "recent-hires-title"
  }, react_1["default"].createElement("div", {
    className: "overflow-hidden rounded-lg bg-white shadow"
  }, react_1["default"].createElement("div", {
    className: "p-6"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-medium text-gray-900",
    id: "recent-hires-title"
  }, "Activities & Reminders"), react_1["default"].createElement("div", {
    className: "mt-6 flow-root"
  }, react_1["default"].createElement("ul", {
    role: "list",
    className: "-my-5 divide-y divide-gray-200"
  }, recentHires.map(function (person) {
    return react_1["default"].createElement("li", {
      key: person.handle,
      className: "py-4"
    }, react_1["default"].createElement("div", {
      className: "flex items-center space-x-4"
    }, react_1["default"].createElement("div", {
      className: "flex-shrink-0"
    }, react_1["default"].createElement("img", {
      className: "h-8 w-8 rounded-full",
      src: person.imageUrl,
      alt: ""
    })), react_1["default"].createElement("div", {
      className: "min-w-0 flex-1"
    }, react_1["default"].createElement("p", {
      className: "truncate text-sm font-medium text-gray-900"
    }, person.name), react_1["default"].createElement("p", {
      className: "truncate text-sm text-gray-500"
    }, '@' + person.handle)), react_1["default"].createElement("div", null, react_1["default"].createElement("a", {
      href: person.href,
      className: "inline-flex items-center rounded-full border border-gray-300 bg-white px-2.5 py-0.5 text-sm font-medium leading-5 text-gray-700 shadow-sm hover:bg-gray-50"
    }, "View"))));
  }))), react_1["default"].createElement("div", {
    className: "mt-6"
  }, react_1["default"].createElement("a", {
    href: "#",
    className: "flex w-full items-center justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50"
  }, "View all")))));
};

exports["default"] = ActivityReminder;

/***/ }),

/***/ "./resources/js/Shared/Components/HomePageComponent/BlogPanel.tsx":
/*!************************************************************************!*\
  !*** ./resources/js/Shared/Components/HomePageComponent/BlogPanel.tsx ***!
  \************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

function classNames() {
  var classes = [];

  for (var _i = 0; _i < arguments.length; _i++) {
    classes[_i] = arguments[_i];
  }

  return classes.filter(Boolean).join(' ');
}

var actions = [{
  icon: outline_1.ClockIcon,
  name: 'Request time off',
  href: '#',
  iconForeground: 'text-teal-700',
  iconBackground: 'bg-teal-50'
}, {
  icon: outline_1.BadgeCheckIcon,
  name: 'Benefits',
  href: '#',
  iconForeground: 'text-purple-700',
  iconBackground: 'bg-purple-50'
}, {
  icon: outline_1.UsersIcon,
  name: 'Schedule a one-on-one',
  href: '#',
  iconForeground: 'text-sky-700',
  iconBackground: 'bg-sky-50'
}, {
  icon: outline_1.CashIcon,
  name: 'Payroll',
  href: '#',
  iconForeground: 'text-yellow-700',
  iconBackground: 'bg-yellow-50'
}, {
  icon: outline_1.ReceiptRefundIcon,
  name: 'Submit an expense',
  href: '#',
  iconForeground: 'text-rose-700',
  iconBackground: 'bg-rose-50'
}, {
  icon: outline_1.AcademicCapIcon,
  name: 'Training',
  href: '#',
  iconForeground: 'text-indigo-700',
  iconBackground: 'bg-indigo-50'
}];

var BlogPanel = function BlogPanel() {
  return react_1["default"].createElement("section", {
    "aria-labelledby": "quick-links-title"
  }, react_1["default"].createElement("div", {
    className: "divide-y divide-gray-200 overflow-hidden rounded-lg bg-gray-200 shadow sm:grid sm:grid-cols-2 sm:gap-px sm:divide-y-0"
  }, react_1["default"].createElement("h2", {
    className: "sr-only",
    id: "quick-links-title"
  }, "Quick links"), actions.map(function (action, actionIdx) {
    return react_1["default"].createElement("div", {
      key: action.name,
      className: classNames(actionIdx === 0 ? 'rounded-tl-lg rounded-tr-lg sm:rounded-tr-none' : '', actionIdx === 1 ? 'sm:rounded-tr-lg' : '', actionIdx === actions.length - 2 ? 'sm:rounded-bl-lg' : '', actionIdx === actions.length - 1 ? 'rounded-bl-lg rounded-br-lg sm:rounded-bl-none' : '', 'group relative bg-white p-6 focus-within:ring-2 focus-within:ring-inset focus-within:ring-cyan-500')
    }, react_1["default"].createElement("div", null, react_1["default"].createElement("span", {
      className: classNames(action.iconBackground, action.iconForeground, 'inline-flex rounded-lg p-3 ring-4 ring-white')
    }, react_1["default"].createElement(action.icon, {
      className: "h-6 w-6",
      "aria-hidden": "true"
    }))), react_1["default"].createElement("div", {
      className: "mt-8"
    }, react_1["default"].createElement("h3", {
      className: "text-lg font-medium"
    }, react_1["default"].createElement("a", {
      href: action.href,
      className: "focus:outline-none"
    }, react_1["default"].createElement("span", {
      className: "absolute inset-0",
      "aria-hidden": "true"
    }), action.name)), react_1["default"].createElement("p", {
      className: "mt-2 text-sm text-gray-500"
    }, "Doloribus dolores nostrum quia qui natus officia quod et dolorem. Sit repellendus qui ut at blanditiis et quo et molestiae.")), react_1["default"].createElement("span", {
      className: "pointer-events-none absolute top-6 right-6 text-gray-300 group-hover:text-gray-400",
      "aria-hidden": "true"
    }, react_1["default"].createElement("svg", {
      className: "h-6 w-6",
      xmlns: "http://www.w3.org/2000/svg",
      fill: "currentColor",
      viewBox: "0 0 24 24"
    }, react_1["default"].createElement("path", {
      d: "M20 4h1a1 1 0 00-1-1v1zm-1 12a1 1 0 102 0h-2zM8 3a1 1 0 000 2V3zM3.293 19.293a1 1 0 101.414 1.414l-1.414-1.414zM19 4v12h2V4h-2zm1-1H8v2h12V3zm-.707.293l-16 16 1.414 1.414 16-16-1.414-1.414z"
    }))));
  })));
};

exports["default"] = BlogPanel;

/***/ }),

/***/ "./resources/js/Shared/Components/HomePageComponent/RecentOpeningAnnouncement.tsx":
/*!****************************************************************************************!*\
  !*** ./resources/js/Shared/Components/HomePageComponent/RecentOpeningAnnouncement.tsx ***!
  \****************************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var announcements = [{
  id: 1,
  title: 'Office closed on July 2nd',
  href: '#',
  preview: 'Cum qui rem deleniti. Suscipit in dolor veritatis sequi aut. Vero ut earum quis deleniti. Ut a sunt eum cum ut repudiandae possimus. Nihil ex tempora neque cum consectetur dolores.'
}, {
  id: 2,
  title: 'New password policy',
  href: '#',
  preview: 'Alias inventore ut autem optio voluptas et repellendus. Facere totam quaerat quam quo laudantium cumque eaque excepturi vel. Accusamus maxime ipsam reprehenderit rerum id repellendus rerum. Culpa cum vel natus. Est sit autem mollitia.'
}, {
  id: 3,
  title: 'Office closed on July 2nd',
  href: '#',
  preview: 'Tenetur libero voluptatem rerum occaecati qui est molestiae exercitationem. Voluptate quisquam iure assumenda consequatur ex et recusandae. Alias consectetur voluptatibus. Accusamus a ab dicta et. Consequatur quis dignissimos voluptatem nisi.'
}];

var RecentOpeningAnnouncement = function RecentOpeningAnnouncement() {
  return react_1["default"].createElement("section", {
    "aria-labelledby": "announcements-title"
  }, react_1["default"].createElement("div", {
    className: "overflow-hidden rounded-lg bg-white shadow"
  }, react_1["default"].createElement("div", {
    className: "p-6"
  }, react_1["default"].createElement("h2", {
    className: "text-base font-medium text-gray-900",
    id: "announcements-title"
  }, "Recent Job Openings"), react_1["default"].createElement("div", {
    className: "mt-6 flow-root"
  }, react_1["default"].createElement("ul", {
    role: "list",
    className: "-my-5 divide-y divide-gray-200"
  }, announcements.map(function (announcement) {
    return react_1["default"].createElement("li", {
      key: announcement.id,
      className: "py-5"
    }, react_1["default"].createElement("div", {
      className: "relative focus-within:ring-2 focus-within:ring-cyan-500"
    }, react_1["default"].createElement("h3", {
      className: "text-sm font-semibold text-gray-800"
    }, react_1["default"].createElement("a", {
      href: announcement.href,
      className: "hover:underline focus:outline-none"
    }, react_1["default"].createElement("span", {
      className: "absolute inset-0",
      "aria-hidden": "true"
    }), announcement.title)), react_1["default"].createElement("p", {
      className: "mt-1 text-sm text-gray-600 line-clamp-2"
    }, announcement.preview)));
  }))), react_1["default"].createElement("div", {
    className: "mt-6"
  }, react_1["default"].createElement("a", {
    href: "#",
    className: "flex w-full items-center justify-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50"
  }, "View all")))));
};

exports["default"] = RecentOpeningAnnouncement;

/***/ }),

/***/ "./resources/js/Shared/Components/Image/EmptyProfileImage.tsx":
/*!********************************************************************!*\
  !*** ./resources/js/Shared/Components/Image/EmptyProfileImage.tsx ***!
  \********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var EmptyProfileImage = function EmptyProfileImage(_a) {
  var classes = _a.classes;
  return react_1["default"].createElement("span", {
    className: classes
  }, react_1["default"].createElement("svg", {
    className: "h-full w-full text-gray-300",
    fill: "currentColor",
    viewBox: "0 0 24 24"
  }, react_1["default"].createElement("path", {
    d: "M24 20.993V24H0v-2.996A14.977 14.977 0 0112.004 15c4.904 0 9.26 2.354 11.996 5.993zM16.002 8.999a4 4 0 11-8 0 4 4 0 018 0z"
  })));
};

exports["default"] = EmptyProfileImage;

/***/ }),

/***/ "./resources/js/Shared/Components/LanguageSelector/LanguageSelector.tsx":
/*!******************************************************************************!*\
  !*** ./resources/js/Shared/Components/LanguageSelector/LanguageSelector.tsx ***!
  \******************************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var i18next_1 = __importDefault(__webpack_require__(/*! i18next */ "./node_modules/i18next/dist/cjs/i18next.js"));

var i18n_1 = __webpack_require__(/*! @/i18n/i18n */ "./resources/js/i18n/i18n.ts");

var LanguageSelector = function LanguageSelector() {
  return react_1["default"].createElement("div", null, react_1["default"].createElement("label", {
    htmlFor: "languages",
    className: "sr-only block text-sm font-medium text-gray-700"
  }, "Language Selector"), react_1["default"].createElement("select", {
    id: "languages",
    name: "languages",
    className: "mt-1 block w-full rounded-md border-gray-300 py-2 pl-3 pr-10 text-base focus:border-fuchsia-500 focus:outline-none focus:ring-fuchsia-500 sm:text-sm",
    defaultValue: i18next_1["default"].language,
    onChange: function onChange(e) {
      return i18next_1["default"].changeLanguage(e.target.value);
    }
  }, Object.keys(i18n_1.availableLanguages).map(function (language) {
    return react_1["default"].createElement("option", {
      key: language,
      value: language
    }, i18n_1.availableLanguages[language].nativeName);
  })));
};

exports["default"] = LanguageSelector;

/***/ }),

/***/ "./resources/js/Shared/Components/Layout/AppLayout.tsx":
/*!*************************************************************!*\
  !*** ./resources/js/Shared/Components/Layout/AppLayout.tsx ***!
  \*************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var Footer_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Footer/Footer */ "./resources/js/Shared/Components/Footer/Footer.tsx"));

var DesktopHeader_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Header/DesktopHeader */ "./resources/js/Shared/Components/Header/DesktopHeader.tsx"));

var MobileHeader_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Header/MobileHeader */ "./resources/js/Shared/Components/Header/MobileHeader.tsx"));

var react_2 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var react_helmet_1 = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");

var AppLayout = function AppLayout(_a) {
  var children = _a.children,
      title = _a.title;
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement(react_helmet_1.Helmet, null, react_1["default"].createElement("title", null, title ? "JobAdssist - ".concat(title) : 'JobAdssist')), react_1["default"].createElement("div", {
    className: "min-h-full"
  }, react_1["default"].createElement(react_2.Popover, {
    as: "header",
    className: "bg-gradient-to-r from-fuchsia-800 to-purple-600 pb-24"
  }, function (_a) {
    var open = _a.open;
    return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement(DesktopHeader_1["default"], {
      open: open
    }), react_1["default"].createElement(MobileHeader_1["default"], null));
  }), react_1["default"].createElement("main", {
    className: "-mt-20 pb-8"
  }, react_1["default"].createElement("div", {
    className: "mx-auto max-w-3xl px-4 sm:px-6 lg:max-w-7xl lg:px-8"
  }, react_1["default"].createElement("div", {
    className: "space-y-8 overflow-hidden rounded-lg bg-white py-8 px-4 sm:p-6 lg:pb-8"
  }, children))), react_1["default"].createElement(Footer_1["default"], null)));
};

exports["default"] = AppLayout;

/***/ }),

/***/ "./resources/js/Shared/Components/Layout/GuestLayout.tsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/Layout/GuestLayout.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var GuestHeader_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Header/GuestHeader */ "./resources/js/Shared/Components/Header/GuestHeader.tsx"));

var GuestFooter_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/Footer/GuestFooter */ "./resources/js/Shared/Components/Footer/GuestFooter.tsx"));

var GuestLayout = function GuestLayout(_a) {
  var children = _a.children;
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement(GuestHeader_1["default"], null), react_1["default"].createElement("div", null, children), react_1["default"].createElement(GuestFooter_1["default"], null));
};

exports["default"] = GuestLayout;

/***/ }),

/***/ "./resources/js/Shared/Components/LoadingScreen.tsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/LoadingScreen.tsx ***!
  \**********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var LoadingScreen = function LoadingScreen() {
  return react_1["default"].createElement("p", null, "Loading....");
};

exports["default"] = LoadingScreen;

/***/ }),

/***/ "./resources/js/Shared/Components/Modal/ModalPortal.tsx":
/*!**************************************************************!*\
  !*** ./resources/js/Shared/Components/Modal/ModalPortal.tsx ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_dom_1 = __importDefault(__webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js"));

var ModalPortal = function ModalPortal(_a) {
  var children = _a.children;
  return react_dom_1["default"].createPortal(children, document.body);
};

exports["default"] = ModalPortal;

/***/ }),

/***/ "./resources/js/Shared/Components/Modal/Notification.tsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/Modal/Notification.tsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_2 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var outline_1 = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");

var useTimeout_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTimeout */ "./resources/js/Hooks/useTimeout.ts"));

var Notification = function Notification(_a) {
  var show = _a.show,
      title = _a.title,
      body = _a.body,
      closeNotification = _a.closeNotification,
      messageType = _a.messageType;

  var _b = (0, useTimeout_1["default"])(),
      setTimeout = _b[0],
      clearTimeout = _b[1]; // remove the notification after 3 seconds


  setTimeout(function () {
    closeNotification();
    clearTimeout();
  }, 2000);
  return react_1["default"].createElement(react_1["default"].Fragment, null, react_1["default"].createElement("div", {
    "aria-live": "assertive",
    className: "pointer-events-none fixed inset-0 flex items-end px-4 py-6 sm:items-start sm:p-6"
  }, react_1["default"].createElement("div", {
    className: "flex w-full flex-col items-end space-y-6"
  }, react_1["default"].createElement(react_2.Transition, {
    show: show,
    as: react_1.Fragment,
    enter: "transform ease-out duration-300 transition",
    enterFrom: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2",
    enterTo: "translate-y-0 opacity-100 sm:translate-x-0",
    leave: "transition ease-in duration-100",
    leaveFrom: "opacity-100",
    leaveTo: "opacity-0"
  }, react_1["default"].createElement("div", {
    className: "pointer-events-auto w-full max-w-sm overflow-hidden rounded-lg bg-gradient-to-r from-purple-800 to-fuchsia-600 shadow-lg ring-1 ring-black ring-opacity-5"
  }, react_1["default"].createElement("div", {
    className: "p-4"
  }, react_1["default"].createElement("div", {
    className: "flex items-start"
  }, react_1["default"].createElement("div", {
    className: "flex-shrink-0"
  }, messageType === 'error' ? react_1["default"].createElement(outline_1.ExclamationCircleIcon, {
    className: "h-6 w-6 text-rose-400",
    "aria-hidden": "true"
  }) : react_1["default"].createElement(outline_1.CheckCircleIcon, {
    className: "h-6 w-6 text-lime-400",
    "aria-hidden": "true"
  })), react_1["default"].createElement("div", {
    className: "ml-3 w-0 flex-1 pt-0.5"
  }, react_1["default"].createElement("p", {
    className: "text-sm font-medium text-fuchsia-50"
  }, title), body && react_1["default"].createElement("p", {
    className: "mt-1 text-sm text-fuchsia-50"
  }, body)))))))));
};

exports["default"] = Notification;

/***/ }),

/***/ "./resources/js/Shared/Components/Modal/PermissionModal.tsx":
/*!******************************************************************!*\
  !*** ./resources/js/Shared/Components/Modal/PermissionModal.tsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/index.cjs");

var react_2 = __importStar(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PermissionModal = function PermissionModal(_a) {
  var data = _a.data,
      setData = _a.setData,
      isActive = _a.isActive,
      handleActive = _a.handleActive,
      pageInfo = _a.pageInfo,
      processing = _a.processing,
      handleSubmit = _a.handleSubmit,
      error = _a.error;
  return react_2["default"].createElement(react_1.Transition.Root, {
    show: isActive,
    as: react_2.Fragment
  }, react_2["default"].createElement(react_1.Dialog, {
    as: "div",
    className: "fixed inset-0 z-10 overflow-y-auto",
    onClose: handleActive(false)
  }, react_2["default"].createElement("div", {
    className: "flex min-h-screen items-end justify-center px-4 pt-4 pb-20 text-center sm:block sm:p-0"
  }, react_2["default"].createElement(react_1.Transition.Child, {
    as: react_2.Fragment,
    enter: "ease-out duration-300",
    enterFrom: "opacity-0",
    enterTo: "opacity-100",
    leave: "ease-in duration-200",
    leaveFrom: "opacity-100",
    leaveTo: "opacity-0"
  }, react_2["default"].createElement(react_1.Dialog.Overlay, {
    className: "fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity"
  })), react_2["default"].createElement("span", {
    className: "hidden sm:inline-block sm:h-screen sm:align-middle",
    "aria-hidden": "true"
  }, "\u200B"), react_2["default"].createElement(react_1.Transition.Child, {
    as: react_2.Fragment,
    enter: "ease-out duration-300",
    enterFrom: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95",
    enterTo: "opacity-100 translate-y-0 sm:scale-100",
    leave: "ease-in duration-200",
    leaveFrom: "opacity-100 translate-y-0 sm:scale-100",
    leaveTo: "opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
  }, react_2["default"].createElement("form", {
    onSubmit: handleSubmit,
    className: "inline-block transform overflow-hidden rounded-lg bg-white text-left align-bottom shadow-xl transition-all sm:my-8 sm:w-full sm:max-w-lg sm:align-middle"
  }, react_2["default"].createElement("div", {
    className: "bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4"
  }, react_2["default"].createElement("div", {
    className: "sm:flex sm:items-start"
  }, react_2["default"].createElement("div", {
    className: "mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left"
  }, react_2["default"].createElement(react_1.Dialog.Title, {
    as: "h3",
    className: "text-lg font-medium leading-6 text-violet-900"
  }, pageInfo.title), react_2["default"].createElement("div", {
    className: "mt-2"
  }, react_2["default"].createElement("p", {
    className: "text-sm text-gray-500"
  }, pageInfo.description)))), react_2["default"].createElement("div", {
    className: "text mt-3 pl-4 text-center"
  }, react_2["default"].createElement("label", {
    htmlFor: "password",
    className: "sr-only"
  }, "Password"), react_2["default"].createElement("input", {
    type: "password",
    name: "password",
    id: "password",
    className: "block w-full rounded-md border-gray-300 shadow-sm focus:border-gray-500 focus:ring-gray-500 sm:text-sm",
    placeholder: "Password",
    value: data.password,
    onChange: function onChange(e) {
      return setData('password', e.currentTarget.value);
    },
    required: true
  }), error && react_2["default"].createElement("p", {
    className: "mt-2 text-sm text-red-600",
    id: "password-error"
  }, "The provided password was incorrect."))), react_2["default"].createElement("div", {
    className: "bg-gray-50 px-4 py-3 sm:flex sm:flex-row-reverse sm:px-6"
  }, react_2["default"].createElement("button", {
    type: "submit",
    className: "inline-flex w-full justify-center rounded-md border border-transparent px-4 py-2 text-base font-medium text-white shadow-sm focus:outline-none sm:ml-3 sm:w-auto sm:text-sm ".concat(pageInfo.btnName.startsWith('Delete') ? ' bg-red-600 hover:bg-red-300 focus:ring-red-500' : ' bg-violet-600 hover:bg-violet-700  focus:ring-gray-50'),
    disabled: processing
  }, pageInfo.btnName), react_2["default"].createElement("button", {
    type: "button",
    className: "mt-3 inline-flex w-full justify-center rounded-md border border-gray-300 bg-white bg-slate-600 px-4 py-2 text-base font-medium text-white shadow-sm hover:bg-slate-700 focus:outline-none sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm",
    onClick: handleActive(false)
  }, "Cancel")))))));
};

exports["default"] = PermissionModal;

/***/ }),

/***/ "./resources/js/Shared/Components/Navigation/MainNavigation.tsx":
/*!**********************************************************************!*\
  !*** ./resources/js/Shared/Components/Navigation/MainNavigation.tsx ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var useRoute_1 = __importDefault(__webpack_require__(/*! @/Hooks/useRoute */ "./resources/js/Hooks/useRoute.ts"));

var useTypedPage_1 = __importDefault(__webpack_require__(/*! @/Hooks/useTypedPage */ "./resources/js/Hooks/useTypedPage.ts"));

function classNames() {
  var classes = [];

  for (var _i = 0; _i < arguments.length; _i++) {
    classes[_i] = arguments[_i];
  }

  return classes.filter(Boolean).join(' ');
}

var MainNavigation = function MainNavigation() {
  var selectedPage = (0, useTypedPage_1["default"])().props.selectedPage;
  var navigation = [{
    name: 'Home',
    href: 'dashboard',
    current: false
  }, {
    name: 'Profile',
    href: 'profile.show',
    current: false
  }, {
    name: 'Resume Manager',
    href: 'resume.manager.index',
    current: false
  }, {
    name: 'Job Search',
    href: 'dashboard',
    current: false
  }, {
    name: 'Openings',
    href: 'dashboard',
    current: false
  }, {
    name: 'Settings',
    href: 'settings.show',
    current: false
  }];
  navigation.filter(function (nav) {
    nav.current = nav.name === selectedPage;
  });
  var route = (0, useRoute_1["default"])();
  return react_1["default"].createElement("nav", {
    className: "flex space-x-4"
  }, navigation.map(function (item) {
    return react_1["default"].createElement(inertia_react_1.Link, {
      key: item.name,
      href: route(item.href),
      className: classNames(item.current ? 'bg-fuchsia-50 text-fuchsia-900 hover:bg-purple-100 hover:text-fuchsia-500' : 'text-fuchsia-100 hover:bg-fuchsia-50 hover:text-fuchsia-900', 'inline-flex items-center rounded-md py-2 px-3 text-sm font-medium'),
      "aria-current": item.current ? 'page' : undefined
    }, item.name);
  }));
};

exports["default"] = MainNavigation;

/***/ }),

/***/ "./resources/js/Shared/Svg/PersonalDevelopmentIllustration.tsx":
/*!*********************************************************************!*\
  !*** ./resources/js/Shared/Svg/PersonalDevelopmentIllustration.tsx ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var PersonalDevelopmentIllustration = function PersonalDevelopmentIllustration() {
  return react_1["default"].createElement("svg", {
    id: "e35487a4-c163-43e6-b333-760f8fd358d9",
    "data-name": "Layer 1",
    width: "865.76",
    height: "682.89",
    viewBox: "0 0 865.76 682.89",
    className: "w-full lg:absolute lg:inset-y-0 lg:left-0 lg:h-full lg:w-auto lg:max-w-none"
  }, react_1["default"].createElement("path", {
    d: "M559.51,68.66s6.1-3.6,11.45-4.31a5.91,5.91,0,0,1,5.9,3c1.15,2.07,3.22,4.7,6.36,5h12.23V57.82H584a6.32,6.32,0,0,1-4.7-2.08l-1.77-1.94a5.12,5.12,0,0,0-3.44-1.67c-3.47-.23-10.44-.28-14.62,2.45Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    clipPath: "url(#fd76c3b0-43e9-4e39-bfa4-257bf3adc6a3)"
  }, react_1["default"].createElement("path", {
    d: "M574.13,52.13V64.82s2,.45,3.35,3.5a9.48,9.48,0,0,0,2.64,2.8l.31-14.42a28,28,0,0,0-2.16-2.13A6.07,6.07,0,0,0,574.13,52.13Z",
    fill: "#fff",
    opacity: "0.4"
  })), react_1["default"].createElement("polygon", {
    points: "836.75 634.8 28.94 621.55 18.94 564.44 846.82 564.44 836.75 634.8",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("polygon", {
    points: "18.94 564.44 121.13 527.75 753.19 530.83 846.82 564.44 18.94 564.44",
    fill: "#bcbec0"
  }), react_1["default"].createElement("polygon", {
    points: "113.69 490.01 121.13 527.75 753.19 530.83 755.17 491.9 113.69 490.01",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("polygon", {
    points: "755.17 491.9 699.67 473.13 177.36 473.13 113.69 490.01 755.17 491.9",
    fill: "#bcbec0"
  }), react_1["default"].createElement("polygon", {
    points: "173.95 446.5 177.36 473.13 699.67 473.13 699.67 449.41 173.95 446.5",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("polygon", {
    points: "212.42 442.15 173.95 446.5 699.67 449.41 660.28 439.25 212.42 442.15",
    fill: "#bcbec0"
  }), react_1["default"].createElement("polygon", {
    points: "210.24 418.92 212.42 442.15 660.28 439.25 660.28 418.92 210.24 418.92",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("polygon", {
    points: "210.24 418.92 239.54 416.36 638.43 415.96 660.28 418.92 210.24 418.92",
    fill: "#bcbec0"
  }), react_1["default"].createElement("polygon", {
    points: "239.54 416.36 239.54 399.74 258.93 395.48 258.93 384.61 278.98 381.12 278.98 367.9 294.81 367.9 294.81 356.32 309.06 356.32 309.06 345.77 321.72 345.77 321.72 335.74 332.8 335.74 332.8 327.3 344.94 327.3 344.94 318.33 354.97 318.33 623.83 142.23 552.06 83.74 565.06 83.74 667.63 154.08 518.82 307.68 518.82 319.58 530.97 319.58 531.97 330.52 541.11 329.85 545.31 344.99 565.06 345.53 565.06 358.51 583.42 359.05 583.96 366.73 594.23 366.73 594.23 378.8 615.32 382.3 615.05 397.71 638.03 400.95 638.43 415.96 239.54 416.36",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("line", {
    x1: "258.92",
    y1: "395.48",
    x2: "615.05",
    y2: "397.71",
    fill: "none",
    stroke: "#bcbec0",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("line", {
    x1: "278.98",
    y1: "381.12",
    x2: "615.32",
    y2: "382.3",
    fill: "none",
    stroke: "#bcbec0",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("path", {
    d: "M388.25,481.39a66.34,66.34,0,0,1,2.43,12.84s9.37-3.12,12.49,4.51V476.88Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M436.78,412.88h22.11a34.46,34.46,0,0,0-1.6-10.54,14.66,14.66,0,0,1-.82-6.73c.55-5.15,1.12-13.67-.68-16.81,0,0-7.75-8.92-15.5,0,0,0-.8,12.78-1,18.08a32.1,32.1,0,0,1-1.91,9.49A14.33,14.33,0,0,0,436.78,412.88Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "440.98",
    y: "351.85",
    width: "11.72",
    height: "21.32",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M389.55,265.93s-14.67,13-9.7,47.38c0,0,3.71,68.48,8.55,86.43,0,0-16.72,17.56-9.58,50.06,0,0,4.86,34.79,14,34.41,0,0,12.62-.77,14.91-13.39,0,0-1.15-55.82,2.3-68.05,0,0,4.39-3.23,4.68-15,0,0,1.31-72.55,11.88-91.67l7.9-.76v53.14s13.22,13,21,6.12c0,0,10.41-31.35,6.58-43.59l-4.2-13.76s23.32-11.83,18-28.09l-8.38-19.91Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M498.42,255.7l.88,1.93L513.09,249s-.66-5.19-5.7-4.52C507.39,244.45,496.76,248.13,498.42,255.7Z",
    fill: "#e6e7e8"
  }), react_1["default"].createElement("path", {
    d: "M452.84,137.8l6.07-.62s-6.61,12.81-1.21,36A30.72,30.72,0,0,0,459,177.4l1.1,2.83s-10.18,17.22-9.61,34.48a6.8,6.8,0,0,0,.83,3l13.93,26.16s-25.5,2.83-34.25,10.56l-4.89-7.47-8,9.27H390.3s-5.71-.52.88-6.44c0,0,25.39-24.21,21.27-33.22,0,0-4.31-7.4-7.82-12.46l12.45-61.46S445.58,133.48,452.84,137.8Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M460.57,180.68c1.82,1.54,7.91,7.71,11.6,24.61a1.66,1.66,0,0,1,0,.22c0,1.66.42,21.85,10.28,34.19l0,.05c.41.61,10.38,15.51,14.92,19.29,0,0,2.32,1.29,1.81-2.83,0,0-3.34-5.65,7.92-10.53l.13-.05a4.09,4.09,0,0,1,4.78,2.27c.58,1.09,2.31.39,1.87-.76-.37-1-1.76-2.37-5.31-4.26a.82.82,0,0,1-.22-.16c-1.12-1.08-11.55-11.48-13.29-25.33a.51.51,0,0,0,0-.13c-.42-1.36-7.87-25.86-13.43-39.86-1.32-3.31-2.47-6.67-3.48-10.08-2.3-7.8-8-22.83-18.93-30a1,1,0,0,0-1.49.47c-1.51,3.59-8.92,23.71,2.18,42.16A3.63,3.63,0,0,0,460.57,180.68Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M460.57,180.68c1.82,1.54,7.91,7.71,11.6,24.61a1.66,1.66,0,0,1,0,.22c0,1.66.42,21.85,10.28,34.19l0,.05c.41.61,10.38,15.51,14.92,19.29,0,0,2.32,1.29,1.81-2.83,0,0-3.34-5.65,7.92-10.53l.13-.05a4.09,4.09,0,0,1,4.78,2.27c.58,1.09,2.31.39,1.87-.76-.37-1-1.76-2.37-5.31-4.26a.82.82,0,0,1-.22-.16c-1.12-1.08-11.55-11.48-13.29-25.33a.51.51,0,0,0,0-.13c-.42-1.36-7.87-25.86-13.43-39.86-1.32-3.31-2.47-6.67-3.48-10.08-2.3-7.8-8-22.83-18.93-30a1,1,0,0,0-1.49.47c-1.51,3.59-8.92,23.71,2.18,42.16A3.63,3.63,0,0,0,460.57,180.68Z",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M499.3,257.63a104.11,104.11,0,0,0-.26,11.93s-.19,12.7-2.64,16.49a2.37,2.37,0,0,0-.35,2c.38,1.25,1.64,2.28,5.72-2.68,0,0,2.52.8,4.07-.8,0,0,9.31,4.28,10.11-2.68a14.09,14.09,0,0,0,2.68-11s-.52-13.81-6-21.65Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M508.69,285s-4.51-9-1.3-10.62c0,0,2.09-2,5.24,5.57,0,0,2.12-2.52,3.19,1.49",
    fill: "none",
    stroke: "#f7a48b",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("path", {
    d: "M503.94,260.64s-.49,12.95,7.16,13.51",
    fill: "none",
    stroke: "#f7a48b",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("path", {
    d: "M501.77,285.35a14.58,14.58,0,0,1-1.62-9.57",
    fill: "none",
    stroke: "#f7a48b",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("path", {
    d: "M505.84,284.55s-3.27-2.17-1.91-8.77",
    fill: "none",
    stroke: "#f7a48b",
    strokeMiterlimit: "10"
  }), react_1["default"].createElement("path", {
    d: "M525.29,340.53s-18.4-38.32-19.45-56a4,4,0,0,1-4.07.8s5.68,42.2,19.06,61.13l-2.05,2.59a4.34,4.34,0,0,0-.94,2.3c-.61,6-2.12,31.31,13.33,48.37l21.54-33s-13.86-26-16.87-42Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M541.11,407.64s-3.28-.76-7.62-11.43l19.87-30.73,3.77,7.7Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M541.11,407.64s-3.28-.76-7.62-11.43l19.87-30.73,3.77,7.7Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M541.11,407.64,551,406l28.8-40.15s-16.6-5.86-22.71,7.3Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M535.84,324.72s-11-34.9-18.76-44.56c0,0-1.79-2.09-3.68-.66a10.28,10.28,0,0,1-.72-1.77,2.76,2.76,0,0,1,4.26-3c4.19,3,13.48,12.06,23.08,37.88a34.29,34.29,0,0,0,3,6.18c5.44,8.86,21.59,34.05,36.81,47.12,0,0-16.57-5.93-22.71,7.3C557.13,373.18,540.65,356.89,535.84,324.72Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M535.84,324.72s-11-34.9-18.76-44.56c0,0-1.79-2.09-3.68-.66a10.28,10.28,0,0,1-.72-1.77,2.76,2.76,0,0,1,4.26-3c4.19,3,13.48,12.06,23.08,37.88a34.29,34.29,0,0,0,3,6.18c5.44,8.86,21.59,34.05,36.81,47.12,0,0-16.57-5.93-22.71,7.3C557.13,373.18,540.65,356.89,535.84,324.72Z",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M390.3,256.21l-1.8,6.67a2.87,2.87,0,0,0,1.05,3.05c5.7,4.26,26.47,17.55,50.58,6.07a92.8,92.8,0,0,0,27.31-22.75l-2.2-5.4s-25.83,2.42-34.25,10.56l-4.89-7.47-8,9.27Z",
    fill: "#e6e7e8"
  }), react_1["default"].createElement("path", {
    d: "M393.62,304.51s11.65,3.06,21.12-4.59c0,0,10.28-11.08,24-8.41,1.88-.38,25.13,5.74,37.85-18.16,0,0,2.73,12.38-18.76,23.9,0,0-14.47,11.25-12.85,22.6,0,0-.59,28-6.24,32.12l-4.24-3.49V295.34l-7.9.76s-7.15,12.4-9.34,46.44l-2.54,45.23s.38,10.87-4.68,15c0,0-3.85,26-2.3,68.05,0,0-1.09,11.48-14.91,13.39,0,0-6.36,1.61-11.27-20.23,0,0,5.84,13.75,11.51,11,0,0,7.3-2.44,7.3-25.14,0,0-1.35-28.39,2.71-47.59,0,0-6,1.09-11.36,2.17,0,0,4.28-3.34,13.25-6.22,0,0,10-53,3.78-90.57C408.77,307.68,402.52,308.1,393.62,304.51Z",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M441.77,384.29s1.86.53,3.85,15.45l1.22,13.14h5s0-18.37,2.88-25.11c0,0,1.79-3.29,1.1-9C455.79,378.8,445.44,375.16,441.77,384.29Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M441.77,384.29s1.86.53,3.85,15.45l1.22,13.14h5s0-18.37,2.88-25.11c0,0,1.79-3.29,1.1-9C455.79,378.8,445.44,375.16,441.77,384.29Z",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M403.17,498.74s3.15,8.14,2.45,28.53a34,34,0,0,0,.63,7.92c.85,4.15,2.33,10.1,4.26,12.37a2.48,2.48,0,0,1,.63,1.59v.33a2.33,2.33,0,0,1-2.32,2.33H381.58V512.27s5.68-8.72,14.78-9.53Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "591.44 267.39 595.1 267.39 595.1 280.21 702.56 341.68 695.55 344.31 591.44 282.33 591.44 267.39",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "695.55 415.96 699.67 416.36 702.56 341.68 695.55 342.54 695.55 415.96",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "596.44 351.86 595.99 341.68 598.06 343.22 598.06 354.86 596.44 351.86",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "46.58 439.25 46.58 511.97 145 437.62 145 449.41 178.4 387.33 149.31 362.9 149.66 376.91 46.58 439.25",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "46.58 439.25 39.75 437.44 146.79 376.19 146.79 360.2 149.31 362.9 149.66 376.91 46.58 439.25",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "39.75 437.44 42.73 511.97 46.58 511.97 46.58 439.25 39.75 437.44",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "145 437.62 142.84 439.25 143.03 452.66 145 449.41 145 437.62",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "559.01",
    y: "48.09",
    width: "1",
    height: "35.65",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M399.11,125.19s2.19-36.45,29.06-41.11c0,0,23.57-4.39,24.67,18.36V137.8s-3.84,3-7.95,1.65c0,0-1.37,19.73-8.22,20,0,0-18.37,34-28.24,41.39a28.18,28.18,0,0,0-10.84,15.4.26.26,0,0,1-.51-.05,27.74,27.74,0,0,1,2-11.24s-3.29-.82-4.66,9.59c0,0,0,7.95-10.69,9.87v-11s-3.84,3.83-7.13,2.74c0,0,8.93-8.41,10.55-17.62a14.84,14.84,0,0,0,.17-2c.16-6.27,1.5-40.9,11.21-45.63C398.56,151,398.47,130.25,399.11,125.19Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M387,205.4a83.71,83.71,0,0,1-2.75-10,51.72,51.72,0,0,1-1-10.31,55.72,55.72,0,0,1,3.9-20.35c.61-1.64,1.35-3.21,1.94-4.8s1.14-3.23,1.6-4.87.87-3.31,1.23-5c.18-.83.36-1.69.51-2.49,0-.19.09-.42.1-.59l.08-.65.19-1.29a81.78,81.78,0,0,1,2.17-10.24,27.21,27.21,0,0,1,1.85-5,12.8,12.8,0,0,1,3.27-4.27l-1.19,5.05c-.41,1.64-.83,3.28-1.31,4.9-.87,3.29-1.64,6.62-2.36,10C393.81,152.43,391,159,388,165.13a55.32,55.32,0,0,0-4.41,20,51.46,51.46,0,0,0,.73,10.29A84.54,84.54,0,0,0,387,205.4Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M403.42,205.4a50.06,50.06,0,0,0,12.95-9,61.55,61.55,0,0,0,9.88-12.15c.69-1.1,1.29-2.26,1.93-3.39.32-.57.65-1.12.95-1.7l.8-1.78c.53-1.19,1.1-2.35,1.59-3.55l1.3-3.67.65-1.83.49-1.88,1-3.76c.57-2.52.94-5.1,1.43-7.63l4.78.8c-.66,2.66-1.22,5.33-2,8l-1.32,3.88-.65,1.94-.82,1.88-1.63,3.75c-.6,1.23-1.29,2.41-1.93,3.61l-1,1.79c-.35.59-.74,1.15-1.11,1.72-.76,1.13-1.46,2.29-2.26,3.39a63.66,63.66,0,0,1-11.21,11.68c-1,.87-2.13,1.64-3.23,2.41s-2.26,1.46-3.38,2.18A43.46,43.46,0,0,1,403.42,205.4Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M441.77,384.29a.58.58,0,0,1-1.1-.18c-.66-5.2-1.51-18.68,7.6-18.33,0,0,6.86-1.82,7.52,13C455.79,378.8,446.37,375.27,441.77,384.29Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M381.58,512.27s13.27-8.67,17.43,2.08l4.16-15.61s-1.73-8-12.49-4.51Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M381.58,512.27s5.63,15.27,6.67,40.6h4.13s3.42-31.16,6.63-38.52C399,514.35,395.42,503.36,381.58,512.27Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M381.58,512.27s5.63,15.27,6.67,40.6h4.13s3.42-31.16,6.63-38.52C399,514.35,395.42,503.36,381.58,512.27Z",
    opacity: "0.4"
  }), react_1["default"].createElement("polygon", {
    points: "563.84 292.03 591.44 267.39 591.44 280.57 695.55 342.54 695.55 415.96 595.99 341.68 596.44 351.86 563.84 292.03",
    fill: "#9f26d3",
    "data-primary": "true"
  }));
};

exports["default"] = PersonalDevelopmentIllustration;

/***/ }),

/***/ "./resources/js/Shared/Svg/TrendyIllustration.tsx":
/*!********************************************************!*\
  !*** ./resources/js/Shared/Svg/TrendyIllustration.tsx ***!
  \********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var TrendyIllustration = function TrendyIllustration() {
  return react_1["default"].createElement("svg", {
    id: "af0f8b38-dd8c-42e3-8bad-13e98b067767",
    "data-name": "Layer 1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "865.76",
    height: "682.89",
    viewBox: "0 0 865.76 682.89",
    className: "rounded-lg shadow-xl ring-1 ring-black ring-opacity-5"
  }, react_1["default"].createElement("rect", {
    x: "225.7",
    y: "475.49",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("rect", {
    x: "225.7",
    y: "529.71",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("polygon", {
    points: "285.39 529.71 304.93 475.49 274.86 475.49 255.31 529.71 285.39 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "319.49 475.49 312.88 475.49 293.34 529.71 299.95 529.71 319.49 475.49",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "265.85 583.93 285.39 529.71 255.31 529.71 235.77 583.93 265.85 583.93",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "299.95 529.71 293.34 529.71 273.8 583.93 280.4 583.93 299.95 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("rect", {
    x: "301.5",
    y: "402.55",
    width: "5.49",
    height: "157.08",
    transform: "translate(785.32 176.85) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("rect", {
    x: "301.5",
    y: "456.34",
    width: "5.49",
    height: "157.08",
    transform: "translate(839.12 230.64) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M223,473.06V586.3H385.52V473.06ZM380,580.81H228.44V532.58H380Zm0-53.72H228.44V478.85H380Z",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("rect", {
    x: "388.26",
    y: "475.49",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("rect", {
    x: "388.26",
    y: "529.71",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("polygon", {
    points: "464.35 529.71 483.9 475.49 453.82 475.49 434.28 529.71 464.35 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "498.45 475.49 491.85 475.49 472.3 529.71 478.91 529.71 498.45 475.49",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "444.81 583.93 464.35 529.71 434.28 529.71 414.73 583.93 444.81 583.93",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "478.91 529.71 472.3 529.71 452.76 583.93 459.37 583.93 478.91 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("rect", {
    x: "464.06",
    y: "402.55",
    width: "5.49",
    height: "157.08",
    transform: "translate(947.89 14.29) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("rect", {
    x: "464.06",
    y: "456.34",
    width: "5.49",
    height: "157.08",
    transform: "translate(1001.68 68.08) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M385.52,473.06V586.3H548.08V473.06ZM542.6,580.81H391V532.58H542.6Zm0-53.72H391V478.85H542.6Z",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("rect", {
    x: "550.82",
    y: "475.49",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("rect", {
    x: "550.82",
    y: "529.71",
    width: "157.08",
    height: "54.22",
    fill: "#ececec"
  }), react_1["default"].createElement("polygon", {
    points: "617.67 529.71 637.22 475.49 607.14 475.49 587.6 529.71 617.67 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "651.77 475.49 645.17 475.49 625.62 529.71 632.23 529.71 651.77 475.49",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "598.13 583.93 617.67 529.71 587.6 529.71 568.05 583.93 598.13 583.93",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "632.23 529.71 625.62 529.71 606.08 583.93 612.69 583.93 632.23 529.71",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("rect", {
    x: "626.62",
    y: "402.55",
    width: "5.49",
    height: "157.08",
    transform: "translate(1110.45 -148.28) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("rect", {
    x: "626.62",
    y: "456.34",
    width: "5.49",
    height: "157.08",
    transform: "translate(1164.24 -94.48) rotate(90)",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M548.08,473.06V586.3H710.64V473.06ZM705.16,580.81H553.57V532.58H705.16Zm0-53.72H553.57V478.85H705.16Z",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "694.93 594.46 238.67 594.46 236.3 586.3 697.3 586.3 694.93 594.46",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M471.52,507.68A4.72,4.72,0,1,1,466.8,503,4.72,4.72,0,0,1,471.52,507.68Z",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("circle", {
    cx: "466.8",
    cy: "506.35",
    r: "4.72",
    fill: "#fafafa"
  }), react_1["default"].createElement("circle", {
    cx: "466.8",
    cy: "557.49",
    r: "4.72",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M471.52,556.15a4.72,4.72,0,1,1-4.72-4.71A4.72,4.72,0,0,1,471.52,556.15Z",
    fill: "#fafafa"
  }), react_1["default"].createElement("path", {
    d: "M309,507.68a4.72,4.72,0,1,1-4.71-4.71A4.71,4.71,0,0,1,309,507.68Z",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("circle", {
    cx: "304.24",
    cy: "506.35",
    r: "4.72",
    fill: "#fafafa"
  }), react_1["default"].createElement("circle", {
    cx: "304.24",
    cy: "557.49",
    r: "4.72",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M309,556.15a4.72,4.72,0,1,1-4.71-4.71A4.71,4.71,0,0,1,309,556.15Z",
    fill: "#fafafa"
  }), react_1["default"].createElement("path", {
    d: "M637,507.68a4.72,4.72,0,1,1-4.72-4.71A4.72,4.72,0,0,1,637,507.68Z",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("circle", {
    cx: "632.23",
    cy: "506.35",
    r: "4.72",
    fill: "#fafafa"
  }), react_1["default"].createElement("circle", {
    cx: "632.23",
    cy: "557.49",
    r: "4.72",
    fill: "#e1e1e1"
  }), react_1["default"].createElement("path", {
    d: "M637,556.15a4.72,4.72,0,1,1-4.72-4.71A4.72,4.72,0,0,1,637,556.15Z",
    fill: "#fafafa"
  }), react_1["default"].createElement("rect", {
    x: "172.35",
    y: "43.62",
    width: "609.6",
    height: "353.87",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "172.35",
    y: "43.62",
    width: "609.6",
    height: "353.87",
    fill: "#fff",
    opacity: "0.5"
  }), react_1["default"].createElement("path", {
    d: "M172.35,43.62V72.16h102v-11a9.85,9.85,0,0,1,9.86-9.86h67.89a9.86,9.86,0,0,1,9.86,9.86v11h420V43.62Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M197.78,57.89a6.55,6.55,0,1,1-6.55-6.55A6.55,6.55,0,0,1,197.78,57.89Z",
    fill: "#fff",
    opacity: "0.7"
  }), react_1["default"].createElement("path", {
    d: "M217.43,57.89a6.55,6.55,0,1,1-6.55-6.55A6.55,6.55,0,0,1,217.43,57.89Z",
    fill: "#fff",
    opacity: "0.7"
  }), react_1["default"].createElement("path", {
    d: "M237.08,57.89a6.55,6.55,0,1,1-6.55-6.55A6.55,6.55,0,0,1,237.08,57.89Z",
    fill: "#fff",
    opacity: "0.7"
  }), react_1["default"].createElement("rect", {
    x: "361.91",
    y: "126.73",
    width: "397.7",
    height: "248.56",
    fill: "#fff",
    opacity: "0.3"
  }), react_1["default"].createElement("rect", {
    x: "193.69",
    y: "126.73",
    width: "116.92",
    height: "116.92",
    fill: "#fff",
    opacity: "0.3"
  }), react_1["default"].createElement("rect", {
    x: "193.69",
    y: "126.73",
    width: "116.92",
    height: "116.92",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("rect", {
    x: "221.02",
    y: "274.62",
    width: "69.2",
    height: "4.94",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "640.24",
    y: "88.65",
    width: "117.13",
    height: "17.95",
    fill: "#007cff"
  }), react_1["default"].createElement("rect", {
    x: "640.24",
    y: "88.65",
    width: "117.13",
    height: "17.95",
    fill: "#fff",
    opacity: "0.9"
  }), react_1["default"].createElement("rect", {
    x: "757.38",
    y: "88.65",
    width: "2.24",
    height: "17.95",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "479.04",
    y: "88.65",
    width: "2.24",
    height: "17.95",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "361.91",
    y: "88.65",
    width: "117.13",
    height: "17.95",
    fill: "#007cff"
  }), react_1["default"].createElement("rect", {
    x: "361.91",
    y: "88.65",
    width: "117.13",
    height: "17.95",
    fill: "#fff",
    opacity: "0.9"
  }), react_1["default"].createElement("path", {
    d: "M351.52,373.48h0A5.48,5.48,0,0,1,346,368V298.16a5.48,5.48,0,0,1,5.48-5.48h0a5.48,5.48,0,0,1,5.48,5.48V368A5.48,5.48,0,0,1,351.52,373.48Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M212.13,277.09a8.75,8.75,0,1,1-8.75-8.74A8.74,8.74,0,0,1,212.13,277.09Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("rect", {
    x: "221.02",
    y: "302.11",
    width: "88.64",
    height: "4.94",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M212.13,304.58a8.75,8.75,0,1,1-8.75-8.74A8.75,8.75,0,0,1,212.13,304.58Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("rect", {
    x: "221.02",
    y: "329.6",
    width: "76.27",
    height: "4.94",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M212.13,332.07a8.75,8.75,0,1,1-8.75-8.75A8.75,8.75,0,0,1,212.13,332.07Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("rect", {
    x: "221.02",
    y: "357.08",
    width: "69.2",
    height: "4.94",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M212.13,359.55a8.75,8.75,0,1,1-8.75-8.74A8.74,8.74,0,0,1,212.13,359.55Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M593.2,409h62.13c1.34,0,1.53-2.09.18-2.09H593.38c-1.34,0-1.53,2.09-.18,2.09Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M665.33,409h7.85c1.34,0,1.53-2.09.18-2.09h-7.85c-1.34,0-1.53,2.09-.18,2.09Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M359.33,35.19H297.2c-1.35,0-1.53,2.09-.18,2.09h62.13c1.34,0,1.53-2.09.18-2.09Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M287.2,35.19h-7.85c-1.34,0-1.53,2.09-.18,2.09H287c1.35,0,1.53-2.09.19-2.09Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "624.9",
    y: "170.85",
    width: "125.97",
    height: "117.03",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "624.9",
    y: "170.85",
    width: "125.97",
    height: "117.03",
    fill: "#fff",
    opacity: "0.9"
  }), react_1["default"].createElement("rect", {
    x: "750.87",
    y: "170.85",
    width: "2.56",
    height: "117.03",
    transform: "translate(1504.3 458.73) rotate(180)",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "624.86",
    y: "257.56",
    width: "126.05",
    height: "30.31",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "643.96",
    y: "181.73",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "643.96",
    y: "210.82",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "643.96",
    y: "239.92",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "632.24",
    y: "267.93",
    width: "111.28",
    height: "10.34",
    fill: "#fff"
  }), react_1["default"].createElement("rect", {
    x: "496.36",
    y: "86.28",
    width: "125.97",
    height: "144.56",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "496.36",
    y: "86.28",
    width: "125.97",
    height: "144.56",
    fill: "#fff",
    opacity: "0.9"
  }), react_1["default"].createElement("rect", {
    x: "622.33",
    y: "86.28",
    width: "2.56",
    height: "144.56",
    transform: "translate(1247.23 317.12) rotate(180)",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "500.78",
    y: "90.7",
    width: "117.13",
    height: "17.95",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "496.28",
    y: "170.85",
    width: "126.05",
    height: "30.31",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("rect", {
    x: "515.42",
    y: "124.69",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "515.42",
    y: "153.79",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "503.71",
    y: "181.8",
    width: "111.28",
    height: "10.34",
    fill: "#fff"
  }), react_1["default"].createElement("rect", {
    x: "515.42",
    y: "211.98",
    width: "87.85",
    height: "8.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M153.36,475.6l-4.84,129.13C155.41,607.85,169,600,169,600l23.76-126.93Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M143.16,598.85l-1.28,18.06,54.3,1.55c5.25-19.61-27.17-16.91-27.17-16.91V600A31,31,0,0,1,143.16,598.85Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M143.16,598.85l-1.28,18.06,54.3,1.55c5.25-19.61-27.17-16.91-27.17-16.91V600A31,31,0,0,1,143.16,598.85Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M161.21,473,149,614.41l-21-.5S114.43,508.83,113.81,501s2.43-29.1,3-30.39Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M160.29,482.7c-.08.89-.18,2-.31,3.55-.31,3.17-.73,7.61-1.25,13.07-1.06,11-2.52,26.33-4,43.12s-2.67,32.11-3.52,43.16c-.42,5.46-.77,10-1,13.09-.13,1.48-.31,2.67-.31,3.56a7.54,7.54,0,0,1-.17,1.24,8.83,8.83,0,0,1,0-1.26c0-.88.07-2,.15-3.57.19-3.16.45-7.63.76-13.09.72-11.07,1.82-26.35,3.29-43.2s3-32.1,4.22-43.11c.67-5.45,1.18-9.93,1.55-13,.19-1.49.34-2.66.45-3.55a8.43,8.43,0,0,1,.22-1.22A5.8,5.8,0,0,1,160.29,482.7Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M126.73,604.82c-1.48.49-3.8,2-3.9,3.51h0a11.69,11.69,0,0,0,2,10.11c6.41,8.36,20.39,23.53,24.65,28a3.93,3.93,0,0,0,4.14,1,5.35,5.35,0,0,0,.82-.42,12.94,12.94,0,0,0,4.62-16.91l-9.43-18.48,0-4.21c-7.37,8.46-22.74-2.73-22.74-2.73Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M126.61,204.69s9.45-.84,11.43-.24a226.87,226.87,0,0,0,46,8.78,17.32,17.32,0,0,0,8-.63,6.8,6.8,0,0,0,4-4.53c-.12,1.55-.12,3.1-.12,4.65l14.83,4.22c2.72-4.53,2.58-10.3,1.17-15.39s-4-9.79-5.91-14.71c-7.62-19.36-5.53-38.82-4.66-61.51l-73.14-.76C123,148,121.19,181.37,126.61,204.69Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M259.26,247.45l1.24,13.4,13.7-3.55s26.4,1.95,28.79,1.55a10.09,10.09,0,0,0,3-.76l.36-1.32a26.2,26.2,0,0,0,6-.2c2.92-.51.65-2,2.92-2.51a8,8,0,0,0,3-1.17l.13-2.25c2.83-.94,0-3.86,0-3.86l-17-.5c-2.33-.91-13.81-.25-14.09-.77s6.48-1.56,10.13-2.08a1.22,1.22,0,0,0,.26-.06,2,2,0,0,0,1.42-2.47c-2.92-2.62-9.72-.44-11.81-.58s-11.93-1.07-14.23,0a8.73,8.73,0,0,0-3.44,4.47Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M277.23,244.68a14.54,14.54,0,0,1,4.65,2,10.6,10.6,0,0,1,3.54,3.72,3.88,3.88,0,0,1,.68,2c-.1,0-.38-.73-1.1-1.77a12,12,0,0,0-3.49-3.4,37.56,37.56,0,0,0-6.44-2.8A4.94,4.94,0,0,1,277.23,244.68Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M300.78,254.74a10.13,10.13,0,0,1,4.21.74c1,.7,1.1,1.55,1,1.55a3.62,3.62,0,0,0-1.25-1,13.26,13.26,0,0,0-4-.5,26,26,0,0,1-5.9-.71A25.66,25.66,0,0,1,300.78,254.74Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M298.67,251.29a45,45,0,0,0,6.66.47,27.49,27.49,0,0,1,3.74.11,9.82,9.82,0,0,1,3,.77,3.91,3.91,0,0,1,1.69,1.26c.29.48.15.81.09.79s0-.29-.31-.63a4.16,4.16,0,0,0-1.66-1,17.46,17.46,0,0,0-6.55-.65,30.08,30.08,0,0,1-6.75-.67,11.05,11.05,0,0,1-2.71-.71A11.36,11.36,0,0,1,298.67,251.29Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M301.36,248.19c1.83,0,4.34.12,7.1.28,1.4.15,2.72.05,3.94.15a15.05,15.05,0,0,1,3.21.53,4.73,4.73,0,0,1,2,1c.35.4.42.71.35.74s-.15-.24-.54-.54a5,5,0,0,0-1.94-.71,18,18,0,0,0-3.1-.36c-1.2-.11-2.53,0-3.93-.11-2.78-.15-5.3-.32-7.12-.48a12.09,12.09,0,0,1-2.93-.39A13.21,13.21,0,0,1,301.36,248.19Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("polygon", {
    points: "150.27 175.47 143.94 174.85 141.5 186.63 146.53 202 155.41 205.75 171.54 209.32 187.48 208.98 190.89 202.3 178.31 184.99 150.27 175.47",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M216.26,232.07a49.9,49.9,0,0,1,1.89,20.36c-1.24,11.85-7.09,15.4-7.09,15.4l3,70.41-77.39-12.85-9.43-94.22,21.46-33.48s23.75,7,33.09,7.17,8.58-5,8.58-5Z",
    fill: "#e6e7e8"
  }), react_1["default"].createElement("path", {
    d: "M216.16,324s-2.09,14.18-2.85,33.66-22.19,123.42-22.19,123.42l-75.84-4.07s-10.14-82.6-14.21-108.9,12.3-62.32,12.3-62.32Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M162.93,410.68c2.48,22.84,4.3,41.46,4,41.46s-2.35-18.51-4.84-41.35-4.26-41.46-4-41.46S160.45,387.84,162.93,410.68Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M163.16,289.52a3.7,3.7,0,0,1,1.45-2.14,4,4,0,0,1,6.13,2.16,4,4,0,0,1-2.74,5,3.91,3.91,0,0,1-3.72-.84,3.76,3.76,0,0,1-1.23-2.28,1.91,1.91,0,0,1,0-.95c.07,0,.07.34.21.9a3.58,3.58,0,0,0,1.27,2,3.47,3.47,0,0,0,3.24.6,3.41,3.41,0,0,0,.34-6.39,3.47,3.47,0,0,0-3.29.27,3.69,3.69,0,0,0-1.47,1.81c-.2.53-.3.85-.3.85A1.57,1.57,0,0,1,163.16,289.52Z",
    fill: "#263238"
  }), react_1["default"].createElement("path", {
    d: "M178.34,177.19l-.41,8.71s12.45,16.92,17.33,24.74,13.8,23.42,16.9,29.17c8.81,16.57-3.29,27.77-3.29,27.77l-.6,11.3-2.64,85.75L223.08,360l-3.64-71.87h0a8.4,8.4,0,0,0,11,2.84c14.91-8,42.45-32,42.45-32l-9.31-17.73-34.14,15.2v-.84c.05-.82-4.82-9.2-4.82-9.2l.25-4.47c.08-1.67-6.36-12.08-6.36-12.08s-1.59-17.16-4-24.28-10.81-17.64-13-18.89-19.86-5.26-19.86-5.26Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M222.24,276.52a33.68,33.68,0,0,1,.83-9.17,21,21,0,0,1,3.63-8.45,7.89,7.89,0,0,1,2.12-1.87,2.16,2.16,0,0,1,1-.38,12,12,0,0,0-2.69,2.54,23.23,23.23,0,0,0-3.36,8.32,40,40,0,0,0-1,9,37.56,37.56,0,0,1,0,3.78A13,13,0,0,1,222.24,276.52Z",
    fill: "#263238"
  }), react_1["default"].createElement("path", {
    d: "M216.43,257.25a20,20,0,0,0,.94-9.31,53.29,53.29,0,0,0-4.11-13.51c-2-5-4.06-10.52-6.65-16.11a51.38,51.38,0,0,0-4.46-7.75c-1.66-2.36-3.55-4.44-5.3-6.47a66.06,66.06,0,0,1-8-11.84c-2.14-3.46-3.94-6.16-5.24-8l-1.55-2.13a3.44,3.44,0,0,1-.5-.76,5.51,5.51,0,0,1,.6.68c.35.47.86,1.15,1.56,2.05,1.36,1.82,3.3,4.49,5.44,7.93a68.17,68.17,0,0,0,8,11.67,81.71,81.71,0,0,1,5.38,6.5,53,53,0,0,1,4.52,7.88c2.62,5.65,4.61,11.19,6.61,16.17a52.23,52.23,0,0,1,4,13.68,19.64,19.64,0,0,1-1.18,9.54,15.59,15.59,0,0,1-1.21,2.33,3.89,3.89,0,0,1-.56.71A25.83,25.83,0,0,0,216.43,257.25Z",
    fill: "#fff",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M220.21,296.75c-.2-1.08-.45-2.45-.73-4-.59-3.37-1.47-8-2.22-13.22a72.43,72.43,0,0,1-.86-13.4c0-.86.09-1.63.15-2.32s.12-1.27.2-1.73a6.72,6.72,0,0,1,.31-1.48c.11,0-.18,2.13-.18,5.54a89.76,89.76,0,0,0,1.05,13.3c.78,5.09,1.49,9.69,2,13.23.24,1.71.42,3.09.52,4a6.7,6.7,0,0,1,0,1.55A6.47,6.47,0,0,1,220.21,296.75Z",
    fill: "#263238"
  }), react_1["default"].createElement("g", {
    opacity: "0.3"
  }, react_1["default"].createElement("path", {
    d: "M229.55,256.49c-3,2.25-3.89,5.12-4.9,8.72s-1.38,7.35-2.23,11a1.68,1.68,0,0,1-.3.7c-.62.68-1.72.06-2.17-.73a8.29,8.29,0,0,1-.36-5.49,26.49,26.49,0,0,1,2.9-8.88,10.7,10.7,0,0,1,6.94-5.35"
  })), react_1["default"].createElement("path", {
    d: "M144.19,182.79c-.05.9,12.53,29.56,17.44,42.79s11.46,44.52,12.87,56.68-4.23,79.05-4.23,79.05l-67.35-23.13c2.17-19.29,20.45-58.07,20.45-58.07L116.12,240s-15.56-49.71-12.41-53.37S132.05,178,132.05,178l4.4-4.78,8.89-1S144.23,181.89,144.19,182.79Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    opacity: "0.3"
  }, react_1["default"].createElement("path", {
    d: "M122.59,287.61a5.88,5.88,0,0,0,3.1,2c3.68,1.09,7.61-.09,11.44-1.46s7.76-2.79,11.59-4.34a23.27,23.27,0,0,0,5.41-2.78A45.78,45.78,0,0,1,158,278c4.13-2.42,11.3.08,15.64-1.93l-.37-2.25c-2.7-.65-4.05-.71-6.75-1.31-6.89-1.48-13.62,2.59-20.37,4.65-7.12,2.16-14.87,2.16-21.63,5.26a4.93,4.93,0,0,0-2.33,1.77A3.35,3.35,0,0,0,122.59,287.61Z"
  })), react_1["default"].createElement("path", {
    d: "M94.91,196.43c-2.88,6.3-4,17.16-4,17.16L84.72,238l-7.15,38.75a24.29,24.29,0,0,0,3.56,9.66c3.06,4.76,14.64,3.58,26.24,1.39S154.29,275,154.29,275l-2.7-18.76-37.73.5s13.9-30.86,11.93-41.5S113.93,183.37,113,182.9C109.22,181.18,97.8,190.13,94.91,196.43Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M132.91,179.6c.06.7.14,1.63.23,2.79.22,2.41.59,5.91,1.38,10.16a103.9,103.9,0,0,0,3.85,14.73c.93,2.7,2,5.5,3.27,8.34a17.29,17.29,0,0,0,1,2.08,3,3,0,0,0,1.83,1.1l4.65,1.46,1.55.47.7.22-.62.4-5.8,3.78.09-.48L160.79,248l4.65,6.93,1.21,1.88a3.07,3.07,0,0,1,.39.68,3.51,3.51,0,0,1-.48-.6l-1.32-1.82-4.75-6.82L144.58,225l-.19-.29.29-.19,5.79-3.78.09.6-1.55-.47-4.65-1.45a3.7,3.7,0,0,1-2.16-1.37,18.49,18.49,0,0,1-1.07-2.19c-1.26-2.87-2.34-5.69-3.26-8.42a99.62,99.62,0,0,1-3.72-14.83A90.38,90.38,0,0,1,133,182.41c-.11-1.26-.11-2.16-.11-2.81a4,4,0,0,1,0-1A8,8,0,0,1,132.91,179.6Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M132.91,179.6c.06.7.14,1.63.23,2.79.22,2.41.59,5.91,1.38,10.16a103.9,103.9,0,0,0,3.85,14.73c.93,2.7,2,5.5,3.27,8.34a17.29,17.29,0,0,0,1,2.08,3,3,0,0,0,1.83,1.1l4.65,1.46,1.55.47.7.22-.62.4-5.8,3.78.09-.48L160.79,248l4.65,6.93,1.21,1.88a3.07,3.07,0,0,1,.39.68,3.51,3.51,0,0,1-.48-.6l-1.32-1.82-4.75-6.82L144.58,225l-.19-.29.29-.19,5.79-3.78.09.6-1.55-.47-4.65-1.45a3.7,3.7,0,0,1-2.16-1.37,18.49,18.49,0,0,1-1.07-2.19c-1.26-2.87-2.34-5.69-3.26-8.42a99.62,99.62,0,0,1-3.72-14.83A90.38,90.38,0,0,1,133,182.41c-.11-1.26-.11-2.16-.11-2.81a4,4,0,0,1,0-1A8,8,0,0,1,132.91,179.6Z",
    fill: "#fff",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M112,183.29a7.14,7.14,0,0,1,2.19.42,13.29,13.29,0,0,1,6.2,5.1,28.1,28.1,0,0,1,2.72,4.92,41.74,41.74,0,0,1,2.17,6.07A46.56,46.56,0,0,1,127,214.41a34.5,34.5,0,0,1-1.17,7.56c-.65,2.37-1.28,4.65-1.87,6.83-1.23,4.34-2.39,8.25-3.45,11.49s-2,5.85-2.73,7.62c-.39.87-.67,1.55-.93,2a2.88,2.88,0,0,1-.39.65,4.71,4.71,0,0,1,.27-.71c.21-.48.46-1.15.8-2,.66-1.79,1.54-4.42,2.52-7.68s2.22-7.12,3.3-11.5,2.81-9.09,3-14.26a47.08,47.08,0,0,0-1.7-14.41,45.86,45.86,0,0,0-2.07-6,28.52,28.52,0,0,0-2.61-4.87A13.2,13.2,0,0,0,114,184a11.17,11.17,0,0,0-2.85-.62A2.33,2.33,0,0,1,112,183.29Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M107.26,254.75a27.75,27.75,0,0,1,7.29,2.19c-.06.16-3.33-.73-7.43-1.55s-7.45-1.21-7.45-1.41A26.56,26.56,0,0,1,107.26,254.75Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M249.68,269.31l-55,62.76S152.36,288.76,154.3,289c.87.09,55.18-57.79,55.18-57.79Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M249.68,269.31l-55,62.76S152.36,288.76,154.3,289c.87.09,55.18-57.79,55.18-57.79Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M152.12,258.51l2.54,16.26,6.3-1.45s14.79,1.17,17.84.39S189.24,272,189.24,272a4.21,4.21,0,0,0,.22,2.84,4.15,4.15,0,0,0,2.53,2l2.54-5.32c1.2,2.12,3,.15,3,.15l1.75-4.65c1.37,2.71,3.76.2,3.76.2l.51-6.21a9.74,9.74,0,0,0,3.1,2.05c1.43.45,1.55-2,1.55-2l-6.48-8.62L187,250.19l-21.91,8.61Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M192.69,252.35a21.22,21.22,0,0,1,2.9.82c1.16.37,2.31.79,3.43,1.27a17.39,17.39,0,0,1,3.23,1.82,2.34,2.34,0,0,1,1,1.44,8.69,8.69,0,0,1,.17,1.4,6.43,6.43,0,0,1-.09,2.76c-.08,0-.13-1-.39-2.7-.08-.4-.14-.88-.25-1.32a1.68,1.68,0,0,0-.81-1.07,20.33,20.33,0,0,0-3.1-1.71c-1.16-.51-2.31-.93-3.37-1.3s-2-.73-2.77-.93a7.93,7.93,0,0,0-2.66-.17A3.45,3.45,0,0,1,192.69,252.35Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M191.07,257.62c1.54.44,3.71.83,6.06,1.66a4.23,4.23,0,0,1,1.7.9,5.14,5.14,0,0,1,.76,1.56,5.69,5.69,0,0,1,.45,2.88,9.09,9.09,0,0,1-.59,1.83,3.5,3.5,0,0,1-.28.65,2.21,2.21,0,0,1,0-.71,9.21,9.21,0,0,0,.36-1.83,8.84,8.84,0,0,0-1.23-4,3.69,3.69,0,0,0-1.44-.72c-2.26-.8-4.4-1.27-6-1.83a6.42,6.42,0,0,1-2.34-1.16A24.09,24.09,0,0,1,191.07,257.62Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M195.05,270.13a3.38,3.38,0,0,0,.18-1.86,2.3,2.3,0,0,0-1.44-1.55,7.32,7.32,0,0,0-2.36-.45l-2.06-.19c-.59-.06-1.09-.17-1.38-.17h-.47s.08-.33.45-.33a11.06,11.06,0,0,1,1.41,0h2.08a7.53,7.53,0,0,1,2.61.47,2.89,2.89,0,0,1,1.78,2.06,3.47,3.47,0,0,1-.37,2.19,5.12,5.12,0,0,1-1.19,1.55A10.11,10.11,0,0,1,195.05,270.13Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M126.27,141.59c1.6,10.45,7.65,13.26,16.57,18.92L198,137.24c4.38-4.42,6.59-21.46,1.83-34.6-2.33-6.56-4.33-13.71-10.08-17.67A36.54,36.54,0,0,0,175,78.63c-10.24-1.32-19.62-.35-28.27,5.3-9.71,6.35-13,17.92-16.3,29.06C128.42,119.9,125.2,134.51,126.27,141.59Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M141,100.7l-.95,86.89c18.12,8.58,32.85,11.14,38.08-3.24l.54-12.08c.4-5.35.4-5.61.4-5.61s14-1.71,17.71-19.63c1.87-8.89,2.07-23.61,1.87-35.68-.17-10.85-6.7-24.47-17.56-23.83L147,94.45A6.38,6.38,0,0,0,141,100.7Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M178.77,171.14c-18.12.84-23.5-12.07-23.5-12.07a46,46,0,0,0,23.84,7.59Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M181.42,84.72a51,51,0,0,0-26.62-1.1c-6.21,1.43-12.63,4.45-15.52,10.16-4.15,8.24.56,18.48-2,27.34a6.57,6.57,0,0,0,7.76.45,12.93,12.93,0,0,0,4.93-6.53l-.66,3.68c5.47.94,11.07-1.71,15.05-5.6s6.59-8.91,9.15-13.84a24.28,24.28,0,0,1-6.1,13.54,6.6,6.6,0,0,0,5.77-1.33,14.66,14.66,0,0,0,3.73-4.83,39.89,39.89,0,0,0,4.65-21.21",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M180.77,86h0v-.4C180.77,85.68,180.76,85.82,180.77,86Z",
    fill: "#263238"
  }), react_1["default"].createElement("path", {
    d: "M177.16,113.44a8.86,8.86,0,0,1,3.89.48,4.34,4.34,0,0,0,3.79-.46,4.67,4.67,0,0,0,1.22-2.31,53.93,53.93,0,0,0,2.19-18.25,54.9,54.9,0,0,1,3.24,10.49,19.71,19.71,0,0,1-.85,10.86l0,0a1.55,1.55,0,0,0,2.19.13,3.7,3.7,0,0,0,1.17-2.18,27.35,27.35,0,0,0,.2-9.16c1.52,3.34,1.81,7.11,2.5,10.71s1.95,7.35,4.84,9.62a48.35,48.35,0,0,0,.74-13.84,41.85,41.85,0,0,0-3.1-11.3,32.71,32.71,0,0,0-6.41-10.86c-4.93-5.1-7.32-3.29-10.86-1.81l-1.13.37A51.75,51.75,0,0,1,177.16,113.44Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M164.06,205.51c-5.32,3.52-10.81,5.21-16.07,7.23a37.33,37.33,0,0,1-11.64-13c.67,3,1.48,5.91,2.42,8.8l-1.55,1.29a74.27,74.27,0,0,1-7.88-38.4l11.47-61.24,15.79-.84c3.07,11.86-6.42,24-4.22,36.07.92,5,3.77,9.52,3.93,14.56.13,4.78-2.16,9.34-2.07,14.09.17,5.63,3.52,10.56,6.38,15.42S165.91,200.22,164.06,205.51Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("g", {
    id: "a6d313f0-0718-4366-8596-763a629c16b7",
    "data-name": "freepik--plant-2--inject-2"
  }, react_1["default"].createElement("path", {
    d: "M729.22,439.84h0a.15.15,0,0,0-.07-.19.18.18,0,0,0-.2.09,187.75,187.75,0,0,0-9.86,30.88,283.26,283.26,0,0,0-5.45,32.09l-.77,8.1c-.13,1.43-.27,2.7-.37,4.06l-.21,4.08c-.13,2.71-.29,5.42-.37,8.13l.08,8.16.09,4.07c0,1.35.15,2.71.24,4.06.21,2.72.37,5.43.63,8.12.51,5.41,1.32,10.79,2.17,16.14a.14.14,0,0,0,.14.12.17.17,0,0,0,.14-.15h0c0-2.71-.13-5.4-.2-8.12s-.31-5.39-.27-8.09,0-5.39-.09-8.09l-.07-4,.07-4v-8.08l.22-8.08.08-4c0-1.34.16-2.69.23-4l.5-8.06a287.78,287.78,0,0,1,4.57-32A188.2,188.2,0,0,1,729.22,439.84Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M730.38,482a.16.16,0,0,0-.09-.18.14.14,0,0,0-.18.08h0a191.39,191.39,0,0,0-9.11,41.6c-.71,7.06-1.51,14.14-1.59,21.24a174.36,174.36,0,0,0,.75,21.32.18.18,0,0,0,.12.13.17.17,0,0,0,.17-.13c.81-7.13,1.31-14.14,1.8-21.19s.57-14.12,1-21.17a185.26,185.26,0,0,1,2.51-21A190.49,190.49,0,0,1,730.38,482Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M753.8,496.93a.13.13,0,0,0-.2,0,71.5,71.5,0,0,0-15.7,13.64,60.59,60.59,0,0,0-6.12,8.56l-1.26,2.37-1,2.47a21.73,21.73,0,0,0-.87,2.5c-.25.84-.53,1.68-.76,2.52a100.83,100.83,0,0,0-3.24,20.65,119.51,119.51,0,0,0,.77,20.8.16.16,0,0,0,.15.13.14.14,0,0,0,.14-.13l1.78-20.59a187.14,187.14,0,0,1,2.61-20.3c.19-.83.43-1.64.64-2.47a21.1,21.1,0,0,1,.72-2.43l.84-2.36,1.07-2.26a61.21,61.21,0,0,1,5.61-8.56,70.53,70.53,0,0,1,14.7-14.27h0a.14.14,0,0,0,.18-.09.14.14,0,0,0-.08-.18Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M742.47,523.41a22.69,22.69,0,0,0-7.14,8.56,50.05,50.05,0,0,0-4,10.49c-.29.9-.5,1.81-.7,2.73l-.65,2.74a44.74,44.74,0,0,0-.68,5.55c0,1.87-.09,3.74,0,5.59a49.63,49.63,0,0,0,.73,5.57.15.15,0,0,0,.11.11.14.14,0,0,0,.17-.11h0a46.11,46.11,0,0,0,1-5.47c.3-1.83.57-3.62.8-5.42s.5-3.58.64-5.4a37.14,37.14,0,0,1,.77-5.37,48.79,48.79,0,0,1,3.05-10.39,22.86,22.86,0,0,1,6-8.92h0a.18.18,0,0,0-.08-.26Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M709.62,544c-.77-9.14-2.27-18.19-3.7-27.24s-3-18.06-4.66-27.11-3.5-18-5.44-26.91a.14.14,0,0,0-.14-.14.15.15,0,0,0-.15.14q2.36,13.5,4.29,27.11c1.29,9.08,2.65,18.09,3.79,27.11s2,18.17,3.13,27.21l3.35,27.21a.13.13,0,0,0,.13.13.15.15,0,0,0,.15-.14c.1-2.28.1-4.58.16-6.88s-.08-4.58-.13-6.87C710.36,553.15,710,548.58,709.62,544Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M704.14,543.87c-.49-3.62-1.16-7.22-1.84-10.81L701,527.72c-.41-1.77-.81-3.55-1.34-5.31a149.35,149.35,0,0,0-7-20.79l-1.08-2.51-1.23-2.47c-.77-1.65-1.71-3.24-2.6-4.82a40.75,40.75,0,0,0-6.59-8.75.15.15,0,0,0-.2,0,.14.14,0,0,0,0,.17h0a40.79,40.79,0,0,1,6,9c.78,1.63,1.62,3.22,2.31,4.89l1.07,2.5,1,2.54a150.48,150.48,0,0,1,6.1,20.79c.82,3.51,1.42,7.14,2,10.65l.86,5.35c.31,1.78.7,3.55.94,5.35.57,3.57,1.23,7.13,1.85,10.71s1.22,7.14,2,10.75a.16.16,0,0,0,.15.11.14.14,0,0,0,.14-.14h0c0-3.65-.09-7.31-.26-11S704.52,547.51,704.14,543.87Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M696.58,544.33c-1.7-4.79-3.66-9.49-5.8-14.08a125,125,0,0,0-7.27-13.36.17.17,0,0,0-.19,0,.15.15,0,0,0,0,.2,124.21,124.21,0,0,1,6.22,13.77c1.86,4.67,3.53,9.42,4.92,14.27s2.46,9.73,3.74,14.57,2.43,9.7,3.93,14.6a.14.14,0,0,0,.13.11.14.14,0,0,0,.14-.14h0A72.53,72.53,0,0,0,701,559.06,95.53,95.53,0,0,0,696.58,544.33Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M696.26,560.31a32.84,32.84,0,0,0-4-8.43,34.82,34.82,0,0,0-5.85-7.28,26.47,26.47,0,0,0-3.73-2.85,15.24,15.24,0,0,0-4.28-1.91.12.12,0,0,0-.15.08.14.14,0,0,0,0,.19h0a21,21,0,0,1,7,5.45,33.54,33.54,0,0,1,4.9,7.39c1.42,2.62,2.09,5.52,3.31,8.23.53,1.43,1,2.79,1.67,4.17a33,33,0,0,0,1.84,4.28h0a.16.16,0,0,0,.13.07.14.14,0,0,0,.14-.14h0a31.66,31.66,0,0,0-.17-4.67C696.86,563.35,696.6,561.82,696.26,560.31Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M696.83,475.88a26,26,0,0,1-18.43-19.53c-3.46-16.13,7.67-9.22-2.31-34.56s-7.68-31.11-4.28-38.41,5.37-5,8.44,4.6,14.27,24.59,18.44,52.63S696.83,475.88,696.83,475.88Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M726.37,448.61s-10-3.45-10.37-22.27,7.3-15,8.46-38.41,2.68-40,6.9-40.33,15.37,11.53,19.21,37.64-5,43.4-12.67,51.09S726.37,448.61,726.37,448.61Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M691.45,498.54s-2.31-13.83-6.92-20.36-14.58-12.29-21.12-25-13.82-18.82-13.44-.77,8.45,27.27,18.44,33.42S691.45,498.54,691.45,498.54Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M687.23,522.34s-9.22-17.66-22.66-30.72S640.31,472,644.21,478.56s8.45,6.15,15,17.67,4.61,20,13.83,26.5S687.23,522.34,687.23,522.34Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M686.07,545c-.38-1.14-.77-9.59-13.82-16.51s-25.34-4.28-23.82-.77,9.61,9.22,17.68,12.69S686.07,545,686.07,545Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M725.63,499.31s-5.71-7.3-2.68-22.66,10-11.53,17.66-19.59,6.15-9.22,13.83-17.66,14.59-13.06,17.66-9.61-1.54,14.27-6.15,20.35-11.9,9.22-15.32,16.91-1.93,20-10.76,25.68S725.63,499.31,725.63,499.31Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M744.83,503.54s13.83-15.7,23.82-27.27,13.83-20.36,14.27-12.67,3.45,13.81-4.61,23.8S744.83,503.54,744.83,503.54Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M737.54,528.88s-.38-6.15,21.51-20.35,31.1-16.91,28.81-11.14-11.92,11.51-22.28,21.88S737.54,528.88,737.54,528.88Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M697.75,474.65a.4.4,0,0,1-.4-.33c-.05-.28-5.2-27.77-10.51-47.06S678,395.89,678,395.77a.4.4,0,0,1,.27-.5.41.41,0,0,1,.51.28c0,.12,3.51,12.18,8.83,31.5s10.48,46.84,10.53,47.12a.41.41,0,0,1-.33.47Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M725.51,449.31l-.12,0a.41.41,0,0,1-.26-.51c.09-.3,9.36-30,12.4-47.86,2.75-16.15.25-28.58-1.41-36.8-.19-.93-.36-1.8-.52-2.61a.42.42,0,0,1,.33-.48.41.41,0,0,1,.47.32c.16.82.33,1.69.52,2.61,1.67,8.28,4.19,20.79,1.41,37.09-3.05,17.87-12.33,47.67-12.43,48A.41.41,0,0,1,725.51,449.31Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M690.24,498.36a.39.39,0,0,1-.35-.2A146.62,146.62,0,0,0,675.77,478c-2.16-2.56-4.17-4.78-6.11-6.93-5.52-6.11-9.88-10.93-12.77-18.13a.41.41,0,1,1,.75-.31c2.84,7.07,7.15,11.85,12.62,17.89,2,2.16,4,4.38,6.14,7a146.7,146.7,0,0,1,14.19,20.26.4.4,0,0,1-.15.56A.35.35,0,0,1,690.24,498.36Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M725.63,499.72h-.07a.4.4,0,0,1-.33-.47c0-.18,3.18-18.64,10.45-28.21,2.82-3.71,6.68-8.18,10.78-12.91,6.55-7.57,13.32-15.4,16.85-21.3a.39.39,0,0,1,.55-.14.4.4,0,0,1,.14.56c-3.56,6-10.35,13.82-16.93,21.41-4.08,4.72-7.94,9.18-10.74,12.87-7.15,9.41-10.27,27.68-10.3,27.86A.41.41,0,0,1,725.63,499.72Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M685.17,545a.46.46,0,0,1-.22-.06c-.13-.09-13-8.46-22.1-13a.4.4,0,1,1,.36-.72c9.13,4.56,22.06,13,22.19,13a.4.4,0,0,1-.23.74Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M722.76,626.05H705.85a7.42,7.42,0,0,1-6.85-4.57c-5.52-13.22-20.27-51.44-11.41-62.27,8.46-10.31,44.92-10.31,53.38,0,8.87,10.83-5.87,49-11.42,62.27A7.41,7.41,0,0,1,722.76,626.05Z",
    fill: "#5d296b",
    "data-secondary": "true"
  })));
};

exports["default"] = TrendyIllustration;

/***/ }),

/***/ "./resources/js/Shared/Svg/UpgradeIllustration.tsx":
/*!*********************************************************!*\
  !*** ./resources/js/Shared/Svg/UpgradeIllustration.tsx ***!
  \*********************************************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var UpgradeIllustration = function UpgradeIllustration() {
  return react_1["default"].createElement("svg", {
    id: "e7135bce-b96f-4057-9a11-4cb1a14ea915",
    "data-name": "Layer 1",
    xmlns: "http://www.w3.org/2000/svg",
    width: "865.76",
    height: "682.89",
    viewBox: "0 0 865.76 682.89",
    className: "h-full w-full object-cover"
  }, react_1["default"].createElement("path", {
    d: "M624,479.6c0,9.16-6.86,16.57-15.32,16.57H457.44c-8.49,0-15.34-7.41-15.34-16.57V155.54c0-18.66,6.85-19.57,15.34-19.57h30.29c1.76,0,3.2,2,3.2,4.48v1.07c0,5.81,3.18,5.83,7.09,5.83h70c4,0,7.09,0,7.09-5.83v-1.07c0-2.48,1.42-4.48,3.22-4.48h30.29c8.46,0,15.32,2.48,15.32,19.57Z",
    fill: "#fff"
  }), react_1["default"].createElement("polygon", {
    points: "813.2 56.45 813.2 162.14 661.41 162.14 655.94 57.19 813.2 56.45",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "655.94",
    y: "57.19",
    width: "151.8",
    height: "104.73",
    fill: "#fff"
  }), react_1["default"].createElement("path", {
    d: "M808.43,162.64H655.22V56.47H808.43Zm-151.79-1.45H807V57.91H656.66Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "672.38",
    y: "70.2",
    width: "118.92",
    height: "78.7",
    fill: "#fff"
  }), react_1["default"].createElement("path", {
    d: "M791.29,70.2h-8.81L758,70.29l-85.6.18.35-.34v78.69h0l-.37-.38,118.9.08-.29.3c.06-24,.12-43.59.16-57.27,0-6.83.06-12.17.08-15.83V75.6c0,3.64,0,8.92.08,15.77,0,13.71.09,33.44.15,57.45v.29h-.3l-118.9.07h-.38v-.36h0v-79h.34L758,70l24.42.09h8.11A4.54,4.54,0,0,1,791.29,70.2Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M655.94,56.9a12.82,12.82,0,0,1,2.61,1.69c1.55,1.12,3.67,2.71,6,4.54s4.3,3.55,5.71,4.85a11.7,11.7,0,0,1,2.17,2.22c-.14.16-3.79-2.85-8.34-6.49S655.81,57.06,655.94,56.9Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M807.73,57.19c.13.16-3.44,3.21-8,6.8s-8.33,6.37-8.46,6.21,3.45-3.21,8-6.79S807.61,57,807.73,57.19Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M807.62,161.92l-16.93-13.7",
    fill: "#fff"
  }), react_1["default"].createElement("path", {
    d: "M790.69,148.22c.13-.17,4,2.77,8.7,6.56s8.35,7,8.23,7.14-4-2.77-8.71-6.56S790.6,148.37,790.69,148.22Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M655.94,161.91c-.13-.16,3.41-3.17,7.91-6.68s8.23-6.3,8.37-6.15-3.41,3.18-7.91,6.69S656.07,162.07,655.94,161.91Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "696.53",
    y: "94.73",
    width: "74.82",
    height: "26.35",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("g", {
    id: "b8e27f73-2e4d-42cf-bd7f-cc498777473b",
    "data-name": "freepik--background-complete--inject-2"
  }, react_1["default"].createElement("path", {
    d: "M133.52,156a34.92,34.92,0,0,1-7.14,12.33c-2.86,3.31-5.88,4.17-9.88,2.38a14.46,14.46,0,0,0-12.33-.19c-4.53,2-6.7,1.15-10-2.83a36.29,36.29,0,0,1-8.77-25c.16-5.88,2.3-11.13,7.43-14.56a12.42,12.42,0,0,1,12.81-.9,9.57,9.57,0,0,0,8.43.14,14.81,14.81,0,0,1,14,.36,11.77,11.77,0,0,1,1.12.71c3.2,2.36,3.22,2.72.43,5.44a13.53,13.53,0,0,0,0,19.13,13.21,13.21,0,0,0,1.76,1.49Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M121.44,111.53c1.08,6.1-5.67,14.15-11.39,13C108.73,117.5,116.88,110.77,121.44,111.53Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M151.56,194.25a11.57,11.57,0,0,0,4-.84,10.54,10.54,0,0,0,6.47-9V99a10.25,10.25,0,0,0-1-4,10.45,10.45,0,0,0-6.23-5.44,12.59,12.59,0,0,0-4.32-.46h-83a10.34,10.34,0,0,0-10.26,9.22c-.11,1.65,0,3.39,0,5.08v79.87a11.08,11.08,0,0,0,.65,4.08,10.33,10.33,0,0,0,5.25,5.77,10,10,0,0,0,3.75,1c1.29.09,2.61,0,3.9,0H92.47l43.24.1h0l-43.24.09H66.58a10.84,10.84,0,0,1-9.43-7,11.84,11.84,0,0,1-.68-4.21V98.29a10.89,10.89,0,0,1,10.89-9.74h83a13.29,13.29,0,0,1,4.48.5,10.93,10.93,0,0,1,6.52,5.69,10.56,10.56,0,0,1,1,4.2v42.24c0,18.26,0,33-.13,43.25a10.57,10.57,0,0,1-9.63,9.78Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M99.11,289.86c-6.69-.72-6.81-.79-6.73-7.19s.09-12.94.1-19.42c0-1.18.11-2,1.62-2,10.31.15,20.64.24,31,.36a8.62,8.62,0,0,1,1,.15v22.26c0,4.67-1,5.65-5.64,5.88a12.87,12.87,0,0,0-1.46.18c0,2.72.08,5.44,0,8.08a3.48,3.48,0,0,1-3.23,3.72,3.77,3.77,0,0,1-1-.06,3.15,3.15,0,0,1-3-3.3.86.86,0,0,1,0-.16v-6.8c0-1.36-.49-1.88-1.81-1.8a16.3,16.3,0,0,1-2.21,0c-1.17-.09-1.57.44-1.56,1.55v6.41c0,2.72-1.36,4.09-3.61,4.09s-3.53-1.37-3.56-4.09S99.11,292.52,99.11,289.86Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M120.27,241.55c-.86,1.57-1.79,3.39-2.62,4.91,6.81,3.28,8.68,8.41,8.37,13.12H92.51a15.6,15.6,0,0,1,8.77-13.21l-2.6-5.08.48-.27,2.91,5a19.6,19.6,0,0,1,14.82,0l2.83-4.76Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M135,272.33v7.72c0,2.57-1.36,3.9-3.52,3.94a3.47,3.47,0,0,1-3.7-3.23,3.25,3.25,0,0,1,0-.54c0-5.21,0-10.43.11-15.65a3.58,3.58,0,1,1,7.15-.09c0,.11,0,.23,0,.35v7.5Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M90.74,272c0,2.51.08,5,0,7.52-.14,3.18-2.72,5-5.16,3.68a3.65,3.65,0,0,1-1.86-2.45q-.16-8.37.15-16.75c.08-2.36,1.36-3.36,3.29-3.3a3.44,3.44,0,0,1,3.63,3.24,3.62,3.62,0,0,1,0,.57c.08,2.5,0,5,0,7.49Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M151.48,324.16a12,12,0,0,0,4-.84,10.52,10.52,0,0,0,6.47-9V228.9a10,10,0,0,0-1-4,10.41,10.41,0,0,0-6.21-5.44,12.84,12.84,0,0,0-4.32-.47H67.24a10.59,10.59,0,0,0-4.87,1.22,10.25,10.25,0,0,0-5.45,8c-.12,1.63,0,3.39,0,5.08v79.87a11.38,11.38,0,0,0,.64,4.09,10.32,10.32,0,0,0,9,6.74c1.28.07,2.61,0,3.91,0H92.42l43.22.06h0l-43.24.09H66.5a10.35,10.35,0,0,1-3.93-1,10.87,10.87,0,0,1-5.51-6,11.59,11.59,0,0,1-.68-4.21v-85a10.88,10.88,0,0,1,5.78-8.18,11.25,11.25,0,0,1,5.15-1.28h83a13.14,13.14,0,0,1,4.46.49,10.83,10.83,0,0,1,7.57,9.89v42.24c0,18.25,0,33-.12,43.25a10.68,10.68,0,0,1-6.66,9.07,10.84,10.84,0,0,1-3,.71Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M133.57,400.24H107.65V381.19l25.92-3.78Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M107.58,402.84h26v22.51L107.58,422Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M104.78,381.33v18.91H85V385c0-.37.65-1,1.06-1.05C92.25,383,98.43,382.17,104.78,381.33Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M85.2,402.77h19.61v18.78L85.2,419Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M151.4,454.06a11.28,11.28,0,0,0,4-.83,10.59,10.59,0,0,0,6.47-9V358.81a10.26,10.26,0,0,0-1-4,10.41,10.41,0,0,0-6.23-5.45,12.56,12.56,0,0,0-4.31-.47h-83a10.58,10.58,0,0,0-4.87,1.21,10.69,10.69,0,0,0-3.7,3.34A10.47,10.47,0,0,0,57,358.1c-.12,1.64,0,3.4-.07,5.07v79.88a11.11,11.11,0,0,0,.64,4.08,10.35,10.35,0,0,0,5.25,5.76,10.21,10.21,0,0,0,3.76,1c1.28.08,2.61,0,3.89,0H92.3l43.23.09h0L92.3,454H66.39A10.81,10.81,0,0,1,57,447a11.79,11.79,0,0,1-.68-4.2V360.38c0-.84,0-1.7.07-2.58a10.89,10.89,0,0,1,10.89-9.74h83a13.23,13.23,0,0,1,4.47.5,10.85,10.85,0,0,1,6.53,5.7,10.69,10.69,0,0,1,1,4.19v42.23c0,18.26,0,33-.14,43.25a10.69,10.69,0,0,1-6.75,9.38,11.27,11.27,0,0,1-3,.71C151.74,454.07,151.4,454.06,151.4,454.06Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M200.55,113a3.51,3.51,0,1,1-3.52-3.5h0A3.52,3.52,0,0,1,200.55,113Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M196.88,113a1.93,1.93,0,0,1-.48,0H195l-2.25.08a12.6,12.6,0,0,0-3,.33,10.88,10.88,0,0,0-8.28,9.52c-.12,1.81,0,3.81,0,5.87,0,16.53,0,39.31.1,64.47s0,47.94-.1,64.49a13.12,13.12,0,0,1-4.31,10.08,12.92,12.92,0,0,1-7.68,3.31c-2.32.09-4.08,0-5.32,0h0c1.2,0,3,0,5.3-.13A12.75,12.75,0,0,0,181,258c0-16.55-.07-39.33-.11-64.49S181,145.52,181,129c0-2.07,0-4,.09-5.91a11.58,11.58,0,0,1,1.81-5,11.14,11.14,0,0,1,6.81-4.73,12.07,12.07,0,0,1,3.06-.27h3.62Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M165.78,271.41a3.51,3.51,0,1,1-3.52-3.5h0A3.51,3.51,0,0,1,165.78,271.41Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("polygon", {
    points: "51.69 517.2 51.69 555.32 107.44 555.32 168.95 555.32 168.95 517.2 51.69 517.2",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("polygon", {
    points: "46.77 506.54 46.77 517.2 173.04 517.2 173.04 506.54 107.44 506.54 46.77 506.54",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("polygon", {
    points: "109.9 555.32 168.95 555.32 168.95 517.2 173.04 517.2 173.04 506.54 107.44 506.54 107.44 516.22 109.9 555.32",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "68.09",
    y: "523.35",
    width: "23.79",
    height: "5.74",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "65.03",
    y: "536.26",
    width: "30.54",
    height: "12.12",
    fill: "#fff"
  }), react_1["default"].createElement("rect", {
    x: "689.66",
    y: "386.64",
    width: "120.7",
    height: "228.16",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "653.83",
    y: "386.64",
    width: "120.7",
    height: "228.16",
    fill: "#f5f5f5"
  }), react_1["default"].createElement("path", {
    d: "M765.64,433.61H662.05V397.44H765.64Zm-102.32-1.28H764.38V398.71H663.32Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M694.9,398.08h39.31v.49a6.73,6.73,0,0,1-6.73,6.74H701.62a6.73,6.73,0,0,1-6.73-6.74h0v-.48Z",
    fill: "#ebebeb"
  }), react_1["default"].createElement("path", {
    d: "M765.64,479.55H662.05V443.38H765.64Zm-102.32-1.28H764.38V444.65H663.32Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M694.9,444h39.31v.48a6.73,6.73,0,0,1-6.73,6.73H701.62a6.73,6.73,0,0,1-6.73-6.73h0V444Z",
    fill: "#ebebeb"
  }), react_1["default"].createElement("path", {
    d: "M765.64,525.49H662.05V489.32H765.64Zm-102.32-1.28H764.38V490.6H663.32Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M694.9,490h39.31v.48a6.73,6.73,0,0,1-6.73,6.73H701.62a6.73,6.73,0,0,1-6.73-6.73h0V490Z",
    fill: "#ebebeb"
  }), react_1["default"].createElement("path", {
    d: "M765.64,571.39H662.05V535.26H765.64Zm-102.32-1.28H764.38V536.5H663.32Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M694.9,535.9h39.31v.47a6.73,6.73,0,0,1-6.73,6.74H701.62a6.73,6.73,0,0,1-6.73-6.74h0v-.47Z",
    fill: "#ebebeb"
  }), react_1["default"].createElement("rect", {
    x: "648.48",
    y: "378.11",
    width: "131.09",
    height: "11.36",
    fill: "#ebebeb"
  }), react_1["default"].createElement("rect", {
    x: "779.58",
    y: "378.11",
    width: "39.42",
    height: "11.36",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "653.83",
    y: "614.79",
    width: "6.34",
    height: "11.65",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "693.03",
    y: "614.79",
    width: "6.34",
    height: "11.65",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "764.61",
    y: "614.79",
    width: "6.34",
    height: "11.65",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("rect", {
    x: "800.41",
    y: "614.79",
    width: "6.34",
    height: "11.65",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("polygon", {
    points: "765.59 347.76 777.81 379.02 770.22 378.98 764.12 359.78 765.59 347.76",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("polygon", {
    points: "723.87 318.89 719.66 378.77 763.03 379.67 767.9 318.89 723.87 318.89",
    fill: "#ebebeb"
  }), react_1["default"].createElement("polygon", {
    points: "728.21 323.7 724.71 373.55 756.93 375.06 761.44 323.4 728.21 323.7",
    fill: "#fafafa"
  }), react_1["default"].createElement("polygon", {
    points: "735.27 330.75 732.66 365.79 750.45 365.79 753.57 330.75 735.27 330.75",
    fill: "#e0e0e0"
  })), react_1["default"].createElement("path", {
    d: "M617.8,124.93h-169a17.26,17.26,0,0,0-17.15,17.37v348a17.26,17.26,0,0,0,17.11,17.42h169a17.29,17.29,0,0,0,17.17-17.41h0v-348A17.29,17.29,0,0,0,617.8,124.93Zm6.43,354.67c0,9.16-6.86,16.57-15.32,16.57H457.68c-8.49,0-15.34-7.41-15.34-16.57V155.54c0-18.66,6.85-19.57,15.34-19.57H488c1.76,0,3.2,2,3.2,4.48v1.07c0,5.81,3.18,5.83,7.09,5.83h70c4,0,7.09,0,7.09-5.83v-1.07c0-2.48,1.42-4.48,3.22-4.48h30.29c8.46,0,15.32,2.48,15.32,19.57Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M542.84,140.09H523.73a1.18,1.18,0,0,1-1.18-1.18h0a1.18,1.18,0,0,1,1.18-1.18h19.11a1.18,1.18,0,0,1,1.18,1.18h0A1.18,1.18,0,0,1,542.84,140.09Z",
    fill: "#fff"
  }), react_1["default"].createElement("g", {
    id: "bd0f6cf3-c758-4038-aa40-c991501f6e08",
    "data-name": "freepik--u5Zjqx--inject-18"
  }, react_1["default"].createElement("path", {
    d: "M482.74,143.25l-.29.29-.17.16a4,4,0,0,0-5.64,0l-.16-.17-.29-.29h0l.37-.33a4.78,4.78,0,0,1,.86-.54,4.14,4.14,0,0,1,1.14-.38,1.38,1.38,0,0,0,.56-.08H480a5,5,0,0,1,1,.25,4.94,4.94,0,0,1,.7.33,5.06,5.06,0,0,1,.67.47l.31.28Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M477.57,144.62l-.47-.46a3.36,3.36,0,0,1,4.72,0l-.47.46A2.68,2.68,0,0,0,477.57,144.62Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M480.89,145.09l-.46.46a1.36,1.36,0,0,0-1.91,0l0,0-.46-.46a2,2,0,0,1,2.83,0Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M479.88,146a.42.42,0,0,1-.42.42.41.41,0,0,1-.41-.41h0a.41.41,0,0,1,.41-.41h0a.43.43,0,0,1,.42.4Z",
    fill: "#5d296b",
    "data-secondary": "true"
  })), react_1["default"].createElement("path", {
    d: "M454.2,144.13a1.21,1.21,0,1,1,1.21,1.21h0a1.2,1.2,0,0,1-1.2-1.2Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M458,144.13a1.21,1.21,0,1,1,1.21,1.21,1.21,1.21,0,0,1-1.21-1.21Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M461.79,144.13a1.21,1.21,0,1,1,1.21,1.21,1.21,1.21,0,0,1-1.21-1.21Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M465.59,144.13a1.21,1.21,0,1,1,1.21,1.21h0a1.2,1.2,0,0,1-1.2-1.2Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M469.39,144.13a1.21,1.21,0,1,1,1.21,1.21h0a1.2,1.2,0,0,1-1.2-1.2Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M610.6,146.77h-9.83v-4.56h9.83Zm-9.32-.51h8.8v-3.54h-8.79Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "601.59",
    y: "142.99",
    width: "6.33",
    height: "3",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "610.6",
    y: "143.27",
    width: "0.87",
    height: "2.43",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "599.8",
    y: "474.51",
    width: "9.69",
    height: "1.38",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "599.8",
    y: "477.93",
    width: "9.69",
    height: "1.39",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "599.8",
    y: "481.36",
    width: "9.69",
    height: "1.39",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "536.33 478.77 533.57 481.71 530.82 478.77 531.3 478.31 533.57 480.73 535.84 478.31 536.33 478.77",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M533.57,484.75a5,5,0,1,1,5-5A5,5,0,0,1,533.57,484.75Zm0-9.35a4.35,4.35,0,1,0,4.34,4.36v0A4.34,4.34,0,0,0,533.57,475.4Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M541.22,283a12.22,12.22,0,1,0-12.28-12.16h0A12.22,12.22,0,0,0,541.22,283Zm-10.79-12.15a10.73,10.73,0,1,1,10.8,10.66h0A10.73,10.73,0,0,1,530.43,270.82ZM541.22,283a12.22,12.22,0,1,0-12.28-12.16h0A12.22,12.22,0,0,0,541.22,283Zm-10.79-12.15a10.73,10.73,0,1,1,10.8,10.66h0A10.73,10.73,0,0,1,530.43,270.82ZM541.22,283a12.22,12.22,0,1,0-12.28-12.16h0A12.22,12.22,0,0,0,541.22,283Zm-10.79-12.15a10.73,10.73,0,1,1,10.8,10.66h0A10.73,10.73,0,0,1,530.43,270.82ZM541.3,296.5a25.75,25.75,0,1,0-25.89-25.61h0a25.76,25.76,0,0,0,25.89,25.6ZM527,270.84a14.11,14.11,0,1,1,14.22,14h0a14.13,14.13,0,0,1-14.19-14Zm1.9,0a12.22,12.22,0,1,0,12.16-12.28h0a12.22,12.22,0,0,0-12.14,12.26Zm1.49,0a10.73,10.73,0,1,1,10.8,10.66h0a10.73,10.73,0,0,1-10.78-10.68h0ZM541.22,283a12.22,12.22,0,1,0-12.28-12.16h0A12.22,12.22,0,0,0,541.22,283Zm-10.79-12.15a10.73,10.73,0,1,1,10.8,10.66h0a10.73,10.73,0,0,1-10.78-10.68h0ZM541.22,283a12.22,12.22,0,1,0-12.28-12.16h0A12.22,12.22,0,0,0,541.22,283Zm-10.79-12.15a10.73,10.73,0,1,1,10.8,10.66h0a10.73,10.73,0,0,1-10.78-10.68h0Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "536.06 246.1 545.96 246.04 545.37 239.04 536.56 239.09 536.06 246.1",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "554.92 249.33 561.96 256.28 566.5 250.91 560.24 244.73 554.92 249.33",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "565.98 264.94 566.04 274.84 573.04 274.26 572.99 265.45 565.98 264.94",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "562.76 283.81 555.8 290.85 561.17 295.39 567.36 289.12 562.76 283.81",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "547.14 294.87 537.24 294.92 537.83 301.93 546.63 301.88 547.14 294.87",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "528.27 291.64 521.23 284.68 516.7 290.05 522.96 296.24 528.27 291.64",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "517.22 276.02 517.16 266.12 510.15 266.71 510.2 275.52 517.22 276.02",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "520.44 257.16 527.4 250.12 522.03 245.58 515.84 251.84 520.44 257.16",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    opacity: "0.5"
  }, react_1["default"].createElement("path", {
    d: "M510.17,243.43a15.63,15.63,0,1,0-11.74,18.74,15.64,15.64,0,0,0,11.74-18.74Zm-13.32,11.9a8.57,8.57,0,1,1,6.43-10.27h0A8.59,8.59,0,0,1,496.85,255.33Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "488.65 232.99 494.51 231.65 493.24 227.57 488.03 228.77 488.65 232.99",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "500.26 232.4 505.35 235.6 507.33 231.81 502.8 228.98 500.26 232.4",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "508.88 240.2 510.23 246.05 514.3 244.78 513.1 239.57 508.88 240.2",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "509.47 251.8 506.28 256.9 510.06 258.87 512.9 254.34 509.47 251.8",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "501.68 260.42 495.82 261.77 497.09 265.84 502.31 264.64 501.68 260.42",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "490.07 261.01 484.98 257.82 483 261.6 487.53 264.44 490.07 261.01",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "481.45 253.22 480.1 247.36 476.03 248.64 477.23 253.85 481.45 253.22",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "480.86 241.61 484.05 236.52 480.27 234.54 477.43 239.07 480.86 241.61",
    fill: "#9f26d3",
    "data-primary": "true"
  })), react_1["default"].createElement("g", {
    opacity: "0.5"
  }, react_1["default"].createElement("path", {
    d: "M578.06,228.33a13.42,13.42,0,1,0-12.19,14.55,13.42,13.42,0,0,0,12.19-14.55Zm-12.72,8.51a7.36,7.36,0,1,1,6.68-8,7.36,7.36,0,0,1-6.68,8h0Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "560.99 216.93 566.13 216.48 565.53 212.87 560.95 213.27 560.99 216.93",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "570.93 217.8 574.88 221.11 577.01 218.13 573.49 215.18 570.93 217.8",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "577.34 225.44 577.8 230.58 581.41 229.98 581.01 225.4 577.34 225.44",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "576.47 235.38 573.16 239.33 576.14 241.46 579.09 237.94 576.47 235.38",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "568.83 241.79 563.69 242.25 564.3 245.86 568.87 245.46 568.83 241.79",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "558.9 240.92 554.94 237.61 552.81 240.59 556.33 243.54 558.9 240.92",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "552.48 233.28 552.03 228.14 548.42 228.75 548.82 233.32 552.48 233.28",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "553.35 223.35 556.66 219.39 553.68 217.26 550.73 220.78 553.35 223.35",
    fill: "#9f26d3",
    "data-primary": "true"
  })), react_1["default"].createElement("path", {
    d: "M507.84,328.16l.35-.7a4.17,4.17,0,0,0,2.82,1c1.6,0,2.3-.67,2.3-1.51,0-2.35-5.26-.91-5.26-4.07,0-1.26,1-2.34,3.15-2.34a5,5,0,0,1,2.65.75l-.31.72a4.51,4.51,0,0,0-2.34-.7c-1.56,0-2.26.69-2.26,1.55,0,2.34,5.26.92,5.26,4,0,1.25-1,2.33-3.19,2.33A4.56,4.56,0,0,1,507.84,328.16Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M518.91,326.24v3H518v-3l-3.42-5.59h1l2.93,4.8,2.93-4.8h.9Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M522.73,328.16l.35-.7a4.17,4.17,0,0,0,2.82,1c1.6,0,2.3-.67,2.3-1.51,0-2.35-5.26-.91-5.26-4.07,0-1.26,1-2.34,3.15-2.34a5,5,0,0,1,2.65.75l-.31.72a4.51,4.51,0,0,0-2.34-.7c-1.56,0-2.26.69-2.26,1.55,0,2.34,5.26.92,5.26,4,0,1.25-1,2.33-3.19,2.33A4.56,4.56,0,0,1,522.73,328.16Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M532.59,321.43h-3v-.78h6.91v.78h-3v7.77h-.91Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M544,328.41v.79h-6v-8.55h5.86v.78h-5v3h4.42v.77h-4.42v3.17Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M555,320.65v8.55h-.84v-6.84l-3.36,5.75h-.42L547,322.4v6.8h-.86v-8.55h.75L550.6,327l3.67-6.34Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M500.77,340.18v-5h2.07v4.94c0,1.71.74,2.41,2,2.41s2-.7,2-2.41v-4.94h2.05v5c0,2.64-1.51,4.1-4.06,4.1S500.77,342.82,500.77,340.18Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M518.7,338.41c0,2-1.5,3.24-3.89,3.24H513v2.47h-2.07v-9h3.88C517.2,335.16,518.7,336.4,518.7,338.41Zm-2.1,0c0-1-.64-1.56-1.91-1.56H513V340h1.69c1.27,0,1.91-.57,1.91-1.55Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M526.35,339.5h1.9v3.64a6.17,6.17,0,0,1-3.62,1.14,4.64,4.64,0,1,1-.43-9.27h.43a4.79,4.79,0,0,1,3.75,1.55l-1.33,1.23a3,3,0,0,0-2.31-1,2.87,2.87,0,1,0,0,5.74h0a3.14,3.14,0,0,0,1.61-.38Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M535.88,344.12l-1.74-2.49h-1.9v2.49h-2.1v-9H534c2.39,0,3.89,1.24,3.89,3.25a2.9,2.9,0,0,1-1.85,2.83l2,2.88Zm-2-7.27h-1.64V340h1.69c1.27,0,1.91-.59,1.91-1.57s-.64-1.56-1.91-1.56Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M545.48,342.2h-4.16l-.8,1.92H538.4l4-9h2.05l4,9h-2.18Zm-.66-1.57-1.42-3.43L542,340.63Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M549.38,335.16h4.07c2.93,0,4.94,1.77,4.94,4.48s-2,4.48-4.94,4.48h-4.07Zm4,7.26a2.78,2.78,0,1,0,0-5.56h-1.9v5.56Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M566.89,342.46v1.66H560v-9h6.77v1.67H562v1.94h4.13v1.61H562v2.08Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M506.47,406.93a3.12,3.12,0,0,1,3.08-3.16h.18a3,3,0,0,1,2.27.91l-.41.41a2.53,2.53,0,0,0-1.84-.73,2.58,2.58,0,1,0-.32,5.15h.32a2.42,2.42,0,0,0,1.84-.75l.41.42a3,3,0,0,1-2.28.91,3.11,3.11,0,0,1-3.25-3Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M518.6,403.82V410H518v-2.87h-3.89V410h-.65v-6.22h.65v2.75H518v-2.78Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M525,409.47V410h-4.4v-6.22h4.27v.57h-3.61v2.18h3.22v.56h-3.22v2.31Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M526,406.93a3.12,3.12,0,0,1,3.08-3.16h.18a3,3,0,0,1,2.27.91l-.42.41a2.46,2.46,0,0,0-1.83-.73,2.58,2.58,0,1,0-.32,5.15h.32a2.35,2.35,0,0,0,1.83-.75l.42.42a3,3,0,0,1-2.28.91,3.11,3.11,0,0,1-3.25-3Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M534.84,407.12l-1.27,1.29V410h-.66v-6.22h.66v3.75l3.65-3.76H538l-2.69,2.82,2.88,3.4h-.78Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M539.23,403.82h.65V410h-.65Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M547.14,403.82V410h-.54l-4-5v5H542v-6.22h.54l4,5.05v-5.05Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M553.5,406.93h.64v2.37a3.34,3.34,0,0,1-2.28.79,3.16,3.16,0,0,1,0-6.32,3.07,3.07,0,0,1,2.31.9l-.41.42a2.55,2.55,0,0,0-1.87-.73,2.58,2.58,0,1,0-.32,5.15h.32a2.6,2.6,0,0,0,1.62-.48Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M555.53,409.57a.48.48,0,0,1,.48-.48.49.49,0,0,1,.48.48.48.48,0,0,1-.48.48.47.47,0,0,1-.48-.48Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M557.42,409.57a.47.47,0,0,1,.46-.48h0a.48.48,0,0,1,.48.46v0a.48.48,0,0,1-.48.48h0a.47.47,0,0,1-.47-.47Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M559.3,409.57a.47.47,0,0,1,.46-.48h0a.48.48,0,0,1,.48.46v0a.48.48,0,0,1-.48.48h0a.47.47,0,0,1-.47-.47Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("rect", {
    x: "473.47",
    y: "371.21",
    width: "66.58",
    height: "23.79",
    rx: "11.22",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    opacity: "0.2"
  }, react_1["default"].createElement("polygon", {
    points: "502.43 371.21 493.48 394.71 500.06 394.71 508.4 371.21 502.43 371.21",
    fill: "#fff"
  })), react_1["default"].createElement("g", {
    opacity: "0.2"
  }, react_1["default"].createElement("polygon", {
    points: "518.58 371.21 509.62 394.71 516.2 394.71 524.55 371.21 518.58 371.21",
    fill: "#fff"
  })), react_1["default"].createElement("g", {
    opacity: "0.2"
  }, react_1["default"].createElement("path", {
    d: "M534.39,373l-8.33,21.88H530a14.48,14.48,0,0,0,3-1l5.75-16.18A12.72,12.72,0,0,0,534.39,373Z",
    fill: "#fff"
  })), react_1["default"].createElement("g", {
    opacity: "0.2"
  }, react_1["default"].createElement("path", {
    d: "M486.75,371.21l-8.24,21.61a11.77,11.77,0,0,0,5.86,1.9l8.34-23.51Z",
    fill: "#fff"
  })), react_1["default"].createElement("path", {
    d: "M581.27,395.21H485.43a12.47,12.47,0,0,1,0-24.93h95.84a12.47,12.47,0,1,1,0,24.93Zm-95.84-23.93a11.47,11.47,0,0,0,0,22.93h95.84a11.47,11.47,0,1,0,0-22.93Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M561,382.65l.39-3.9h4v.61h-3.44l-.28,2.7a2.63,2.63,0,0,1,1.51-.44,2.22,2.22,0,0,1,1.72.71,2.64,2.64,0,0,1,.65,1.87,2.82,2.82,0,0,1-.63,2,2.25,2.25,0,0,1-1.77.7,2.43,2.43,0,0,1-1.69-.58,2.38,2.38,0,0,1-.74-1.63h.63a1.9,1.9,0,0,0,.56,1.24,1.76,1.76,0,0,0,1.24.42,1.54,1.54,0,0,0,1.3-.55,2.42,2.42,0,0,0,.44-1.55,2.12,2.12,0,0,0-.49-1.43,1.7,1.7,0,0,0-1.34-.56,2.47,2.47,0,0,0-.81.12,2.14,2.14,0,0,0-.73.46Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M567.22,382.65l.38-3.9h4v.61h-3.44l-.28,2.7a2.63,2.63,0,0,1,1.51-.44,2.2,2.2,0,0,1,1.72.71,2.64,2.64,0,0,1,.65,1.87,2.82,2.82,0,0,1-.63,2,2.24,2.24,0,0,1-1.76.7,2.44,2.44,0,0,1-1.7-.58,2.38,2.38,0,0,1-.74-1.63h.63a1.9,1.9,0,0,0,.56,1.24,1.78,1.78,0,0,0,1.25.42,1.54,1.54,0,0,0,1.29-.55,2.42,2.42,0,0,0,.44-1.55,2.12,2.12,0,0,0-.49-1.43,1.7,1.7,0,0,0-1.34-.56,2.47,2.47,0,0,0-.81.12,2.14,2.14,0,0,0-.73.46Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M572.86,380.29a1.64,1.64,0,0,1,.44-1.19,1.48,1.48,0,0,1,1.12-.47,1.5,1.5,0,0,1,1.13.47,1.72,1.72,0,0,1,.44,1.22v.39a1.65,1.65,0,0,1-.44,1.17,1.43,1.43,0,0,1-1.12.47,1.49,1.49,0,0,1-1.12-.46,1.67,1.67,0,0,1-.45-1.22Zm.53.42a1.24,1.24,0,0,0,.28.84,1,1,0,0,0,.76.33.93.93,0,0,0,.75-.33,1.35,1.35,0,0,0,.28-.87v-.4a1.24,1.24,0,0,0-.29-.84.92.92,0,0,0-.75-.34.94.94,0,0,0-.74.33,1.32,1.32,0,0,0-.29.89Zm1.25,5.38-.41-.27,3.91-6.25.41.26Zm2.25-1.3a1.62,1.62,0,0,1,.44-1.18,1.46,1.46,0,0,1,1.12-.47,1.49,1.49,0,0,1,1.12.46,1.69,1.69,0,0,1,.45,1.23v.39a1.67,1.67,0,0,1-.44,1.18,1.49,1.49,0,0,1-1.12.47,1.48,1.48,0,0,1-1.12-.47,1.66,1.66,0,0,1-.45-1.21Zm.53.43a1.24,1.24,0,0,0,.28.84,1,1,0,0,0,.76.33.93.93,0,0,0,.75-.33,1.28,1.28,0,0,0,.28-.87v-.4a1.31,1.31,0,0,0-.28-.86,1,1,0,0,0-.76-.32.94.94,0,0,0-.75.33,1.25,1.25,0,0,0-.28.87Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    opacity: "0.3"
  }, react_1["default"].createElement("path", {
    d: "M324.26,476.55l12.8,6.23,4.64,13.44-8.65,9.35,2.61,3.08a30.59,30.59,0,0,0,19.19-49.8L417.66,329A30.59,30.59,0,0,0,445,283.2l-2.59-3.09-3.46,15.52-13.44,4.64-12.79-6.22-4.65-13.44,5.32-13.31-1.87-2.22c-8.51,2.11-16.42,11.49-20.55,20a30.57,30.57,0,0,0,3.92,32.87L332.05,447.77A30.62,30.62,0,0,0,303.78,465c-4.6,9.49-4.53,21.34.34,29.84l6.7-13.62Z"
  })), react_1["default"].createElement("path", {
    d: "M321.67,473.47l12.79,6.2,4.64,13.45L333,505.55a30.6,30.6,0,0,0,19.21-49.81L415.05,326a30.6,30.6,0,0,0,27.33-45.84l-6.08,12.43-13.44,4.65L410.06,291l-4.64-13.44,6.08-12.44a30.6,30.6,0,0,0-19.23,49.77L329.45,444.75a30.6,30.6,0,0,0-27.29,45.84l6.09-12.43Z",
    fill: "#e0e0e0"
  }), react_1["default"].createElement("path", {
    d: "M236.29,403.12s-30.73,16.46-39.5,80.78c0,0-14.62,75.58,50.68,81.16,0,0,71.92,15.47,126.11,7.36l87.32-6.22,25-24,14.73-68.79L291.7,393.91Z",
    fill: "#d1d3d4"
  }), react_1["default"].createElement("path", {
    d: "M448.76,427l9-5.94s48.45-35.76,53.7,10.67V482a132.66,132.66,0,0,0,.93,15.16c.46,4,.4,9-2.55,9.81L444,428Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M448.76,427l9-5.94s48.45-35.76,53.7,10.67V482a132.66,132.66,0,0,0,.93,15.16c.46,4,.4,9-2.55,9.81L444,428Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M375.87,384.52l3.61-13.5-7.23-19.26s-7.73,2.3-9.34,7Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M275.73,398.16,298.12,446c13.61,8.56,30.95,13.93,30.95,13.93,8.55-7.78,68.07-33.06,68.07-33.06l-34.23-68.08c.39-3.11,9.34-7,9.34-7-9.34-31-24.9-30.73-24.9-30.73l-16.49-2-39.16-7.78c-7,1.49-24.1,3.56-24.1,3.56-17.5,2.34-24.12,48.63-24.12,48.63s6.62-8.56,25.29-2.34c0,0,13.61,4.28,12.06,9.34Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("g", {
    opacity: "0.2"
  }, react_1["default"].createElement("path", {
    d: "M282.58,371.57a3.76,3.76,0,0,1-2.05-.68l.6-.92a2.26,2.26,0,0,0,2.3.3c3.38-1.58,5.3-11.93,5.76-15.79l1.1.13c-.07.59-1.78,14.49-6.39,16.66A3.1,3.1,0,0,1,282.58,371.57Z"
  })), react_1["default"].createElement("path", {
    d: "M457.8,421l16.58-70.29a3.59,3.59,0,0,0-3.65-4.4l-87.57,4a4.81,4.81,0,0,0-4.49,3.75l-16.13,71.24a8.34,8.34,0,0,1-6,6.36l-44.31,21.68,16.88,6.52s47.63-27.06,68.07-33.06c0,0,33.2-15.93,41.7-9.31,0,0,6.61,4.25,9.92,9.44Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M457.8,421l16.58-70.29a3.59,3.59,0,0,0-3.65-4.4l-87.57,4a4.81,4.81,0,0,0-4.49,3.75l-16.13,71.24a8.34,8.34,0,0,1-6,6.36l-44.31,21.68,16.88,6.52s47.63-27.06,68.07-33.06c0,0,33.2-15.93,41.7-9.31,0,0,6.61,4.25,9.92,9.44Z",
    fill: "#fff",
    opacity: "0.4"
  }), react_1["default"].createElement("path", {
    d: "M362.91,431.7c1.78-.23,3.56-.35,5.33-.43s3.56-.11,5.34-.12,3.55,0,5.33.12,3.56.2,5.34.43c-1.78.23-3.56.35-5.34.43s-3.55.11-5.33.12-3.56,0-5.34-.13S364.69,431.92,362.91,431.7Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M329.07,459.9s71.51,26.59,93.71,13.36c0,0,9,21.73,48.65,47.71,0,0,15.59,13.7,17.95,9l-5.67-4s2.13-26.22,23.15-22.91c0,0,4.72,2.83,2.36-3.54l-42-45.58L448.76,427s-4.88-6.92-9.92-9.44c0,0-5.51-4.19-19.37.71C419.47,418.24,362.41,438.51,329.07,459.9Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M471.76,437.8a43.6,43.6,0,0,1-.42,4.44c-.19,1.47-.45,2.92-.72,4.37s-.59,2.9-1,4.34-.76,2.86-1.26,4.27a43.6,43.6,0,0,1,.42-4.44c.19-1.47.45-2.92.72-4.37s.6-2.9,1-4.33S471.26,439.21,471.76,437.8Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M597.5,547.58s-5.17,3.7-22,14.58c0,0-15.51,9.26-34.3,7.63,0,0-18.78-1.1-22.86-3l-2.46-9.53s-4.89-8.17-1.36-10.07c0,0,3.82,1.95,7.63-2.72,0,0,10.07-12.53,13.61-15.52,0,0,5.24-3.28,11.62,2.89a28.88,28.88,0,0,0,9.93,6.42,36.92,36.92,0,0,0,14.38,2.4S598.05,539,597.5,547.58Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M578.72,574.28c-17.42,11.84-54.17,7.48-54.17,7.48-9-.82-6.26-15-6.26-15,4.08,1.9,22.86,3,22.86,3,18.79,1.63,34.3-7.63,34.3-7.63,16.88-10.88,22-14.58,22-14.58C620.64,552.87,578.72,574.28,578.72,574.28Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M537.34,533.58a21,21,0,0,1,3,.37,24.32,24.32,0,0,1,2.91.74,22,22,0,0,1,2.8,1.16,16.5,16.5,0,0,1,2.57,1.69l-.12-.08c1.43.68,2.87,1.33,4.29,2s2.85,1.38,4.26,2.09,2.82,1.44,4.22,2.2,2.78,1.51,4.15,2.32c-1.49-.55-3-1.15-4.42-1.76s-2.91-1.24-4.36-1.89-2.88-1.31-4.32-2-2.85-1.38-4.28-2.06l-.06,0-.05,0a18.29,18.29,0,0,0-4.93-3A35.74,35.74,0,0,0,537.34,533.58Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M568.65,560.26a16.81,16.81,0,0,1-.51-4.35,20.2,20.2,0,0,1,.48-4.38,15.87,15.87,0,0,1,1.63-4.14,10,10,0,0,1,3-3.19,17.75,17.75,0,0,0-2.27,3.59,25.2,25.2,0,0,0-1.32,4A40.94,40.94,0,0,0,569,556C568.84,557.37,568.72,558.8,568.65,560.26Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M455.22,551.15s3.92,2.36,13.18-.58c0,0,29.07-9.53,41.28,12,0,0-4.15-17.64-3.66-24.49l-16.64-8.16s-1.06,2.6-5.73-.24c0,0-2.09,3.27-9.17,4.74a21.31,21.31,0,0,0-10.3,7.33S459.37,549.93,455.22,551.15Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M446.61,558.64s4.23-8.37,8.61-7.49c0,0,3,1.77,11.4-.06,0,0,19.6-7.17,35.52,2.71a18.24,18.24,0,0,1,8.73,17.88,15.55,15.55,0,0,1-8.51,12.14,139,139,0,0,1-55.75-25.18Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M451.75,567.2s-14.18-13.15,1.46-6.94c0,0,7.85,5.61,14.41,1.75a21.61,21.61,0,0,1,7.89-2.9c5.47-.8,14-1.21,21.56,2a12.33,12.33,0,0,1,7.48,10.47c.54,7.11-.55,17.15-11.73,14.24l-9.17-4.4s-1.34-1.71-10.14-.49C473.51,580.89,467.88,584.31,451.75,567.2Z",
    fill: "#e6e7e8"
  }), react_1["default"].createElement("path", {
    d: "M501.53,538.24a10.83,10.83,0,0,1,.21,2,18,18,0,0,1-.44,4,10.2,10.2,0,0,1-.65,1.93,9.46,9.46,0,0,1-.21-2,16.39,16.39,0,0,1,.44-4A10,10,0,0,1,501.53,538.24Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M494.29,547.58a7.62,7.62,0,0,1,2.11-1.36,8.59,8.59,0,0,1,2.45-.69,5.44,5.44,0,0,1,2.61.3,3.2,3.2,0,0,1,1.87,1.75,4.88,4.88,0,0,0-2.11-.93,8,8,0,0,0-2.24,0C497.45,546.8,495.93,547.23,494.29,547.58Z",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M304.45,283s-2.89,17.34-12.75,28.23c0,0,26.46,27.23,39.16,7.78l1.15-13-25.3-22.44Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M330.86,319l.76-6.69-22.86-16.12S307.17,315.69,330.86,319Z",
    fill: "#f7a48b"
  }), react_1["default"].createElement("ellipse", {
    cx: "332.01",
    cy: "278",
    rx: "37.59",
    ry: "25.59",
    transform: "translate(26.7 585.09) rotate(-85.11)",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M304.52,263.91s-2-28.73,15.28-28.73c0,0,16.8-7.41,24.71,0,0,0,1.48,4.95,7.9,4.95,0,0,7.41,1.22,10.38,7.77,0,0,8.77,13-5.25,23.34l-1.88-7.66a10.14,10.14,0,0,0-10.61-7.71,36.5,36.5,0,0,0-5.92.92,20.15,20.15,0,0,0-9.51,5.34C324.16,267.58,310.76,278.71,304.52,263.91Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("ellipse", {
    cx: "306.71",
    cy: "273.48",
    rx: "6.67",
    ry: "10.13",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M243.48,363.43s-8.85,39.3-9.16,62c0,0-1.89,7.24,11.34,7.87,0,0,35,7.73,44,10.86.47.16.86.32,1.17.46,6.42,3,33.82,2.54,33.82,2.54L356.5,431.7s6.29-1.26,6.29-8.82l1.58-6.3s-11-3.46-17.32,3.15c0,0-14.8,11-30.86,4.09,0,0-23.77-6.58-41.54-14.57a3,3,0,0,1-1.59-3.79l2.67-7.3,5.1-27.73s1.4-2.58-2.87-5C278,365.44,253.91,351.84,243.48,363.43Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("rect", {
    x: "270.62",
    y: "404.35",
    width: "1.1",
    height: "7.37",
    transform: "translate(-203.53 529.21) rotate(-70.71)",
    fill: "#f7a48b"
  }), react_1["default"].createElement("path", {
    d: "M483.71,525.93l30.77,21.26s4,1.84,7.62-2.72c0,0,9.53-12,13.61-15.52l-23.5-20a28.21,28.21,0,0,1-4.18-4.42L506.86,503s-12.52-3.35-19.29,9.89C487.57,512.91,483.07,521.81,483.71,525.93Z",
    fill: "#f9b499"
  }), react_1["default"].createElement("path", {
    d: "M225.58,437.8a17.33,17.33,0,0,1,3-1.18c1-.32,2-.58,3.09-.8A50.32,50.32,0,0,1,238,435a80.46,80.46,0,0,1,12.8.14,123.54,123.54,0,0,1,12.68,1.78c4.19.82,8.34,1.82,12.46,2.91a245.92,245.92,0,0,1,24.28,8.1c2,.78,4,1.6,5.94,2.39l5.89,2.51c3.91,1.65,7.88,3.2,11.85,4.71,15.94,6,32.13,11.36,48.72,15.12a161.34,161.34,0,0,0,25.17,4,77.3,77.3,0,0,0,12.73-.12,36.6,36.6,0,0,0,12.28-3.19,35.29,35.29,0,0,1-12.24,3.51,74,74,0,0,1-12.8.38,162.83,162.83,0,0,1-25.34-3.63c-8.34-1.85-16.57-4.12-24.72-6.65-4.07-1.26-8.12-2.6-12.16-4s-8-2.88-12-4.4-7.94-3.11-11.88-4.79l-5.86-2.52c-2-.8-3.94-1.62-5.91-2.4-7.92-3.1-15.95-5.94-24.13-8.27-4.1-1.13-8.23-2.16-12.39-3s-8.38-1.5-12.6-1.93A79.72,79.72,0,0,0,238,435.2a51.82,51.82,0,0,0-6.33.76c-1,.19-2.08.43-3.1.72A17.63,17.63,0,0,0,225.58,437.8Z",
    fill: "#bcbec0"
  }), react_1["default"].createElement("line", {
    x1: "362.91",
    y1: "358.76",
    x2: "349.32",
    y2: "334.38",
    fill: "none",
    stroke: "#000",
    strokeMiterlimit: "10",
    opacity: "0.2"
  }), react_1["default"].createElement("path", {
    d: "M202.92,528a317.11,317.11,0,0,0-.46-33.35A424.89,424.89,0,0,0,192,426.74c-2.91-12.58-6.5-24.95-10.25-37.32-.1-.38-.71-.28-.6.12,11.56,43.53,21.05,88.45,20.4,133.7-.18,12.67-1.65,25.11-2.86,37.69,0,.33.52.47.61.13C201.69,550.32,202.46,539,202.92,528Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M181.16,389.57s-17.51,44.45-13.59,54.9,12,12.68,12,12.68-10.72,15.3-3.23,29.65,24.08,13.67,25.29,29.94C201.58,516.74,202.51,448.71,181.16,389.57Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M199.68,488.64a.41.41,0,0,0,0-.22,407.41,407.41,0,0,0-17-89.43c0-.09-.17-.06-.15,0q5.21,19.5,9.2,39.21a19.42,19.42,0,0,0-8.35-4.66c-.06,0-.1.07,0,.1A22.13,22.13,0,0,1,192,439.4c.5,2.58,1,5.16,1.49,7.73-3.92-4.37-9.34-6.53-14.78-8.61-.08,0-.11.1,0,.13,5.66,2.29,11.29,5.27,15.12,10.14q2,11.07,3.51,22.2a21.27,21.27,0,0,0-10.43-6c-.12,0-.2.16-.08.2a22,22,0,0,1,10.6,6.78h.07c.35,2.61.67,5.23,1,7.85a26.69,26.69,0,0,0-7.32-4.42c-.07,0-.11.08,0,.12a25,25,0,0,1,7.33,4.81h.1c.28,2.48.59,5,.84,7.48a35.66,35.66,0,0,0-14.86-10.61.09.09,0,0,0-.13,0,.1.1,0,0,0,0,.14l0,0a38.74,38.74,0,0,1,14.64,11.27.34.34,0,0,0,.37.12c.38,3.82.73,7.66,1,11.5h.09C200.27,496.51,200,492.57,199.68,488.64Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M185.61,429.16a22,22,0,0,0-10.53-3.28c-.08,0-.09.12,0,.14a41.6,41.6,0,0,1,10.41,3.39C185.63,429.48,185.74,429.25,185.61,429.16Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M187.72,476.23a21.9,21.9,0,0,0-7.89-2.71c-.1,0-.1.14,0,.16a29.59,29.59,0,0,1,7.81,2.73C187.74,476.47,187.83,476.29,187.72,476.23Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M249,445.19a119.31,119.31,0,0,0-20.28,17.73A73.22,73.22,0,0,0,214.46,487a180.41,180.41,0,0,0-10.58,57.67A139.84,139.84,0,0,0,207,577.17a.32.32,0,0,0,.41.17.31.31,0,0,0,.19-.29,182,182,0,0,1,.33-56.68c3-18.4,7.73-38.4,19.42-53.35,6.36-8.14,14.3-14.54,22.15-21.15A.44.44,0,0,0,249,445.19Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M206.46,531.42a108.83,108.83,0,0,1,14.77-18.89c8.76-8.75,16.14-15,17.28-20.77s-8.78-10.06-8.78-10.06,12.12,2.18,15.45-2.92,4.92-34.55,4.92-34.55S228,460,217.76,482.06,206.46,531.42,206.46,531.42Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M242.47,451.8c-9.29,9.12-18.63,18.83-24.08,30.81a117.93,117.93,0,0,0-4.82,12.7h0c-.29.87-.56,1.74-.83,2.62a.33.33,0,0,0,0,.14,251.27,251.27,0,0,0-6.27,27.22c0,.06.08.1.1,0,1-4.6,2-9.24,3.16-13.87h.13a19.43,19.43,0,0,1,5.8-1.48c.13,0,.12-.24,0-.23a21.61,21.61,0,0,0-5.74,1.05c1.08-4.18,2.24-8.34,3.54-12.45a62.46,62.46,0,0,1,13-1.67v-.06a48.76,48.76,0,0,0-12.86,1.08c.23-.7.46-1.4.7-2.11a66,66,0,0,1,8.71-1.46m0-.1a42.23,42.23,0,0,0-8.53,1c1.21-3.55,2.51-7,3.94-10.46a65.08,65.08,0,0,1,4.17-8.09,52.57,52.57,0,0,1,7.06-1.2v-.12a19,19,0,0,0-6.59.65c1.2-1.87,2.4-3.71,3.68-5.48h0c4.95-1.1,10-2.12,15-1.2a.08.08,0,0,0,0-.16c-4.81-1.26-9.63-.53-14.44.42.52-.7,1.06-1.38,1.6-2.07a33.2,33.2,0,0,1,9.23-1.46c.07,0,.09-.09,0-.09a24.13,24.13,0,0,0-8.62.76c4.1-5.08,8.61-9.85,13.08-14.57.25-.23.09-.36-.12-.15Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M242.21,471.9a19.46,19.46,0,0,0-4.38-.16c-.18,0-.17.31,0,.31,1.46,0,2.9-.06,4.36,0C242.31,472.08,242.32,471.91,242.21,471.9Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M226.33,500.49a19.79,19.79,0,0,0-6.79,1.36v.1c2.26-.54,4.53-.76,6.79-1.13a.17.17,0,0,0,.12-.21A.16.16,0,0,0,226.33,500.49Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M221.63,490.61a14.63,14.63,0,0,0-2.88.26c-.1,0-.1.17,0,.17a14.8,14.8,0,0,0,2.9-.13.16.16,0,0,0,.09-.2A.14.14,0,0,0,221.63,490.61Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M141.79,476.47a101.48,101.48,0,0,1,20.47,9.63,61.1,61.1,0,0,1,17.07,16.23,151.32,151.32,0,0,1,21.9,44.21,117.87,117.87,0,0,1,5,27,.26.26,0,0,1-.31.2.26.26,0,0,1-.2-.2,153,153,0,0,0-13.37-45.75c-6.68-14.18-15.13-29.29-28-38.68-7-5.12-14.94-8.43-22.81-12C141.08,476.92,141.39,476.33,141.79,476.47Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M196.13,536.45a91.58,91.58,0,0,0-16.33-11.87c-9.11-5.07-16.53-8.43-18.79-12.82s4.81-10.17,4.81-10.17-9.3,4.56-13.17,1.2-12-26.84-12-26.84,21.49,7.65,34.91,23.14S196.13,536.45,196.13,536.45Z",
    fill: "#9f26d3",
    "data-primary": "true"
  }), react_1["default"].createElement("path", {
    d: "M195,531.61c-10.75-20.18-25.09-38.43-44.75-50.48h0a99.56,99.56,0,0,1,14.37,11.26,22.63,22.63,0,0,0-11.54,2.41c-.09,0,0,.14,0,.12a31.52,31.52,0,0,1,11.89-2h.13c.7.66,1.39,1.33,2.08,2a35.71,35.71,0,0,0-6.18.82c-.06,0,0,.09,0,.08a32.44,32.44,0,0,1,6.52-.54,136.79,136.79,0,0,1,10.14,11.51,10.5,10.5,0,0,0-4.32.31.08.08,0,0,0-.08.08.08.08,0,0,0,.08.09,22.2,22.2,0,0,1,4.61-.06c.59.75,1.2,1.49,1.76,2.25a41.8,41.8,0,0,0-13,1.26m0,.1a52.81,52.81,0,0,1,13.4-.87c.54.73,1.07,1.48,1.6,2.22a27.79,27.79,0,0,0-7.76.89.08.08,0,0,0-.08.07.07.07,0,0,0,.07.07h0a30,30,0,0,1,8-.62q3.47,4.81,6.65,9.74a7.38,7.38,0,0,0-3.4.2m0,.1a14.2,14.2,0,0,1,3.68.18q2.87,4.47,5.54,9c.31.27.72.08.59-.19Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M178.29,514.88a16.52,16.52,0,0,0-6.25.76v.09a47.87,47.87,0,0,1,6.21-.64C178.41,515.08,178.43,514.89,178.29,514.88Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("path", {
    d: "M159.54,490.28a11.11,11.11,0,0,0-2.93.21.08.08,0,0,0,0,.16c1-.09,1.9-.06,2.83-.08a.16.16,0,0,0,.2-.1A.16.16,0,0,0,159.54,490.28Z",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "223.84 603.16 175.34 603.16 182.25 552.5 184.02 539.51 215.16 539.51 216.93 552.5 223.84 603.16",
    fill: "#5d296b",
    "data-secondary": "true"
  }), react_1["default"].createElement("polygon", {
    points: "216.93 552.5 182.25 552.5 184.02 539.51 215.16 539.51 216.93 552.5",
    opacity: "0.2"
  }), react_1["default"].createElement("rect", {
    x: "179.56",
    y: "535",
    width: "40.04",
    height: "11",
    fill: "#5d296b",
    "data-secondary": "true"
  }));
};

exports["default"] = UpgradeIllustration;

/***/ }),

/***/ "./resources/js/app.tsx":
/*!******************************!*\
  !*** ./resources/js/app.tsx ***!
  \******************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __createBinding = this && this.__createBinding || (Object.create ? function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  Object.defineProperty(o, k2, {
    enumerable: true,
    get: function get() {
      return m[k];
    }
  });
} : function (o, m, k, k2) {
  if (k2 === undefined) k2 = k;
  o[k2] = m[k];
});

var __setModuleDefault = this && this.__setModuleDefault || (Object.create ? function (o, v) {
  Object.defineProperty(o, "default", {
    enumerable: true,
    value: v
  });
} : function (o, v) {
  o["default"] = v;
});

var __importStar = this && this.__importStar || function (mod) {
  if (mod && mod.__esModule) return mod;
  var result = {};
  if (mod != null) for (var k in mod) {
    if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
  }

  __setModuleDefault(result, mod);

  return result;
};

var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));

var react_1 = __importDefault(__webpack_require__(/*! react */ "./node_modules/react/index.js"));

var react_dom_1 = __webpack_require__(/*! react-dom */ "./node_modules/react-dom/index.js");

var inertia_react_1 = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");

var progress_1 = __webpack_require__(/*! @inertiajs/progress */ "./node_modules/@inertiajs/progress/dist/index.js");

var LoadingScreen_1 = __importDefault(__webpack_require__(/*! @/Shared/Components/LoadingScreen */ "./resources/js/Shared/Components/LoadingScreen.tsx"));

var app = document.getElementById('app');
(0, react_dom_1.render)(react_1["default"].createElement(inertia_react_1.InertiaApp, {
  initialPage: app ? JSON.parse(app.dataset.page) : '{}',
  resolveComponent: function resolveComponent(name) {
    return Promise.resolve().then(function () {
      return __importStar(__webpack_require__("./resources/js/Pages sync recursive ^\\.\\/.*$")("./".concat(name)));
    }).then(function (module) {
      return module["default"];
    });
  },
  initialComponent: LoadingScreen_1["default"]
}), app);
progress_1.InertiaProgress.init({
  color: '#FBBF24'
});

/***/ }),

/***/ "./resources/js/i18n/i18n.ts":
/*!***********************************!*\
  !*** ./resources/js/i18n/i18n.ts ***!
  \***********************************/
/***/ (function(__unused_webpack_module, exports, __webpack_require__) {

"use strict";


var __importDefault = this && this.__importDefault || function (mod) {
  return mod && mod.__esModule ? mod : {
    "default": mod
  };
};

Object.defineProperty(exports, "__esModule", ({
  value: true
}));
exports.availableLanguages = void 0;

var i18next_1 = __importDefault(__webpack_require__(/*! i18next */ "./node_modules/i18next/dist/cjs/i18next.js"));

var react_i18next_1 = __webpack_require__(/*! react-i18next */ "./node_modules/react-i18next/dist/es/index.js");

var i18next_browser_languagedetector_1 = __importDefault(__webpack_require__(/*! i18next-browser-languagedetector */ "./node_modules/i18next-browser-languagedetector/dist/esm/i18nextBrowserLanguageDetector.js"));

var en_json_1 = __importDefault(__webpack_require__(/*! @/translations/en.json */ "./resources/js/translations/en.json"));

var sv_json_1 = __importDefault(__webpack_require__(/*! @/translations/sv.json */ "./resources/js/translations/sv.json"));

var resources = {
  en: en_json_1["default"],
  sv: sv_json_1["default"]
};
exports.availableLanguages = {
  en: {
    nativeName: 'English'
  },
  sv: {
    nativeName: 'Svenska'
  }
};
i18next_1["default"].use(react_i18next_1.initReactI18next).use(i18next_browser_languagedetector_1["default"]).init({
  resources: resources,
  defaultNS: 'common',
  fallbackLng: 'en'
});

/***/ }),

/***/ "./resources/css/app.css":
/*!*******************************!*\
  !*** ./resources/css/app.css ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./resources/js/Pages sync recursive ^\\.\\/.*$":
/*!*******************************************!*\
  !*** ./resources/js/Pages/ sync ^\.\/.*$ ***!
  \*******************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./Auth/ForgotPassword": "./resources/js/Pages/Auth/ForgotPassword.tsx",
	"./Auth/ForgotPassword.tsx": "./resources/js/Pages/Auth/ForgotPassword.tsx",
	"./Auth/Login": "./resources/js/Pages/Auth/Login.tsx",
	"./Auth/Login.tsx": "./resources/js/Pages/Auth/Login.tsx",
	"./Auth/Register": "./resources/js/Pages/Auth/Register.tsx",
	"./Auth/Register.tsx": "./resources/js/Pages/Auth/Register.tsx",
	"./Auth/TwoFactorChallenge": "./resources/js/Pages/Auth/TwoFactorChallenge.tsx",
	"./Auth/TwoFactorChallenge.tsx": "./resources/js/Pages/Auth/TwoFactorChallenge.tsx",
	"./Home/Index": "./resources/js/Pages/Home/Index.tsx",
	"./Home/Index.tsx": "./resources/js/Pages/Home/Index.tsx",
	"./PrivacyPolicy": "./resources/js/Pages/PrivacyPolicy.tsx",
	"./PrivacyPolicy.tsx": "./resources/js/Pages/PrivacyPolicy.tsx",
	"./Profile/Show": "./resources/js/Pages/Profile/Show.tsx",
	"./Profile/Show.tsx": "./resources/js/Pages/Profile/Show.tsx",
	"./ResumeManager/Index": "./resources/js/Pages/ResumeManager/Index.tsx",
	"./ResumeManager/Index.tsx": "./resources/js/Pages/ResumeManager/Index.tsx",
	"./ResumeManager/WorkExperiences/WorkExperience": "./resources/js/Pages/ResumeManager/WorkExperiences/WorkExperience.tsx",
	"./ResumeManager/WorkExperiences/WorkExperience.tsx": "./resources/js/Pages/ResumeManager/WorkExperiences/WorkExperience.tsx",
	"./Setting/Components/ChangePasswordForm": "./resources/js/Pages/Setting/Components/ChangePasswordForm.tsx",
	"./Setting/Components/ChangePasswordForm.tsx": "./resources/js/Pages/Setting/Components/ChangePasswordForm.tsx",
	"./Setting/Components/DeleteUserForm": "./resources/js/Pages/Setting/Components/DeleteUserForm.tsx",
	"./Setting/Components/DeleteUserForm.tsx": "./resources/js/Pages/Setting/Components/DeleteUserForm.tsx",
	"./Setting/Components/LogoutOtherBrowserSessions": "./resources/js/Pages/Setting/Components/LogoutOtherBrowserSessions.tsx",
	"./Setting/Components/LogoutOtherBrowserSessions.tsx": "./resources/js/Pages/Setting/Components/LogoutOtherBrowserSessions.tsx",
	"./Setting/Components/ProfilePersonalInfo": "./resources/js/Pages/Setting/Components/ProfilePersonalInfo.tsx",
	"./Setting/Components/ProfilePersonalInfo.tsx": "./resources/js/Pages/Setting/Components/ProfilePersonalInfo.tsx",
	"./Setting/Components/TwoFactorAuthentication": "./resources/js/Pages/Setting/Components/TwoFactorAuthentication.tsx",
	"./Setting/Components/TwoFactorAuthentication.tsx": "./resources/js/Pages/Setting/Components/TwoFactorAuthentication.tsx",
	"./Setting/Components/TwoFactorAuthenticationConfirmation": "./resources/js/Pages/Setting/Components/TwoFactorAuthenticationConfirmation.tsx",
	"./Setting/Components/TwoFactorAuthenticationConfirmation.tsx": "./resources/js/Pages/Setting/Components/TwoFactorAuthenticationConfirmation.tsx",
	"./Setting/Show": "./resources/js/Pages/Setting/Show.tsx",
	"./Setting/Show.tsx": "./resources/js/Pages/Setting/Show.tsx",
	"./TermsOfService": "./resources/js/Pages/TermsOfService.tsx",
	"./TermsOfService.tsx": "./resources/js/Pages/TermsOfService.tsx",
	"./Welcome": "./resources/js/Pages/Welcome.tsx",
	"./Welcome.tsx": "./resources/js/Pages/Welcome.tsx"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/js/Pages sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "?2128":
/*!********************************!*\
  !*** ./util.inspect (ignored) ***!
  \********************************/
/***/ (() => {

/* (ignored) */

/***/ }),

/***/ "./resources/js/translations/en.json":
/*!*******************************************!*\
  !*** ./resources/js/translations/en.json ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"common":{"settings":{"Personal.Information.Title":"Personal Information","Personal.Information.Description":"Update your account\'s profile information and email address.","Update.Password.Title":"Update Password","Update.Password.Description":"Ensure your account is using a long, random password to stay secure.","Current.Password":"Current Password","New.Password":"New Password","Confirm.Password":"Confirm Password","Two.Factor.Authentication.Title":"Two Factor Authentication","Two.Factor.Authentication.Description":"Add additional security to your account using two factor authentication.","Two.Factor.Authentication.Status.Message":"You have not enabled two factor authentication.","Two.Factor.Authentication.Instruction":"When two factor authentication is enabled, you will be prompted for a secure, random token during authentication. You may retrieve this token from your phone\'s Authenticator application.","Enable":"Enable","Browser.Sessions.Title":"Browser Sessions","Browser.Sessions.Description":"Manage and log out your active sessions on other browsers and devices.","Browser.Sessions.Instruction":"If necessary, you may log out of all of your other browser sessions across all of your devices. Some of your recent sessions are listed below; however, this list may not be exhaustive. If you feel your account has been compromised, you should also update your password.","Browser.Sessions.Button.Label":"Log out other browser sessions","Delete.Account.Title":"Delete Account","Delete.Account.Description":"Permanently delete your account.","Delete.Account.Instruction":"Once your account is deleted, all of it\'s resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.","Setttings":"Setttings"},"general":{"Save":"Save","ID":"ID","Text":"Text","Priority":"Priority","Style":"Style","Description":"Description","Create":"Create","Simulate":"Simulate","Edit":"Edit","Delete":"Delete","Search":"Search","Logout":"Logout"},"createAndEditForm":{"BallText":{"Title":{"Add.New.Ball.Text":"New Ball Text"},"Description":{"Add.New.Ball.Text.Description":"Get started by filling in the information below to create a new Ball Text."}}},"login":{"Headline":{"Sign.Into.Account.Label":"Sign in to your account"},"Form":{"E-mail":"E-mail","Password":"Password","Remember.Me":"Remember me","Forgot.Password":"Forgot your password","Log.In":"Log in"}},"errors":{"Whoops!!":"Whoops!!","Non.matching.Credentials":"These credentials do not match our records."},"modal":{"Delete.Action":"Delete Action","Delete.Action.Confirmation.Message":"Are you sure you want to delete this data? Please note that this data will be permanently removed. This action cannot be undone."},"pagination":{"Showing":"Showing","To":"to","Of":"of"},"page":{"title":{"Home":"Home"}},"register":{"Create.Account.Button":"Create An Account","Full.Name.Label":"Full Name","Password.Confirmation.Label":"Password Confirmation","Terms.And.Privacy.Policy":"I agree to the <2>Terms of Service</2> and <2>Privacy Policy</2>","Already.Registered.Link":"Already registered?"},"welcome":{"First.Headline":"Searching for a job should not be boring","Second.Headline":"Kick start your career in a fun and easy way.","Marketing.Pitch":"A career search done right and made for this century. Let us help you get started on your career journey. Whether you want a career change or looking for a job, we are here to make it happen.","First.Card.Title":"Create Professional Resume","First.Card.Description":"An intuitive application layout that allows you to create a Personalize and professional resume.","Second.Card.Title":"Browse Jobs","Second.Card.Description":"A place to explore different jobs in different fields. If you want to change career or just explore other opportunities.","Third.Card.Title":"Create Personalize Letter","Third.Card.Description":"Write your professional story in a fun way. Express and show your skills and past experiences in a fluent manner.","Fourth.Card.Title":"Job Search Advice & Tips","Fourth.Card.Description":"Explore the different types of resume, and when to use them. Get tips on what to add and what to avoid in a resume.","Fifth.Card.Title":"Job Ad Notification & Reminders","Fifth.Card.Description":"Don\'t let an opportunity pass you by. Get relevant job ad notifications. Customise the frequency of the notifications.","Sixth.Card.Title":"Tailor Made Job Search","Sixth.Card.Description":"A list of selected jobs that matches your profile and personality. This helps narrow your search to what is important."}}}');

/***/ }),

/***/ "./resources/js/translations/sv.json":
/*!*******************************************!*\
  !*** ./resources/js/translations/sv.json ***!
  \*******************************************/
/***/ ((module) => {

"use strict";
module.exports = JSON.parse('{"common":{"settings":{"Personal.Information.Title":"Personlig information","Personal.Information.Description":"Uppdatera ditt kontos profilinformation och e-postadress.","Update.Password.Title":"Uppdatera lösenord","Update.Password.Description":"Försäkra dig om att ditt konto använder sig av ett långt och slumpmässigt utvalt lösenord för att förbli säkert.","Current.Password":"Nuvarande lösenord","New.Password":"Nytt lösenord","Confirm.Password":"Bekräfta lösenord","Two.Factor.Authentication.Title":"Tvåfaktorsautenticering","Two.Factor.Authentication.Description":"För ökad säkerhet, använd tvåfaktorsautenticering.","Two.Factor.Authentication.Status.Message":"Du har inte aktiverat tvåfaktorsautenticering.","Two.Factor.Authentication.Instruction":"När tvåfaktorsautenticering är aktiverad använder du ditt lösenord tillsammans med en säker slumpmässigt utvald kod vid varje inloggning. Denna kod tillhandahålls av din Authenticator app i din telefon","Enable":"Aktivera","Browser.Sessions.Title":"Browseraktiviteter","Browser.Sessions.Description":"Hantera och logga ut all aktivitet på andra browsers och enheter.","Browser.Sessions.Instruction":"Du kan, om nödvändigt logga ut från alla dina andra enheter. Några av dina tidigare aktiviteter är listade här nedan. Denna lista kan dock vara ofullständig. Om du tror att ditt konto har blivit äventyrat på något sätt är det viktigt att du även uppdaterar ditt lösenord.","Browser.Sessions.Button.Label":"Logga ut från andra enheter och browsers.","Delete.Account.Title":"Radera konto","Delete.Account.Description":"Radera ditt konto permanent.","Delete.Account.Instruction":"När ditt konto är raderat kommer all information och data vara raderat permanent. Var snäll och ladda ner all data och information du vill behålla innan du raderar ditt konto.","Setttings":"Inställningar"},"general":{"Save":"Spara","ID":"ID","Text":"Text","Priority":"Prioritet","Style":"Stil","Description":"Beskrivning","Create":"Skapa","Simulate":"Simulera","Edit":"Redigera","Delete":"Radera","Search":"Sök","Logout":"Logga ut"},"createAndEditForm":{"BallText":{"Title":{"Add.New.Ball.Text":"Ny Ball Text"},"Description":{"Add.New.Ball.Text.Description":"Börja med att fylla i nedanstående information för att skapa en ny Ball Text"}}},"login":{"Headline":{"Sign.Into.Account.Label":"Logga in på ditt konto"},"Form":{"E-mail":"E-postadress","Password":"Lösenord","Remember.Me":"Kom ihåg mig","Forgot.Password":"Glömt ditt lösenord?","Log.In":"Logga in"}},"errors":{"Whoops!!":"Nämen OJ!","Non.matching.Credentials":"Den angivna e-postadressen eller lösenordet finns tyvärr inte registrerat hos oss"},"modal":{"Delete.Action":"Radera händelse","Delete.Action.Confirmation.Message":"Är du säker på att du vill radera detta innehåll? Kom ihåg att all data och information blir raderad permanent och kan inte återskapas. Denna handling kan inte ångras"},"pagination":{"Showing":"Visar","To":"till","Of":"av"},"page":{"title":{"Home":"Hem"}},"register":{"Create.Account.Button":"Skapa konto","Full.Name.Label":"Fullständigt namn","Password.Confirmation.Label":"Bekräfta lösenord","Terms.And.Privacy.Policy":"Jag accpterar <2>Användarvilkoren</2> och <2>Integritetspolicyn</2>.","Already.Registered.Link":"Redan registrerad?"},"welcome":{"First.Headline":"Att leta efter ett jobb ska inte vara tråkigt","Second.Headline":"Kickstarta din karriär på ett roligt och lätt sätt.","Marketing.Pitch":"En webbapplikation för din karriär. Gjord rätt och gjord för vårt århundrade. Låt oss hjälpa dig att starta din karriärresa. Oavsett om du vill byta karriär eller söker jobb inom ditt eget yrke så är vi här för att hjälpa dig nå dina mål.","First.Card.Title":"Skapa ett professionellt CV","First.Card.Description":"En tydlig och lättförstålig layout som hjälper dig att skapa ett personligt och proffsigt CV.","Second.Card.Title":"Sök efter jobb","Second.Card.Description":"En plats att utforska olika jobb inom olika yrkesgrupper. För dig som vill byta karriär eller bara utforska vilka möjlighter som finns.","Third.Card.Title":"Skapa personligt brev","Third.Card.Description":"Beskriv din arbets- och studie-historik på ett roligt sätt. Beskriv och visa dina tidigare erfatenheter i en sammanhängande text.","Fourth.Card.Title":"Jobbsökartips & råd","Fourth.Card.Description":"Utforska olika typer av CV:n och se när de passar bäst. Få tips om vad du ska ta med och vad du bör undvika i ditt CV.","Fifth.Card.Title":"Notiser & påminnelser för platsannonser","Fifth.Card.Description":"Missa inga möjlighter. Få notiser för relevanta jobb. Anpassa frekvensen för dina notiser.","Sixth.Card.Title":"Skräddarsytt jobbsökande","Sixth.Card.Description":"En lista med utvalda jobb som matchar din profil och personlighet. Detta hjälper till att förfina din sökning till det som är viktigt."}}}');

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["css/app","/js/vendor"], () => (__webpack_exec__("./resources/js/app.tsx"), __webpack_exec__("./resources/css/app.css")));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
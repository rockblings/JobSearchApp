"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Layout_FourColumnTableWithActions_jsx"],{

/***/ "./resources/js/Pages/Shared/Layout/FourColumnTableWithActions.jsx":
/*!*************************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/FourColumnTableWithActions.jsx ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/General/Pagination'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");





var FourColumnTableWithActions = function FourColumnTableWithActions(_ref) {
  var tableHeaders = _ref.tableHeaders,
      data = _ref.data,
      editItem = _ref.editItem,
      editUrl = _ref.editUrl,
      deleteUrl = _ref.deleteUrl;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("table", {
      className: "min-w-full divide-y divide-gray-200",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("thead", {
        className: "bg-violet-50 shadow-sm",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("tr", {
          children: [tableHeaders.map(function (column, colIdx) {
            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("th", {
              scope: "col",
              className: "px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider",
              children: column.header
            }, colIdx);
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("th", {
            scope: "col",
            className: "relative px-6 py-3",
            children: [editUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Edit"
            }), deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Delete"
            })]
          })]
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("tbody", {
        children: data.map(function (item, itemIdx) {
          return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("tr", {
            className: itemIdx % 2 === 0 ? 'bg-white' : 'bg-gray-50',
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "align-text-top px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900",
              children: item.firstColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "align-text-top px-6 py-4 text-sm text-gray-500",
              children: item.secondColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "align-text-top px-6 py-4 whitespace-nowrap text-sm text-gray-500",
              children: item.thirdColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "align-text-top px-6 py-4 whitespace-nowrap text-sm text-gray-500",
              children: item.fourthColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("td", {
              className: "align-text-top px-6 py-4 whitespace-nowrap text-right text-sm font-medium space-x-4",
              children: [editUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
                href: "#",
                className: "text-violet-600 hover:text-violet-900",
                onClick: function onClick() {
                  return editItem(item);
                },
                children: "Edit"
              }), editUrl && deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
                className: "text-gray-300",
                "aria-hidden": "true",
                children: "|"
              }), deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
                href: "#",
                className: "text-violet-600 hover:text-violet-900",
                children: "Delete"
              })]
            })]
          }, item.firstColumn);
        })
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/General/Pagination'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {})]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FourColumnTableWithActions);

/***/ })

}]);
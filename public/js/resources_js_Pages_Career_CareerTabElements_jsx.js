"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Career_CareerTabElements_jsx"],{

/***/ "./resources/js/Pages/Career/CareerTabElements.jsx":
/*!*********************************************************!*\
  !*** ./resources/js/Pages/Career/CareerTabElements.jsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var CareerTabElements = [{
  name: 'Work Pattern',
  href: 'balltext.index',
  current: true
}, {
  name: 'Point Text',
  href: 'balltext.index',
  current: false
}, {
  name: 'Work Style',
  href: 'balltext.index',
  current: false
}, {
  name: 'Reference Guide',
  href: 'balltext.index',
  current: false
}, {
  name: 'Interview Guide',
  href: 'balltext.index',
  current: false
}, {
  name: 'Control Point',
  href: 'balltext.index',
  current: false
}, {
  name: 'Interview Questions',
  href: 'balltext.index',
  current: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CareerTabElements);

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Pricing_Index_jsx"],{

/***/ "./resources/js/Pages/Pricing/Index.jsx":
/*!**********************************************!*\
  !*** ./resources/js/Pages/Pricing/Index.jsx ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _Pages_Shared_GuestHeader__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/GuestHeader */ "./resources/js/Pages/Shared/GuestHeader.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");







var tiers = [{
  id: 1,
  name: 'Talent',
  href: '/pricing/checkout/1',
  priceMonthly: 5000,
  description: 'An in-depth precision test for recruitment and development.',
  includedFeatures: ['JobMatch Chart', 'Key report', 'Personal work style', 'Summary report', 'Reference Guide', 'Interview Guide', 'Sales and customer service profile', 'Motivation profile', 'Stress profile', 'Leadership profile', 'Team report and quick match', 'Evaluation Guide']
}, {
  id: 2,
  name: 'Career',
  href: '/pricing/checkout/2',
  priceMonthly: 3500,
  description: 'The most advanced career guide on the market to date.',
  includedFeatures: ['Interactive career search tool', 'Candidate summary report', 'Personal career tips', 'Talent and challenges', 'Personal presentation']
}, {
  id: 3,
  name: 'Logiq',
  href: '/pricing/checkout/3',
  priceMonthly: 2000,
  description: 'As a complement to JobMatch Talent to verify the candidate’s logical ability in different ways',
  includedFeatures: ['Logic result']
}, {
  id: 4,
  name: 'JM Five',
  href: '/pricing/checkout/4',
  priceMonthly: 1500,
  description: 'A stream lined test based on the five principles.',
  includedFeatures: ['Five factor diagram', 'Personal work style', 'Reference Guide', 'Interview Guide', 'Motivation profile', 'Stress profile', 'Leadership profile', 'Evaluation Guide']
}];

var Index = function Index() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(react_helmet__WEBPACK_IMPORTED_MODULE_1__.Helmet, {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("title", {
        children: "JobMatch - Pricing"
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_Pages_Shared_GuestHeader__WEBPACK_IMPORTED_MODULE_2__["default"], {
      currentNav: "Pricing"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
      className: "bg-white",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
        className: "max-w-7xl mx-auto py-24 px-4 sm:px-6 lg:px-8",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
          className: "sm:flex sm:flex-col sm:align-center",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("h1", {
            className: "text-5xl font-extrabold text-gray-900 sm:text-center",
            children: "Pricing Plans"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("p", {
            className: "mt-5 text-xl text-gray-500 sm:text-center",
            children: "Please choose a product and a payment plan."
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
            className: "relative self-center mt-6 bg-gray-100 rounded-lg p-0.5 flex sm:mt-8",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("button", {
              type: "button",
              className: "relative w-1/2 bg-white border-gray-200 rounded-md shadow-sm py-2 text-sm font-medium text-gray-900 whitespace-nowrap focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:z-10 sm:w-auto sm:px-8",
              children: "One time payment"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("button", {
              type: "button",
              className: "ml-0.5 relative w-1/2 border border-transparent rounded-md py-2 text-sm font-medium text-gray-700 whitespace-nowrap focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:z-10 sm:w-auto sm:px-8",
              children: "Subscription"
            })]
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
          className: "mt-12 space-y-4 sm:mt-16 sm:space-y-0 sm:grid sm:grid-cols-2 sm:gap-6 lg:max-w-4xl lg:mx-auto xl:max-w-none xl:mx-0 xl:grid-cols-4",
          children: tiers.map(function (tier) {
            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
              className: "border border-gray-200 rounded-lg shadow-sm divide-y divide-gray-200",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
                className: "p-6",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("h2", {
                  className: "text-lg leading-6 font-medium text-gray-900",
                  children: tier.name
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("p", {
                  className: "mt-4 text-sm text-gray-500",
                  children: tier.description
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("p", {
                  className: "mt-8",
                  children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("span", {
                    className: "text-4xl font-extrabold text-gray-900",
                    children: [tier.priceMonthly, "kr"]
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__.Link, {
                  as: "button",
                  href: tier.href,
                  className: "mt-8 block w-full bg-gray-800 border border-gray-800 rounded-md py-2 text-sm font-semibold text-white text-center hover:bg-gray-900",
                  children: ["Buy ", tier.name]
                })]
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
                className: "pt-6 pb-8 px-6",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("h3", {
                  className: "text-xs font-medium text-gray-900 tracking-wide uppercase",
                  children: "What's included"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("ul", {
                  role: "list",
                  className: "mt-6 space-y-4",
                  children: tier.includedFeatures.map(function (feature) {
                    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("li", {
                      className: "flex space-x-3",
                      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.CheckIcon, {
                        className: "flex-shrink-0 h-5 w-5 text-green-500",
                        "aria-hidden": "true"
                      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                        className: "text-sm text-gray-500",
                        children: feature
                      })]
                    }, feature);
                  })
                })]
              })]
            }, tier.name);
          })
        })]
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Index);

/***/ }),

/***/ "./resources/js/Pages/Shared/ApplicationLogo.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Pages/Shared/ApplicationLogo.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var ApplicationLogo = function ApplicationLogo() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("svg", {
    fill: "#ED8B00",
    width: "149",
    height: "24",
    id: "jm-logo",
    viewBox: "0 0 148.9 24",
    xmlns: "http://www.w3.org/2000/svg",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("g", {
      className: "logo-jobmatch",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("g", {
        className: "word-jobmatch",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M10.411 16.656c0 4.502-2.638 6.612-7.069 6.612-.991 0-1.912-.113-2.746-.286-.351-.072-.596-.389-.596-.746v-2.608c0-.446.384-.797.828-.763.582.044 1.08.077 1.494.077 2.251 0 3.341-.703 3.341-2.778v-14.359c0-.422.343-.765.766-.765h3.216c.423 0 .766.343.766.765v14.851z",
          className: "jobmatch-j"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M16.338 14.139c0 2.744 1.864 4.713 4.502 4.713 2.567 0 4.502-2.004 4.502-4.713 0-2.743-1.935-4.783-4.502-4.783-2.638 0-4.502 2.04-4.502 4.783m13.855.153c-.104 5.246-4.564 9.175-9.901 8.909-4.88-.245-8.853-4.371-8.772-9.256.087-5.224 4.442-9.168 9.832-8.903 4.909.243 8.94 4.336 8.841 9.25",
          className: "jobmatch-o"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M40.389 9.341c2.462 0 4.502 1.829 4.502 4.783 0 2.954-2.005 4.819-4.502 4.819-2.181 0-4.396-1.443-4.396-4.784 0-3.306 2.18-4.818 4.396-4.818zm-5.479 13.506c.423 0 .766-.343.766-.766v-1.415c1.196 1.654 3.201 2.603 5.592 2.603 4.924 0 8.441-3.869 8.441-9.11 0-5.24-3.517-9.109-8.441-9.109-2.18 0-4.009.738-5.205 2.075v-5.319c0-.423-.343-.766-.766-.766h-3.217c-.422 0-.765.343-.765.766v20.275c0 .423.343.766.765.766h2.83z",
          className: "jobmatch-b"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("g", {
          className: "jobmatch-m",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
            d: "M64.165 7.546c-1.055-1.583-2.814-2.497-5.171-2.497-2.075 0-3.763.738-4.923 1.935v-.783c0-.423-.343-.765-.766-.765h-2.83c-.297 0-.57.224-.697.47 1.827 2.121 2.797 4.945 2.797 8.254 0 3.303-.954 6.152-2.773 8.272.131.235.385.414.673.414h3.217c.422 0 .765-.343.765-.766v-9.364c0-2.075 1.302-3.376 3.201-3.376 1.864 0 3.095 1.301 3.095 3.376v9.364c0 .423.343.766.766.766h3.217c.422 0 .765-.343.765-.766v-9.469c0-1.935 1.301-3.271 3.201-3.271 1.864 0 3.095 1.301 3.095 3.376v9.364c0 .423.342.766.765.766h3.217c.423 0 .766-.343.766-.766v-10.032c0-4.186-2.427-6.999-6.577-6.999-2.427 0-4.502.984-5.803 2.497z",
            className: "jobmatch-m-letter"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
            d: "M49.802 5.856c-.055.104-.093.22-.093.346v2.07c.976 1.653 1.532 3.656 1.532 5.887 0 2.229-.556 4.233-1.532 5.886v2.036c0 .134.045.255.106.366 1.819-2.12 2.895-4.986 2.895-8.288 0-3.31-1.081-6.182-2.908-8.303",
            className: "jobmatch-m-edge"
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M88.349 16.34v-.611h-3.729c-1.547 0-2.426.798-2.426 1.959 0 1.196 1.019 2.098 2.497 2.098 1.269 0 3.658-1.109 3.658-3.446m4.748-3.834v9.575c0 .423-.343.766-.766.766h-2.83c-.423 0-.765-.343-.765-.766v-.923c-1.232 1.302-2.463 2.111-5.312 2.111-3.798 0-6.013-2.451-6.013-5.406 0-3.165 2.567-5.324 6.577-5.324h4.361v-.314c0-1.9-1.091-3.06-3.236-3.06-1.618 0-2.811.662-4.04 1.824-.316.299-.822.266-1.102-.067l-1.498-1.776c-.246-.291-.248-.729.014-1.007 1.955-2.088 4.311-3.089 7.153-3.089 4.432 0 7.457 2.497 7.457 7.456",
          className: "jobmatch-a"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M106.476 19.7v2.537c0 .357-.245.673-.594.746-.815.172-1.69.285-2.677.285-4.291 0-6.964-2.075-6.964-6.542v-7.28h-1.978c-.423 0-.766-.343-.766-.766v-2.478c0-.423.343-.766.766-.766h1.978v-3.631c0-.422.342-.765.765-.765h3.217c.423 0 .766.343.766.765v3.631h4.159c.423 0 .766.343.766.766v2.478c0 .423-.343.766-.766.766h-4.159v6.858c0 2.005 1.055 2.709 3.236 2.709.383 0 .864-.033 1.421-.076.445-.034.83.317.83.763",
          className: "jobmatch-t"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M122.258 8.855c.237-.328.235-.788-.036-1.089-1.521-1.686-3.717-2.717-6.432-2.717-5.205 0-9.32 3.94-9.32 9.075 0 5.169 4.115 9.144 9.32 9.144 2.668 0 4.865-1.038 6.393-2.712.273-.299.278-.76.042-1.089l-1.507-2.102c-.322-.449-.982-.507-1.355-.1-.9.986-2.004 1.642-3.501 1.642-3.366 0-5.428-3.149-4.23-6.885.333-1.037 1.501-2.142 2.555-2.415 2.427-.63 4.008.087 5.185 1.418.374.422 1.054.358 1.384-.098l1.502-2.072z",
          className: "jobmatch-c"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("path", {
          d: "M139.385 12.049v10.032c0 .423-.343.766-.766.766h-3.181c-.423 0-.766-.343-.766-.766v-9.364c0-2.04-1.302-3.376-3.236-3.376-1.934 0-3.306 1.371-3.306 3.376v9.364c0 .423-.343.766-.765.766h-3.217c-.423 0-.766-.343-.766-.766v-20.275c0-.423.343-.766.766-.766h3.217c.422 0 .765.343.765.766v5.108c1.16-1.161 2.814-1.864 4.783-1.864 3.975 0 6.472 2.813 6.472 6.999",
          className: "jobmatch-h"
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("circle", {
        cx: "145.5",
        cy: "20.1",
        r: "3.4",
        className: "dot"
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ApplicationLogo);

/***/ }),

/***/ "./resources/js/Pages/Shared/GuestHeader.jsx":
/*!***************************************************!*\
  !*** ./resources/js/Pages/Shared/GuestHeader.jsx ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/popover/popover.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _ApplicationLogo__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ApplicationLogo */ "./resources/js/Pages/Shared/ApplicationLogo.jsx");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var solutions = [{
  name: 'Jobs Ad Notification',
  description: 'Get a better understanding of where your traffic is coming from.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.InboxIcon
}, {
  name: 'Job Search',
  description: 'Speak directly to your customers in a more meaningful way.',
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.AnnotationIcon
}, {
  name: 'CV Maker',
  description: "Your customers' data will be safe and secure.",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ChatAlt2Icon
}, {
  name: 'Jobs Application Management',
  description: "Connect with third-party tools that you're already using.",
  href: '#',
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.QuestionMarkCircleIcon
}];
var navigation = [{
  name: 'About Us',
  href: 'pricing.index',
  current: false
}];

function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
}

var GuestHeader = function GuestHeader(_ref) {
  var currentNav = _ref.currentNav;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
      component = _usePage.component;

  if (currentNav) {
    navigation.map(function (nav) {
      if (nav.name === currentNav) {
        nav.current = true;
      }
    });
  }

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("header", {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover, {
      className: "relative bg-violet-50",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
        className: "flex justify-between items-center mx-auto px-4 py-6 sm:px-6 md:justify-start md:space-x-10",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
          className: "flex justify-start lg:w-0 lg:flex-1",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("a", {
            href: "/",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
              className: "sr-only",
              children: "JobAdsist"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
              className: "text-fuchsia-800 text-4xl font-extrabold tracking-tight",
              children: "JobAdsist"
            })]
          })
        }), !component.startsWith('Auth') && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
            className: "-mr-2 -my-2 md:hidden",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Button, {
              className: "bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                className: "sr-only",
                children: "Open menu"
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.MenuIcon, {
                className: "h-6 w-6",
                "aria-hidden": "true"
              })]
            })
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Group, {
            as: "nav",
            className: "hidden md:flex space-x-10",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover, {
              className: "relative",
              children: function children(_ref2) {
                var open = _ref2.open;
                return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Button, {
                    className: classNames(open ? 'text-gray-900' : 'text-gray-500', 'group rounded-md inline-flex items-center text-base font-medium hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-yellow-600'),
                    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                      children: "Solutions"
                    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ChevronDownIcon, {
                      className: classNames(open ? 'text-gray-600' : 'text-gray-400', 'ml-2 h-5 w-5 group-hover:text-gray-500'),
                      "aria-hidden": "true"
                    })]
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition, {
                    as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                    enter: "transition ease-out duration-200",
                    enterFrom: "opacity-0 translate-y-1",
                    enterTo: "opacity-100 translate-y-0",
                    leave: "transition ease-in duration-150",
                    leaveFrom: "opacity-100 translate-y-0",
                    leaveTo: "opacity-0 translate-y-1",
                    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Panel, {
                      className: "absolute z-10 -ml-4 mt-3 transform w-screen max-w-md lg:max-w-2xl lg:ml-0 lg:left-1/2 lg:-translate-x-1/2",
                      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                        className: "rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 overflow-hidden",
                        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                          className: "relative grid gap-6 bg-white px-5 py-6 sm:gap-8 sm:p-8 lg:grid-cols-2",
                          children: solutions.map(function (item) {
                            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("a", {
                              href: item.href,
                              className: "-m-3 p-3 flex items-start rounded-lg hover:bg-gray-50",
                              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                                className: "flex-shrink-0 flex items-center justify-center h-10 w-10 rounded-md bg-indigo-600 text-white sm:h-12 sm:w-12",
                                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(item.icon, {
                                  className: "h-6 w-6",
                                  "aria-hidden": "true"
                                })
                              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
                                className: "ml-4",
                                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("p", {
                                  className: "text-base font-medium text-gray-900",
                                  children: item.name
                                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("p", {
                                  className: "mt-1 text-sm text-gray-500",
                                  children: item.description
                                })]
                              })]
                            }, item.name);
                          })
                        })
                      })
                    })
                  })]
                });
              }
            }), navigation.map(function (item) {
              return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link, {
                href: route(item.href),
                className: classNames(item.current ? 'text-yellow-600' : 'text-base font-medium text-gray-500 hover:text-gray-900'),
                children: item.name
              }, item.name);
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
            className: "hidden md:flex items-center justify-end md:flex-1 lg:w-0",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link, {
              href: route('login'),
              className: "ml-8 whitespace-nowrap inline-flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-fuchsia-700 hover:bg-fuchsia-400 shadow-lg shadow-fuchsia-500/50",
              children: "Log in"
            })
          })]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition, {
        as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
        enter: "duration-200 ease-out",
        enterFrom: "opacity-0 scale-95",
        enterTo: "opacity-100 scale-100",
        leave: "duration-100 ease-in",
        leaveFrom: "opacity-100 scale-100",
        leaveTo: "opacity-0 scale-95",
        children: !component.startsWith('Auth') && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Panel, {
          focus: true,
          className: "absolute z-30 top-0 inset-x-0 p-2 transition transform origin-top-right md:hidden",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
            className: "rounded-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
              className: "pt-5 pb-6 px-5",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
                className: "flex items-center justify-between",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {}), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                  className: "-mr-2",
                  children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Popover.Button, {
                    className: "bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-gray-500",
                    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                      className: "sr-only",
                      children: "Close menu"
                    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.XIcon, {
                      className: "h-6 w-6",
                      "aria-hidden": "true"
                    })]
                  })
                })]
              })
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
              className: "py-6 px-5",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                className: "mt-2",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link, {
                  href: route('login'),
                  className: "w-full flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-gray-600 hover:bg-gray-700",
                  children: "Log in"
                })
              })
            })]
          })
        })
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (GuestHeader);

/***/ })

}]);

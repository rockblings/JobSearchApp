"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Logiq_NormGroup_ResultText_Index_jsx"],
    {
        /***/ "./resources/js/Pages/Career/CareerMenu.jsx":
            /*!**************************************************!*\
  !*** ./resources/js/Pages/Career/CareerMenu.jsx ***!
  \**************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                var CareerMenu = [
                    {
                        name: "Text",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Rules",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Score Config",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Profiles",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Tags",
                        href: "#",
                        active: false,
                    },
                ];
                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    CareerMenu;

                /***/
            },

        /***/ "./resources/js/Pages/Logiq/LogiqMenu.jsx":
            /*!************************************************!*\
  !*** ./resources/js/Pages/Logiq/LogiqMenu.jsx ***!
  \************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                var LogiqMenu = [
                    {
                        name: "Norm Groups",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Score Config",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Test List",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Traits",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Questions",
                        href: "#",
                        active: false,
                    },
                ];
                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    LogiqMenu;

                /***/
            },

        /***/ "./resources/js/Pages/Logiq/NormGroup/ResultText/Index.jsx":
            /*!*****************************************************************!*\
  !*** ./resources/js/Pages/Logiq/NormGroup/Text/Index.jsx ***!
  \*****************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_Layout_Layout__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/Components */ "./resources/js/Pages/Shared/Layout/Layout.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_Dashboard__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/Dashboard */ "./resources/js/Pages/Shared/Layout/Dashboard.jsx"
                    );
                /* harmony import */ var _Pages_Shared_General_FourColumnTableWithActions__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/General/FourColumnTableWithActions */ "./resources/js/Pages/Shared/General/FourColumnTableWithActions.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var data = [
                    {
                        firstColumn: "583",
                        secondColumn:
                            "Där ditt arbete till stor del består av att sitta i långa möten eller sammanträden.",
                        thirdColumn: 200,
                        fourthColumn: "negative",
                    },
                    {
                        firstColumn: "582",
                        secondColumn:
                            "Få eller inga möjligheter till sociala kontakter under arbetstid.",
                        thirdColumn: 120,
                        fourthColumn: "negative",
                    },
                    {
                        firstColumn: "581",
                        secondColumn:
                            "Arbetssituationer som innebär långa arbetspass utan pauser.",
                        thirdColumn: 130,
                        fourthColumn: "negative",
                    },
                    {
                        firstColumn: "375",
                        secondColumn:
                            "Där du kan ha fullt upp att göra, så att du får utlopp för din höga energinivå.",
                        thirdColumn: 120,
                        fourthColumn: "positive",
                    },
                    {
                        firstColumn: "100",
                        secondColumn:
                            "Planering och uppföljning, att arbeta ut enskilda steg mot ett givet mål",
                        thirdColumn: 100,
                        fourthColumn: "positive",
                    },
                ];
                var tableHeaders = [
                    {
                        header: "Id",
                    },
                    {
                        header: "Text",
                    },
                    {
                        header: "Priority",
                    },
                    {
                        header: "Type",
                    },
                ];

                var Index = function Index() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
                        className: "mt-8 flex flex-col",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                            "div",
                            {
                                className: "-my-2 sm:-mx-6 lg:-mx-8",
                                children: /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                    "div",
                                    {
                                        className:
                                            "py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                            "div",
                                            {
                                                className:
                                                    "shadow overflow-hidden border-b border-gray-200 sm:rounded-lg",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                                    _Pages_Shared_General_FourColumnTableWithActions__WEBPACK_IMPORTED_MODULE_2__[
                                                        "default"
                                                    ],
                                                    {
                                                        tableHeaders:
                                                            tableHeaders,
                                                        data: data,
                                                        editUrl:
                                                            "work-patterns.edit",
                                                        deleteUrl:
                                                            "work-patterns.delete",
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            }
                        ),
                    });
                };

                Index.layout = function (page) {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                        _Pages_Shared_Layout_Layout__WEBPACK_IMPORTED_MODULE_0__[
                            "default"
                        ],
                        {
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(
                                _Pages_Shared_Layout_Dashboard__WEBPACK_IMPORTED_MODULE_1__[
                                    "default"
                                ],
                                {
                                    children: page,
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Index;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/General/FourColumnTableWithActions.jsx":
            /*!**************************************************************************!*\
  !*** ./resources/js/Pages/Shared/General/FourColumnTableWithActions.jsx ***!
  \**************************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_General_Pagination__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/General/Pagination */ "./resources/js/Pages/Shared/General/Pagination.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var FourColumnTableWithActions =
                    function FourColumnTableWithActions(_ref) {
                        var tableHeaders = _ref.tableHeaders,
                            data = _ref.data,
                            editUrl = _ref.editUrl,
                            deleteUrl = _ref.deleteUrl;
                        return /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.Fragment,
                            {
                                children: [
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                        "table",
                                        {
                                            className:
                                                "min-w-full divide-y divide-gray-200",
                                            children: [
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                    "thead",
                                                    {
                                                        className: "bg-gray-50",
                                                        children:
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                "tr",
                                                                {
                                                                    children: [
                                                                        tableHeaders.map(
                                                                            function (
                                                                                column,
                                                                                colIdx
                                                                            ) {
                                                                                return /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "th",
                                                                                    {
                                                                                        scope: "col",
                                                                                        className:
                                                                                            "px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider",
                                                                                        children:
                                                                                            column.header,
                                                                                    },
                                                                                    colIdx
                                                                                );
                                                                            }
                                                                        ),
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                            "th",
                                                                            {
                                                                                scope: "col",
                                                                                className:
                                                                                    "relative px-6 py-3",
                                                                                children:
                                                                                    [
                                                                                        editUrl &&
                                                                                            /*#__PURE__*/ (0,
                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                                "span",
                                                                                                {
                                                                                                    className:
                                                                                                        "sr-only",
                                                                                                    children:
                                                                                                        "Edit",
                                                                                                }
                                                                                            ),
                                                                                        deleteUrl &&
                                                                                            /*#__PURE__*/ (0,
                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                                "span",
                                                                                                {
                                                                                                    className:
                                                                                                        "sr-only",
                                                                                                    children:
                                                                                                        "Delete",
                                                                                                }
                                                                                            ),
                                                                                    ],
                                                                            }
                                                                        ),
                                                                    ],
                                                                }
                                                            ),
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                    "tbody",
                                                    {
                                                        children: data.map(
                                                            function (
                                                                item,
                                                                itemIdx
                                                            ) {
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                    "tr",
                                                                    {
                                                                        className:
                                                                            itemIdx %
                                                                                2 ===
                                                                            0
                                                                                ? "bg-white"
                                                                                : "bg-gray-50",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "td",
                                                                                    {
                                                                                        className:
                                                                                            "align-text-top px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900",
                                                                                        children:
                                                                                            item.firstColumn,
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "td",
                                                                                    {
                                                                                        className:
                                                                                            "align-text-top px-6 py-4 text-sm text-gray-500",
                                                                                        children:
                                                                                            item.secondColumn,
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "td",
                                                                                    {
                                                                                        className:
                                                                                            "align-text-top px-6 py-4 whitespace-nowrap text-sm text-gray-500",
                                                                                        children:
                                                                                            item.thirdColumn,
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "td",
                                                                                    {
                                                                                        className:
                                                                                            "align-text-top px-6 py-4 whitespace-nowrap text-sm text-gray-500",
                                                                                        children:
                                                                                            item.fourthColumn,
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                                    "td",
                                                                                    {
                                                                                        className:
                                                                                            "align-text-top px-6 py-4 whitespace-nowrap text-right text-sm font-medium space-x-4",
                                                                                        children:
                                                                                            [
                                                                                                editUrl &&
                                                                                                    /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                                        "a",
                                                                                                        {
                                                                                                            href: "#",
                                                                                                            className:
                                                                                                                "text-violet-600 hover:text-violet-900",
                                                                                                            children:
                                                                                                                "Edit",
                                                                                                        }
                                                                                                    ),
                                                                                                editUrl &&
                                                                                                    deleteUrl &&
                                                                                                    /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                                        "span",
                                                                                                        {
                                                                                                            className:
                                                                                                                "text-gray-300",
                                                                                                            "aria-hidden":
                                                                                                                "true",
                                                                                                            children:
                                                                                                                "|",
                                                                                                        }
                                                                                                    ),
                                                                                                deleteUrl &&
                                                                                                    /*#__PURE__*/ (0,
                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                                        "a",
                                                                                                        {
                                                                                                            href: "#",
                                                                                                            className:
                                                                                                                "text-violet-600 hover:text-violet-900",
                                                                                                            children:
                                                                                                                "Delete",
                                                                                                        }
                                                                                                    ),
                                                                                            ],
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    },
                                                                    item.firstColumn
                                                                );
                                                            }
                                                        ),
                                                    }
                                                ),
                                            ],
                                        }
                                    ),
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                        _Pages_Shared_General_Pagination__WEBPACK_IMPORTED_MODULE_0__[
                                            "default"
                                        ],
                                        {}
                                    ),
                                ],
                            }
                        );
                    };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    FourColumnTableWithActions;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/General/Pagination.jsx":
            /*!**********************************************************!*\
  !*** ./resources/js/Pages/Shared/General/Pagination.jsx ***!
  \**********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var Pagination = function Pagination() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                        "div",
                        {
                            className:
                                "bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6",
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "flex-1 flex justify-between sm:hidden",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                "a",
                                                {
                                                    href: "#",
                                                    className:
                                                        "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50",
                                                    children: "Previous",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                "a",
                                                {
                                                    href: "#",
                                                    className:
                                                        "ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50",
                                                    children: "Next",
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                    "div",
                                    {
                                        className:
                                            "hidden sm:flex-1 sm:flex sm:items-center sm:justify-between",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                "div",
                                                {
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                        "p",
                                                        {
                                                            className:
                                                                "text-sm text-gray-700",
                                                            children: [
                                                                "Showing ",
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "font-medium",
                                                                        children:
                                                                            "1",
                                                                    }
                                                                ),
                                                                " to ",
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "font-medium",
                                                                        children:
                                                                            "10",
                                                                    }
                                                                ),
                                                                " of",
                                                                " ",
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "font-medium",
                                                                        children:
                                                                            "97",
                                                                    }
                                                                ),
                                                                " results",
                                                            ],
                                                        }
                                                    ),
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                "div",
                                                {
                                                    children: /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                        "nav",
                                                        {
                                                            className:
                                                                "relative z-0 inline-flex rounded-md shadow-sm -space-x-px",
                                                            "aria-label":
                                                                "Pagination",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "span",
                                                                                    {
                                                                                        className:
                                                                                            "sr-only",
                                                                                        children:
                                                                                            "Previous",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.ChevronLeftIcon,
                                                                                    {
                                                                                        className:
                                                                                            "h-5 w-5",
                                                                                        "aria-hidden":
                                                                                            "true",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        "aria-current":
                                                                            "page",
                                                                        className:
                                                                            "z-10 bg-violet-50 border-violet-500 text-violet-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "1",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "2",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "3",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700",
                                                                        children:
                                                                            "...",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "8",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "9",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
                                                                        children:
                                                                            "10",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                                    "a",
                                                                    {
                                                                        href: "#",
                                                                        className:
                                                                            "relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    "span",
                                                                                    {
                                                                                        className:
                                                                                            "sr-only",
                                                                                        children:
                                                                                            "Next",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                                    _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.ChevronRightIcon,
                                                                                    {
                                                                                        className:
                                                                                            "h-5 w-5",
                                                                                        "aria-hidden":
                                                                                            "true",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                            ],
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Pagination;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/Dashboard.jsx":
            /*!********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Dashboard.jsx ***!
  \********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _Pages_Shared_Layout_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/TemplateContainer */ "./resources/js/Pages/Shared/Layout/TemplateContainer.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_Tabs__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/Tabs */ "./resources/js/Pages/Shared/Layout/Tabs.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/MainMenuNames */ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var Dashboard = function Dashboard(_ref) {
                    var children = _ref.children;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
                        selectedTab = _usePage.props.metaData.selectedTab;

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment,
                        {
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                _Pages_Shared_Layout_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__[
                                    "default"
                                ],
                                {
                                    title: selectedTab,
                                    children: /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                        "div",
                                        {
                                            className: "py-6",
                                            children: [
                                                selectedTab !==
                                                    _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_3__.SETTINGS_MENU &&
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        _Pages_Shared_Layout_Tabs__WEBPACK_IMPORTED_MODULE_1__[
                                                            "default"
                                                        ],
                                                        {}
                                                    ),
                                                children,
                                            ],
                                        }
                                    ),
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Dashboard;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/Layout.jsx":
            /*!*****************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Components.jsx ***!
  \*****************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MainHeader__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/MainHeader */ "./resources/js/Pages/Shared/Layout/MainHeader.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_SecondaryNavigation__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/SecondaryNavigation */ "./resources/js/Pages/Shared/Layout/SecondaryNavigation.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MobileMenu__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/MobileMenu */ "./resources/js/Pages/Shared/Layout/MobileMenu.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_NarrowSideBar__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/NarrowSideBar */ "./resources/js/Pages/Shared/Layout/NarrowSideBar.jsx"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_7__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/MainMenuNames */ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );
                function _slicedToArray(arr, i) {
                    return (
                        _arrayWithHoles(arr) ||
                        _iterableToArrayLimit(arr, i) ||
                        _unsupportedIterableToArray(arr, i) ||
                        _nonIterableRest()
                    );
                }

                function _nonIterableRest() {
                    throw new TypeError(
                        "Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."
                    );
                }

                function _unsupportedIterableToArray(o, minLen) {
                    if (!o) return;
                    if (typeof o === "string")
                        return _arrayLikeToArray(o, minLen);
                    var n = Object.prototype.toString.call(o).slice(8, -1);
                    if (n === "Object" && o.constructor) n = o.constructor.name;
                    if (n === "Map" || n === "Set") return Array.from(o);
                    if (
                        n === "Arguments" ||
                        /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)
                    )
                        return _arrayLikeToArray(o, minLen);
                }

                function _arrayLikeToArray(arr, len) {
                    if (len == null || len > arr.length) len = arr.length;
                    for (var i = 0, arr2 = new Array(len); i < len; i++) {
                        arr2[i] = arr[i];
                    }
                    return arr2;
                }

                function _iterableToArrayLimit(arr, i) {
                    var _i =
                        arr == null
                            ? null
                            : (typeof Symbol !== "undefined" &&
                                  arr[Symbol.iterator]) ||
                              arr["@@iterator"];
                    if (_i == null) return;
                    var _arr = [];
                    var _n = true;
                    var _d = false;
                    var _s, _e;
                    try {
                        for (
                            _i = _i.call(arr);
                            !(_n = (_s = _i.next()).done);
                            _n = true
                        ) {
                            _arr.push(_s.value);
                            if (i && _arr.length === i) break;
                        }
                    } catch (err) {
                        _d = true;
                        _e = err;
                    } finally {
                        try {
                            if (!_n && _i["return"] != null) _i["return"]();
                        } finally {
                            if (_d) throw _e;
                        }
                    }
                    return _arr;
                }

                function _arrayWithHoles(arr) {
                    if (Array.isArray(arr)) return arr;
                }

                var sidebarNavigation = [
                    {
                        name: _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.TALENT_MENU,
                        href: "balltext.index",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.HomeIcon,
                        current: false,
                    },
                    {
                        name: _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.CAREER_MENU,
                        href: "work-patterns.index",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ViewGridIcon,
                        current: false,
                    },
                    {
                        name: _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.LOGIQ_MENU,
                        href: "normgroup-text.index",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.PhotographIcon,
                        current: false,
                    },
                    /*{ name: SCREEN_MENU, href: '#', icon: UserGroupIcon, current: false },
{ name: FIVE_MENU, href: '#', icon: CollectionIcon, current: false },*/
                    {
                        name: _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.SETTINGS_MENU,
                        href: "settings.show",
                        icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.CogIcon,
                        current: false,
                    },
                ];

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var Layout = function Layout(_ref) {
                    var children = _ref.children;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_7__.usePage)(),
                        _usePage$props$metaDa = _usePage.props.metaData,
                        selectedMainMenu =
                            _usePage$props$metaDa.selectedMainMenu,
                        selectedTab = _usePage$props$metaDa.selectedTab;

                    var _useState = (0,
                        react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
                        _useState2 = _slicedToArray(_useState, 2),
                        mobileMenuOpen = _useState2[0],
                        setMobileMenuOpen = _useState2[1];

                    sidebarNavigation.filter(function (nav) {
                        nav.current = nav.name === selectedMainMenu;
                    });
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.Fragment,
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                    react_helmet__WEBPACK_IMPORTED_MODULE_2__.Helmet,
                                    {
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                            "title",
                                            {
                                                children: selectedTab
                                                    ? "JTC - ".concat(
                                                          selectedTab
                                                      )
                                                    : "JTC",
                                            }
                                        ),
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(
                                    "div",
                                    {
                                        className: "h-screen flex",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                                _Pages_Shared_Layout_NarrowSideBar__WEBPACK_IMPORTED_MODULE_6__[
                                                    "default"
                                                ],
                                                {
                                                    sidebarNavigation:
                                                        sidebarNavigation,
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                                _Pages_Shared_Layout_MobileMenu__WEBPACK_IMPORTED_MODULE_5__[
                                                    "default"
                                                ],
                                                {
                                                    mobileMenuOpen:
                                                        mobileMenuOpen,
                                                    setMobileMenuOpen:
                                                        setMobileMenuOpen,
                                                    sidebarNavigation:
                                                        sidebarNavigation,
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(
                                                "div",
                                                {
                                                    className:
                                                        "flex-1 flex flex-col overflow-hidden",
                                                    children: [
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                                            _Pages_Shared_Layout_MainHeader__WEBPACK_IMPORTED_MODULE_3__[
                                                                "default"
                                                            ],
                                                            {
                                                                setMobileMenuOpen:
                                                                    setMobileMenuOpen,
                                                                classNames:
                                                                    classNames,
                                                            }
                                                        ),
                                                        /*#__PURE__*/ (0,
                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(
                                                            "main",
                                                            {
                                                                className:
                                                                    "flex-1 flex overflow-hidden",
                                                                children: [
                                                                    /*#__PURE__*/ (0,
                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(
                                                                        "section",
                                                                        {
                                                                            "aria-labelledby":
                                                                                "primary-heading",
                                                                            className:
                                                                                "min-w-0 flex-1 h-full flex flex-col overflow-y-auto lg:order-last",
                                                                            children:
                                                                                [
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                                                                        "h1",
                                                                                        {
                                                                                            id: "primary-heading",
                                                                                            className:
                                                                                                "sr-only",
                                                                                            children:
                                                                                                "Account",
                                                                                        }
                                                                                    ),
                                                                                    children,
                                                                                ],
                                                                        }
                                                                    ),
                                                                    selectedMainMenu !==
                                                                        _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.SETTINGS_MENU &&
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(
                                                                            _Pages_Shared_Layout_SecondaryNavigation__WEBPACK_IMPORTED_MODULE_4__[
                                                                                "default"
                                                                            ],
                                                                            {}
                                                                        ),
                                                                ],
                                                            }
                                                        ),
                                                    ],
                                                }
                                            ),
                                        ],
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Layout;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/MainHeader.jsx":
            /*!*********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/MainHeader.jsx ***!
  \*********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/menu/menu.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var MainHeader = function MainHeader(_ref) {
                    var setMobileMenuOpen = _ref.setMobileMenuOpen,
                        classNames = _ref.classNames;

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
                        _usePage$props$user = _usePage.props.user,
                        loggedInUserName = _usePage$props$user.loggedInUserName,
                        userInitials = _usePage$props$user.userInitials;

                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                        "header",
                        {
                            className: "w-full",
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                "div",
                                {
                                    className:
                                        "relative z-10 flex-shrink-0 h-16 bg-white border-b border-gray-200 shadow-sm flex",
                                    children: [
                                        /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                            "button",
                                            {
                                                type: "button",
                                                className:
                                                    "border-r border-gray-200 px-4 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-violet-500 md:hidden",
                                                onClick: function onClick() {
                                                    return setMobileMenuOpen(
                                                        true
                                                    );
                                                },
                                                children: [
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        "span",
                                                        {
                                                            className:
                                                                "sr-only",
                                                            children:
                                                                "Open sidebar",
                                                        }
                                                    ),
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.MenuAlt2Icon,
                                                        {
                                                            className:
                                                                "h-6 w-6",
                                                            "aria-hidden":
                                                                "true",
                                                        }
                                                    ),
                                                ],
                                            }
                                        ),
                                        /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                            "div",
                                            {
                                                className:
                                                    "flex-1 flex justify-between px-4 sm:px-6",
                                                children: [
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                        "div",
                                                        {
                                                            className:
                                                                "flex-1 flex",
                                                            children:
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                    "form",
                                                                    {
                                                                        className:
                                                                            "w-full flex md:ml-0",
                                                                        action: "#",
                                                                        method: "GET",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                    "label",
                                                                                    {
                                                                                        htmlFor:
                                                                                            "search-field",
                                                                                        className:
                                                                                            "sr-only",
                                                                                        children:
                                                                                            "Search all files",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                                    "div",
                                                                                    {
                                                                                        className:
                                                                                            "relative w-full text-gray-400 focus-within:text-gray-600",
                                                                                        children:
                                                                                            [
                                                                                                /*#__PURE__*/ (0,
                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                    "div",
                                                                                                    {
                                                                                                        className:
                                                                                                            "pointer-events-none absolute inset-y-0 left-0 flex items-center",
                                                                                                        children:
                                                                                                            /*#__PURE__*/ (0,
                                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.SearchIcon,
                                                                                                                {
                                                                                                                    className:
                                                                                                                        "flex-shrink-0 h-5 w-5",
                                                                                                                    "aria-hidden":
                                                                                                                        "true",
                                                                                                                }
                                                                                                            ),
                                                                                                    }
                                                                                                ),
                                                                                                /*#__PURE__*/ (0,
                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                    "input",
                                                                                                    {
                                                                                                        name: "search-field",
                                                                                                        id: "search-field",
                                                                                                        className:
                                                                                                            "h-full w-full border-transparent py-2 pl-8 pr-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-0 focus:border-transparent focus:placeholder-gray-400",
                                                                                                        placeholder:
                                                                                                            "Search",
                                                                                                        type: "search",
                                                                                                    }
                                                                                                ),
                                                                                            ],
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                        }
                                                    ),
                                                    /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                        "div",
                                                        {
                                                            className:
                                                                "ml-2 flex items-center space-x-4 sm:ml-6 sm:space-x-6",
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Menu,
                                                                    {
                                                                        as: "div",
                                                                        className:
                                                                            "relative flex-shrink-0",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                    "div",
                                                                                    {
                                                                                        children:
                                                                                            /*#__PURE__*/ (0,
                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                                                _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                                                    .Menu
                                                                                                    .Button,
                                                                                                {
                                                                                                    className:
                                                                                                        "bg-white rounded-full flex text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500",
                                                                                                    children:
                                                                                                        [
                                                                                                            /*#__PURE__*/ (0,
                                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                "span",
                                                                                                                {
                                                                                                                    className:
                                                                                                                        "sr-only",
                                                                                                                    children:
                                                                                                                        "Open user menu",
                                                                                                                }
                                                                                                            ),
                                                                                                            /*#__PURE__*/ (0,
                                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                "img",
                                                                                                                {
                                                                                                                    className:
                                                                                                                        "h-8 w-8 rounded-full",
                                                                                                                    src: userInitials,
                                                                                                                    alt: loggedInUserName,
                                                                                                                }
                                                                                                            ),
                                                                                                        ],
                                                                                                }
                                                                                            ),
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition,
                                                                                    {
                                                                                        as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
                                                                                        enter: "transition ease-out duration-100",
                                                                                        enterFrom:
                                                                                            "transform opacity-0 scale-95",
                                                                                        enterTo:
                                                                                            "transform opacity-100 scale-100",
                                                                                        leave: "transition ease-in duration-75",
                                                                                        leaveFrom:
                                                                                            "transform opacity-100 scale-100",
                                                                                        leaveTo:
                                                                                            "transform opacity-0 scale-95",
                                                                                        children:
                                                                                            /*#__PURE__*/ (0,
                                                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                                                    .Menu
                                                                                                    .Items,
                                                                                                {
                                                                                                    className:
                                                                                                        "origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none",
                                                                                                    children:
                                                                                                        /*#__PURE__*/ (0,
                                                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_5__
                                                                                                                .Menu
                                                                                                                .Item,
                                                                                                            {
                                                                                                                children:
                                                                                                                    function children(
                                                                                                                        _ref2
                                                                                                                    ) {
                                                                                                                        var active =
                                                                                                                            _ref2.active;
                                                                                                                        return /*#__PURE__*/ (0,
                                                                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                                                            _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link,
                                                                                                                            {
                                                                                                                                href: route(
                                                                                                                                    "logout"
                                                                                                                                ),
                                                                                                                                className:
                                                                                                                                    classNames(
                                                                                                                                        active
                                                                                                                                            ? "bg-violet-800 text-white"
                                                                                                                                            : "text-violet-800 hover:bg-violet-800 hover:text-white",
                                                                                                                                        "block px-4 py-2 text-sm"
                                                                                                                                    ),
                                                                                                                                method: "post",
                                                                                                                                children:
                                                                                                                                    "Logout",
                                                                                                                            }
                                                                                                                        );
                                                                                                                    },
                                                                                                            }
                                                                                                        ),
                                                                                                }
                                                                                            ),
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(
                                                                    "button",
                                                                    {
                                                                        type: "button",
                                                                        className:
                                                                            "flex bg-violet-600 p-1 rounded-full items-center justify-center text-white hover:bg-violet-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500",
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                    _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.PlusSmIcon,
                                                                                    {
                                                                                        className:
                                                                                            "h-6 w-6",
                                                                                        "aria-hidden":
                                                                                            "true",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(
                                                                                    "span",
                                                                                    {
                                                                                        className:
                                                                                            "sr-only",
                                                                                        children:
                                                                                            "Add file",
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    }
                                                                ),
                                                            ],
                                                        }
                                                    ),
                                                ],
                                            }
                                        ),
                                    ],
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    MainHeader;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx":
            /*!************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/MainMenuNames.jsx ***!
  \************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ TALENT_MENU: () =>
                            /* binding */ TALENT_MENU,
                        /* harmony export */ CAREER_MENU: () =>
                            /* binding */ CAREER_MENU,
                        /* harmony export */ LOGIQ_MENU: () =>
                            /* binding */ LOGIQ_MENU,
                        /* harmony export */ SCREEN_MENU: () =>
                            /* binding */ SCREEN_MENU,
                        /* harmony export */ FIVE_MENU: () =>
                            /* binding */ FIVE_MENU,
                        /* harmony export */ SETTINGS_MENU: () =>
                            /* binding */ SETTINGS_MENU,
                        /* harmony export */
                    }
                );
                var TALENT_MENU = "Talent";
                var CAREER_MENU = "Career";
                var LOGIQ_MENU = "Logiq";
                var SCREEN_MENU = "Screen";
                var FIVE_MENU = "Five";
                var SETTINGS_MENU = "Settings";

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/MobileMenu.jsx":
            /*!*********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/MobileMenu.jsx ***!
  \*********************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react */ "./node_modules/react/index.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js"
                    );
                /* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js"
                    );
                /* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var MobileMenu = function MobileMenu(_ref) {
                    var mobileMenuOpen = _ref.mobileMenuOpen,
                        setMobileMenuOpen = _ref.setMobileMenuOpen,
                        sidebarNavigation = _ref.sidebarNavigation;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                        _headlessui_react__WEBPACK_IMPORTED_MODULE_3__
                            .Transition.Root,
                        {
                            show: mobileMenuOpen,
                            as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                _headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Dialog,
                                {
                                    as: "div",
                                    className: "md:hidden",
                                    onClose: setMobileMenuOpen,
                                    children: /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                        "div",
                                        {
                                            className:
                                                "fixed inset-0 z-40 flex",
                                            children: [
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_3__
                                                        .Transition.Child,
                                                    {
                                                        as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                        enter: "transition-opacity ease-linear duration-300",
                                                        enterFrom: "opacity-0",
                                                        enterTo: "opacity-100",
                                                        leave: "transition-opacity ease-linear duration-300",
                                                        leaveFrom:
                                                            "opacity-100",
                                                        leaveTo: "opacity-0",
                                                        children:
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                _headlessui_react__WEBPACK_IMPORTED_MODULE_4__
                                                                    .Dialog
                                                                    .Overlay,
                                                                {
                                                                    className:
                                                                        "fixed inset-0 bg-gray-600 bg-opacity-75",
                                                                }
                                                            ),
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                    _headlessui_react__WEBPACK_IMPORTED_MODULE_3__
                                                        .Transition.Child,
                                                    {
                                                        as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                        enter: "transition ease-in-out duration-300 transform",
                                                        enterFrom:
                                                            "-translate-x-full",
                                                        enterTo:
                                                            "translate-x-0",
                                                        leave: "transition ease-in-out duration-300 transform",
                                                        leaveFrom:
                                                            "translate-x-0",
                                                        leaveTo:
                                                            "-translate-x-full",
                                                        children:
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                "div",
                                                                {
                                                                    className:
                                                                        "relative max-w-xs w-full bg-violet-700 pt-5 pb-4 flex-1 flex flex-col",
                                                                    children: [
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                            _headlessui_react__WEBPACK_IMPORTED_MODULE_3__
                                                                                .Transition
                                                                                .Child,
                                                                            {
                                                                                as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
                                                                                enter: "ease-in-out duration-300",
                                                                                enterFrom:
                                                                                    "opacity-0",
                                                                                enterTo:
                                                                                    "opacity-100",
                                                                                leave: "ease-in-out duration-300",
                                                                                leaveFrom:
                                                                                    "opacity-100",
                                                                                leaveTo:
                                                                                    "opacity-0",
                                                                                children:
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                        "div",
                                                                                        {
                                                                                            className:
                                                                                                "absolute top-1 right-0 -mr-14 p-1",
                                                                                            children:
                                                                                                /*#__PURE__*/ (0,
                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                                                    "button",
                                                                                                    {
                                                                                                        type: "button",
                                                                                                        className:
                                                                                                            "h-12 w-12 rounded-full flex items-center justify-center focus:outline-none focus:ring-2 focus:ring-white",
                                                                                                        onClick:
                                                                                                            function onClick() {
                                                                                                                return setMobileMenuOpen(
                                                                                                                    false
                                                                                                                );
                                                                                                            },
                                                                                                        children:
                                                                                                            [
                                                                                                                /*#__PURE__*/ (0,
                                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                    _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.XIcon,
                                                                                                                    {
                                                                                                                        className:
                                                                                                                            "h-6 w-6 text-white",
                                                                                                                        "aria-hidden":
                                                                                                                            "true",
                                                                                                                    }
                                                                                                                ),
                                                                                                                /*#__PURE__*/ (0,
                                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                    "span",
                                                                                                                    {
                                                                                                                        className:
                                                                                                                            "sr-only",
                                                                                                                        children:
                                                                                                                            "Close sidebar",
                                                                                                                    }
                                                                                                                ),
                                                                                                            ],
                                                                                                    }
                                                                                                ),
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                            "div",
                                                                            {
                                                                                className:
                                                                                    "flex-shrink-0 px-4 flex items-center",
                                                                                children:
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                        "img",
                                                                                        {
                                                                                            className:
                                                                                                "h-8 w-auto",
                                                                                            src: "https://tailwindui.com/img/logos/workflow-mark.svg?color=white",
                                                                                            alt: "Workflow",
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                        /*#__PURE__*/ (0,
                                                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                            "div",
                                                                            {
                                                                                className:
                                                                                    "mt-5 flex-1 h-0 px-2 overflow-y-auto",
                                                                                children:
                                                                                    /*#__PURE__*/ (0,
                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                        "nav",
                                                                                        {
                                                                                            className:
                                                                                                "h-full flex flex-col",
                                                                                            children:
                                                                                                /*#__PURE__*/ (0,
                                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                    "div",
                                                                                                    {
                                                                                                        className:
                                                                                                            "space-y-1",
                                                                                                        children:
                                                                                                            sidebarNavigation.map(
                                                                                                                function (
                                                                                                                    item
                                                                                                                ) {
                                                                                                                    return /*#__PURE__*/ (0,
                                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(
                                                                                                                        "a",
                                                                                                                        {
                                                                                                                            href: item.href,
                                                                                                                            className:
                                                                                                                                classNames(
                                                                                                                                    item.current
                                                                                                                                        ? "bg-violet-800 text-white"
                                                                                                                                        : "text-violet-100 hover:bg-violet-800 hover:text-white",
                                                                                                                                    "group py-2 px-3 rounded-md flex items-center text-sm font-medium"
                                                                                                                                ),
                                                                                                                            "aria-current":
                                                                                                                                item.current
                                                                                                                                    ? "page"
                                                                                                                                    : undefined,
                                                                                                                            children:
                                                                                                                                [
                                                                                                                                    /*#__PURE__*/ (0,
                                                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                                        item.icon,
                                                                                                                                        {
                                                                                                                                            className:
                                                                                                                                                classNames(
                                                                                                                                                    item.current
                                                                                                                                                        ? "text-white"
                                                                                                                                                        : "text-violet-300 group-hover:text-white",
                                                                                                                                                    "mr-3 h-6 w-6"
                                                                                                                                                ),
                                                                                                                                            "aria-hidden":
                                                                                                                                                "true",
                                                                                                                                        }
                                                                                                                                    ),
                                                                                                                                    /*#__PURE__*/ (0,
                                                                                                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                                                                                                        "span",
                                                                                                                                        {
                                                                                                                                            children:
                                                                                                                                                item.name,
                                                                                                                                        }
                                                                                                                                    ),
                                                                                                                                ],
                                                                                                                        },
                                                                                                                        item.name
                                                                                                                    );
                                                                                                                }
                                                                                                            ),
                                                                                                    }
                                                                                                ),
                                                                                        }
                                                                                    ),
                                                                            }
                                                                        ),
                                                                    ],
                                                                }
                                                            ),
                                                    }
                                                ),
                                                /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(
                                                    "div",
                                                    {
                                                        className:
                                                            "flex-shrink-0 w-14",
                                                        "aria-hidden": "true",
                                                    }
                                                ),
                                            ],
                                        }
                                    ),
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    MobileMenu;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/NarrowSideBar.jsx":
            /*!************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/NarrowSideBar.jsx ***!
  \************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var NarrowSideBar = function NarrowSideBar(_ref) {
                    var sidebarNavigation = _ref.sidebarNavigation;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
                        className:
                            "hidden w-28 bg-violet-700 overflow-y-auto md:block",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                            "div",
                            {
                                className:
                                    "w-full py-6 flex flex-col items-center",
                                children: [
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                        "div",
                                        {
                                            className:
                                                "flex-shrink-0 flex items-center",
                                            children: /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                "img",
                                                {
                                                    className: "h-8 w-auto",
                                                    src: "https://tailwindui.com/img/logos/workflow-mark.svg?color=white",
                                                    alt: "Workflow",
                                                }
                                            ),
                                        }
                                    ),
                                    /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                        "div",
                                        {
                                            className:
                                                "flex-1 mt-6 w-full px-2 space-y-1",
                                            children: sidebarNavigation.map(
                                                function (item) {
                                                    return /*#__PURE__*/ (0,
                                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(
                                                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.Link,
                                                        {
                                                            href: route(
                                                                item.href
                                                            ),
                                                            className:
                                                                classNames(
                                                                    item.current
                                                                        ? "bg-violet-800 text-white"
                                                                        : "text-violet-100 hover:bg-violet-800 hover:text-white",
                                                                    "group w-full p-3 rounded-md flex flex-col items-center text-xs font-medium"
                                                                ),
                                                            "aria-current":
                                                                item.current
                                                                    ? "page"
                                                                    : undefined,
                                                            children: [
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    item.icon,
                                                                    {
                                                                        className:
                                                                            classNames(
                                                                                item.current
                                                                                    ? "text-white"
                                                                                    : "text-violet-300 group-hover:text-white",
                                                                                "h-6 w-6"
                                                                            ),
                                                                        "aria-hidden":
                                                                            "true",
                                                                    }
                                                                ),
                                                                /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(
                                                                    "span",
                                                                    {
                                                                        className:
                                                                            "mt-2",
                                                                        children:
                                                                            item.name,
                                                                    }
                                                                ),
                                                            ],
                                                        },
                                                        item.name
                                                    );
                                                }
                                            ),
                                        }
                                    ),
                                ],
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    NarrowSideBar;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/SecondaryNavigation.jsx":
            /*!******************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/SecondaryNavigation.jsx ***!
  \******************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js"
                    );
                /* harmony import */ var _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__ =
                    __webpack_require__(
                        /*! @/Pages/Talent/TalentMenu */ "./resources/js/Pages/Talent/TalentMenu.jsx"
                    );
                /* harmony import */ var _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__ =
                    __webpack_require__(
                        /*! @/Pages/Shared/Components/MainMenuNames */ "./resources/js/Pages/Shared/Layout/MainMenuNames.jsx"
                    );
                /* harmony import */ var _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__ =
                    __webpack_require__(
                        /*! @/Pages/Career/CareerMenu */ "./resources/js/Pages/Career/CareerMenu.jsx"
                    );
                /* harmony import */ var _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__ =
                    __webpack_require__(
                        /*! @/Pages/Logiq/LogiqMenu */ "./resources/js/Pages/Logiq/LogiqMenu.jsx"
                    );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var SecondaryNavigation = function SecondaryNavigation() {
                    var secondaryNavigation = [];

                    var _usePage = (0,
                        _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.usePage)(),
                        _usePage$props$metaDa = _usePage.props.metaData,
                        selectedMainMenu =
                            _usePage$props$metaDa.selectedMainMenu,
                        selectedSubMenu = _usePage$props$metaDa.selectedSubMenu;

                    switch (selectedMainMenu) {
                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.TALENT_MENU:
                            secondaryNavigation =
                                _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__[
                                    "default"
                                ];
                            break;

                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.CAREER_MENU:
                            secondaryNavigation =
                                _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__[
                                    "default"
                                ];
                            break;

                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.LOGIQ_MENU:
                            secondaryNavigation =
                                _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__[
                                    "default"
                                ];
                            break;

                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SCREEN_MENU:
                            break;

                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.FIVE_MENU:
                            break;

                        case _Pages_Shared_Layout_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SETTINGS_MENU:
                            break;
                    }

                    secondaryNavigation.filter(function (nav) {
                        nav.active = nav.name === selectedSubMenu;
                    });
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                        "aside",
                        {
                            className:
                                "hidden lg:block lg:flex-shrink-0 lg:order-first",
                            children: /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                                "div",
                                {
                                    className:
                                        "h-full relative flex flex-col w-60 border-r border-gray-200 bg-white overflow-y-auto",
                                    children: /*#__PURE__*/ (0,
                                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                                        "div",
                                        {
                                            className: "mt-3",
                                            children: /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                                                "div",
                                                {
                                                    className:
                                                        "px-3 mt-1 space-y-2",
                                                    role: "group",
                                                    "aria-labelledby":
                                                        "mobile-teams-headline",
                                                    children:
                                                        secondaryNavigation.map(
                                                            function (nav) {
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)(
                                                                    "a",
                                                                    {
                                                                        href: nav.href,
                                                                        className:
                                                                            classNames(
                                                                                nav.active
                                                                                    ? "bg-violet-100 text-violet-700"
                                                                                    : "text-gray-500 hover:text-violet-700 hover:bg-violet-100",
                                                                                "group flex items-center px-3 py-2 text-base leading-5 font-medium rounded-md"
                                                                            ),
                                                                        children:
                                                                            [
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                                                                                    "span",
                                                                                    {
                                                                                        className:
                                                                                            classNames(
                                                                                                nav.active
                                                                                                    ? "bg-green-500"
                                                                                                    : "bg-gray-500",
                                                                                                "w-2.5 h-2.5 mr-4 rounded-full"
                                                                                            ),
                                                                                        "aria-hidden":
                                                                                            "true",
                                                                                    }
                                                                                ),
                                                                                /*#__PURE__*/ (0,
                                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(
                                                                                    "span",
                                                                                    {
                                                                                        className:
                                                                                            "truncate",
                                                                                        children:
                                                                                            nav.name,
                                                                                    }
                                                                                ),
                                                                            ],
                                                                    },
                                                                    nav.name
                                                                );
                                                            }
                                                        ),
                                                }
                                            ),
                                        }
                                    ),
                                }
                            ),
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    SecondaryNavigation;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/Tabs.jsx":
            /*!***************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/TabContainer.jsx ***!
  \***************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var tabs = [
                    {
                        name: "Ball Text",
                        href: "balltext.index",
                        current: true,
                    },
                    {
                        name: "Point Text",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Work Style",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Reference Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Control Point",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Questions",
                        href: "balltext.index",
                        current: false,
                    },
                ];

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var Tabs = function Tabs() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        "div",
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                    "div",
                                    {
                                        className: "sm:hidden",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "label",
                                                {
                                                    htmlFor: "tabs",
                                                    className: "sr-only",
                                                    children: "Select a tab",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "select",
                                                {
                                                    id: "tabs",
                                                    name: "tabs",
                                                    className:
                                                        "block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md",
                                                    defaultValue: tabs.find(
                                                        function (tab) {
                                                            return tab.current;
                                                        }
                                                    ).name,
                                                    children: tabs.map(
                                                        function (tab) {
                                                            return /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "option",
                                                                {
                                                                    children:
                                                                        tab.name,
                                                                },
                                                                tab.name
                                                            );
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    "div",
                                    {
                                        className: "hidden sm:block",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                            "div",
                                            {
                                                className:
                                                    "border-b border-gray-200",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "nav",
                                                    {
                                                        className:
                                                            "flex space-x-4",
                                                        "aria-label": "Tabs",
                                                        children: tabs.map(
                                                            function (tab) {
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: tab.href,
                                                                        className:
                                                                            classNames(
                                                                                tab.current
                                                                                    ? "bg-violet-100 text-violet-700 mb-4"
                                                                                    : "text-gray-500 hover:text-violet-700 hover:bg-violet-100 hover:mb-4",
                                                                                "px-3 py-2 font-medium text-sm rounded-md"
                                                                            ),
                                                                        "aria-current":
                                                                            tab.current
                                                                                ? "page"
                                                                                : undefined,
                                                                        children:
                                                                            tab.name,
                                                                    },
                                                                    tab.name
                                                                );
                                                            }
                                                        ),
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Tabs;

                /***/
            },

        /***/ "./resources/js/Pages/Shared/Layout/TemplateContainer.jsx":
            /*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/TemplateContainer.jsx ***!
  \****************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var TemplateContainer = function TemplateContainer(_ref) {
                    var children = _ref.children,
                        title = _ref.title;
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
                        className: "mt-1",
                        children: /*#__PURE__*/ (0,
                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            "main",
                            {
                                className: "flex-1",
                                children: /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    "div",
                                    {
                                        className:
                                            "relative max-w-4xl mx-auto md:px-8 xl:px-0",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                            "div",
                                            {
                                                className: "pt-10 pb-16",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                                    "div",
                                                    {
                                                        className:
                                                            "px-4 sm:px-6 md:px-0",
                                                        children: [
                                                            /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "h1",
                                                                {
                                                                    className:
                                                                        "text-3xl font-extrabold text-gray-600",
                                                                    children:
                                                                        title,
                                                                }
                                                            ),
                                                            children,
                                                        ],
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            }
                        ),
                    });
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TemplateContainer;

                /***/
            },

        /***/ "./resources/js/Pages/Talent/TalentMenu.jsx":
            /*!**************************************************!*\
  !*** ./resources/js/Pages/Talent/TalentMenu.jsx ***!
  \**************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                var TalentMenu = [
                    {
                        name: "Text",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Rules",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Raw Score",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Profiles",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Tags",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Reports",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Questions",
                        href: "#",
                        active: false,
                    },
                    {
                        name: "Elimination",
                        href: "#",
                        active: false,
                    },
                ];
                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TalentMenu;

                /***/
            },
    },
]);

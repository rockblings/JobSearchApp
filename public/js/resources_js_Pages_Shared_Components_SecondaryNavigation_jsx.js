"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Components_SecondaryNavigation_jsx"],{

/***/ "./resources/js/Pages/Career/CareerMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Career/CareerMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var CareerMenu = [{
  name: 'Text',
  href: '#',
  active: false
}, {
  name: 'Rules',
  href: '#',
  active: false
}, {
  name: 'Score Config',
  href: '#',
  active: false
}, {
  name: 'Profiles',
  href: '#',
  active: false
}, {
  name: 'Tags',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CareerMenu);

/***/ }),

/***/ "./resources/js/Pages/Logiq/LogiqMenu.jsx":
/*!************************************************!*\
  !*** ./resources/js/Pages/Logiq/LogiqMenu.jsx ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var LogiqMenu = [{
  name: 'Norm Groups',
  href: '#',
  active: false
}, {
  name: 'Score Config',
  href: '#',
  active: false
}, {
  name: 'Test List',
  href: '#',
  active: false
}, {
  name: 'Traits',
  href: '#',
  active: false
}, {
  name: 'Questions',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LogiqMenu);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/MainMenuNames.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/MainMenuNames.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TALENT_MENU": () => (/* binding */ TALENT_MENU),
/* harmony export */   "CAREER_MENU": () => (/* binding */ CAREER_MENU),
/* harmony export */   "LOGIQ_MENU": () => (/* binding */ LOGIQ_MENU),
/* harmony export */   "SCREEN_MENU": () => (/* binding */ SCREEN_MENU),
/* harmony export */   "FIVE_MENU": () => (/* binding */ FIVE_MENU),
/* harmony export */   "SETTINGS_MENU": () => (/* binding */ SETTINGS_MENU)
/* harmony export */ });
var TALENT_MENU = 'Talent';
var CAREER_MENU = 'Career';
var LOGIQ_MENU = 'Logiq';
var SCREEN_MENU = 'Screen';
var FIVE_MENU = 'Five';
var SETTINGS_MENU = 'Settings';

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/SecondaryMenuNames.jsx":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/SecondaryMenuNames.jsx ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TEXT_MENU": () => (/* binding */ TEXT_MENU),
/* harmony export */   "RULES_MENU": () => (/* binding */ RULES_MENU),
/* harmony export */   "RAW_SCORE_MENU": () => (/* binding */ RAW_SCORE_MENU),
/* harmony export */   "PROFILES_MENU": () => (/* binding */ PROFILES_MENU),
/* harmony export */   "TAGS_MENU": () => (/* binding */ TAGS_MENU),
/* harmony export */   "REPORTS_MENU": () => (/* binding */ REPORTS_MENU),
/* harmony export */   "QUESTIONS_MENU": () => (/* binding */ QUESTIONS_MENU),
/* harmony export */   "ELIMINATION_MENU": () => (/* binding */ ELIMINATION_MENU)
/* harmony export */ });
var TEXT_MENU = "Text";
var RULES_MENU = "Rules";
var RAW_SCORE_MENU = "Raw Score";
var PROFILES_MENU = "Profiles";
var TAGS_MENU = "Tags";
var REPORTS_MENU = "Reports";
var QUESTIONS_MENU = "Questions";
var ELIMINATION_MENU = "Elimination";

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/SecondaryNavigation.jsx":
/*!**********************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/SecondaryNavigation.jsx ***!
  \**********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Pages/Talent/TalentMenu */ "./resources/js/Pages/Talent/TalentMenu.jsx");
/* harmony import */ var _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/Components/MainMenuNames */ "./resources/js/Pages/Shared/Components/MainMenuNames.jsx");
/* harmony import */ var _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Career/CareerMenu */ "./resources/js/Pages/Career/CareerMenu.jsx");
/* harmony import */ var _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Pages/Logiq/LogiqMenu */ "./resources/js/Pages/Logiq/LogiqMenu.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var SecondaryNavigation = function SecondaryNavigation() {
  var secondaryNavigation = [];

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.usePage)(),
      _usePage$props$metaDa = _usePage.props.metaData,
      selectedMainMenu = _usePage$props$metaDa.selectedMainMenu,
      selectedSubMenu = _usePage$props$metaDa.selectedSubMenu;

  switch (selectedMainMenu) {
    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.TALENT_MENU:
      secondaryNavigation = _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__["default"];
      break;

    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.CAREER_MENU:
      secondaryNavigation = _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__["default"];
      break;

    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.LOGIQ_MENU:
      secondaryNavigation = _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__["default"];
      break;

    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SCREEN_MENU:
      break;

    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.FIVE_MENU:
      break;

    case _Pages_Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SETTINGS_MENU:
      break;
  }

  secondaryNavigation.filter(function (nav) {
    nav.active = nav.name === selectedSubMenu;
  });
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("aside", {
    className: "hidden lg:block lg:flex-shrink-0 lg:order-first",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
      className: "h-full relative flex flex-col w-60 border-r border-gray-200 bg-white overflow-y-auto",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
        className: "mt-3",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
          className: "px-3 mt-1 space-y-2",
          role: "group",
          "aria-labelledby": "mobile-teams-headline",
          children: secondaryNavigation.map(function (nav) {
            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("a", {
              href: nav.href,
              className: classNames(nav.active ? "bg-violet-100 text-violet-700 shadow-sm" : "text-gray-500 hover:text-violet-700 hover:bg-violet-100", "group flex items-center px-3 py-2 text-base leading-5 font-medium rounded-md"),
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                className: classNames(nav.active ? "bg-green-100" : "bg-gray-100", "h-4 w-4 rounded-full flex items-center justify-center"),
                "aria-hidden": "true",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                  className: classNames(nav.active ? "bg-green-400" : "bg-gray-400", "h-2 w-2 rounded-full")
                })
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                className: "ml-2 truncate",
                children: nav.name
              })]
            }, nav.name);
          })
        })
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SecondaryNavigation);

/***/ }),

/***/ "./resources/js/Pages/Talent/TalentMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Talent/TalentMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Pages/Shared/Components/SecondaryMenuNames */ "./resources/js/Pages/Shared/Components/SecondaryMenuNames.jsx");

var TalentMenu = [{
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TEXT_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RULES_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RAW_SCORE_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.PROFILES_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TAGS_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.REPORTS_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.QUESTIONS_MENU,
  href: "#",
  active: false
}, {
  name: _Pages_Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.ELIMINATION_MENU,
  href: "#",
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TalentMenu);

/***/ })

}]);
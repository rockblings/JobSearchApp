"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Talent_EvaluationText_TalentTextTabElements_jsx"],
    {
        /***/ "./resources/js/Pages/Talent/EvaluationText/TalentTextTabElements.jsx":
            /*!****************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx ***!
  \****************************************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                var BALLTEXT_NAME = "Ball Text";
                var POINTTEXT_NAME = "Point Text";
                var WORKSTYLE_NAME = "Work Style";
                var REFERENCEGUIDE_NAME = "Reference Guide";
                var INTERVIEWGUIDE_NAME = "Interview Guide";
                var CONTROLPOINT_NAME = "Control Point";
                var INTERVIEWQUESTION_NAME = "Interview Questiions";
                var BALLTEXT_URL = "balltext.index";
                var POINTTEXT_URL = "pointtext.index";
                var WORKSTYLE_URL = "workstyle.index";
                var REFERENCEGUIDE_URL = "reference-guide.index";
                var INTERVIEWGUIDE_URL = "interview-guide.index";
                var CONTROLPOINT_URL = "control-point.index";
                var INTERVIEWQUESTION_URL = "interview-question.index";
                var TalentTextTabElements = [
                    {
                        name: BALLTEXT_NAME,
                        href: BALLTEXT_URL,
                        current: true,
                    },
                    {
                        name: POINTTEXT_NAME,
                        href: POINTTEXT_URL,
                        current: false,
                    },
                    {
                        name: WORKSTYLE_NAME,
                        href: WORKSTYLE_URL,
                        current: false,
                    },
                    {
                        name: REFERENCEGUIDE_NAME,
                        href: REFERENCEGUIDE_URL,
                        current: false,
                    },
                    {
                        name: INTERVIEWGUIDE_NAME,
                        href: INTERVIEWGUIDE_URL,
                        current: false,
                    },
                    {
                        name: CONTROLPOINT_NAME,
                        href: CONTROLPOINT_URL,
                        current: false,
                    },
                    {
                        name: INTERVIEWQUESTION_NAME,
                        href: INTERVIEWQUESTION_URL,
                        current: false,
                    },
                ];
                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    TalentTextTabElements;

                /***/
            },
    },
]);

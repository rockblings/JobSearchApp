"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([
    ["resources_js_Pages_Shared_Layout_Tabs_jsx"],
    {
        /***/ "./resources/js/Pages/Shared/Layout/Tabs.jsx":
            /*!***************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/TabContainer.jsx ***!
  \***************************************************/
            /***/ (
                __unused_webpack_module,
                __webpack_exports__,
                __webpack_require__
            ) => {
                __webpack_require__.r(__webpack_exports__);
                /* harmony export */ __webpack_require__.d(
                    __webpack_exports__,
                    {
                        /* harmony export */ default: () =>
                            __WEBPACK_DEFAULT_EXPORT__,
                        /* harmony export */
                    }
                );
                /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
                    __webpack_require__(
                        /*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js"
                    );

                var tabs = [
                    {
                        name: "Ball Text",
                        href: "balltext.index",
                        current: true,
                    },
                    {
                        name: "Point Text",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Work Style",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Reference Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Guide",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Control Point",
                        href: "balltext.index",
                        current: false,
                    },
                    {
                        name: "Interview Questions",
                        href: "balltext.index",
                        current: false,
                    },
                ];

                function classNames() {
                    for (
                        var _len = arguments.length,
                            classes = new Array(_len),
                            _key = 0;
                        _key < _len;
                        _key++
                    ) {
                        classes[_key] = arguments[_key];
                    }

                    return classes.filter(Boolean).join(" ");
                }

                var Tabs = function Tabs() {
                    return /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        "div",
                        {
                            children: [
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                                    "div",
                                    {
                                        className: "sm:hidden",
                                        children: [
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "label",
                                                {
                                                    htmlFor: "tabs",
                                                    className: "sr-only",
                                                    children: "Select a tab",
                                                }
                                            ),
                                            /*#__PURE__*/ (0,
                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                "select",
                                                {
                                                    id: "tabs",
                                                    name: "tabs",
                                                    className:
                                                        "block w-full focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md",
                                                    defaultValue: tabs.find(
                                                        function (tab) {
                                                            return tab.current;
                                                        }
                                                    ).name,
                                                    children: tabs.map(
                                                        function (tab) {
                                                            return /*#__PURE__*/ (0,
                                                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                "option",
                                                                {
                                                                    children:
                                                                        tab.name,
                                                                },
                                                                tab.name
                                                            );
                                                        }
                                                    ),
                                                }
                                            ),
                                        ],
                                    }
                                ),
                                /*#__PURE__*/ (0,
                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    "div",
                                    {
                                        className: "hidden sm:block",
                                        children: /*#__PURE__*/ (0,
                                        react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                            "div",
                                            {
                                                className:
                                                    "border-b border-gray-200",
                                                children: /*#__PURE__*/ (0,
                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                    "nav",
                                                    {
                                                        className:
                                                            "flex space-x-4",
                                                        "aria-label": "Tabs",
                                                        children: tabs.map(
                                                            function (tab) {
                                                                return /*#__PURE__*/ (0,
                                                                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                                                    "a",
                                                                    {
                                                                        href: tab.href,
                                                                        className:
                                                                            classNames(
                                                                                tab.current
                                                                                    ? "bg-violet-100 text-violet-700 mb-4 shadow-sm"
                                                                                    : "text-gray-500 hover:text-violet-700 hover:bg-violet-100 hover:mb-4",
                                                                                "px-3 py-2 font-medium text-sm rounded-md"
                                                                            ),
                                                                        "aria-current":
                                                                            tab.current
                                                                                ? "page"
                                                                                : undefined,
                                                                        children:
                                                                            tab.name,
                                                                    },
                                                                    tab.name
                                                                );
                                                            }
                                                        ),
                                                    }
                                                ),
                                            }
                                        ),
                                    }
                                ),
                            ],
                        }
                    );
                };

                /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
                    Tabs;

                /***/
            },
    },
]);

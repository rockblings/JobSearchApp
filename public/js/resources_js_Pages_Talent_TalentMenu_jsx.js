"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Talent_TalentMenu_jsx"],{

/***/ "./resources/js/Pages/Talent/TalentMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Talent/TalentMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/SecondaryMenuNames */ "./resources/js/Shared/Components/SecondaryMenuNames.jsx");

var TalentMenu = [{
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TEXT_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RULES_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RAW_SCORE_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.PROFILES_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TAGS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.REPORTS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.QUESTIONS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.ELIMINATION_MENU,
  href: "#",
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TalentMenu);

/***/ }),

/***/ "./resources/js/Shared/Components/SecondaryMenuNames.jsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/SecondaryMenuNames.jsx ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TEXT_MENU": () => (/* binding */ TEXT_MENU),
/* harmony export */   "RULES_MENU": () => (/* binding */ RULES_MENU),
/* harmony export */   "RAW_SCORE_MENU": () => (/* binding */ RAW_SCORE_MENU),
/* harmony export */   "PROFILES_MENU": () => (/* binding */ PROFILES_MENU),
/* harmony export */   "TAGS_MENU": () => (/* binding */ TAGS_MENU),
/* harmony export */   "REPORTS_MENU": () => (/* binding */ REPORTS_MENU),
/* harmony export */   "QUESTIONS_MENU": () => (/* binding */ QUESTIONS_MENU),
/* harmony export */   "ELIMINATION_MENU": () => (/* binding */ ELIMINATION_MENU)
/* harmony export */ });
var TEXT_MENU = "Text";
var RULES_MENU = "Rules";
var RAW_SCORE_MENU = "Raw Score";
var PROFILES_MENU = "Profiles";
var TAGS_MENU = "Tags";
var REPORTS_MENU = "Reports";
var QUESTIONS_MENU = "Questions";
var ELIMINATION_MENU = "Elimination";

/***/ })

}]);
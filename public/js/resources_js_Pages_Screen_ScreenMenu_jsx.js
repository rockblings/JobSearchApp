"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Screen_ScreenMenu_jsx"],{

/***/ "./resources/js/Pages/Screen/ScreenMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Screen/ScreenMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var ScreenMenu = [{
  name: 'Text',
  href: '#',
  active: false
}, {
  name: 'Rules',
  href: '#',
  active: false
}, {
  name: 'Raw Score',
  href: '#',
  active: false
}, {
  name: 'Profiles',
  href: '#',
  active: false
}, {
  name: 'Tags',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ScreenMenu);

/***/ })

}]);
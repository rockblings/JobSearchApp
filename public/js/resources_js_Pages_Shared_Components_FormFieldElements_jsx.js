"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Components_FormFieldElements_jsx"],{

/***/ "./resources/js/Pages/Shared/Components/FormFieldElements.jsx":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/FormFieldElements.jsx ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "INPUT_FIELD": () => (/* binding */ INPUT_FIELD),
/* harmony export */   "TEXTAREA_FIELD": () => (/* binding */ TEXTAREA_FIELD),
/* harmony export */   "CHECKBOX_FIELD": () => (/* binding */ CHECKBOX_FIELD)
/* harmony export */ });
var INPUT_FIELD = "Input";
var TEXTAREA_FIELD = "Textarea";
var CHECKBOX_FIELD = "Checkbox";

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Components_Radiobox_jsx"],{

/***/ "./resources/js/Pages/Shared/Components/Radiobox.jsx":
/*!***********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Radiobox.jsx ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var Radiobox = function Radiobox(_ref) {
  var id = _ref.id,
      label = _ref.label,
      data = _ref.data,
      setData = _ref.setData,
      name = _ref.name;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex items-center",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
      id: id,
      name: name,
      type: "radio",
      defaultChecked: data[id],
      value: data[id],
      onChange: function onChange(e) {
        return setData(name, e.target.checked);
      },
      className: "h-4 w-4 border-gray-300 text-violet-600 focus:ring-violet-500"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: id,
      className: "ml-3 block text-sm font-medium text-gray-700",
      children: label
    })]
  }, id);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Radiobox);

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Talent_EvaluationText_PointText_Index_js"],{

/***/ "./resources/js/Pages/Career/CareerMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Career/CareerMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var CareerMenu = [{
  name: 'Text',
  href: '#',
  active: false
}, {
  name: 'Rules',
  href: '#',
  active: false
}, {
  name: 'Score Config',
  href: '#',
  active: false
}, {
  name: 'Profiles',
  href: '#',
  active: false
}, {
  name: 'Tags',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CareerMenu);

/***/ }),

/***/ "./resources/js/Pages/Logiq/LogiqMenu.jsx":
/*!************************************************!*\
  !*** ./resources/js/Pages/Logiq/LogiqMenu.jsx ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var LogiqMenu = [{
  name: 'Norm Groups',
  href: '#',
  active: false
}, {
  name: 'Score Config',
  href: '#',
  active: false
}, {
  name: 'Test List',
  href: '#',
  active: false
}, {
  name: 'Traits',
  href: '#',
  active: false
}, {
  name: 'Questions',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LogiqMenu);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/Checkbox */ "./resources/js/Shared/Components/Checkbox.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




var ControlPointLevels = function ControlPointLevels(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("fieldset", {
    className: "space-y-5",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("legend", {
      className: "sr-only",
      children: "Control Point Levels"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelZero",
      label: "Level 0",
      data: data,
      setData: setData,
      name: "levelZero",
      describedby: "level-zero"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelOne",
      label: "Level 1",
      data: data,
      setData: setData,
      name: "levelOne",
      describedby: "level-one"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelTwo",
      label: "Level 2",
      data: data,
      setData: setData,
      name: "levelTwo",
      describedby: "level-two"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelThree",
      label: "Level 3",
      data: data,
      setData: setData,
      name: "levelThree",
      describedby: "level-three"
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ControlPointLevels);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx":
/*!*****************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/Radiobox */ "./resources/js/Shared/Components/Radiobox.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




var PointTextType = function PointTextType(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("fieldset", {
    className: "mt-4",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("legend", {
      className: "sr-only",
      children: "Point Text Type"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "space-y-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-10",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "positive",
        label: "Positive",
        data: data,
        setData: setData,
        name: "pointTextType"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "neutral",
        label: "Neutral",
        data: data,
        setData: setData,
        name: "pointTextType"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "negative",
        label: "Negative",
        data: data,
        setData: setData,
        name: "pointTextType"
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PointTextType);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/PointText/Index.js":
/*!*********************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/PointText/Index.js ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_Layout__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/Layout */ "./resources/js/Shared/Components/Layout.jsx");
/* harmony import */ var _Shared_Components_Dashboard__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Shared/Components/Dashboard */ "./resources/js/Shared/Components/Dashboard.jsx");
/* harmony import */ var _Shared_Components_FourColumnTableWithActions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/FourColumnTableWithActions */ "./resources/js/Shared/Components/FourColumnTableWithActions.jsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _Shared_Components_SlideOverLayout__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Shared/Components/SlideOverLayout */ "./resources/js/Shared/Components/SlideOverLayout.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }









var data = [{
  firstColumn: "X10",
  secondColumn: "Troligen har du just nu en lite besvärlig situation som emellanåt kan påverka ditt humör",
  thirdColumn: 10,
  fourthColumn: "neutral"
}, {
  firstColumn: "X11",
  secondColumn: "Eventuellt kan du ha en lite besvärlig situation någonstans i tillvaron, som tar en del energi från dig",
  thirdColumn: 10,
  fourthColumn: "neutral"
}, {
  firstColumn: "X2000",
  secondColumn: "Är välförberedd",
  thirdColumn: 50,
  fourthColumn: "positive"
}, {
  firstColumn: "X2001",
  secondColumn: "Du har lätt för att vara flexibel",
  thirdColumn: 50,
  fourthColumn: "positive"
}, {
  firstColumn: "X2104",
  secondColumn: "Kan känna dig lite illa till mods om inte arbetsområdet är välorganiserat och i ordning",
  thirdColumn: 50,
  fourthColumn: "negative"
}];
var tableHeaders = [{
  header: "Id"
}, {
  header: "Text"
}, {
  header: "Priority"
}, {
  header: "Type"
}];
var formFields = [{
  key: "firstColumn",
  fieldName: "textId",
  value: ""
}, {
  key: "secondColumn",
  fieldName: "actualText",
  value: ""
}, {
  key: "thirdColumn",
  fieldName: "priority",
  value: ""
}, {
  key: "positive",
  fieldName: "positive",
  value: false
}, {
  key: "neutral",
  fieldName: "neutral",
  value: false
}, {
  key: "negative",
  fieldName: "negative",
  value: false
}];

var Index = function Index() {
  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_3__.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      open = _useState2[0],
      setOpen = _useState2[1];

  var closeSlideOver = function closeSlideOver() {
    setOpen(false); // reset the form

    formFields.map(function (field) {
      field.value = "";
    });
  };

  var handleOnEdit = function handleOnEdit(items) {
    formFields.map(function (field) {
      if (items.hasOwnProperty(field.key)) {
        field.value = items[field.key];
      } else {
        if (field.key === items.fourthColumn) {
          field.value = true;
        }
      }
    });
    setOpen(true);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
      className: "mt-8 flex flex-col",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
        className: "-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
          className: "inline-block min-w-full py-2 align-middle sm:px-6 lg:px-8",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
            className: "overflow-hidden border-b border-gray-200 shadow sm:rounded-lg",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(_Shared_Components_FourColumnTableWithActions__WEBPACK_IMPORTED_MODULE_2__["default"], {
              tableHeaders: tableHeaders,
              data: data,
              editItem: handleOnEdit,
              editUrl: true,
              deleteUrl: "pointtext.delete"
            })
          })
        })
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(_Shared_Components_SlideOverLayout__WEBPACK_IMPORTED_MODULE_4__["default"], {
      open: open,
      closeSlideOver: closeSlideOver,
      title: "Edit PointText",
      formFields: formFields,
      description: "Update the applicable form fields below and click save."
    })]
  });
};

Index.layout = function (page) {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(_Shared_Components_Layout__WEBPACK_IMPORTED_MODULE_0__["default"], {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)(_Shared_Components_Dashboard__WEBPACK_IMPORTED_MODULE_1__["default"], {
      children: page,
      formFields: formFields
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Index);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BALLTEXT_NAME": () => (/* binding */ BALLTEXT_NAME),
/* harmony export */   "POINTTEXT_NAME": () => (/* binding */ POINTTEXT_NAME),
/* harmony export */   "WORKSTYLE_NAME": () => (/* binding */ WORKSTYLE_NAME),
/* harmony export */   "REFERENCEGUIDE_NAME": () => (/* binding */ REFERENCEGUIDE_NAME),
/* harmony export */   "INTERVIEWGUIDE_NAME": () => (/* binding */ INTERVIEWGUIDE_NAME),
/* harmony export */   "CONTROLPOINT_NAME": () => (/* binding */ CONTROLPOINT_NAME),
/* harmony export */   "INTERVIEWQUESTION_NAME": () => (/* binding */ INTERVIEWQUESTION_NAME),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var BALLTEXT_NAME = "Ball Text";
var POINTTEXT_NAME = "Point Text";
var WORKSTYLE_NAME = "Work Style";
var REFERENCEGUIDE_NAME = "Reference Guide";
var INTERVIEWGUIDE_NAME = "Interview Guide";
var CONTROLPOINT_NAME = "Control Point";
var INTERVIEWQUESTION_NAME = "Interview Questions";
var BALLTEXT_URL = "balltext.index";
var POINTTEXT_URL = "pointtext.index";
var WORKSTYLE_URL = "workstyle.index";
var REFERENCEGUIDE_URL = "reference-guide.index";
var INTERVIEWGUIDE_URL = "interview-guide.index";
var CONTROLPOINT_URL = "control-point.index";
var INTERVIEWQUESTION_URL = "interview-question.index";
var TalentTextTab = [{
  name: BALLTEXT_NAME,
  href: BALLTEXT_URL,
  current: false
}, {
  name: POINTTEXT_NAME,
  href: POINTTEXT_URL,
  current: false
}, {
  name: WORKSTYLE_NAME,
  href: WORKSTYLE_URL,
  current: false
}, {
  name: REFERENCEGUIDE_NAME,
  href: REFERENCEGUIDE_URL,
  current: false
}, {
  name: INTERVIEWGUIDE_NAME,
  href: INTERVIEWGUIDE_URL,
  current: false
}, {
  name: CONTROLPOINT_NAME,
  href: CONTROLPOINT_URL,
  current: false
}, {
  name: INTERVIEWQUESTION_NAME,
  href: INTERVIEWQUESTION_URL,
  current: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TalentTextTab);

/***/ }),

/***/ "./resources/js/Pages/Talent/TalentMenu.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Pages/Talent/TalentMenu.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/SecondaryMenuNames */ "./resources/js/Shared/Components/SecondaryMenuNames.jsx");

var TalentMenu = [{
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TEXT_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RULES_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.RAW_SCORE_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.PROFILES_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.TAGS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.REPORTS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.QUESTIONS_MENU,
  href: "#",
  active: false
}, {
  name: _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_0__.ELIMINATION_MENU,
  href: "#",
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TalentMenu);

/***/ }),

/***/ "./resources/js/Shared/Components/Checkbox.jsx":
/*!*****************************************************!*\
  !*** ./resources/js/Shared/Components/Checkbox.jsx ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var Checkbox = function Checkbox(_ref) {
  var id = _ref.id,
      label = _ref.label,
      data = _ref.data,
      setData = _ref.setData,
      name = _ref.name,
      describedby = _ref.describedby;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "relative flex items-start",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "flex h-5 items-center",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
        id: id,
        "aria-describedby": describedby,
        name: name,
        type: "checkbox",
        defaultChecked: data[name],
        value: data[name],
        onChange: function onChange(e) {
          return setData(name, e.target.checked);
        },
        className: "h-4 w-4 rounded border-gray-300 text-violet-600 focus:ring-violet-500"
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "ml-3 text-sm",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
        htmlFor: id,
        className: "font-medium text-gray-700",
        children: label
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Checkbox);

/***/ }),

/***/ "./resources/js/Shared/Components/CommonTextFormFields.jsx":
/*!*****************************************************************!*\
  !*** ./resources/js/Shared/Components/CommonTextFormFields.jsx ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/InputField */ "./resources/js/Shared/Components/InputField.jsx");
/* harmony import */ var _Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Shared/Components/TextAreaField */ "./resources/js/Shared/Components/TextAreaField.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/TalentTextTab */ "./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx");
/* harmony import */ var _Pages_Talent_EvaluationText_PointText_Components_PointTextType__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/PointText/Components/PointTextType */ "./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx");
/* harmony import */ var _Pages_Talent_EvaluationText_ControlPoint_Components_ControlPointLevels__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels */ "./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");









var CommonTextFormFields = function CommonTextFormFields(_ref) {
  var data = _ref.data,
      setData = _ref.setData;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
      selectedTab = _usePage.props.metaData.selectedTab;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)("div", {
    className: "flex flex-1 flex-col justify-between",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)("div", {
      className: "divide-y divide-gray-200 px-4 sm:px-6",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)("div", {
        className: "space-y-8 pt-6 pb-6",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "textId",
          label: "Text Id",
          required: true,
          type: "text",
          id: "textId",
          data: data,
          setData: setData
        }, "textId"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__["default"], {
          label: "Text",
          fieldName: "actualText",
          rows: 6,
          data: data,
          setData: setData,
          required: true
        }, "actualText"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "priority",
          label: "Priority",
          required: true,
          type: "number",
          id: "priority",
          data: data,
          setData: setData
        }, "priority"), selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.POINTTEXT_NAME && selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.INTERVIEWQUESTION_NAME && selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.CONTROLPOINT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "style",
          label: "Style",
          required: true,
          type: "text",
          id: "style",
          data: data,
          setData: setData
        }, "style"), selectedTab === _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.POINTTEXT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Talent_EvaluationText_PointText_Components_PointTextType__WEBPACK_IMPORTED_MODULE_4__["default"], {
          data: data,
          setData: setData
        }), selectedTab === _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.CONTROLPOINT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Talent_EvaluationText_ControlPoint_Components_ControlPointLevels__WEBPACK_IMPORTED_MODULE_5__["default"], {
          data: data,
          setData: setData
        })]
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CommonTextFormFields);

/***/ }),

/***/ "./resources/js/Shared/Components/Dashboard.jsx":
/*!******************************************************!*\
  !*** ./resources/js/Shared/Components/Dashboard.jsx ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/TemplateContainer */ "./resources/js/Shared/Components/TemplateContainer.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/MainMenuNames */ "./resources/js/Shared/Components/MainMenuNames.jsx");
/* harmony import */ var _Shared_Components_TabContainer__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Shared/Components/TabContainer */ "./resources/js/Shared/Components/TabContainer.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var Dashboard = function Dashboard(_ref) {
  var children = _ref.children,
      formFields = _ref.formFields;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.usePage)(),
      selectedTab = _usePage.props.metaData.selectedTab;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_Shared_Components_TemplateContainer__WEBPACK_IMPORTED_MODULE_0__["default"], {
      title: selectedTab,
      formFields: formFields,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
        className: "py-6",
        children: [selectedTab !== _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SETTINGS_MENU && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_Shared_Components_TabContainer__WEBPACK_IMPORTED_MODULE_3__["default"], {}), children]
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Dashboard);

/***/ }),

/***/ "./resources/js/Shared/Components/DesktopTab.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Shared/Components/DesktopTab.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var DesktopTab = function DesktopTab(_ref) {
  var tabs = _ref.tabs;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
    className: "border-b border-gray-200",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("nav", {
      className: "flex space-x-4",
      "aria-label": "Tabs",
      children: tabs.map(function (tab) {
        return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.Link, {
          href: route(tab.href),
          className: classNames(tab.current ? "bg-violet-100 text-violet-700 mb-4 shadow-sm" : "text-gray-500 hover:text-violet-700 hover:bg-violet-100 hover:mb-4", "px-3 py-2 font-small text-sm rounded-md"),
          "aria-current": tab.current ? "page" : undefined,
          children: tab.name
        }, tab.name);
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (DesktopTab);

/***/ }),

/***/ "./resources/js/Shared/Components/FourColumnTableWithActions.jsx":
/*!***********************************************************************!*\
  !*** ./resources/js/Shared/Components/FourColumnTableWithActions.jsx ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_Pagination__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/Pagination */ "./resources/js/Shared/Components/Pagination.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");





var FourColumnTableWithActions = function FourColumnTableWithActions(_ref) {
  var tableHeaders = _ref.tableHeaders,
      data = _ref.data,
      editItem = _ref.editItem,
      editUrl = _ref.editUrl,
      deleteUrl = _ref.deleteUrl;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("table", {
      className: "min-w-full divide-y divide-gray-200",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("thead", {
        className: "bg-violet-50 shadow-sm",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("tr", {
          children: [tableHeaders.map(function (column, colIdx) {
            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("th", {
              scope: "col",
              className: "px-6 py-3 text-left text-xs font-medium uppercase tracking-wider text-gray-500",
              children: column.header
            }, colIdx);
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("th", {
            scope: "col",
            className: "relative px-6 py-3",
            children: [editUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Edit"
            }), deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Delete"
            })]
          })]
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("tbody", {
        children: data.map(function (item, itemIdx) {
          return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("tr", {
            className: itemIdx % 2 === 0 ? "bg-white" : "bg-gray-50",
            children: [item.firstColumn && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "whitespace-nowrap px-6 py-4 align-text-top text-sm font-medium text-gray-900",
              children: item.firstColumn
            }), item.secondColumn && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "px-6 py-4 align-text-top text-sm text-gray-500",
              children: item.secondColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "whitespace-nowrap px-6 py-4 align-text-top text-sm text-gray-500",
              children: item.thirdColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("td", {
              className: "whitespace-nowrap px-6 py-4 align-text-top text-sm text-gray-500",
              children: item.fourthColumn
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("td", {
              className: "space-x-4 whitespace-nowrap px-6 py-4 text-right align-text-top text-sm font-medium",
              children: [editUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
                href: "#",
                className: "text-violet-600 hover:text-violet-900",
                onClick: function onClick() {
                  return editItem(item);
                },
                children: "Edit"
              }), editUrl && deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
                className: "text-gray-300",
                "aria-hidden": "true",
                children: "|"
              }), deleteUrl && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
                href: "#",
                className: "text-violet-600 hover:text-violet-900",
                children: "Delete"
              })]
            })]
          }, item.firstColumn);
        })
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Shared_Components_Pagination__WEBPACK_IMPORTED_MODULE_0__["default"], {})]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (FourColumnTableWithActions);

/***/ }),

/***/ "./resources/js/Shared/Components/InputField.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Shared/Components/InputField.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var InputField = function InputField(_ref) {
  var fieldName = _ref.fieldName,
      label = _ref.label,
      required = _ref.required,
      type = _ref.type,
      id = _ref.id,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: fieldName,
      className: "block text-sm font-medium text-gray-700",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
        type: type,
        name: fieldName,
        id: id,
        className: "shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md",
        required: required,
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (InputField);

/***/ }),

/***/ "./resources/js/Shared/Components/Layout.jsx":
/*!***************************************************!*\
  !*** ./resources/js/Shared/Components/Layout.jsx ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var react_helmet__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react-helmet */ "./node_modules/react-helmet/es/Helmet.js");
/* harmony import */ var _Shared_Components_MainHeader__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Shared/Components/MainHeader */ "./resources/js/Shared/Components/MainHeader.jsx");
/* harmony import */ var _Shared_Components_SecondaryNavigation__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Shared/Components/SecondaryNavigation */ "./resources/js/Shared/Components/SecondaryNavigation.jsx");
/* harmony import */ var _Shared_Components_MobileMenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Shared/Components/MobileMenu */ "./resources/js/Shared/Components/MobileMenu.jsx");
/* harmony import */ var _Shared_Components_NarrowSideBar__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/Shared/Components/NarrowSideBar */ "./resources/js/Shared/Components/NarrowSideBar.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/Shared/Components/MainMenuNames */ "./resources/js/Shared/Components/MainMenuNames.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }













var sidebarNavigation = [{
  name: _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.TALENT_MENU,
  href: "balltext.index",
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.HomeIcon,
  current: false
}, {
  name: _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.CAREER_MENU,
  href: "work-patterns.index",
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ViewGridIcon,
  current: false
}, {
  name: _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.LOGIQ_MENU,
  href: "normgroup.index",
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.PhotographIcon,
  current: false
},
/*{ name: SCREEN_MENU, href: '#', icon: UserGroupIcon, current: false },
{ name: FIVE_MENU, href: '#', icon: CollectionIcon, current: false },*/
{
  name: _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.SETTINGS_MENU,
  href: "settings.show",
  icon: _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.CogIcon,
  current: false
}];

function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var Layout = function Layout(_ref) {
  var children = _ref.children;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_7__.usePage)(),
      _usePage$props$metaDa = _usePage.props.metaData,
      selectedMainMenu = _usePage$props$metaDa.selectedMainMenu,
      selectedTab = _usePage$props$metaDa.selectedTab;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      mobileMenuOpen = _useState2[0],
      setMobileMenuOpen = _useState2[1];

  sidebarNavigation.filter(function (nav) {
    nav.current = nav.name === selectedMainMenu;
  });
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(react_helmet__WEBPACK_IMPORTED_MODULE_2__.Helmet, {
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)("title", {
        children: selectedTab ? "JTC - ".concat(selectedTab) : "JTC"
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("div", {
      className: "flex h-screen",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_NarrowSideBar__WEBPACK_IMPORTED_MODULE_6__["default"], {
        sidebarNavigation: sidebarNavigation
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_MobileMenu__WEBPACK_IMPORTED_MODULE_5__["default"], {
        mobileMenuOpen: mobileMenuOpen,
        setMobileMenuOpen: setMobileMenuOpen,
        sidebarNavigation: sidebarNavigation
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("div", {
        className: "flex flex-1 flex-col overflow-hidden",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_MainHeader__WEBPACK_IMPORTED_MODULE_3__["default"], {
          setMobileMenuOpen: setMobileMenuOpen,
          classNames: classNames
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("main", {
          className: "flex flex-1 overflow-hidden",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("section", {
            "aria-labelledby": "primary-heading",
            className: "flex h-full min-w-0 flex-1 flex-col overflow-y-auto lg:order-last",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)("h1", {
              id: "primary-heading",
              className: "sr-only",
              children: "Account"
            }), children]
          }), selectedMainMenu !== _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_8__.SETTINGS_MENU && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_SecondaryNavigation__WEBPACK_IMPORTED_MODULE_4__["default"], {})]
        })]
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Layout);

/***/ }),

/***/ "./resources/js/Shared/Components/MainHeader.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Shared/Components/MainHeader.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/menu/menu.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var MainHeader = function MainHeader(_ref) {
  var setMobileMenuOpen = _ref.setMobileMenuOpen,
      classNames = _ref.classNames;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
      _usePage$props = _usePage.props,
      loggedInUserName = _usePage$props.user.loggedInUserName,
      selectedSubMenu = _usePage$props.metaData.selectedSubMenu; // create an initial from the logged-in username


  function getInitialFromName(str) {
    return str.split(" ").map(function (word) {
      return word[0];
    }).join("");
  }

  var userInitials = getInitialFromName(loggedInUserName);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("header", {
    className: "w-full",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
      className: "relative z-10 flex-shrink-0 h-16 bg-white border-b border-gray-200 shadow-sm flex",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("button", {
        type: "button",
        className: "border-r border-gray-200 px-4 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-violet-500 md:hidden",
        onClick: function onClick() {
          return setMobileMenuOpen(true);
        },
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
          className: "sr-only",
          children: "Open sidebar"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.MenuAlt2Icon, {
          className: "h-6 w-6",
          "aria-hidden": "true"
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
        className: "flex-1 flex justify-between px-4 sm:px-6",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
          className: "flex-1 flex",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("form", {
            className: "w-full flex md:ml-0",
            action: "#",
            method: "GET",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("label", {
              htmlFor: "search-field",
              className: "sr-only",
              children: "Search current project"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
              className: "relative w-full text-gray-400 focus-within:text-gray-600",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
                className: "pointer-events-none absolute inset-y-0 left-0 flex items-center",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.SearchIcon, {
                  className: "flex-shrink-0 h-5 w-5",
                  "aria-hidden": "true"
                })
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("input", {
                name: "search-field",
                id: "search-field",
                className: "h-full w-full border-transparent py-2 pl-8 pr-3 text-base text-gray-900 placeholder-gray-500 focus:outline-none focus:ring-0 focus:border-transparent focus:placeholder-gray-400",
                placeholder: "Search ".concat(selectedSubMenu),
                type: "search"
              })]
            })]
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
          className: "ml-2 flex items-center space-x-4 sm:ml-6 sm:space-x-6",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Menu, {
            as: "div",
            className: "relative flex-shrink-0",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("div", {
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Menu.Button, {
                className: "bg-white border border-violet-800 rounded-full flex text-sm focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-violet-500",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                  className: "sr-only",
                  children: "Open user menu"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                  className: "h-8 w-8 rounded-full py-1 px-1 text-violet-700",
                  children: userInitials
                })]
              })
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition, {
              as: react__WEBPACK_IMPORTED_MODULE_3__.Fragment,
              enter: "transition ease-out duration-100",
              enterFrom: "transform opacity-0 scale-95",
              enterTo: "transform opacity-100 scale-100",
              leave: "transition ease-in duration-75",
              leaveFrom: "transform opacity-100 scale-100",
              leaveTo: "transform opacity-0 scale-95",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Menu.Items, {
                className: "origin-top-right absolute right-0 mt-2 w-48 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Menu.Item, {
                  children: function children(_ref2) {
                    var active = _ref2.active;
                    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.Link, {
                      href: route("logout"),
                      className: classNames(active ? "bg-violet-800 text-white" : "text-violet-800 hover:bg-violet-800 hover:text-white", "block px-4 py-2 text-sm"),
                      method: "post",
                      children: "Logout"
                    });
                  }
                })
              })
            })]
          })
        })]
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MainHeader);

/***/ }),

/***/ "./resources/js/Shared/Components/MainMenuNames.jsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/MainMenuNames.jsx ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TALENT_MENU": () => (/* binding */ TALENT_MENU),
/* harmony export */   "CAREER_MENU": () => (/* binding */ CAREER_MENU),
/* harmony export */   "LOGIQ_MENU": () => (/* binding */ LOGIQ_MENU),
/* harmony export */   "SCREEN_MENU": () => (/* binding */ SCREEN_MENU),
/* harmony export */   "FIVE_MENU": () => (/* binding */ FIVE_MENU),
/* harmony export */   "SETTINGS_MENU": () => (/* binding */ SETTINGS_MENU)
/* harmony export */ });
var TALENT_MENU = 'Talent';
var CAREER_MENU = 'Career';
var LOGIQ_MENU = 'Logiq';
var SCREEN_MENU = 'Screen';
var FIVE_MENU = 'Five';
var SETTINGS_MENU = 'Settings';

/***/ }),

/***/ "./resources/js/Shared/Components/MobileMenu.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Shared/Components/MobileMenu.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");






function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
}

var MobileMenu = function MobileMenu(_ref) {
  var mobileMenuOpen = _ref.mobileMenuOpen,
      setMobileMenuOpen = _ref.setMobileMenuOpen,
      sidebarNavigation = _ref.sidebarNavigation;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Transition.Root, {
    show: mobileMenuOpen,
    as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Dialog, {
      as: "div",
      className: "md:hidden",
      onClose: setMobileMenuOpen,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
        className: "fixed inset-0 z-40 flex",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Transition.Child, {
          as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
          enter: "transition-opacity ease-linear duration-300",
          enterFrom: "opacity-0",
          enterTo: "opacity-100",
          leave: "transition-opacity ease-linear duration-300",
          leaveFrom: "opacity-100",
          leaveTo: "opacity-0",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Dialog.Overlay, {
            className: "fixed inset-0 bg-gray-600 bg-opacity-75"
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Transition.Child, {
          as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
          enter: "transition ease-in-out duration-300 transform",
          enterFrom: "-translate-x-full",
          enterTo: "translate-x-0",
          leave: "transition ease-in-out duration-300 transform",
          leaveFrom: "translate-x-0",
          leaveTo: "-translate-x-full",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
            className: "relative max-w-xs w-full bg-violet-700 pt-5 pb-4 flex-1 flex flex-col",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Transition.Child, {
              as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
              enter: "ease-in-out duration-300",
              enterFrom: "opacity-0",
              enterTo: "opacity-100",
              leave: "ease-in-out duration-300",
              leaveFrom: "opacity-100",
              leaveTo: "opacity-0",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
                className: "absolute top-1 right-0 -mr-14 p-1",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("button", {
                  type: "button",
                  className: "h-12 w-12 rounded-full flex items-center justify-center focus:outline-none focus:ring-2 focus:ring-white",
                  onClick: function onClick() {
                    return setMobileMenuOpen(false);
                  },
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.XIcon, {
                    className: "h-6 w-6 text-white",
                    "aria-hidden": "true"
                  }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("span", {
                    className: "sr-only",
                    children: "Close sidebar"
                  })]
                })
              })
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
              className: "flex-shrink-0 px-4 flex items-center",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("img", {
                className: "h-8 w-auto",
                src: "https://tailwindui.com/img/logos/workflow-mark.svg?color=white",
                alt: "Workflow"
              })
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
              className: "mt-5 flex-1 h-0 px-2 overflow-y-auto",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("nav", {
                className: "h-full flex flex-col",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
                  className: "space-y-1",
                  children: sidebarNavigation.map(function (item) {
                    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("a", {
                      href: item.href,
                      className: classNames(item.current ? 'bg-violet-800 text-white' : 'text-violet-100 hover:bg-violet-800 hover:text-white', 'group py-2 px-3 rounded-md flex items-center text-sm font-medium'),
                      "aria-current": item.current ? 'page' : undefined,
                      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(item.icon, {
                        className: classNames(item.current ? 'text-white' : 'text-violet-300 group-hover:text-white', 'mr-3 h-6 w-6'),
                        "aria-hidden": "true"
                      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("span", {
                        children: item.name
                      })]
                    }, item.name);
                  })
                })
              })
            })]
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
          className: "flex-shrink-0 w-14",
          "aria-hidden": "true"
        })]
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MobileMenu);

/***/ }),

/***/ "./resources/js/Shared/Components/MobileTab.jsx":
/*!******************************************************!*\
  !*** ./resources/js/Shared/Components/MobileTab.jsx ***!
  \******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/listbox/listbox.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");
/* harmony import */ var _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia */ "./node_modules/@inertiajs/inertia/dist/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }










function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var MobileTab = function MobileTab(_ref) {
  var tabs = _ref.tabs;
  var defaultSelectedTab = tabs.find(function (tab) {
    return tab.current;
  });

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(defaultSelectedTab),
      _useState2 = _slicedToArray(_useState, 2),
      selected = _useState2[0],
      setSelected = _useState2[1];

  var handleTabDropDown = function handleTabDropDown(selected) {
    setSelected(selected);
    _inertiajs_inertia__WEBPACK_IMPORTED_MODULE_2__.Inertia.get(route(selected.href));
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Listbox, {
    value: selected,
    onChange: function onChange(selected) {
      return handleTabDropDown(selected);
    },
    children: function children(_ref2) {
      var open = _ref2.open;
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)("div", {
          className: "mt-1 relative",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Listbox.Button, {
            className: "bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-fuchsia-500 focus:border-fuchsia-500 sm:text-sm",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
              className: "block truncate",
              children: selected.name
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
              className: "absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.SelectorIcon, {
                className: "h-5 w-5 text-gray-400",
                "aria-hidden": "true"
              })
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_6__.Transition, {
            show: open,
            as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
            leave: "transition ease-in duration-100",
            leaveFrom: "opacity-100",
            leaveTo: "opacity-0",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Listbox.Options, {
              className: "absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm",
              children: tabs.map(function (tab) {
                return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_5__.Listbox.Option, {
                  className: function className(_ref3) {
                    var active = _ref3.active;
                    return classNames(active ? "text-white bg-fuchsia-600" : "text-gray-900", "cursor-default select-none relative py-2 pl-3 pr-9");
                  },
                  value: tab,
                  children: function children(_ref4) {
                    var selected = _ref4.selected,
                        active = _ref4.active;
                    return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.Fragment, {
                      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                        className: classNames(selected ? "font-semibold" : "font-normal", "block truncate"),
                        children: tab.name
                      }), selected ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)("span", {
                        className: classNames(active ? "text-white" : "text-fuchsia-600", "absolute inset-y-0 right-0 flex items-center pr-4"),
                        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_4__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_1__.CheckIcon, {
                          className: "h-5 w-5",
                          "aria-hidden": "true"
                        })
                      }) : null]
                    });
                  }
                }, tab.name);
              })
            })
          })]
        })
      });
    }
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MobileTab);

/***/ }),

/***/ "./resources/js/Shared/Components/NarrowSideBar.jsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/NarrowSideBar.jsx ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
}

var NarrowSideBar = function NarrowSideBar(_ref) {
  var sidebarNavigation = _ref.sidebarNavigation;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
    className: "hidden w-28 bg-violet-700 overflow-y-auto md:block",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "w-full py-6 flex flex-col items-center",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
        className: "flex-shrink-0 flex items-center",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("img", {
          className: "h-8 w-auto",
          src: "https://tailwindui.com/img/logos/workflow-mark.svg?color=white",
          alt: "Workflow"
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
        className: "flex-1 mt-6 w-full px-2 space-y-1",
        children: sidebarNavigation.map(function (item) {
          return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.Link, {
            href: route(item.href),
            className: classNames(item.current ? 'bg-violet-800 text-white' : 'text-violet-100 hover:bg-violet-800 hover:text-white', 'group w-full p-3 rounded-md flex flex-col items-center text-xs font-medium'),
            "aria-current": item.current ? 'page' : undefined,
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(item.icon, {
              className: classNames(item.current ? 'text-white' : 'text-violet-300 group-hover:text-white', 'h-6 w-6'),
              "aria-hidden": "true"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "mt-2",
              children: item.name
            })]
          }, item.name);
        })
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (NarrowSideBar);

/***/ }),

/***/ "./resources/js/Shared/Components/Notification.jsx":
/*!*********************************************************!*\
  !*** ./resources/js/Shared/Components/Notification.jsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _Shared_Hooks_useTimeout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Hooks/useTimeout */ "./resources/js/Shared/Hooks/useTimeout.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var Notification = function Notification(_ref) {
  var show = _ref.show,
      title = _ref.title,
      body = _ref.body,
      closeNotification = _ref.closeNotification,
      messageType = _ref.messageType;
  // remove the notification after 3 seconds
  (0,_Shared_Hooks_useTimeout__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
    closeNotification();
  }, 3000);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
      "aria-live": "assertive",
      className: "pointer-events-none fixed inset-0 flex items-end px-4 py-6 sm:items-start sm:p-6",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
        className: "mt-10 flex w-full flex-col items-center space-y-6 sm:items-end",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Transition, {
          show: show,
          as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
          enter: "transform ease-out duration-300 transition",
          enterFrom: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2",
          enterTo: "translate-y-0 opacity-100 sm:translate-x-0",
          leave: "transition ease-in duration-100",
          leaveFrom: "opacity-100",
          leaveTo: "opacity-0",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "pointer-events-auto w-full max-w-sm overflow-hidden rounded-lg bg-violet-800 shadow-lg ring-1 ring-black ring-opacity-5",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "p-4",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "flex items-start",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
                  className: "flex-shrink-0",
                  children: messageType === "error" ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ExclamationCircleIcon, {
                    className: "h-6 w-6 text-white",
                    "aria-hidden": "true"
                  }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.CheckCircleIcon, {
                    className: "h-6 w-6 text-white",
                    "aria-hidden": "true"
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                  className: "ml-3 w-0 flex-1 pt-0.5",
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
                    className: "text-sm font-medium text-white",
                    children: title
                  }), body && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
                    className: "mt-1 text-sm text-white",
                    children: body
                  })]
                })]
              })
            })
          })
        })
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Notification);

/***/ }),

/***/ "./resources/js/Shared/Components/Pagination.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Shared/Components/Pagination.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




var Pagination = function Pagination() {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
    className: "bg-white px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "flex-1 flex justify-between sm:hidden",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
        href: "#",
        className: "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50",
        children: "Previous"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
        href: "#",
        className: "ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50",
        children: "Next"
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "hidden sm:flex-1 sm:flex sm:items-center sm:justify-between",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("p", {
          className: "text-sm text-gray-700",
          children: ["Showing ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
            className: "font-medium",
            children: "1"
          }), " to ", /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
            className: "font-medium",
            children: "10"
          }), " of", ' ', /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
            className: "font-medium",
            children: "97"
          }), " results"]
        })
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("div", {
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("nav", {
          className: "relative z-0 inline-flex rounded-md shadow-sm -space-x-px",
          "aria-label": "Pagination",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("a", {
            href: "#",
            className: "relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Previous"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.ChevronLeftIcon, {
              className: "h-5 w-5",
              "aria-hidden": "true"
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            "aria-current": "page",
            className: "z-10 bg-violet-50 border-violet-500 text-violet-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
            children: "1"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            className: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
            children: "2"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            className: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium",
            children: "3"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
            className: "relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700",
            children: "..."
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            className: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 hidden md:inline-flex relative items-center px-4 py-2 border text-sm font-medium",
            children: "8"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            className: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
            children: "9"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("a", {
            href: "#",
            className: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium",
            children: "10"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("a", {
            href: "#",
            className: "relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("span", {
              className: "sr-only",
              children: "Next"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.ChevronRightIcon, {
              className: "h-5 w-5",
              "aria-hidden": "true"
            })]
          })]
        })
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Pagination);

/***/ }),

/***/ "./resources/js/Shared/Components/Radiobox.jsx":
/*!*****************************************************!*\
  !*** ./resources/js/Shared/Components/Radiobox.jsx ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var Radiobox = function Radiobox(_ref) {
  var id = _ref.id,
      label = _ref.label,
      data = _ref.data,
      setData = _ref.setData,
      name = _ref.name;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex items-center",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
      id: id,
      name: name,
      type: "radio",
      defaultChecked: data[id],
      value: data[id],
      onChange: function onChange(e) {
        return setData(name, e.target.checked);
      },
      className: "h-4 w-4 border-gray-300 text-violet-600 focus:ring-violet-500"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: id,
      className: "ml-3 block text-sm font-medium text-gray-700",
      children: label
    })]
  }, id);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Radiobox);

/***/ }),

/***/ "./resources/js/Shared/Components/SearchWithSortForm.jsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/SearchWithSortForm.jsx ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/solid */ "./node_modules/@heroicons/react/solid/esm/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/menu/menu.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");







function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
}

var SearchWithSortForm = function SearchWithSortForm(_ref) {
  var title = _ref.title,
      sortOptions = _ref.sortOptions;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("label", {
      htmlFor: "mobile-search-candidate",
      className: "sr-only",
      children: "Search"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("label", {
      htmlFor: "desktop-search-candidate",
      className: "sr-only",
      children: "Search"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
      className: "flex rounded-md shadow-sm",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
        className: "relative flex-grow focus-within:z-10",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
          className: "absolute inset-y-0 left-0 pl-3 flex items-center pointer-events-none",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.SearchIcon, {
            className: "h-5 w-5 text-gray-400",
            "aria-hidden": "true"
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("input", {
          type: "text",
          name: "mobile-search-candidate",
          id: "mobile-search-candidate",
          className: "focus:ring-violet-500 focus:border-violet-500 block w-full rounded-none rounded-l-md pl-10 sm:hidden border-gray-300",
          placeholder: "Search"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("input", {
          type: "text",
          name: "desktop-search-candidate",
          id: "desktop-search-candidate",
          className: "hidden focus:ring-violet-500 focus:border-violet-500 w-full rounded-none rounded-l-md pl-10 sm:block sm:text-sm border-gray-300",
          placeholder: "Search ".concat(title)
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
        className: "flex items-center",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Menu, {
          as: "div",
          className: "relative inline-block text-left",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Menu.Button, {
              className: "-ml-px relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-r-md text-gray-700 bg-gray-50 hover:bg-gray-100 focus:outline-none",
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.SortAscendingIcon, {
                className: "h-5 w-5 text-gray-400",
                "aria-hidden": "true"
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("span", {
                className: "ml-2",
                children: "Sort"
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_heroicons_react_solid__WEBPACK_IMPORTED_MODULE_0__.ChevronDownIcon, {
                className: "ml-2.5 -mr-1.5 h-5 w-5 text-gray-400",
                "aria-hidden": "true"
              })]
            })
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Transition, {
            as: react__WEBPACK_IMPORTED_MODULE_1__.Fragment,
            enter: "transition ease-out duration-100",
            enterFrom: "transform opacity-0 scale-95",
            enterTo: "transform opacity-100 scale-100",
            leave: "transition ease-in duration-75",
            leaveFrom: "transform opacity-100 scale-100",
            leaveTo: "transform opacity-0 scale-95",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Menu.Items, {
              className: "origin-top-right absolute right-0 mt-2 w-40 rounded-md shadow-2xl bg-white ring-1 ring-black ring-opacity-5 focus:outline-none z-10",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
                className: "py-1",
                children: sortOptions.map(function (option) {
                  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_3__.Menu.Item, {
                    children: function children(_ref2) {
                      var active = _ref2.active;
                      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("a", {
                        href: option.href,
                        className: classNames(option.current ? 'font-medium text-gray-900' : 'text-gray-500', active ? 'bg-gray-100' : '', 'block px-4 py-2 text-sm'),
                        children: option.name
                      });
                    }
                  }, option.name);
                })
              })
            })
          })]
        })
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SearchWithSortForm);

/***/ }),

/***/ "./resources/js/Shared/Components/SecondaryMenuNames.jsx":
/*!***************************************************************!*\
  !*** ./resources/js/Shared/Components/SecondaryMenuNames.jsx ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TEXT_MENU": () => (/* binding */ TEXT_MENU),
/* harmony export */   "RULES_MENU": () => (/* binding */ RULES_MENU),
/* harmony export */   "RAW_SCORE_MENU": () => (/* binding */ RAW_SCORE_MENU),
/* harmony export */   "PROFILES_MENU": () => (/* binding */ PROFILES_MENU),
/* harmony export */   "TAGS_MENU": () => (/* binding */ TAGS_MENU),
/* harmony export */   "REPORTS_MENU": () => (/* binding */ REPORTS_MENU),
/* harmony export */   "QUESTIONS_MENU": () => (/* binding */ QUESTIONS_MENU),
/* harmony export */   "ELIMINATION_MENU": () => (/* binding */ ELIMINATION_MENU)
/* harmony export */ });
var TEXT_MENU = "Text";
var RULES_MENU = "Rules";
var RAW_SCORE_MENU = "Raw Score";
var PROFILES_MENU = "Profiles";
var TAGS_MENU = "Tags";
var REPORTS_MENU = "Reports";
var QUESTIONS_MENU = "Questions";
var ELIMINATION_MENU = "Elimination";

/***/ }),

/***/ "./resources/js/Shared/Components/SecondaryNavigation.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Shared/Components/SecondaryNavigation.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Pages/Talent/TalentMenu */ "./resources/js/Pages/Talent/TalentMenu.jsx");
/* harmony import */ var _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/MainMenuNames */ "./resources/js/Shared/Components/MainMenuNames.jsx");
/* harmony import */ var _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Career/CareerMenu */ "./resources/js/Pages/Career/CareerMenu.jsx");
/* harmony import */ var _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Pages/Logiq/LogiqMenu */ "./resources/js/Pages/Logiq/LogiqMenu.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var SecondaryNavigation = function SecondaryNavigation() {
  var secondaryNavigation = [];

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.usePage)(),
      _usePage$props$metaDa = _usePage.props.metaData,
      selectedMainMenu = _usePage$props$metaDa.selectedMainMenu,
      selectedSubMenu = _usePage$props$metaDa.selectedSubMenu;

  switch (selectedMainMenu) {
    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.TALENT_MENU:
      secondaryNavigation = _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_1__["default"];
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.CAREER_MENU:
      secondaryNavigation = _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_3__["default"];
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.LOGIQ_MENU:
      secondaryNavigation = _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_4__["default"];
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SCREEN_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.FIVE_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SETTINGS_MENU:
      break;
  }

  secondaryNavigation.filter(function (nav) {
    nav.active = nav.name === selectedSubMenu;
  });
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("aside", {
    className: "hidden lg:order-first lg:block lg:flex-shrink-0",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
      className: "relative flex h-full w-60 flex-col overflow-y-auto border-r border-gray-200 bg-white",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
        className: "mt-3",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("div", {
          className: "mt-1 space-y-2 px-3",
          role: "group",
          "aria-labelledby": "mobile-teams-headline",
          children: secondaryNavigation.map(function (nav) {
            return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsxs)("a", {
              href: nav.href,
              className: classNames(nav.active ? "bg-violet-100 text-violet-700 shadow-sm" : "text-gray-500 hover:bg-violet-100 hover:text-violet-700", "group flex items-center rounded-md px-3 py-2 text-base font-medium leading-5"),
              children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                className: classNames(nav.active ? "bg-green-100" : "bg-gray-100", "flex h-4 w-4 items-center justify-center rounded-full"),
                "aria-hidden": "true",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                  className: classNames(nav.active ? "bg-green-400" : "bg-gray-400", "h-2 w-2 rounded-full")
                })
              }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_5__.jsx)("span", {
                className: "ml-2 truncate",
                children: nav.name
              })]
            }, nav.name);
          })
        })
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SecondaryNavigation);

/***/ }),

/***/ "./resources/js/Shared/Components/SlideOverForm.jsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/SlideOverForm.jsx ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Shared_Components_CommonTextFormFields__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/CommonTextFormFields */ "./resources/js/Shared/Components/CommonTextFormFields.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var SlideOverForm = function SlideOverForm(_ref) {
  var formFields = _ref.formFields,
      closeSlideOver = _ref.closeSlideOver,
      title = _ref.title,
      description = _ref.description,
      openNotification = _ref.openNotification;
  //get the initial state from the default value of the form fields
  var initialState = formFields.reduce(function (fields, key) {
    return _objectSpread(_objectSpread({}, fields), {}, _defineProperty({}, key.fieldName, key.value));
  }, {});

  var _useForm = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.useForm)(initialState),
      data = _useForm.data,
      setData = _useForm.setData,
      processing = _useForm.processing,
      errors = _useForm.errors,
      post = _useForm.post,
      reset = _useForm.reset;

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    post(route("balltext.store"), {
      onFinish: function onFinish() {
        return handleOnSubmitSuccess();
      }
    });
  };

  var handleOnSubmitSuccess = function handleOnSubmitSuccess() {
    reset("textId", "actualText", "priority", "style");
    closeSlideOver();
    openNotification();
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("form", {
      className: "flex h-full flex-col divide-y divide-gray-200 bg-white shadow-xl",
      onSubmit: handleSubmit,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "h-0 flex-1 overflow-y-auto",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          className: "bg-violet-700 py-6 px-4 sm:px-6",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "flex items-center justify-between",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Dialog.Title, {
              className: "text-lg font-medium text-white",
              children: title
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "ml-3 flex h-7 items-center",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("button", {
                type: "button",
                className: "rounded-md bg-violet-700 text-violet-200 hover:text-white focus:outline-none",
                onClick: function onClick() {
                  return closeSlideOver();
                },
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("span", {
                  className: "sr-only",
                  children: "Close panel"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.XIcon, {
                  className: "h-6 w-6",
                  "aria-hidden": "true"
                })]
              })
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "mt-1",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "text-sm text-white",
              children: description
            })
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Shared_Components_CommonTextFormFields__WEBPACK_IMPORTED_MODULE_2__["default"], {
          data: data,
          setData: setData
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "flex flex-shrink-0 justify-end px-4 py-4",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "button",
          className: "rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-violet-500 focus:ring-offset-2",
          onClick: function onClick() {
            return closeSlideOver();
          },
          children: "Cancel"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "submit",
          className: "ml-4 inline-flex justify-center rounded-md border border-transparent bg-violet-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-violet-700 focus:outline-none",
          disabled: processing,
          children: "Save"
        })]
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SlideOverForm);

/***/ }),

/***/ "./resources/js/Shared/Components/SlideOverLayout.jsx":
/*!************************************************************!*\
  !*** ./resources/js/Shared/Components/SlideOverLayout.jsx ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/InputField */ "./resources/js/Shared/Components/InputField.jsx");
/* harmony import */ var _Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Shared/Components/TextAreaField */ "./resources/js/Shared/Components/TextAreaField.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Shared_Components_Notification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Shared/Components/Notification */ "./resources/js/Shared/Components/Notification.jsx");
/* harmony import */ var _Shared_Components_SlideOverForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/Shared/Components/SlideOverForm */ "./resources/js/Shared/Components/SlideOverForm.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }













var SlideOverLayout = function SlideOverLayout(_ref) {
  var open = _ref.open,
      closeSlideOver = _ref.closeSlideOver,
      title = _ref.title,
      formFields = _ref.formFields,
      description = _ref.description;

  // prevent the modal from closing when outside the form is clicked
  var dummyClose = function dummyClose() {};

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__.usePage)(),
      _usePage$props$flash = _usePage.props.flash,
      success = _usePage$props$flash.success,
      error = _usePage$props$flash.error;

  var message = "";
  var messageType = "success";

  if (success) {
    message = success;
  }

  if (error) {
    message = error;
    messageType = "error";
  }

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      show = _useState2[0],
      setShow = _useState2[1];

  var closeNotification = function closeNotification() {
    setShow(false);
  };

  var openNotification = function openNotification() {
    setShow(true);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_8__.Transition.Root, {
      show: open,
      as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_9__.Dialog, {
        as: "div",
        className: "fixed inset-0 overflow-hidden",
        onClose: function onClose() {
          return dummyClose();
        },
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("div", {
          className: "absolute inset-0 overflow-hidden",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_9__.Dialog.Overlay, {
            className: "absolute inset-0"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)("div", {
            className: "fixed inset-y-0 right-0 flex max-w-full pl-16",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_8__.Transition.Child, {
              as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
              enter: "transform transition ease-in-out duration-500 sm:duration-700",
              enterFrom: "translate-x-full",
              enterTo: "translate-x-0",
              leave: "transform transition ease-in-out duration-500 sm:duration-700",
              leaveFrom: "translate-x-0",
              leaveTo: "translate-x-full",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)("div", {
                className: "mt-12 w-screen max-w-md",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_Shared_Components_SlideOverForm__WEBPACK_IMPORTED_MODULE_6__["default"], {
                  formFields: formFields,
                  closeSlideOver: closeSlideOver,
                  title: title,
                  description: description,
                  openNotification: openNotification
                })
              })
            })
          })]
        })
      })
    }), show && message && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_Shared_Components_Notification__WEBPACK_IMPORTED_MODULE_5__["default"], {
      show: show,
      title: message,
      closeNotification: closeNotification,
      messageType: messageType
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SlideOverLayout);

/***/ }),

/***/ "./resources/js/Shared/Components/TabContainer.jsx":
/*!*********************************************************!*\
  !*** ./resources/js/Shared/Components/TabContainer.jsx ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/TalentTextTab */ "./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx");
/* harmony import */ var _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Shared/Components/MainMenuNames */ "./resources/js/Shared/Components/MainMenuNames.jsx");
/* harmony import */ var _Pages_Talent_TalentMenu__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Talent/TalentMenu */ "./resources/js/Pages/Talent/TalentMenu.jsx");
/* harmony import */ var _Pages_Career_CareerMenu__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Pages/Career/CareerMenu */ "./resources/js/Pages/Career/CareerMenu.jsx");
/* harmony import */ var _Pages_Logiq_LogiqMenu__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Pages/Logiq/LogiqMenu */ "./resources/js/Pages/Logiq/LogiqMenu.jsx");
/* harmony import */ var _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/Shared/Components/SecondaryMenuNames */ "./resources/js/Shared/Components/SecondaryMenuNames.jsx");
/* harmony import */ var _Shared_Components_MobileTab__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @/Shared/Components/MobileTab */ "./resources/js/Shared/Components/MobileTab.jsx");
/* harmony import */ var _Shared_Components_DesktopTab__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @/Shared/Components/DesktopTab */ "./resources/js/Shared/Components/DesktopTab.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");












function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(" ");
}

var TabContainer = function TabContainer() {
  var currentPageTabs = [];

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_0__.usePage)(),
      _usePage$props$metaDa = _usePage.props.metaData,
      selectedMainMenu = _usePage$props$metaDa.selectedMainMenu,
      selectedSubMenu = _usePage$props$metaDa.selectedSubMenu;

  switch (selectedMainMenu) {
    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.TALENT_MENU:
      switch (selectedSubMenu) {
        case _Shared_Components_SecondaryMenuNames__WEBPACK_IMPORTED_MODULE_6__.TEXT_MENU:
          currentPageTabs = _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_1__["default"];
          break;
      }

      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.CAREER_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.LOGIQ_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SCREEN_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.FIVE_MENU:
      break;

    case _Shared_Components_MainMenuNames__WEBPACK_IMPORTED_MODULE_2__.SETTINGS_MENU:
      break;
  } //mark the selected tab


  currentPageTabs.map(function (tab) {
    tab.current = !!route().current(tab.href + "*");
  });
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)("div", {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("div", {
      children: [currentPageTabs.length > 0 && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsxs)("div", {
        className: "sm:hidden",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)("label", {
          htmlFor: "tabs",
          className: "sr-only",
          children: "Select a tab"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_MobileTab__WEBPACK_IMPORTED_MODULE_7__["default"], {
          tabs: currentPageTabs
        })]
      }), currentPageTabs.length > 0 && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)("div", {
        className: "hidden sm:block",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_9__.jsx)(_Shared_Components_DesktopTab__WEBPACK_IMPORTED_MODULE_8__["default"], {
          tabs: currentPageTabs
        })
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TabContainer);

/***/ }),

/***/ "./resources/js/Shared/Components/TemplateContainer.jsx":
/*!**************************************************************!*\
  !*** ./resources/js/Shared/Components/TemplateContainer.jsx ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Shared_Components_SearchWithSortForm__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Shared/Components/SearchWithSortForm */ "./resources/js/Shared/Components/SearchWithSortForm.jsx");
/* harmony import */ var _Shared_Components_SlideOverLayout__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Shared/Components/SlideOverLayout */ "./resources/js/Shared/Components/SlideOverLayout.jsx");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }







var sortOptions = [{
  name: "ID",
  href: "#",
  current: true
}, {
  name: "Text",
  href: "#",
  current: false
}, {
  name: "Priority",
  href: "#",
  current: false
}];

var TemplateContainer = function TemplateContainer(_ref) {
  var children = _ref.children,
      title = _ref.title,
      formFields = _ref.formFields;

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_2__.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      openSlideOver = _useState2[0],
      setOpenSlideOver = _useState2[1];

  var closeSlideOver = function closeSlideOver() {
    setOpenSlideOver(false);
  };

  var createTitle = "New ".concat(title);
  var createDescription = "Get started by filling in the information below to create a new ".concat(title, ".");
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
      className: "mt-3 border-b border-gray-200 px-3 pb-5 sm:flex sm:items-center sm:justify-between",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("h3", {
        className: "text-lg font-medium leading-6 text-gray-600",
        children: title
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "mt-3 flex sm:mt-0 sm:ml-4",
        children: [title !== "Settings" && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Shared_Components_SearchWithSortForm__WEBPACK_IMPORTED_MODULE_0__["default"], {
          title: title,
          sortOptions: sortOptions
        }), title !== "Settings" && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "button",
          className: "ml-3 inline-flex items-center rounded-md border border-gray-300 bg-white px-4 py-2 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none",
          children: "Simulate"
        }), title !== "Settings" && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "button",
          className: "ml-3 inline-flex items-center rounded-md border border-transparent bg-violet-600 px-4 py-2 text-sm font-medium text-white shadow-sm hover:bg-violet-700 focus:outline-none",
          onClick: function onClick() {
            return setOpenSlideOver(true);
          },
          children: "Create"
        })]
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
      className: "mt-1",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("main", {
        className: "flex-1",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
          className: "relative mx-auto max-w-4xl md:px-8 xl:px-0",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "pt-10 pb-16",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "px-4 sm:px-6 md:px-0",
              children: children
            })
          })
        })
      }), title !== "Settings" && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Shared_Components_SlideOverLayout__WEBPACK_IMPORTED_MODULE_1__["default"], {
        open: openSlideOver,
        closeSlideOver: closeSlideOver,
        title: createTitle,
        formFields: formFields,
        description: createDescription
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TemplateContainer);

/***/ }),

/***/ "./resources/js/Shared/Components/TextAreaField.jsx":
/*!**********************************************************!*\
  !*** ./resources/js/Shared/Components/TextAreaField.jsx ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var TextAreaField = function TextAreaField(_ref) {
  var label = _ref.label,
      fieldName = _ref.fieldName,
      rows = _ref.rows,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: "description",
      className: "block text-sm font-medium text-gray-900",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("textarea", {
        id: fieldName,
        name: fieldName,
        rows: rows,
        className: "block w-full shadow-sm sm:text-sm focus:ring-violet-500 focus:border-violet-500 border border-gray-300 rounded-md",
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TextAreaField);

/***/ }),

/***/ "./resources/js/Shared/Hooks/useTimeout.jsx":
/*!**************************************************!*\
  !*** ./resources/js/Shared/Hooks/useTimeout.jsx ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


var useTimeout = function useTimeout(callback, delay) {
  var savedCallback = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    savedCallback.current = callback;
  }, [callback]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    var tick = function tick() {
      savedCallback.current();
    };

    if (delay !== null) {
      var id = setTimeout(tick, delay);
      return function () {
        return clearTimeout(id);
      };
    }
  }, [delay]);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useTimeout);

/***/ })

}]);
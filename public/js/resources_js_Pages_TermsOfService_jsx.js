'use strict';
(self['webpackChunk'] = self['webpackChunk'] || []).push([
  ['resources_js_Pages_TermsOfService_jsx'],
  {
    /***/ './resources/js/Pages/TermsOfService.jsx':
      /*!***********************************************!*\
  !*** ./resources/js/Pages/TermsOfService.tsx ***!
  \***********************************************/
      /***/ (
        __unused_webpack_module,
        __webpack_exports__,
        __webpack_require__,
      ) => {
        __webpack_require__.r(__webpack_exports__);
        /* harmony export */ __webpack_require__.d(__webpack_exports__, {
          /* harmony export */ default: () => __WEBPACK_DEFAULT_EXPORT__,
          /* harmony export */
        });
        /* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ =
          __webpack_require__(
            /*! react/jsx-runtime */ './node_modules/react/jsx-runtime.js',
          );

        var TermsOfService = function TermsOfService(_ref) {
          var terms = _ref.terms;
          return /*#__PURE__*/ (0,
          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)('div', {
            className: 'relative py-16 bg-white overflow-hidden',
            children: [
              /*#__PURE__*/ (0,
              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('div', {
                className:
                  'hidden lg:block lg:absolute lg:inset-y-0 lg:h-full lg:w-full',
                children: /*#__PURE__*/ (0,
                react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)('div', {
                  className: 'relative h-full text-lg max-w-prose mx-auto',
                  'aria-hidden': 'true',
                  children: [
                    /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                      'svg',
                      {
                        className:
                          'absolute top-12 left-full transform translate-x-32',
                        width: 404,
                        height: 384,
                        fill: 'none',
                        viewBox: '0 0 404 384',
                        children: [
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'defs',
                            {
                              children: /*#__PURE__*/ (0,
                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                'pattern',
                                {
                                  id: '74b3fd99-0a6f-4271-bef2-e80eeafdf357',
                                  x: 0,
                                  y: 0,
                                  width: 20,
                                  height: 20,
                                  patternUnits: 'userSpaceOnUse',
                                  children: /*#__PURE__*/ (0,
                                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    'rect',
                                    {
                                      x: 0,
                                      y: 0,
                                      width: 4,
                                      height: 4,
                                      className: 'text-gray-200',
                                      fill: 'currentColor',
                                    },
                                  ),
                                },
                              ),
                            },
                          ),
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'rect',
                            {
                              width: 404,
                              height: 384,
                              fill: 'url(#74b3fd99-0a6f-4271-bef2-e80eeafdf357)',
                            },
                          ),
                        ],
                      },
                    ),
                    /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                      'svg',
                      {
                        className:
                          'absolute top-1/2 right-full transform -translate-y-1/2 -translate-x-32',
                        width: 404,
                        height: 384,
                        fill: 'none',
                        viewBox: '0 0 404 384',
                        children: [
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'defs',
                            {
                              children: /*#__PURE__*/ (0,
                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                'pattern',
                                {
                                  id: 'f210dbf6-a58d-4871-961e-36d5016a0f49',
                                  x: 0,
                                  y: 0,
                                  width: 20,
                                  height: 20,
                                  patternUnits: 'userSpaceOnUse',
                                  children: /*#__PURE__*/ (0,
                                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    'rect',
                                    {
                                      x: 0,
                                      y: 0,
                                      width: 4,
                                      height: 4,
                                      className: 'text-gray-200',
                                      fill: 'currentColor',
                                    },
                                  ),
                                },
                              ),
                            },
                          ),
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'rect',
                            {
                              width: 404,
                              height: 384,
                              fill: 'url(#f210dbf6-a58d-4871-961e-36d5016a0f49)',
                            },
                          ),
                        ],
                      },
                    ),
                    /*#__PURE__*/ (0,
                    react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                      'svg',
                      {
                        className:
                          'absolute bottom-12 left-full transform translate-x-32',
                        width: 404,
                        height: 384,
                        fill: 'none',
                        viewBox: '0 0 404 384',
                        children: [
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'defs',
                            {
                              children: /*#__PURE__*/ (0,
                              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                'pattern',
                                {
                                  id: 'd3eb07ae-5182-43e6-857d-35c643af9034',
                                  x: 0,
                                  y: 0,
                                  width: 20,
                                  height: 20,
                                  patternUnits: 'userSpaceOnUse',
                                  children: /*#__PURE__*/ (0,
                                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                                    'rect',
                                    {
                                      x: 0,
                                      y: 0,
                                      width: 4,
                                      height: 4,
                                      className: 'text-gray-200',
                                      fill: 'currentColor',
                                    },
                                  ),
                                },
                              ),
                            },
                          ),
                          /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'rect',
                            {
                              width: 404,
                              height: 384,
                              fill: 'url(#d3eb07ae-5182-43e6-857d-35c643af9034)',
                            },
                          ),
                        ],
                      },
                    ),
                  ],
                }),
              }),
              /*#__PURE__*/ (0,
              react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)('div', {
                className: 'relative px-4 sm:px-6 lg:px-8',
                children: [
                  /*#__PURE__*/ (0,
                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)('div', {
                    className: 'text-lg max-w-prose mx-auto',
                    children: [
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                        'h1',
                        {
                          children: /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'span',
                            {
                              className:
                                'mt-2 block text-3xl text-center leading-8 font-extrabold tracking-tight text-gray-900 sm:text-4xl',
                              children: 'Terms of Service',
                            },
                          ),
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('p', {
                        className: 'mt-8 text-xl text-gray-500 leading-8',
                        children:
                          'Aliquet nec orci mattis amet quisque ullamcorper neque, nibh sem. At arcu, sit dui mi, nibh dui, diam eget aliquam. Quisque id at vitae feugiat egestas ac. Diam nulla orci at in viverra scelerisque eget. Eleifend egestas fringilla sapien.',
                      }),
                    ],
                  }),
                  /*#__PURE__*/ (0,
                  react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)('div', {
                    className:
                      'mt-6 prose prose-indigo prose-lg text-gray-500 mx-auto',
                    children: [
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        'p',
                        {
                          children: [
                            'Faucibus commodo massa rhoncus, volutpat. ',
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'strong',
                              {
                                children: 'Dignissim',
                              },
                            ),
                            ' sed ',
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'strong',
                              {
                                children: 'eget risus enim',
                              },
                            ),
                            '. Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit. Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim.',
                            ' ',
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'a',
                              {
                                href: '#',
                                children: 'Mattis mauris semper',
                              },
                            ),
                            ' sed amet vitae sed turpis id.',
                          ],
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        'ul',
                        {
                          role: 'list',
                          children: [
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'li',
                              {
                                children:
                                  'Quis elit egestas venenatis mattis dignissim.',
                              },
                            ),
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'li',
                              {
                                children:
                                  'Cras cras lobortis vitae vivamus ultricies facilisis tempus.',
                              },
                            ),
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'li',
                              {
                                children:
                                  'Orci in sit morbi dignissim metus diam arcu pretium.',
                              },
                            ),
                          ],
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('p', {
                        children:
                          'Quis semper vulputate aliquam venenatis egestas sagittis quisque orci. Donec commodo sit viverra aliquam porttitor ultrices gravida eu. Tincidunt leo, elementum mattis elementum ut nisl, justo, amet, mattis. Nunc purus, diam commodo tincidunt turpis. Amet, duis sed elit interdum dignissim.',
                      }),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                        'h2',
                        {
                          children: 'From beginner to expert in 30 days',
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('p', {
                        children:
                          'Id orci tellus laoreet id ac. Dolor, aenean leo, ac etiam consequat in. Convallis arcu ipsum urna nibh. Pharetra, euismod vitae interdum mauris enim, consequat vulputate nibh. Maecenas pellentesque id sed tellus mauris, ultrices mauris. Tincidunt enim cursus ridiculus mi. Pellentesque nam sed nullam sed diam turpis ipsum eu a sed convallis diam.',
                      }),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                        'blockquote',
                        {
                          children: /*#__PURE__*/ (0,
                          react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                            'p',
                            {
                              children:
                                'Sagittis scelerisque nulla cursus in enim consectetur quam. Dictum urna sed consectetur neque tristique pellentesque. Blandit amet, sed aenean erat arcu morbi.',
                            },
                          ),
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('p', {
                        children:
                          'Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim. Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit.',
                      }),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        'figure',
                        {
                          children: [
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'img',
                              {
                                className: 'w-full rounded-lg',
                                src: 'https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&auto=format&fit=facearea&w=1310&h=873&q=80&facepad=3',
                                alt: '',
                                width: 1310,
                                height: 873,
                              },
                            ),
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'figcaption',
                              {
                                children:
                                  'Sagittis scelerisque nulla cursus in enim consectetur quam.',
                              },
                            ),
                          ],
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                        'h2',
                        {
                          children: 'Everything you need to get up and running',
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)(
                        'p',
                        {
                          children: [
                            'Purus morbi dignissim senectus mattis ',
                            /*#__PURE__*/ (0,
                            react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(
                              'a',
                              {
                                href: '#',
                                children: 'adipiscing',
                              },
                            ),
                            '. Amet, massa quam varius orci dapibus volutpat cras. In amet eu ridiculus leo sodales cursus tristique. Tincidunt sed tempus ut viverra ridiculus non molestie. Gravida quis fringilla amet eget dui tempor dignissim. Facilisis auctor venenatis varius nunc, congue erat ac. Cras fermentum convallis quam.',
                          ],
                        },
                      ),
                      /*#__PURE__*/ (0,
                      react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)('p', {
                        children:
                          'Faucibus commodo massa rhoncus, volutpat. Dignissim sed eget risus enim. Mattis mauris semper sed amet vitae sed turpis id. Id dolor praesent donec est. Odio penatibus risus viverra tellus varius sit neque erat velit.',
                      }),
                    ],
                  }),
                ],
              }),
            ],
          });
        };

        /* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ =
          TermsOfService;

        /***/
      },
  },
]);

"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Talent_EvaluationText_ControlPoint_Components_PointTextForm_jsx"],{

/***/ "./resources/js/Pages/Shared/Components/InputField.jsx":
/*!*************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/InputField.jsx ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var InputField = function InputField(_ref) {
  var fieldName = _ref.fieldName,
      label = _ref.label,
      required = _ref.required,
      type = _ref.type,
      id = _ref.id,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "relative border border-gray-300 rounded-md px-3 py-2 shadow-sm focus-within:ring-1 focus-within:ring-indigo-600 focus-within:border-indigo-600",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: fieldName,
      className: "absolute -top-2 left-2 -mt-px inline-block px-1 bg-white text-xs font-medium text-gray-900",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
      type: type,
      name: fieldName,
      id: id,
      className: "block w-full border-0 p-0 text-gray-900 placeholder-gray-500 focus:ring-0 sm:text-sm",
      required: required,
      value: data[fieldName],
      onChange: function onChange(e) {
        return setData(fieldName, e.target.value);
      }
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (InputField);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/TextAreaField.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/TextAreaField.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var TextAreaField = function TextAreaField(_ref) {
  var label = _ref.label,
      fieldName = _ref.fieldName,
      rows = _ref.rows,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: "description",
      className: "block text-sm font-medium text-gray-900",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("textarea", {
        id: fieldName,
        name: fieldName,
        rows: rows,
        className: "block w-full shadow-sm sm:text-sm focus:ring-violet-500 focus:border-violet-500 border border-gray-300 rounded-md",
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TextAreaField);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/PointTextForm.jsx":
/*!********************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/PointTextForm.jsx ***!
  \********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Pages/Shared/Components/InputField */ "./resources/js/Pages/Shared/Components/InputField.jsx");
/* harmony import */ var _Pages_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Pages/Shared/Components/TextAreaField */ "./resources/js/Pages/Shared/Components/TextAreaField.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");





var PointTextForm = function PointTextForm(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
    className: "flex-1 flex flex-col justify-between",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)("div", {
      className: "px-4 divide-y divide-gray-200 sm:px-6",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsxs)("div", {
        className: "space-y-8 pt-6 pb-6",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "textId",
          label: "Text Id",
          required: true,
          type: "text",
          id: "textId",
          data: data,
          setData: setData
        }, "textId"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_Pages_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__["default"], {
          label: "Text",
          fieldName: "actualText",
          rows: "actualText",
          data: data,
          setData: setData,
          required: true
        }, "actualText"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)(_Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "priority",
          label: "Priority",
          required: true,
          type: "number",
          id: "priority",
          data: data,
          setData: setData
        }, "priority")]
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PointTextForm);

/***/ })

}]);
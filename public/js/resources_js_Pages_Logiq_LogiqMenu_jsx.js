"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Logiq_LogiqMenu_jsx"],{

/***/ "./resources/js/Pages/Logiq/LogiqMenu.jsx":
/*!************************************************!*\
  !*** ./resources/js/Pages/Logiq/LogiqMenu.jsx ***!
  \************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var LogiqMenu = [{
  name: 'Norm Groups',
  href: '#',
  active: false
}, {
  name: 'Score Config',
  href: '#',
  active: false
}, {
  name: 'Test List',
  href: '#',
  active: false
}, {
  name: 'Traits',
  href: '#',
  active: false
}, {
  name: 'Questions',
  href: '#',
  active: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (LogiqMenu);

/***/ })

}]);
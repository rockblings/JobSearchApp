"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Components_SlideOverLayout_jsx"],{

/***/ "./resources/js/Pages/Shared/Components/Checkbox.jsx":
/*!***********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Checkbox.jsx ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var Checkbox = function Checkbox(_ref) {
  var id = _ref.id,
      label = _ref.label,
      data = _ref.data,
      setData = _ref.setData,
      name = _ref.name,
      describedby = _ref.describedby;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "relative flex items-start",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "flex h-5 items-center",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
        id: id,
        "aria-describedby": describedby,
        name: name,
        type: "checkbox",
        defaultChecked: data[name],
        value: data[name],
        onChange: function onChange(e) {
          return setData(name, e.target.checked);
        },
        className: "h-4 w-4 rounded border-gray-300 text-violet-600 focus:ring-violet-500"
      })
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "ml-3 text-sm",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
        htmlFor: id,
        className: "font-medium text-gray-700",
        children: label
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Checkbox);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/CommonTextFormFields.jsx":
/*!***********************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/CommonTextFormFields.jsx ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Pages/Shared/Components/InputField */ "./resources/js/Pages/Shared/Components/InputField.jsx");
/* harmony import */ var _Pages_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @/Pages/Shared/Components/TextAreaField */ "./resources/js/Pages/Shared/Components/TextAreaField.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/TalentTextTab */ "./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx");
/* harmony import */ var _Pages_Talent_EvaluationText_PointText_Components_PointTextType__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/PointText/Components/PointTextType */ "./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx");
/* harmony import */ var _Pages_Talent_EvaluationText_ControlPoint_Components_ControlPointLevels__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels */ "./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");









var CommonTextFormFields = function CommonTextFormFields(_ref) {
  var data = _ref.data,
      setData = _ref.setData;

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_2__.usePage)(),
      selectedTab = _usePage.props.metaData.selectedTab;

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)("div", {
    className: "flex flex-1 flex-col justify-between",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)("div", {
      className: "divide-y divide-gray-200 px-4 sm:px-6",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsxs)("div", {
        className: "space-y-8 pt-6 pb-6",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "textId",
          label: "Text Id",
          required: true,
          type: "text",
          id: "textId",
          data: data,
          setData: setData
        }, "textId"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_1__["default"], {
          label: "Text",
          fieldName: "actualText",
          rows: 6,
          data: data,
          setData: setData,
          required: true
        }, "actualText"), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "priority",
          label: "Priority",
          required: true,
          type: "number",
          id: "priority",
          data: data,
          setData: setData
        }, "priority"), selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.POINTTEXT_NAME && selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.INTERVIEWQUESTION_NAME && selectedTab !== _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.CONTROLPOINT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_0__["default"], {
          fieldName: "style",
          label: "Style",
          required: true,
          type: "text",
          id: "style",
          data: data,
          setData: setData
        }, "style"), selectedTab === _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.POINTTEXT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Talent_EvaluationText_PointText_Components_PointTextType__WEBPACK_IMPORTED_MODULE_4__["default"], {
          data: data,
          setData: setData
        }), selectedTab === _Pages_Talent_EvaluationText_TalentTextTab__WEBPACK_IMPORTED_MODULE_3__.CONTROLPOINT_NAME && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_6__.jsx)(_Pages_Talent_EvaluationText_ControlPoint_Components_ControlPointLevels__WEBPACK_IMPORTED_MODULE_5__["default"], {
          data: data,
          setData: setData
        })]
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (CommonTextFormFields);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/InputField.jsx":
/*!*************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/InputField.jsx ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var InputField = function InputField(_ref) {
  var fieldName = _ref.fieldName,
      label = _ref.label,
      required = _ref.required,
      type = _ref.type,
      id = _ref.id,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: fieldName,
      className: "block text-sm font-medium text-gray-700",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
        type: type,
        name: fieldName,
        id: id,
        className: "shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md",
        required: required,
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (InputField);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/Notification.jsx":
/*!***************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Notification.jsx ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _Pages_Shared_Hooks_useTimeout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/Hooks/useTimeout */ "./resources/js/Pages/Shared/Hooks/useTimeout.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");








var Notification = function Notification(_ref) {
  var show = _ref.show,
      title = _ref.title,
      body = _ref.body,
      closeNotification = _ref.closeNotification,
      messageType = _ref.messageType;
  // remove the notification after 3 seconds
  (0,_Pages_Shared_Hooks_useTimeout__WEBPACK_IMPORTED_MODULE_2__["default"])(function () {
    closeNotification();
  }, 3000);
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
      "aria-live": "assertive",
      className: "fixed inset-0 flex items-end px-4 py-6 pointer-events-none sm:p-6 sm:items-start",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
        className: "mt-10 w-full flex flex-col items-center space-y-6 sm:items-end",
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Transition, {
          show: show,
          as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
          enter: "transform ease-out duration-300 transition",
          enterFrom: "translate-y-2 opacity-0 sm:translate-y-0 sm:translate-x-2",
          enterTo: "translate-y-0 opacity-100 sm:translate-x-0",
          leave: "transition ease-in duration-100",
          leaveFrom: "opacity-100",
          leaveTo: "opacity-0",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "max-w-sm w-full bg-violet-800 shadow-lg rounded-lg pointer-events-auto ring-1 ring-black ring-opacity-5 overflow-hidden",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "p-4",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                className: "flex items-start",
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
                  className: "flex-shrink-0",
                  children: messageType === 'error' ? /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.ExclamationCircleIcon, {
                    className: "h-6 w-6 text-white",
                    "aria-hidden": "true"
                  }) : /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__.CheckCircleIcon, {
                    className: "h-6 w-6 text-white",
                    "aria-hidden": "true"
                  })
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
                  className: "ml-3 w-0 flex-1 pt-0.5",
                  children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
                    className: "text-sm font-medium text-white",
                    children: title
                  }), body && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
                    className: "mt-1 text-sm text-white",
                    children: body
                  })]
                })]
              })
            })
          })
        })
      })
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Notification);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/Radiobox.jsx":
/*!***********************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/Radiobox.jsx ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var Radiobox = function Radiobox(_ref) {
  var id = _ref.id,
      label = _ref.label,
      data = _ref.data,
      setData = _ref.setData,
      name = _ref.name;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    className: "flex items-center",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("input", {
      id: id,
      name: name,
      type: "radio",
      defaultChecked: data[id],
      value: data[id],
      onChange: function onChange(e) {
        return setData(name, e.target.checked);
      },
      className: "h-4 w-4 border-gray-300 text-violet-600 focus:ring-violet-500"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: id,
      className: "ml-3 block text-sm font-medium text-gray-700",
      children: label
    })]
  }, id);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Radiobox);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/SlideOverForm.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/SlideOverForm.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Shared_Components_CommonTextFormFields__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/Components/CommonTextFormFields */ "./resources/js/Pages/Shared/Components/CommonTextFormFields.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }









var SlideOverForm = function SlideOverForm(_ref) {
  var formFields = _ref.formFields,
      closeSlideOver = _ref.closeSlideOver,
      title = _ref.title,
      description = _ref.description,
      openNotification = _ref.openNotification;
  //get the initial state from the default value of the form fields
  var initialState = formFields.reduce(function (fields, key) {
    return _objectSpread(_objectSpread({}, fields), {}, _defineProperty({}, key.fieldName, key.value));
  }, {});

  var _useForm = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.useForm)(initialState),
      data = _useForm.data,
      setData = _useForm.setData,
      processing = _useForm.processing,
      errors = _useForm.errors,
      post = _useForm.post,
      reset = _useForm.reset;

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    post(route("balltext.store"), {
      onFinish: function onFinish() {
        return handleOnSubmitSuccess();
      }
    });
  };

  var handleOnSubmitSuccess = function handleOnSubmitSuccess() {
    reset("textId", "actualText", "priority", "style");
    closeSlideOver();
    openNotification();
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.Fragment, {
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("form", {
      className: "flex h-full flex-col divide-y divide-gray-200 bg-white shadow-xl",
      onSubmit: handleSubmit,
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "h-0 flex-1 overflow-y-auto",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          className: "bg-violet-700 py-6 px-4 sm:px-6",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "flex items-center justify-between",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_4__.Dialog.Title, {
              className: "text-lg font-medium text-white",
              children: title
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "ml-3 flex h-7 items-center",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("button", {
                type: "button",
                className: "rounded-md bg-violet-700 text-violet-200 hover:text-white focus:outline-none",
                onClick: function onClick() {
                  return closeSlideOver();
                },
                children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("span", {
                  className: "sr-only",
                  children: "Close panel"
                }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_heroicons_react_outline__WEBPACK_IMPORTED_MODULE_0__.XIcon, {
                  className: "h-6 w-6",
                  "aria-hidden": "true"
                })]
              })
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
            className: "mt-1",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "text-sm text-white",
              children: description
            })
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Pages_Shared_Components_CommonTextFormFields__WEBPACK_IMPORTED_MODULE_2__["default"], {
          data: data,
          setData: setData
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "flex flex-shrink-0 justify-end px-4 py-4",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "button",
          className: "rounded-md border border-gray-300 bg-white py-2 px-4 text-sm font-medium text-gray-700 shadow-sm hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-violet-500 focus:ring-offset-2",
          onClick: function onClick() {
            return closeSlideOver();
          },
          children: "Cancel"
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "submit",
          className: "ml-4 inline-flex justify-center rounded-md border border-transparent bg-violet-600 py-2 px-4 text-sm font-medium text-white shadow-sm hover:bg-violet-700 focus:outline-none",
          disabled: processing,
          children: "Save"
        })]
      })]
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SlideOverForm);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/SlideOverLayout.jsx":
/*!******************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/SlideOverLayout.jsx ***!
  \******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/transitions/transition.esm.js");
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/dialog/dialog.esm.js");
/* harmony import */ var _heroicons_react_outline__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @heroicons/react/outline */ "./node_modules/@heroicons/react/outline/esm/index.js");
/* harmony import */ var _Pages_Shared_Components_InputField__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/Components/InputField */ "./resources/js/Pages/Shared/Components/InputField.jsx");
/* harmony import */ var _Pages_Shared_Components_TextAreaField__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @/Pages/Shared/Components/TextAreaField */ "./resources/js/Pages/Shared/Components/TextAreaField.jsx");
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Shared_Components_Notification__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @/Pages/Shared/Components/Notification */ "./resources/js/Pages/Shared/Components/Notification.jsx");
/* harmony import */ var _Pages_Shared_Components_SlideOverForm__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @/Pages/Shared/Components/SlideOverForm */ "./resources/js/Pages/Shared/Components/SlideOverForm.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }













var SlideOverLayout = function SlideOverLayout(_ref) {
  var open = _ref.open,
      closeSlideOver = _ref.closeSlideOver,
      title = _ref.title,
      formFields = _ref.formFields,
      description = _ref.description;

  // prevent the modal from closing when outside the form is clicked
  var dummyClose = function dummyClose() {};

  var _usePage = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_4__.usePage)(),
      _usePage$props$flash = _usePage.props.flash,
      success = _usePage$props$flash.success,
      error = _usePage$props$flash.error;

  var message = '';
  var messageType = 'success';

  if (success) {
    message = success;
  }

  if (error) {
    message = error;
    messageType = 'error';
  }

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      show = _useState2[0],
      setShow = _useState2[1];

  var closeNotification = function closeNotification() {
    setShow(false);
  };

  var openNotification = function openNotification() {
    setShow(true);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)(react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.Fragment, {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_8__.Transition.Root, {
      show: open,
      as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_9__.Dialog, {
        as: "div",
        className: "fixed inset-0 overflow-hidden",
        onClose: function onClose() {
          return dummyClose();
        },
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsxs)("div", {
          className: "absolute inset-0 overflow-hidden",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_9__.Dialog.Overlay, {
            className: "absolute inset-0"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)("div", {
            className: "fixed inset-y-0 pl-16 max-w-full right-0 flex",
            children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_8__.Transition.Child, {
              as: react__WEBPACK_IMPORTED_MODULE_0__.Fragment,
              enter: "transform transition ease-in-out duration-500 sm:duration-700",
              enterFrom: "translate-x-full",
              enterTo: "translate-x-0",
              leave: "transform transition ease-in-out duration-500 sm:duration-700",
              leaveFrom: "translate-x-0",
              leaveTo: "translate-x-full",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)("div", {
                className: "w-screen max-w-md mt-12",
                children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_Pages_Shared_Components_SlideOverForm__WEBPACK_IMPORTED_MODULE_6__["default"], {
                  formFields: formFields,
                  closeSlideOver: closeSlideOver,
                  title: title,
                  description: description,
                  openNotification: openNotification
                })
              })
            })
          })]
        })
      })
    }), show && message && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_7__.jsx)(_Pages_Shared_Components_Notification__WEBPACK_IMPORTED_MODULE_5__["default"], {
      show: show,
      title: message,
      closeNotification: closeNotification,
      messageType: messageType
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SlideOverLayout);

/***/ }),

/***/ "./resources/js/Pages/Shared/Components/TextAreaField.jsx":
/*!****************************************************************!*\
  !*** ./resources/js/Pages/Shared/Components/TextAreaField.jsx ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var TextAreaField = function TextAreaField(_ref) {
  var label = _ref.label,
      fieldName = _ref.fieldName,
      rows = _ref.rows,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: "description",
      className: "block text-sm font-medium text-gray-900",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("textarea", {
        id: fieldName,
        name: fieldName,
        rows: rows,
        className: "block w-full shadow-sm sm:text-sm focus:ring-violet-500 focus:border-violet-500 border border-gray-300 rounded-md",
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TextAreaField);

/***/ }),

/***/ "./resources/js/Pages/Shared/Hooks/useTimeout.jsx":
/*!********************************************************!*\
  !*** ./resources/js/Pages/Shared/Hooks/useTimeout.jsx ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");


var useTimeout = function useTimeout(callback, delay) {
  var savedCallback = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    savedCallback.current = callback;
  }, [callback]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    var tick = function tick() {
      savedCallback.current();
    };

    if (delay !== null) {
      var id = setTimeout(tick, delay);
      return function () {
        return clearTimeout(id);
      };
    }
  }, [delay]);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useTimeout);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx":
/*!*************************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/ControlPoint/Components/ControlPointLevels.jsx ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Pages_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Pages/Shared/Components/Checkbox */ "./resources/js/Pages/Shared/Components/Checkbox.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




var ControlPointLevels = function ControlPointLevels(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("fieldset", {
    className: "space-y-5",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("legend", {
      className: "sr-only",
      children: "Control Point Levels"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelZero",
      label: "Level 0",
      data: data,
      setData: setData,
      name: "levelZero",
      describedby: "level-zero"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelOne",
      label: "Level 1",
      data: data,
      setData: setData,
      name: "levelOne",
      describedby: "level-one"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelTwo",
      label: "Level 2",
      data: data,
      setData: setData,
      name: "levelTwo",
      describedby: "level-two"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Checkbox__WEBPACK_IMPORTED_MODULE_0__["default"], {
      id: "levelThree",
      label: "Level 3",
      data: data,
      setData: setData,
      name: "levelThree",
      describedby: "level-three"
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (ControlPointLevels);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx":
/*!*****************************************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/PointText/Components/PointTextType.jsx ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _Pages_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @/Pages/Shared/Components/Radiobox */ "./resources/js/Pages/Shared/Components/Radiobox.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");




var PointTextType = function PointTextType(_ref) {
  var data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("fieldset", {
    className: "mt-4",
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)("legend", {
      className: "sr-only",
      children: "Point Text Type"
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsxs)("div", {
      className: "space-y-4 sm:flex sm:items-center sm:space-y-0 sm:space-x-10",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "positive",
        label: "Positive",
        data: data,
        setData: setData,
        name: "pointTextType"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "neutral",
        label: "Neutral",
        data: data,
        setData: setData,
        name: "pointTextType"
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_1__.jsx)(_Pages_Shared_Components_Radiobox__WEBPACK_IMPORTED_MODULE_0__["default"], {
        id: "negative",
        label: "Negative",
        data: data,
        setData: setData,
        name: "pointTextType"
      })]
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (PointTextType);

/***/ }),

/***/ "./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx":
/*!********************************************************************!*\
  !*** ./resources/js/Pages/Talent/EvaluationText/TalentTextTab.jsx ***!
  \********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BALLTEXT_NAME": () => (/* binding */ BALLTEXT_NAME),
/* harmony export */   "POINTTEXT_NAME": () => (/* binding */ POINTTEXT_NAME),
/* harmony export */   "WORKSTYLE_NAME": () => (/* binding */ WORKSTYLE_NAME),
/* harmony export */   "REFERENCEGUIDE_NAME": () => (/* binding */ REFERENCEGUIDE_NAME),
/* harmony export */   "INTERVIEWGUIDE_NAME": () => (/* binding */ INTERVIEWGUIDE_NAME),
/* harmony export */   "CONTROLPOINT_NAME": () => (/* binding */ CONTROLPOINT_NAME),
/* harmony export */   "INTERVIEWQUESTION_NAME": () => (/* binding */ INTERVIEWQUESTION_NAME),
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
var BALLTEXT_NAME = "Ball Text";
var POINTTEXT_NAME = "Point Text";
var WORKSTYLE_NAME = "Work Style";
var REFERENCEGUIDE_NAME = "Reference Guide";
var INTERVIEWGUIDE_NAME = "Interview Guide";
var CONTROLPOINT_NAME = "Control Point";
var INTERVIEWQUESTION_NAME = "Interview Questions";
var BALLTEXT_URL = "balltext.index";
var POINTTEXT_URL = "pointtext.index";
var WORKSTYLE_URL = "workstyle.index";
var REFERENCEGUIDE_URL = "reference-guide.index";
var INTERVIEWGUIDE_URL = "interview-guide.index";
var CONTROLPOINT_URL = "control-point.index";
var INTERVIEWQUESTION_URL = "interview-question.index";
var TalentTextTab = [{
  name: BALLTEXT_NAME,
  href: BALLTEXT_URL,
  current: false
}, {
  name: POINTTEXT_NAME,
  href: POINTTEXT_URL,
  current: false
}, {
  name: WORKSTYLE_NAME,
  href: WORKSTYLE_URL,
  current: false
}, {
  name: REFERENCEGUIDE_NAME,
  href: REFERENCEGUIDE_URL,
  current: false
}, {
  name: INTERVIEWGUIDE_NAME,
  href: INTERVIEWGUIDE_URL,
  current: false
}, {
  name: CONTROLPOINT_NAME,
  href: CONTROLPOINT_URL,
  current: false
}, {
  name: INTERVIEWQUESTION_NAME,
  href: INTERVIEWQUESTION_URL,
  current: false
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TalentTextTab);

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_Layout_TextAreaField_jsx"],{

/***/ "./resources/js/Pages/Shared/Layout/TextAreaField.jsx":
/*!************************************************************!*\
  !*** ./resources/js/Pages/Shared/Layout/TextAreaField.jsx ***!
  \************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



var TextAreaField = function TextAreaField(_ref) {
  var label = _ref.label,
      fieldName = _ref.fieldName,
      rows = _ref.rows,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxs)("div", {
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("label", {
      htmlFor: "description",
      className: "block text-sm font-medium text-gray-900",
      children: label
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("div", {
      className: "mt-1",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)("textarea", {
        id: fieldName,
        name: fieldName,
        rows: rows,
        className: "block w-full shadow-sm sm:text-sm focus:ring-violet-500 focus:border-violet-500 border border-gray-300 rounded-md",
        value: data[fieldName],
        onChange: function onChange(e) {
          return setData(fieldName, e.target.value);
        }
      })
    })]
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (TextAreaField);

/***/ })

}]);
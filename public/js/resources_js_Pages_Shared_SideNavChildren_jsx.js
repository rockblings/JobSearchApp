"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Shared_SideNavChildren_jsx"],{

/***/ "./resources/js/Pages/Shared/SideNavChildren.jsx":
/*!*******************************************************!*\
  !*** ./resources/js/Pages/Shared/SideNavChildren.jsx ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var _headlessui_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @headlessui/react */ "./node_modules/@headlessui/react/dist/components/disclosure/disclosure.esm.js");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");



function classNames() {
  for (var _len = arguments.length, classes = new Array(_len), _key = 0; _key < _len; _key++) {
    classes[_key] = arguments[_key];
  }

  return classes.filter(Boolean).join(' ');
}

var SideNavChildren = function SideNavChildren(_ref) {
  var children = _ref.children;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_1__.Disclosure.Panel, {
    className: "space-y-1",
    "static": true,
    children: children.map(function (subItem) {
      return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_0__.jsx)(_headlessui_react__WEBPACK_IMPORTED_MODULE_1__.Disclosure.Button, {
        as: "a",
        href: subItem.href,
        className: classNames(subItem.active ? 'bg-violet-50 text-violet-900 hover:bg-violet-700 hover:text-white' : 'text-gray-600 hover:bg-violet-700 hover:text-white', 'group w-full flex items-center pl-2 pr-1 py-2 text-left text-sm font-medium rounded-md focus:outline-none'),
        children: subItem.name
      }, subItem.name);
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (SideNavChildren);

/***/ })

}]);
"use strict";
(self["webpackChunk"] = self["webpackChunk"] || []).push([["resources_js_Pages_Resume_WorkExperience_Create_jsx"],{

/***/ "./resources/js/Pages/Resume/WorkExperience/Create.jsx":
/*!*************************************************************!*\
  !*** ./resources/js/Pages/Resume/WorkExperience/Create.jsx ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/Components'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/Dashboard'"); e.code = 'MODULE_NOT_FOUND'; throw e; }());
/* harmony import */ var _inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @inertiajs/inertia-react */ "./node_modules/@inertiajs/inertia-react/dist/index.js");
/* harmony import */ var _Pages_Shared_MonthPicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @/Pages/Shared/MonthPicker */ "./resources/js/Pages/Shared/MonthPicker.jsx");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");







var Create = function Create() {
  var _useForm = (0,_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.useForm)({
    jobTitle: '',
    company: '',
    fromDate: new Date(),
    toDate: new Date(),
    city: '',
    country: '',
    summary: ''
  }),
      data = _useForm.data,
      setData = _useForm.setData,
      post = _useForm.post,
      processing = _useForm.processing,
      errors = _useForm.errors,
      reset = _useForm.reset;

  var handleSubmit = function handleSubmit(event) {
    event.preventDefault();
    reset('jobTitle', 'company', 'fromDate', 'toDate', 'city', 'country', 'summary');
    console.log(data);
  };

  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("form", {
    className: "space-y-8 divide-y divide-gray-200",
    onSubmit: handleSubmit,
    children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
      className: "space-y-8 divide-y divide-gray-200",
      children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "pt-8",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("h3", {
            className: "text-lg leading-6 font-medium text-gray-900",
            children: "Work Experience Information"
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
            className: "mt-1 text-sm text-gray-500",
            children: "Make sure all information are accurate before saving."
          })]
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          className: "mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "job-title",
              className: "block text-sm font-medium text-gray-700",
              children: "Job Title / Position"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                id: "job-title",
                name: "jobTitle",
                type: "text",
                autoComplete: "jobTitle",
                placeholder: "Job Title / Position",
                "aria-invalid": "true",
                "aria-describedby": "jobTitle-error",
                value: data.jobTitle,
                onChange: function onChange(e) {
                  return setData('jobTitle', e.target.value);
                },
                required: true,
                className: "shadow-sm focus:ring-fuchsia-500 focus:border-fuchsia-500 block w-full sm:text-sm border-gray-300 rounded-md"
              })
            }), errors.jobTitle && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "mt-2 text-sm text-red-600",
              id: "jobTitle-error",
              children: errors.jobTitle
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "company",
              className: "block text-sm font-medium text-gray-700",
              children: "Company"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                id: "company",
                name: "company",
                type: "text",
                autoComplete: "company",
                placeholder: "Company",
                "aria-invalid": "true",
                "aria-describedby": "company-error",
                value: data.company,
                onChange: function onChange(e) {
                  return setData('company', e.target.value);
                },
                required: true,
                className: "shadow-sm focus:ring-fuchsia-500 focus:border-fuchsia-500 block w-full sm:text-sm border-gray-300 rounded-md"
              })
            }), errors.company && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "mt-2 text-sm text-red-600",
              id: "company-error",
              children: errors.company
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "fromDate",
              className: "block text-sm font-medium text-gray-700",
              children: "From Date"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Pages_Shared_MonthPicker__WEBPACK_IMPORTED_MODULE_2__["default"], {
                id: "fromDate",
                name: "fromDate",
                data: data.fromDate,
                setData: setData
              })
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "toDate",
              className: "block text-sm font-medium text-gray-700",
              children: "To Date"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_Pages_Shared_MonthPicker__WEBPACK_IMPORTED_MODULE_2__["default"], {
                id: "toDate",
                name: "toDate",
                data: data.toDate,
                setData: setData
              })
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "city",
              className: "block text-sm font-medium text-gray-700",
              children: "City"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                id: "city",
                name: "city",
                type: "text",
                autoComplete: "city",
                placeholder: "City",
                "aria-invalid": "true",
                "aria-describedby": "city-error",
                value: data.city,
                onChange: function onChange(e) {
                  return setData('city', e.target.value);
                },
                required: true,
                className: "shadow-sm focus:ring-fuchsia-500 focus:border-fuchsia-500 block w-full sm:text-sm border-gray-300 rounded-md"
              })
            }), errors.city && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "mt-2 text-sm text-red-600",
              id: "city-error",
              children: errors.city
            })]
          }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-3",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "country",
              className: "block text-sm font-medium text-gray-700",
              children: "Country"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("input", {
                id: "country",
                name: "country",
                type: "text",
                autoComplete: "country",
                placeholder: "Country",
                "aria-invalid": "true",
                "aria-describedby": "country-error",
                value: data.country,
                onChange: function onChange(e) {
                  return setData('country', e.target.value);
                },
                required: true,
                className: "shadow-sm focus:ring-fuchsia-500 focus:border-fuchsia-500 block w-full sm:text-sm border-gray-300 rounded-md"
              })
            }), errors.country && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "mt-2 text-sm text-red-600",
              id: "country-error",
              children: errors.country
            })]
          })]
        })]
      }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
        children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
          className: "mt-6 grid grid-cols-1 gap-y-6 gap-x-4 sm:grid-cols-6",
          children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
            className: "sm:col-span-6",
            children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("label", {
              htmlFor: "summary",
              className: "block text-sm font-medium text-gray-700",
              children: "Work Experience Summary"
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
              className: "mt-1",
              children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("textarea", {
                id: "summary",
                name: "summary",
                rows: 3,
                className: "shadow-sm focus:ring-fuchsia-500 focus:border-fuchsia-500 block w-full sm:text-sm border border-gray-300 rounded-md",
                defaultValue: data.summary,
                onChange: function onChange(e) {
                  return setData('summary', e.target.value);
                }
              })
            }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
              className: "mt-2 text-sm text-gray-500",
              children: "Write a few sentences about what you did."
            })]
          }), errors.summary && /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("p", {
            className: "mt-2 text-sm text-red-600",
            id: "summary-error",
            children: errors.summary
          })]
        })
      })]
    }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
      className: "pt-5",
      children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsxs)("div", {
        className: "flex justify-end",
        children: [/*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("div", {
          className: "flex justify-end",
          children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(_inertiajs_inertia_react__WEBPACK_IMPORTED_MODULE_1__.Link, {
            as: "button",
            href: route('work-experience'),
            type: "button",
            className: "ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-gray-400 hover:bg-gray-900 focus:outline-none",
            children: "Cancel"
          })
        }), /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)("button", {
          type: "submit",
          className: "ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-fuchsia-700 hover:bg-fuchsia-400 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500",
          disabled: processing,
          children: "Save"
        })]
      })
    })]
  });
};

Create.layout = function (page) {
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/Components'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
    title: "Resume",
    selectedSideBarMenu: "Resume",
    children: /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_3__.jsx)(Object(function webpackMissingModule() { var e = new Error("Cannot find module '@/Pages/Shared/Dashboard'"); e.code = 'MODULE_NOT_FOUND'; throw e; }()), {
      children: page,
      pageTitle: "Add New: Work Experience"
    })
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Create);

/***/ }),

/***/ "./resources/js/Pages/Shared/MonthPicker.jsx":
/*!***************************************************!*\
  !*** ./resources/js/Pages/Shared/MonthPicker.jsx ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-datepicker */ "./node_modules/react-datepicker/dist/react-datepicker.min.js");
/* harmony import */ var react_datepicker__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_datepicker__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react_datepicker_dist_react_datepicker_css__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react-datepicker/dist/react-datepicker.css */ "./node_modules/react-datepicker/dist/react-datepicker.css");
/* harmony import */ var react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! react/jsx-runtime */ "./node_modules/react/jsx-runtime.js");





var MonthPicker = function MonthPicker(_ref) {
  var name = _ref.name,
      id = _ref.id,
      data = _ref.data,
      setData = _ref.setData;
  return /*#__PURE__*/(0,react_jsx_runtime__WEBPACK_IMPORTED_MODULE_2__.jsx)((react_datepicker__WEBPACK_IMPORTED_MODULE_3___default()), {
    selected: data,
    onChange: function onChange(date) {
      return setData(name, date);
    },
    locale: "sv-SE",
    dateFormat: "yyyy-MM",
    className: "shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md",
    showMonthYearPicker: true,
    name: name,
    id: id,
    value: data
  });
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (MonthPicker);

/***/ })

}]);

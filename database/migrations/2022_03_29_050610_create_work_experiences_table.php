<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('work_experiences', function (Blueprint $table) {
      $table->id();
      $table->uuid()->unique()->index();
      $table->string('job_title');
      $table->text('job_description');
      $table->string('company');
      $table->string('company_email')->nullable();
      $table->string('company_telephone')->nullable();
      $table->text('company_address')->nullable();
      $table->string('company_website')->nullable();
      $table->date('started_at');
      $table->date('finished_at')->nullable();
      $table->boolean('current')->default(false);
      $table->foreignId('user_id')->index()->constrained()->cascadeOnDelete();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('work_experiences');
  }
};

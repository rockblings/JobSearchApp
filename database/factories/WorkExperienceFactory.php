<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\WorkExperience>
 */
class WorkExperienceFactory extends Factory
{
  /**
   * Define the model's default state.
   * @return array<string, mixed>
   */
  public function definition()
  {
    return [
      'user_id' => User::factory()->create(),
      'job_title' => $this->faker->jobTitle(),
      'job_description' => $this->faker->sentences(4, true),
      'company' => $this->faker->company(),
      'company_email' => $this->faker->companyEmail(),
      'company_telephone' => $this->faker->phoneNumber(),
      'company_address' => $this->faker->address(),
      'company_website' => $this->faker->url(),
      'current' => $current = $this->faker->boolean(),
      'started_at' => $start = now()->subMonths($this->faker->numberBetween(1, 18)),
      'finished_at' => $current ? $start->addMonths($this->faker->numberBetween(1, 12)) : null
    ];
  }
}

<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ResumeManagerController;
use App\Http\Controllers\UserSettingsController;
use App\Http\Controllers\WorkExperienceController;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

Route::get('/', static function () {
  return Inertia::render('Welcome', []);
})->name('welcome');

Route::group(['middleware' => ['auth', 'verified']], static function () {
  Route::get('/dashboard', [HomeController::class, 'index'])->name('dashboard');
  Route::get('/user/settings', [UserSettingsController::class, 'show'])->name(
    'settings.show'
  );
  Route::controller(ProfileController::class)->group(function () {
    Route::get('/profile', 'show')->name('profile.show');
    Route::post('/profile', 'store')->name('profile.store');
  });
  Route::controller(ResumeManagerController::class)->group(function () {
    Route::get('/resume-manager', 'index')->name('resume.manager.index');
  });
  Route::group(['prefix' => 'resume-manager'], static function () {
    Route::controller(WorkExperienceController::class)->group(function () {
      Route::get('/work-experience', 'index')->name('work.experience.index');
      Route::post('/work-experience', 'store')->name('work.experience.store');
    });
  });
});

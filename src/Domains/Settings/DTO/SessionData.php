<?php

namespace Domains\Settings\DTO;

class SessionData
{
  public function __construct(
    public readonly string|null $id,
    public readonly int|null $user_id,
    public readonly string|null $ip_address,
    public readonly string|null $user_agent,
    public readonly string|null $payload,
    public readonly float|int|string $last_activity,
  ) {
  }
}

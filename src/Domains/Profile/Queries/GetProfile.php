<?php

namespace Domains\Profile\Queries;

use App\Http\Resources\ProfileResource;
use App\Models\Profile;
use App\Models\User;
use Contracts\Profile\GetsProfile;
use Exception;

class GetProfile implements GetsProfile
{
  /**
   * @param User $user
   * @return ProfileResource|array
   * @throws Exception
   */
  public function __invoke(User $user): ProfileResource|array
  {
    $profile = Profile::query()->whereBelongsTo($user, 'user')->first();
    if (!$profile) {
      return [
        'description' => '',
        'language' => '',
        'phoneNumber' => '',
        'country' => 0,
        'photoUrl' => '',
      ];
    }

    if (isset($profile['profile_photo_path'])) {
      /** @var string $photoName */
      $photoName = $profile->getAttribute('profile_photo_path');
      if ($photoName !== '') {
        $profile['profile_photo_path'] = (new GetProfileImage())->url($photoName);
      }
    }
    return new ProfileResource($profile);
  }
}

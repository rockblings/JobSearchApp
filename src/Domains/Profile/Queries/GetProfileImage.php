<?php

namespace Domains\Profile\Queries;

use Contracts\Profile\GetsProfileImage;
use Illuminate\Support\Facades\URL;

/**
 * @param string $photoName
 *
 * @return string
 */
class GetProfileImage implements GetsProfileImage
{
  public function url(string $photoName): string
  {
    return URL::to('/') . '/storage/images/' . $photoName;
  }
}

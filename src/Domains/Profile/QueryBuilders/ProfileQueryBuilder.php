<?php

namespace Domains\Profile\QueryBuilders;

use Illuminate\Database\Eloquent\Builder;

/**
 * @template TModelClass of \App\Models\Profile
 *
 * @extends Builder<TModelClass>
 */
class ProfileQueryBuilder extends Builder
{
}

<?php

namespace Domains\Profile\DTO;

use Contracts\Profile\ProfileDataContract;
use Illuminate\Http\UploadedFile;

final class ProfileData implements ProfileDataContract
{
  public function __construct(
    protected readonly string|null $name,
    protected readonly string|null $email,
    protected readonly string|null $description,
    protected readonly string|null $language,
    protected readonly string|null $phoneNumber,
    protected readonly int|null $country,
    public readonly UploadedFile|null $photo,
    protected readonly string|null $photoName,
  ) {
  }

  /**
   * @return array
   */
  public function toArray(): array
  {
    return [
      'description' => $this->description,
      'language' => $this->language,
      'telephone' => $this->phoneNumber,
      'country' => $this->country,
      'profile_photo_path' => $this->photoName,
    ];
  }
}

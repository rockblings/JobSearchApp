<?php

namespace Domains\Profile\Actions;

use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Contracts\Profile\SavesProfile;
use Contracts\Profile\SavesProfilePhoto;
use Domains\Profile\DTO\ProfileData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class SaveProfile implements SavesProfile
{
  /**
   * @param ProfileRequest $request
   * @param SavesProfilePhoto $savesProfilePhoto
   * @return User
   *
   * @throws ValidationException
   */
  public function __invoke(ProfileRequest $request, SavesProfilePhoto $savesProfilePhoto): User
  {
    $profile = $request->toArray();
    $photoName = '';
    if ($request->hasFile('photo')) {
      $photoName = $savesProfilePhoto($request, $photoName);
    }
    $profile['photoName'] = $photoName;
    $attributes = (new ProfileData(...$profile))->toArray();

    /** @var User $user */
    $user = Auth::user();
    // update the username and email
    (new UpdateUserProfileInformation())->update($user, $profile);

    $user->profile()->create($attributes);
    return $user;
  }
}

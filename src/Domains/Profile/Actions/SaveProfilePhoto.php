<?php

namespace Domains\Profile\Actions;

use App\Http\Requests\ProfileRequest;
use Contracts\Profile\SavesProfilePhoto;
use Domains\Profile\DTO\ProfilePhotoData;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use function storage_path;

class SaveProfilePhoto implements SavesProfilePhoto
{
  /**
   * @param ProfileRequest $request
   * @param string|null $photoName
   *
   * @return string
   */
  public function __invoke(ProfileRequest $request, ?string $photoName): string
  {
    if ($photoName) {
      Storage::delete($photoName);
    }
    /** @var UploadedFile $photo */
    $photo = $request->file('photo');
    $photoName = $request->hasFile('photo') ? $photo->hashName() : Str::random(16);
    Image::make($photo)
      ->fit(320, 320, function ($constraint) {
        $constraint->upsize();
      })
      ->save(storage_path('app/public/images/' . $photoName));
    return $photoName;
  }
}

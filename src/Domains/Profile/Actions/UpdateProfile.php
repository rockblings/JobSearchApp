<?php

namespace Domains\Profile\Actions;

use App\Actions\Fortify\UpdateUserProfileInformation;
use App\Http\Requests\ProfileRequest;
use App\Models\Profile;
use App\Models\User;
use Contracts\Profile\SavesProfilePhoto;
use Contracts\Profile\UpdatesProfile;
use Domains\Profile\DTO\ProfileData;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UpdateProfile implements UpdatesProfile
{
  /**
   * @param ProfileRequest $request
   * @param SavesProfilePhoto $savesProfilePhoto
   * @return bool
   *
   * @throws ValidationException
   */
  public function __invoke(ProfileRequest $request, SavesProfilePhoto $savesProfilePhoto): bool
  {
    $requestData = $request->toArray();
    /** @var User $user */
    $user = Auth::user();
    /** @var Profile $profile */
    $profile = $user->profile;
    /** @var string $photoName */
    $photoName = $profile['profile_photo_path'];
    if ($request->hasFile('photo')) {
      $photoName = $savesProfilePhoto($request, $photoName);
    }
    (new UpdateUserProfileInformation())->update($user, $requestData);
    $requestData['photoName'] = $photoName;
    $attributes = (new ProfileData(...$requestData))->toArray();
    return $profile->update($attributes);
  }
}

<?php

namespace Domains\Profile\Providers;

use Contracts\Profile\GetsProfile;
use Contracts\Profile\ProfileDataContract;
use Contracts\Profile\SavesProfile;
use Contracts\Profile\SavesProfilePhoto;
use Contracts\Profile\UpdatesProfile;
use Domains\Profile\Actions\SaveProfile;
use Domains\Profile\Actions\SaveProfilePhoto;
use Domains\Profile\Actions\UpdateProfile;
use Domains\Profile\DTO\ProfileData;
use Domains\Profile\Queries\GetProfile;
use Illuminate\Support\ServiceProvider;

class ProfileServiceProvider extends ServiceProvider
{
  /** @var array<class-string, class-string> */
  public array $bindings = [
    GetsProfile::class => GetProfile::class,
    SavesProfile::class => SaveProfile::class,
    SavesProfilePhoto::class => SaveProfilePhoto::class,
    UpdatesProfile::class => UpdateProfile::class,
    ProfileDataContract::class => ProfileData::class,
  ];
}

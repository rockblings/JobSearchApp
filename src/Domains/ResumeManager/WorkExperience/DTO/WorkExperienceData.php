<?php

namespace Domains\ResumeManager\WorkExperience\DTO;

use Contracts\ResumeManager\WorkExperience\WorkExperienceDataContract;
use Illuminate\Support\Carbon;

final class WorkExperienceData implements WorkExperienceDataContract
{
  public function __construct(
    protected readonly string|null $xp,
    protected readonly string $title,
    protected readonly string $description,
    protected readonly string $name,
    protected readonly string|null $email,
    protected readonly string|null $address,
    protected readonly string|null $telephone,
    protected readonly string|null $website,
    protected readonly Carbon|string $startDate,
    protected readonly Carbon|null|string $endDate,
    protected readonly bool $status,
  ) {
  }

  /**
   * @return array
   */
  public function toArray(): array
  {
    return [
      'job_title' => $this->title,
      'job_description' => $this->description,
      'company' => $this->name,
      'company_email' => $this->email,
      'company_address' => $this->address,
      'company_telephone' => $this->telephone,
      'company_website' => $this->website,
      'started_at' => $this->startDate,
      'finished_at' => $this->endDate,
      'current' => $this->status,
    ];
  }
}

<?php

namespace Domains\ResumeManager\WorkExperience\Actions;

use App\Http\Requests\WorkExperienceRequest;
use App\Models\User;
use Contracts\ResumeManager\WorkExperience\SavesWorkExperience;
use Domains\ResumeManager\WorkExperience\DTO\WorkExperienceData;
use RuntimeException;

/**
 * @param WorkExperienceRequest $request
 * @return User
 */
class SaveWorkExperience implements SavesWorkExperience
{

  public function __invoke(WorkExperienceRequest $request): User
  {
    $workExperiences = $request->toArray();
    if (count($workExperiences) === 0) {
      throw new RuntimeException('No work experience data provided', 406);
    }

    /** @var User $user */
    $user = $request->user();

    foreach ($workExperiences as $workExperience):
      $attributes = (new WorkExperienceData(...$workExperience))->toArray();
      if (!isset($workExperience->xp)) {
        $user->experiences()->create($attributes);
      } else {
        $user->experiences()->update($attributes);
      }

    endforeach;

    return $user;
  }
}

<?php

namespace Domains\ResumeManager\WorkExperience\Collection;

use App\Http\Resources\WorkExperienceResource;
use Illuminate\Http\Resources\Json\ResourceCollection;

class WorkExperienceCollection extends ResourceCollection
{
  /**
   * Transform the resource collection into an array.
   *
   * @return array
   */
  public function toArray($request): array
  {
    return [
      'data' => WorkExperienceResource::collection($this->collection),
    ];
  }
}

<?php

namespace Domains\ResumeManager\WorkExperience\Queries;

use App\Models\User;
use App\Models\WorkExperience;
use Contracts\ResumeManager\WorkExperience\GetsWorkExperience;
use Domains\ResumeManager\WorkExperience\Collection\WorkExperienceCollection;
use Exception;

class GetWorkExperience implements GetsWorkExperience
{

  /**
   * @inheritDoc
   * @throws Exception
   */
  public function __invoke(User $user): WorkExperienceCollection|array
  {
    $workExperiences = WorkExperience::query()->whereBelongsTo($user, 'user')->get();
    return new WorkExperienceCollection($workExperiences);
  }
}

<?php

namespace Domains\ResumeManager\WorkExperience\Providers;

use Contracts\ResumeManager\WorkExperience\GetsWorkExperience;
use Contracts\ResumeManager\WorkExperience\SavesWorkExperience;
use Contracts\ResumeManager\WorkExperience\WorkExperienceDataContract;
use Domains\ResumeManager\WorkExperience\Actions\SaveWorkExperience;
use Domains\ResumeManager\WorkExperience\DTO\WorkExperienceData;
use Domains\ResumeManager\WorkExperience\Queries\GetWorkExperience;
use Illuminate\Support\ServiceProvider;

class WorkExperienceServiceProvider extends ServiceProvider
{
  /**
   * @var array<class-string, class-string>
   */
  public array $bindings = [
    GetsWorkExperience::class => GetWorkExperience::class,
    SavesWorkExperience::class => SaveWorkExperience::class,
    WorkExperienceDataContract::class => WorkExperienceData::class,
  ];
}

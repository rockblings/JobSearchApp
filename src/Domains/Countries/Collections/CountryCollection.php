<?php

namespace Domains\Countries\Collections;

use App\Http\Resources\CountryResource;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use JsonSerializable;
use stdClass;

class CountryCollection extends ResourceCollection
{
  /**
   * Transform the resource collection into an array.
   *
   * @param Request $request
   *
   * @return array<int, stdClass>|Arrayable<int, stdClass>|JsonSerializable
   */
  public function toArray($request): array|JsonSerializable|Arrayable
  {
    return CountryResource::collection($this->collection);
  }
}

<?php

namespace Domains\Countries\Providers;

use Contracts\Countries\GetsCountryList;
use Domains\Countries\Queries\GetCountryList;
use Illuminate\Support\ServiceProvider;

class CountryServiceProvider extends ServiceProvider
{
  /** @var array<class-string, class-string> */
  public array $bindings = [
    GetsCountryList::class => GetCountryList::class,
  ];
}

<?php

namespace Domains\Countries\Queries;

use App\Models\Country;
use Contracts\Countries\GetsCountryList;
use Domains\Countries\Collections\CountryCollection;

class GetCountryList implements GetsCountryList
{
  /**
   * @return CountryCollection
   */
  public function __invoke(): CountryCollection
  {
    return new CountryCollection(
      Country::all()
    );
  }
}

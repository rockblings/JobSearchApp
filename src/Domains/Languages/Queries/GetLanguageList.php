<?php

namespace Domains\Languages\Queries;

use App\Models\Language;
use Contracts\Languages\GetsLanguageList;
use Domains\Languages\Collections\LanguageCollection;

class GetLanguageList implements GetsLanguageList
{
  /**
   * @return LanguageCollection
   */
  public function __invoke(): LanguageCollection
  {
    return new LanguageCollection(
      Language::all()
    );
  }
}

<?php

namespace Domains\Languages\Providers;

use Contracts\Languages\GetsLanguageList;
use Domains\Languages\Queries\GetLanguageList;
use Illuminate\Support\ServiceProvider;

class LanguageServiceProvider extends ServiceProvider
{
  /**
   * @var array<class-string, class-string>
   */
  public array $bindings = [
    GetsLanguageList::class => GetLanguageList::class
  ];
}

<?php

declare(strict_types=1);

namespace Shared;

use App\Models\CustomBaseModel;
use Illuminate\Support\Str;

trait HasUuid
{
  /**
   * @return void
   */
  public static function bootHasUuid(): void
  {
    static::creating(fn(CustomBaseModel $model) => $model->uuid = Str::uuid()->toString());
  }
}

<?php

namespace Contracts\Profile;

use App\Http\Resources\ProfileResource;
use App\Models\User;

interface GetsProfile
{
  /**
   * @param User $user
   * @return ProfileResource|array
   */
  public function __invoke(User $user): ProfileResource|array;
}

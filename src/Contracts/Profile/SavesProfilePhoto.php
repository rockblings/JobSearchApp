<?php

namespace Contracts\Profile;

use App\Http\Requests\ProfileRequest;

interface SavesProfilePhoto
{
  /**
   * @param ProfileRequest $request
   * @param string|null $photoName
   *
   * @return string
   */
  public function __invoke(ProfileRequest $request, ?string $photoName): string;
}

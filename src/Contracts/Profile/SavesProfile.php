<?php

namespace Contracts\Profile;

use App\Http\Requests\ProfileRequest;
use App\Models\User;

interface SavesProfile
{
  /**
   * @param ProfileRequest $request
   * @param SavesProfilePhoto $savesProfilePhoto
   * @return User
   */
  public function __invoke(ProfileRequest $request, SavesProfilePhoto $savesProfilePhoto): User;
}

<?php

namespace Contracts\Profile;

/**
 * @return array
 */
interface ProfileDataContract
{
  public function toArray(): array;
}

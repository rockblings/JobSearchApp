<?php

namespace Contracts\Profile;

/**
 * @param string $photoName
 *
 * @return string
 */
interface GetsProfileImage
{
  public function url(string $photoName): string;
}

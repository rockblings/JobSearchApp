<?php

namespace Contracts\Profile;

use App\Http\Requests\ProfileRequest;

interface UpdatesProfile
{
  /**
   * @param ProfileRequest $request
   * @param SavesProfilePhoto $savesProfilePhoto
   * @return bool
   */
  public function __invoke(ProfileRequest $request, SavesProfilePhoto $savesProfilePhoto): bool;
}

<?php

namespace Contracts\ResumeManager\WorkExperience;

/**
 * @return array
 */
interface WorkExperienceDataContract
{
  public function toArray(): array;
}

<?php

namespace Contracts\ResumeManager\WorkExperience;

use App\Http\Requests\WorkExperienceRequest;
use App\Models\User;

/**
 * @param WorkExperienceRequest $request
 * @return User
 */
interface SavesWorkExperience
{
  public function __invoke(WorkExperienceRequest $request): User;
}

<?php

namespace Contracts\ResumeManager\WorkExperience;

use App\Models\User;
use Domains\ResumeManager\WorkExperience\Collection\WorkExperienceCollection;

interface GetsWorkExperience
{
  /**
   * Gets the work experience of the user.
   *
   * @param User $user
   * @return WorkExperienceCollection|array
   */
  public function __invoke(User $user): WorkExperienceCollection|array;
}

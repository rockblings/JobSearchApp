<?php

namespace Contracts\Countries;

use Domains\Countries\Collections\CountryCollection;

interface GetsCountryList
{
  /**
   * @return CountryCollection
   */
  public function __invoke(): CountryCollection;
}

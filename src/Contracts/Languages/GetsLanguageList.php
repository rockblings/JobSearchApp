<?php

namespace Contracts\Languages;

use Domains\Languages\Collections\LanguageCollection;

interface GetsLanguageList
{
  /**
   * @return LanguageCollection
   */
  public function __invoke(): LanguageCollection;
}

const colors = require('tailwindcss/colors');
module.exports = {
  content: [
    './resources/views/**/*.blade.php',
    './resources/js/**/*.tsx',
    './resources/js/**/*.jsx',
  ],
  theme: {
    extend: {
      colors: {
        brand: {
          50: '#FFE8D9',
          100: '#FFD0B5',
          200: '#FFB088',
          300: '#FF9466',
          400: '#F9703E',
          500: '#F35627',
          600: '#DE3A11',
          700: '#C52707',
          800: '#AD1D07',
          900: '#841003',
        },
      },
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    require('@tailwindcss/typography'),
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
};

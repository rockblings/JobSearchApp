<?php

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Domains\Profile\Actions\SaveProfile;
use Domains\Profile\Actions\SaveProfilePhoto;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\ValidationException;

it('can save a profile', function () {
  $this->user = User::factory()->create();
  $this->actingAs($this->user);

  $photo = UploadedFile::fake()->image('profile.jpg');

  $mockedRequestData = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a test description',
    'language' => '1',
    'phoneNumber' => '123456789',
    'country' => '1',
    'photo' => $photo,
  ];

  app()->resolving(ProfileRequest::class, function ($resolved) use ($mockedRequestData) {
    $resolved->merge($mockedRequestData);
  });

  $action = app(SaveProfile::class);
  $saveProfilePhotoAction = app(SaveProfilePhoto::class);

  try {
    $request = app(ProfileRequest::class, $mockedRequestData);
    $profile = $action($request, $saveProfilePhotoAction);
    expect($profile)->toBeInstanceOf(User::class);
  } catch (ValidationException $e) {
    dd($e);
  }
});

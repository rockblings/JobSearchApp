<?php

use App\Http\Requests\ProfileRequest;
use App\Models\User;
use Domains\Profile\Actions\SaveProfile;
use Domains\Profile\Actions\SaveProfilePhoto;
use Domains\Profile\Actions\UpdateProfile;
use Illuminate\Http\UploadedFile;
use Illuminate\Validation\ValidationException;

beforeEach(function () {
  $this->user = User::factory()->create();
  $this->actingAs($this->user);
  $photo = UploadedFile::fake()->image('profile.jpg');

  $mockedRequestData = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a test description',
    'language' => '1',
    'phoneNumber' => '123456789',
    'country' => '1',
    'photo' => $photo,
  ];

  app()->resolving(ProfileRequest::class, function ($resolved) use ($mockedRequestData) {
    $resolved->merge($mockedRequestData);
  });

  $action = app(SaveProfile::class);
  $saveProfilePhotoAction = app(SaveProfilePhoto::class);

  try {
    $request = app(ProfileRequest::class, $mockedRequestData);
    $action($request, $saveProfilePhotoAction);
  } catch (ValidationException $e) {
    dd($e);
  }
});

it('can update an existing profile', function () {
  $photo = UploadedFile::fake()->image('updatedprofile.jpg');

  $mockedRequestData = [
    'name' => 'John Steve',
    'email' => 'john@example.com',
    'description' => 'This is a test description. Update',
    'language' => '2',
    'phoneNumber' => '123456789',
    'country' => '2',
    'photo' => $photo,
  ];

  app()->resolving(ProfileRequest::class, function ($resolved) use ($mockedRequestData) {
    $resolved->merge($mockedRequestData);
  });

  $saveProfilePhotoAction = app(SaveProfilePhoto::class);
  $updateAction = app(UpdateProfile::class);

  try {
    $request = app(ProfileRequest::class, $mockedRequestData);
    $updateProfile = $updateAction($request, $saveProfilePhotoAction);
    expect($updateProfile)->toBeTrue();
  } catch (ValidationException $e) {
    dd($e);
  }
});

<?php

use App\Models\Profile;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\UploadedFile;
use Inertia\Testing\AssertableInertia as Assert;

beforeEach(function () {
  $this->user = User::factory()->create();
});

it('can load user profile page for authenticated user', function () {
  $this->actingAs($this->user);
  Profile::factory()->create([
    'user_id' => $this->user->id,
  ]);

  $response = $this->get('/profile')->assertOk();

  $response->assertInertia(
    fn(Assert $page) => $page
      ->component('Profile/Show')
      ->has('selectedPage')
      ->has('profile')
      ->has('countries')
      ->has('languages')
  );
});

it('redirects unauthenticated user to login page when they visit the profile page', function () {
  $response = $this->get('/profile')
    ->assertRedirect('/login')
    ->assertStatus(302);
});

it('allows authenticated user to create and update their profile', function () {
  $this->actingAs($this->user);
  $profile = Profile::factory()->create([
    'user_id' => $this->user->id,
    'profile_photo_path' => ''
  ]);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)
    ->assertRedirect('/profile');

  expect($this->user->fresh()->name)->toBe('John Doe');
  expect($this->user->fresh()->email)->toBe('john@example.com');
  expect($this->user->fresh()->profile->description)->toBe('This is a user description');
  expect($this->user->fresh()->profile->language)->toBe(1);
  expect($this->user->fresh()->profile->telephone)->toBe('788403873');
  expect($this->user->fresh()->profile->country)->toBe(22);
});

it('only allows an authenticated user to create and update their profile', function () {
  $this->withoutExceptionHandling();
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes);
})->throws(AuthenticationException::class, 'Unauthenticated.');

it('expects photo parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects name parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects email parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'description' => 'This is a user description',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects description parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'language' => 1,
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects language parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'phoneNumber' => '788403873',
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects phoneNumber parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'language' => 1,
    'country' => 22,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

it('expects country parameter', function () {
  $this->actingAs($this->user);
  $attributes = [
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'description' => 'This is a user description',
    'phoneNumber' => '788403873',
    'language' => 1,
    'photo' => UploadedFile::fake()->image('profile.jpg')
  ];

  $response = $this->post('/profile', $attributes)->assertStatus(500);
});

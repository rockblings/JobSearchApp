<?php

use App\Models\User;
use Inertia\Testing\AssertableInertia as Assert;

it('can load dashboard page for authenticated user', function () {
    $this->user = User::factory()->create();
    $this->actingAs($this->user);

    $response = $this->get('/dashboard')->assertOk();

    $response->assertInertia(
        fn (Assert $page) => $page
    ->component('Home/Index')
    ->has('selectedPage')
    ->has('imageUrl')
    );
});

it('redirects unauthenticated user to login page when they visit the dashboard', function () {
    $response = $this->get('/dashboard')
    ->assertRedirect('/login')
    ->assertStatus(302);
});

<?php

use App\Models\User;
use Inertia\Testing\AssertableInertia as Assert;

it('can load resume manager page for authenticated user', function () {
    $this->withoutExceptionHandling();
    $this->actingAs(User::factory()->create());

    $response = $this->get('/resume-manager')->assertOk();
    $response->assertInertia(
        fn (Assert $page) => $page
    ->component('ResumeManager/Index')
    ->has('selectedPage')
    );
});

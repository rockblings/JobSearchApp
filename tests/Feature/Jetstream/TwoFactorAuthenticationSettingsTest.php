<?php

use App\Models\User;

beforeEach(function () {
    $this->user = User::factory()->create();
    $this->actingAs($this->user);
    $this->withSession(['auth.password_confirmed_at' => time()]);
});

test('two factor authentication can be enabled', function () {
    $this->post('/user/two-factor-authentication');

    expect($this->user->fresh()->two_factor_secret)->not->toBeEmpty();
    expect($this->user->fresh()->recoveryCodes())->toHaveCount(8);
});

test('recovery codes can be generated', function () {
    $this->post('/user/two-factor-authentication');
    $this->post('/user/two-factor-recovery-codes');

    $user = $this->user->fresh();

    $this->post('/user/two-factor-recovery-codes');

    expect($user->recoveryCodes())->toHaveCount(8);
    expect(array_diff($user->recoveryCodes(), $user->fresh()->recoveryCodes()))->toHaveCount(8);
});

test('two-factor authentication can be disabled', function () {
    $this->post('/user/two-factor-authentication');

    expect($this->user->fresh()->two_factor_secret)->not->toBeEmpty();

    $this->delete('/user/two-factor-authentication');

    expect($this->user->fresh()->two_factor_secret)->toBeEmpty();
});

<?php

use Domains\Profile\Queries\GetProfileImage;

it('returns an image path', function () {
  $imagePath = (new GetProfileImage())->url('photo.jpg');
  expect($imagePath)->toBeString();
  expect($imagePath)->toEqual("http://jobsearch.test:8001/storage/images/photo.jpg");
});

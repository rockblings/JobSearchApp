<?php

use App\Models\Country;
use Domains\Countries\Collections\CountryCollection;
use Domains\Countries\Queries\GetCountryList;

it('can fetch country list', function () {
  Country::factory(10)->create();
  $getCountryListQuery = app(GetCountryList::class);
  $countryList = $getCountryListQuery();
  expect($countryList)->toBeInstanceOf(CountryCollection::class);
});

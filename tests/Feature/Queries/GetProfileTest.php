<?php

use App\Http\Resources\ProfileResource;
use App\Models\Profile;
use App\Models\User;
use Contracts\Profile\GetsProfile;

it('can fetch a profile', function () {
  $this->user = User::factory()->create();
  $this->actingAs($this->user);

  $profileData = [
    'user_id' => $this->user->id,
    'description' => 'This is a profile description',
    'language' => '1',
    'telephone' => '123456789',
    'country' => 0,
    'profile_photo_path' => 'profile.jpg',
  ];

  Profile::factory()->create($profileData);
  $getsProfileQuery = app(GetsProfile::class);
  $profileQuery = $getsProfileQuery($this->user);

  expect($profileQuery)->toBeInstanceOf(ProfileResource::class);
});

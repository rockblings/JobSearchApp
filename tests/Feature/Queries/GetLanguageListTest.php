<?php

use App\Models\Language;
use Domains\Languages\Collections\LanguageCollection;
use Domains\Languages\Queries\GetLanguageList;

it('can fetch language list', function () {
  Language::factory(10)->create();
  $getLanguageList = app(GetLanguageList::class);
  $languageList = $getLanguageList();
  expect($languageList)->toBeInstanceOf(LanguageCollection::class);
});

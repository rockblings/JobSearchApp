<?php

declare(strict_types=1);

use NunoMaduro\PhpInsights\Domain\Insights\Composer\ComposerMustContainName;
use NunoMaduro\PhpInsights\Domain\Insights\CyclomaticComplexityIsHigh;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenDefineFunctions;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenFinalClasses;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenNormalClasses;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenPrivateMethods;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenTraits;
use NunoMaduro\PhpInsights\Domain\Metrics\Architecture\Classes;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Files\LineLengthSniff;
use SlevomatCodingStandard\Sniffs\Commenting\UselessFunctionDocCommentSniff;
use SlevomatCodingStandard\Sniffs\Functions\FunctionLengthSniff;
use SlevomatCodingStandard\Sniffs\Functions\UnusedParameterSniff;
use SlevomatCodingStandard\Sniffs\Namespaces\AlphabeticallySortedUsesSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\DeclareStrictTypesSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\DisallowMixedTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\ParameterTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\PropertyTypeHintSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\ReturnTypeHintSniff;

return [

  /*
  |--------------------------------------------------------------------------
  | Default Preset
  |--------------------------------------------------------------------------
  |
  | This option controls the default preset that will be used by PHP Insights
  | to make your code reliable, simple, and clean. However, you can always
  | adjust the `Metrics` and `Insights` below in this configuration file.
  |
  | Supported: "default", "laravel", "symfony", "magento2", "drupal"
  |
  */

  'preset' => 'laravel',

  /*
  |--------------------------------------------------------------------------
  | IDE
  |--------------------------------------------------------------------------
  |
  | This options allow to add hyperlinks in your terminal to quickly open
  | files in your favorite IDE while browsing your PhpInsights report.
  |
  | Supported: "textmate", "macvim", "emacs", "sublime", "phpstorm",
  | "atom", "vscode".
  |
  | If you have another IDE that is not in this list but which provide an
  | url-handler, you could fill this config with a pattern like this:
  |
  | myide://open?url=file://%f&line=%l
  |
  */

  'ide' => 'phpstorm',

  /*
  |--------------------------------------------------------------------------
  | Configuration
  |--------------------------------------------------------------------------
  |
  | Here you may adjust all the various `Insights` that will be used by PHP
  | Insights. You can either add, remove or configure `Insights`. Keep in
  | mind that all added `Insights` must belong to a specific `Metric`.
  |
  */

  'exclude' => [
    'app/Exceptions/Handler.php',
    'app/Console/Kernel.php',
    'app/Http/Middleware/HandleInertiaRequests.php',
    'app/Http/Middleware/RedirectIfAuthenticated.php',
    'app/Http/Middleware/ShareInertiaData.php',
    'app/Providers/RouteServiceProvider.php',
    'app/Actions/Jetstream/CreateTeam.php',
    'src/Domain/Talent/PointText/Enum/PointTextType.php',
    'app/Providers/EventServiceProvider.php',
    'app/Providers/FortifyServiceProvider.php',
    'app/Providers/JetstreamServiceProvider.php',
    'app/Providers/AuthServiceProvider.php',
    'app/Policies/TeamPolicy.php',
    'app/Providers/AppServiceProvider.php',
    '/composer.json',
    'routes/jetstream.php',
    'app/Actions/Fortify/UpdateUserPassword.php',
    'app/Models/User.php',
    'app/Http/Middleware/EncryptCookies.php',
    'app/Models/Team.php',
    'app/Models/TeamInvitation.php',
    'app/Http/Middleware/PreventRequestsDuringMaintenance.php',
    'app/Http/Middleware/VerifyCsrfToken.php',
    'app/Http/Middleware/PreventRequestsDuringMaintenance.php',
    'app/Actions/Jetstream/InviteTeamMember.php',
    'app/Actions/Jetstream/AddTeamMember.php',
    'app/Actions/Fortify/PasswordValidationRules.php',
    'app/Actions/Jetstream/RemoveTeamMember.php',
    'app/Actions/Fortify/UpdateUserProfileInformation.php',
    'app/Http/Middleware/Authenticate.php',
  ],

  'add' => [
    Classes::class => [
      ForbiddenFinalClasses::class,
    ],
  ],

  'remove' => [
    AlphabeticallySortedUsesSniff::class,
    DeclareStrictTypesSniff::class,
    DisallowMixedTypeHintSniff::class,
    ForbiddenDefineFunctions::class,
    ForbiddenNormalClasses::class,
    ForbiddenTraits::class,
    ParameterTypeHintSniff::class,
    PropertyTypeHintSniff::class,
    ReturnTypeHintSniff::class,
    UselessFunctionDocCommentSniff::class,
    ComposerMustContainName::class,
  ],

  'config' => [
    ForbiddenPrivateMethods::class => [
      'title' => 'The usage of private methods is not idiomatic in Laravel.',
    ],
    LineLengthSniff::class => [
      'lineLimit' => 180,
      'absoluteLineLimit' => 200
    ],
    UnusedParameterSniff::class => [
      'exclude' => [
        'app/Console/Kernel.php',
        'app/Exceptions/Handler.php',
        'app/Http/Resources/ProfileResource.php',
        'src/Domain/Country/Collections/CountryCollection.php',
        'src/Domain/Languages/Collections/LanguageCollection.php',
        'app/Http/Resources/CountryResource.php',
        'app/Http/Resources/LanguageResource.php'
      ],
    ],
    CyclomaticComplexityIsHigh::class => [
      'maxComplexity' => 5,
    ],
    FunctionLengthSniff::class => [
      'maxLinesLength' => 30,
    ]
  ],

  /*
  |--------------------------------------------------------------------------
  | Requirements
  |--------------------------------------------------------------------------
  |
  | Here you may define a level you want to reach per `Insights` category.
  | When a score is lower than the minimum level defined, then an error
  | code will be returned. This is optional and individually defined.
  |
  */

  'requirements' => [
    'min-quality' => 90,
    'min-complexity' => 90,
    'min-architecture' => 90,
    'min-style' => 90,
//        'disable-security-check' => false,
  ],

  /*
  |--------------------------------------------------------------------------
  | Threads
  |--------------------------------------------------------------------------
  |
  | Here you may adjust how many threads (core) PHPInsights can use to perform
  | the analyse. This is optional, don't provide it and the tool will guess
  | the max core number available. This accept null value or integer > 0.
  |
  */

  'threads' => null,

];
